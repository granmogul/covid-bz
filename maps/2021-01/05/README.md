[covid-bz](../../../README.md) / [maps](../../README.md) / [2021-01](../../2021-01/README.md) / [05](../05)
# Dienstag, 5. Januar 2021

- [PDF, Daten und Text](../05)
  - [tägliche Pressemitteilung](20210105.txt)
  - Karten
    - [Summe der PCR- und AG-Positiven der letzten 7 Tagen](CovidCartinaPositiviPcrAgUltimi7giorni-2021-01-05.pdf)
    - [Summe der PCR- **und AG**-Positiven der letzten **21** Tagen](CovidCartinaPositiviPcrAgUltimi21giorni-2021-01-05.pdf)
    - [**Anstieg bzw. Rückgang** der PCR- und AG-Positiven der letzten 7 Tagen im Vergleich zu den letzten 21 Tagen](CovidCartinaAccelerazione7su21giorniPositiviPcrAgUltimi-2021-01-05.pdf)
    - [Karte mit aktueller Quarantäne](CovidCartinaQuarantena-2021-01-05.pdf)
  - [weiteres Material](../05)

## Massentests

### Bruneck - Betriebstests
Die Serie Betriebstest wird vom Montag, 5. Jan. bis Mittwoch 13. Jan. stattfinden.
Am Montag (gestern) hat man mit **GKN Sinter Metals** begonnen. **Driveline** und **Intercable** folgen ab 7. Januar.
Es wird auch im **Altersheim** durchgefürt, mit bis zu 170 Mitarbeiter und Bewohner.
(Tageszeitung vom 5. Jan, Seite 13)

### Gais und Ahrntal

[RAI Tagesschau 5. Jan. 2021](https://www.rainews.it/tgr/tagesschau/articoli/2021/01/tag-Massentest-in-Gais-und-Ahrntal-kein-Massentest-in-Sand-in-Taufers-ce1f4b2e-ba4a-4485-8143-9bd9851ffc8c.html):In den Gemeinden Ahrntal und Gais kann sich die Bevölkerung am kommenden Wochenende auf das Corona-Virus testen lassen.

In der Gemeinde Ahrntal mit rund 6.0000 Einwohnern wird ein Testzentrum in der Mehrzweckhalle in Luttach aufgebaut. In der Gemeinde Gais mit über 3.000 Einwohnern wird es zwei Testzentren geben. 
Kein Massentest in Sand in Taufers
Sand in Taufers hingegen organsiert keinen Massentest. Man sei organisatorisch nicht in der Lage, sagte Bürgermeister Josef Nöckler. Seiner Ansicht nach gebe es auch keinen neuen Hotspot in Sand in Taufers. Einziger Problembereich sei das Alters- und Pflegeheim in Taufers.


## Seniorenheime

Für die [suspekten Heime](Traueranzeigen-Seniorenheime-Hotspots-Welle2-20210105.txt) ist ein  neuer Fall gefunden worden: **Martinsbrunn** in Meran.

Es ist ein zweiter Trauerfall bzgl. **Seniorenheim Sand in Taufers**, der laut Informationen Covid-bedingt war.
Laut Bürgermeister gibt es *keinen neuen Hotspot in Sand in Taufers. Einziger Problembereich sei das Alters- und Pflegeheim in Taufers.*
([RAI Tagesschau 5. Jan. 2021](https://www.rainews.it/tgr/tagesschau/articoli/2021/01/tag-Massentest-in-Gais-und-Ahrntal-kein-Massentest-in-Sand-in-Taufers-ce1f4b2e-ba4a-4485-8143-9bd9851ffc8c.html)) 
Weil, laut Bürgermeister die Gemeinde  organisatorisch nicht in der Lage ist, machen sie auch nächstes Wochenende nicht am Massentest mit. 


Für Details siehe 
- [Liste der suspekten Heime](Traueranzeigen-Seniorenheime-Hotspots-Welle2-20210105.txt)
- [Grafiken](Traueranzeigen-SeniorenheimeCumuliert_2020-05-01_to_2021-01-02.pdf)


## Keep In Touch

- Mastodon: https://troet.cafe/@COVID_BZ_Statistics
 
