[covid-bz](../../../README.md) / [maps](../../README.md) / [2021-01](../../2021-01/README.md) / [07](../07)
# Donnerstag, 7. Januar 2021

- [PDF, Daten und Text](../07)
  - [tägliche Pressemitteilung](20210107.txt)
  - Karten
    - [Summe der PCR- und AG-Positiven der letzten 7 Tagen](CovidCartinaPositiviPcrAgUltimi7giorni-2021-01-07.pdf)
    - [Summe der PCR- **und AG**-Positiven der letzten **21** Tagen](CovidCartinaPositiviPcrAgUltimi21giorni-2021-01-07.pdf)
    - [**Anstieg bzw. Rückgang** der PCR- und AG-Positiven der letzten 7 Tagen im Vergleich zu den letzten 21 Tagen](CovidCartinaAccelerazione7su21giorniPositiviPcrAgUltimi-2021-01-07.pdf)
    - [Karte mit aktueller Quarantäne](CovidCartinaQuarantena-2021-01-07.pdf)
  - [weiteres Material](../07)


## Seniorenheime

Zu den aufgelisteten [suspekten Heime](Traueranzeigen-Seniorenheime-Hotspots-Welle2-20210107.txt) sind 3 weiterer Fälle hinzugekommen, 
- Meran, Villa Eden
- Meran, Martinsbrunn
- Bolzano, Villa Europa

Auch wenn nicht aufgelistet, ist das Altenheim der **Stiftung St. Josef in Sand in Taufers** zu erwähnen, weil heute es den dritten
bestätigten Covid-Todesfall gab, der dritte seit dem 29. Dezember. Für zwei weitere zwischen dem 23.Dez. 5.Jan. gibt es (noch) keine Informationen dazu.

Für Details siehe 
- [Liste der suspekten Heime](Traueranzeigen-Seniorenheime-Hotspots-Welle2-20210107.txt)
- [Grafiken](Traueranzeigen-SeniorenheimeCumuliert_2020-05-01_to_2021-01-03.pdf)


## Keep In Touch

- Mastodon: https://troet.cafe/@COVID_BZ_Statistics

