[covid-bz](../../../README.md) / [maps](../../README.md) / [2021-01](../../2021-01/README.md) / [11](../11)
# Montag, 11. Januar 2021

- [PDF, Daten und Text](../11)
  - [tägliche Pressemitteilung](20210111.txt)
  - Karten
    - [Summe der PCR- und AG-Positiven der letzten 7 Tagen](CovidCartinaPositiviPcrAgUltimi7giorni-2021-01-11.pdf)
    - [Summe der PCR- **und AG**-Positiven der letzten **21** Tagen](CovidCartinaPositiviPcrAgUltimi21giorni-2021-01-11.pdf)
    - [**Anstieg bzw. Rückgang** der PCR- und AG-Positiven der letzten 7 Tagen im Vergleich zu den letzten 21 Tagen](CovidCartinaAccelerazione7su21giorniPositiviPcrAgUltimi-2021-01-11.pdf)
    - [Karte mit aktueller Quarantäne](CovidCartinaQuarantena-2021-01-11.pdf)
  - [weiteres Material](../11)


## Seniorenheime

Heute sind für die als ["suspekt" aufgelisteten Seniorenheime](Traueranzeigen-Seniorenheime-Hotspots-Welle2-20210111.txt) keine Todesfälle gefunden worden.


Für Details siehe 
- [Liste der suspekten Heime](Traueranzeigen-Seniorenheime-Hotspots-Welle2-20210111.txt)
- [Grafiken](Traueranzeigen-SeniorenheimeCumuliert_2020-05-01_to_2021-01-08.pdf)


## Massentest am 9. und 10. in Ahrntal und Gais im Pustertal und in Graun im Vinschgau

siehe: https://troet.cafe/@COVID_BZ_Statistics/105539535484256849

### Graun
#### Grundinfo
- 2.400 Einw. 
- In den letzten 7 Tagen 25 positive (PCR+AG), in den 14 Tagen davor 23. 
- Die Summe der Positive der letzten 7 Tagen je 100.000 Einwohner beträgt: 1000
- vom 11.10. bis 23.12. hatte sie 1 offizielle Covid-Todefälle zu beklagen. Je 100.000 Einwohner waren es 40.

#### Massentest (9. + 10. Dez)
- (coming soon)

### Ahrntal
#### Grundinfo
- 6.000 Einw. 
- In den letzten 7 Tagen 15 Positive (PCR+AG), in den 14 Tagen davor 17. 
- Die Summe der Positive der letzten 7 Tagen je 100.000 Einwohner beträgt: 250
- vom 11.10. bis 23.12. hatte sie 2 offizielle Covid-Todefälle zu beklagen. Je 100.000 Einwohner waren es 30.

#### Massentest (9. + 10. Dez)
- (coming soon)

## Gais
#### Grundinfo
- 3.300 Einw. 
- In den letzten 7 Tagen 8 Positive (PCR+AG), in den 14 Tagen davor 9. 
- Die Summe der Positive der letzten 7 Tagen je 100.000 Einwohner beträgt: 240
- vom 11.10. bis 23.12. hatte sie 1 offizielle Covid-Todefälle zu beklagen. Je 100.000 Einwohner waren es 30.

#### Massentest (9. + 10. Dez)
- (coming soon)

## Keep In Touch

- Mastodon: https://troet.cafe/@COVID_BZ_Statistics

