[covid-bz](../../../README.md) / [maps](../../README.md) / [2021-01](../../2021-01/README.md) / [22](../22)
# Freitag, 22. Januar 2021
 
- [PDF, Daten und Text](../22)
  - [tägliche Pressemitteilung](20210122.txt)
  - Karten
    - [Summe der PCR- und AG-Positiven der letzten 7 Tagen](CovidCartinaPositiviPcrAgUltimi7giorni-2021-01-22.pdf)
    - [Summe der PCR- **und AG**-Positiven der letzten **21** Tagen](CovidCartinaPositiviPcrAgUltimi21giorni-2021-01-22.pdf)
    - [**Anstieg bzw. Rückgang** der PCR- und AG-Positiven der letzten 7 Tagen im Vergleich zu den letzten 21 Tagen](CovidCartinaAccelerazione7su21giorniPositiviPcrAgUltimi-2021-01-22.pdf)
    - [Karte mit aktueller Quarantäne](CovidCartinaQuarantena-2021-01-22.pdf)
  - [weiteres Material](../22)
 
 
## Seniorenheime
Für nicht offizielle Informationen bezüglich Seniorenheime siehe 
- [Liste der suspekten Heime](Traueranzeigen-Seniorenheime-Hotspots-Welle2-20210122.txt)
- [Grafiken](Traueranzeigen-SeniorenheimeCumuliert_2020-05-01_to_2021-01-19.pdf)

### Statistik anhand offizieller Zahlen
Während der 2. Welle gab es in Südtirol laut Landesrätin Deeg bisher 219 Covid-Tote in den Seniorenheime, davon 146 direkt im Heim und 73 im Spital.
Also um die 40-45% aller Covid-#Todesfälle  in dieser 2. Welle.
Im Verhältnis zu den Heimbewohnern, sind es um die 5% innerhalb 2 Monaten, wegen einer einzigen Krankheit.
Von den 219 Covid-Tote sind 41 auf einen einzigen Heim zurückzuführen: Don Bosco in Bozen, wo fast 1/4 aller Heimgäste innerhalb von 2 Monaten mit Covid gestorben sind.

### Antwortschreiben der Landesrätin Deeg

*danke für Ihre Mail und für Ihr Interesse an der Thematik. In der Tat gab es da einen Fehler, der in der Dolomitenausgabe vom Montag, 18. Jänner 2021 richtiggestellt wurde. Insgesamt sind 370 Bewohnerinnen und Bewohner von Seniorenwohnheimen an oder mit Corona verstorben (263 davon im Heim, 107 im Krankenhaus), dies umfasst sowohl die erste als auch die zweite Welle. In der zweiten Welle sind bisher 146 Heimbewohnerinnen im Heim und 73 Bewohner in einem Krankenhaus (also insgesamt 219 Heimbewohner) verstorben. Die Verstorbenen in den Seniorenwohnheimen bzw. die verstorbenen Heimbewohner/innen werden von der Statistik des Sanitätsbetriebes miterfasst.*
 
 
## Links zu anderen Südtirolbezogene Webseiten
 
- Dashboard von [Markus Falk](https://www.markusfalk.com/)
  - [Version 1](https://www.markusfalk.com/dashboard/#lang=de&dset=ST&reg=all&tab=PRED&from=2020-08-11&to=true)
  - [Version 2](https://www.markusfalk.com/dashboard-beta/#lang=de&dset=ST_ALL&reg=all&tab=PRED&from=2020-08-11&to=true)
- Tabellen von [Moritz Mair](https://moritzmair.info/)
  - [Impfzahlen in Südtirol nach Alter](https://covvac.moritzmair.info/?tab=1)
- Ivan Sieder
  - [Interaktive Karten](https://map.corona-bz.simedia.cloud/)
- Zivilschutz Südtirol
  - [Grafiken](http://www.provinz.bz.it/sicherheit-zivilschutz/zivilschutz/aktuelle-daten-zum-coronavirus.asp)
  - [Aktuelle Excel-Datei mit Positiven (PCR+AG)](https://afbs.provinz.bz.it/upload/coronavirus/CoronavirusPositivi.xlsx)
  - [Aktuelle Excel-Datei mit Quarantäne](https://afbs.provinz.bz.it/upload/coronavirus/CoronavirusQuarantaene.xlsx)
- [Protezione civile Italia](https://github.com/pcm-dpc/COVID-19/blob/master/README.md)
  - [dati (CSV)](https://github.com/pcm-dpc/COVID-19/tree/master/dati-regioni)
  - [dati (JSON)](https://github.com/pcm-dpc/COVID-19/tree/master/dati-json/dpc-covid19-ita-regioni.json)
  - [scheda riepilogativa (PDF)](https://github.com/pcm-dpc/COVID-19/raw/master/schede-riepilogative/regioni/dpc-covid19-ita-scheda-regioni-latest.pdf)
 
## Keep In Touch
 
- Mastodon: https://troet.cafe/@COVID_BZ_Statistics
- Matrix: https://matrix.to/#/@antoniogulino:matrix.org
