[covid-bz](../../../README.md) / [maps](../../README.md) / [2021-01](../../2021-01/README.md) / [28](../28)
# Donnerstag, 28. Januar 2021
 
- [PDF, Daten und Text](../28)
  - [tägliche Pressemitteilung](20210128.txt)
  - Karten
    - [Summe der PCR- und AG-Positiven der letzten 7 Tagen](CovidCartinaPositiviPcrAgUltimi7giorni-2021-01-28.pdf)
    - [Summe der PCR- **und AG**-Positiven der letzten **21** Tagen](CovidCartinaPositiviPcrAgUltimi21giorni-2021-01-28.pdf)
    - [**Anstieg bzw. Rückgang** der PCR- und AG-Positiven der letzten 7 Tagen im Vergleich zu den letzten 21 Tagen](CovidCartinaAccelerazione7su21giorniPositiviPcrAgUltimi-2021-01-28.pdf)
    - [Karte mit aktueller Quarantäne](CovidCartinaQuarantena-2021-01-28.pdf)
  - [weiteres Material](../28)
 
 
## Seniorenheime
 
Zwei neue Fälle im St. Sisinius in Laas. 
 
Für Details siehe 
- [Liste der suspekten Heime](Traueranzeigen-Seniorenheime-Hotspots-Welle2-20210128.txt)
- [Grafiken](Traueranzeigen-SeniorenheimeCumuliert_2020-05-01_to_2021-01-25.pdf)
 
 
## Massentest in Tramin am 30.-31.Januar
 
Am Wochenende 30.-31.Januar findet in Tramin ein Massentest mit PCR-Tests statt.

Bürgermeister: *„Wir haben in Tramin mittlerweile mehr als 75 Fälle und deswegen haben wir mit der Sanität gemeinsam beschlossen, dass wir diese Tests durchführen, um das Infektionsgeschehen in den Griff zu bekommen“* und *„Wir hatten insgesamt 25 Fälle im Kindergarten und auch eine Klasse in der Grundschule ist in Quarantäne“*

Patrick Franzoni (Projektleiter der "Aktion Südtirol testet"): *„Wir haben mehrere Strategien zur Verfügung und weil wir ein sehr effizientes Labor in Bozen haben, haben wir mittlerweile auch die Möglichkeit, eine Gemeinde mit PCR-Tests durchzutesten. Wenn wir beim Testen erfolgreich sein wollen, müssen wir imstande sein mit verschiedenen Möglichkeiten zu arbeiten und deswegen setzen wir dieses Mal PCR-Tests ein. Die Abstriche werden alle zwei Stunden nach Bozen ins Labor geliefert, wo diese dann ausgewertet werden – der normale Betrieb im Labor läuft aber trotzdem weiter. Mir wurde versprochen, dass die Ergebnisse – zumindest die positiven – schnellstmöglich, also innerhalb von fünf bis sechs Stunden übermittelt werden.“* Er erwartet sich ein ähnliches Ergebnis wie wenn man Antigen-Tests einsetzen würde. 

Teilnehmen können alle Bürger von Tramin, aber auch jene Personen, die im Gemeindegebiet von Tramin arbeiten und auch die Mittelschüler von Kurtatsch, die die Mittelschule in Tramin besuchen.
 
- Quelle: 
  - [Gemeinde Tramin](https://www.gemeinde.tramin.bz.it/system/web/news.aspx?menuonr=219551312&detailonr=225207044-559 )
  - [Tageszeitung](https://www.tageszeitung.it/2021/01/28/tramin-testet/)
 
# Links zu anderen Südtirolbezogene Webseiten
 
- Dashboard von [Markus Falk](https://www.markusfalk.com/)
  - [Version 1](https://www.markusfalk.com/dashboard/#lang=de&dset=ST&reg=all&tab=PRED&from=2020-08-11&to=true)
  - [Version 2](https://www.markusfalk.com/dashboard-beta/#lang=de&dset=ST_ALL&reg=all&tab=PRED&from=2020-08-11&to=true)
- Tabellen von [Moritz Mair](https://moritzmair.info/)
  - [Impfzahlen in Südtirol nach Alter](https://covvac.moritzmair.info/?tab=1)
- Ivan Sieder
  - [Interaktive Karten](https://map.corona-bz.simedia.cloud/)
- Zivilschutz Südtirol
  - [Grafiken](http://www.provinz.bz.it/sicherheit-zivilschutz/zivilschutz/aktuelle-daten-zum-coronavirus.asp)
  - [Aktuelle Excel-Datei mit Positiven (PCR+AG)](https://afbs.provinz.bz.it/upload/coronavirus/CoronavirusPositivi.xlsx)
  - [Aktuelle Excel-Datei mit Quarantäne](https://afbs.provinz.bz.it/upload/coronavirus/CoronavirusQuarantaene.xlsx)
- [Protezione civile Italia](https://github.com/pcm-dpc/COVID-19/blob/master/README.md)
  - [dati (CSV)](https://github.com/pcm-dpc/COVID-19/tree/master/dati-regioni)
  - [dati (JSON)](https://github.com/pcm-dpc/COVID-19/tree/master/dati-json/dpc-covid19-ita-regioni.json)
  - [scheda riepilogativa (PDF)](https://github.com/pcm-dpc/COVID-19/raw/master/schede-riepilogative/regioni/dpc-covid19-ita-scheda-regioni-latest.pdf)
 
## Keep In Touch
 
- Mastodon: https://troet.cafe/@COVID_BZ_Statistics
- Matrix: https://matrix.to/#/@antoniogulino:matrix.org
