[covid-bz](../../../README.md) / [maps](../../README.md) / [2021-01](../../2021-01/README.md) / [02](../02)
# Samstag, 2. Januar 2021

- [PDF, Daten und Text](../02)
  - [tägliche Pressemitteilung](20210102.txt)
  - [Karte mit Summe der PCR- und AG-Positiven der letzten 7 Tagen](CovidCartinaPositiviPcrAgUltimi7giorni-2021-01-02.pdf)
  - **NEW** [Karte mit Summe der PCR- **und AG**-Positiven der letzten **21** Tagen](CovidCartinaPositiviPcrAgUltimi21giorni-2021-01-02.pdf)
  - [Karte mit aktueller Quarantäne](CovidCartinaQuarantena-2021-01-02.pdf)
  - [weiteres Material](../02)

## 2. + 3. Januar: Lokaler Massentest in drei Pustrer Gemeinden: Bruneck, Mühlwald, Prettau

Es gibt einen "optischen" Wiederspruch, wenn man sich das Pustertal anschaut 
und die Farben der 3 Gemeinden (Bruneck, Mühlwald, Prettau)
mit lokalem Massentest und die Farben der Gemeinden um Toblach herum
(Toblach, Niederdorf, Prags, Gsies, Welsberg-Taisten).

Selbst vor 7 Tagen ([Karte PCR+AG 26. Dez.](../../2020-12/26/CovidCartinaPositiviPcrAgUltimi7giorni-2020-12-26.pdf))
sah es ähnlich aus.

Als  vor 3-4 Wochen nördlich von Bruneck es ziemlich rot  aussah,
hat es nicht lange gedauert, bis ein Seniorenheim "involviert" wurde
und bald darauf den ersten Todesfall zu trauern hatte.

## Seniorenheime
Für die [suspekten Heime](Traueranzeigen-Seniorenheime-Hotspots-Welle2-20210102.txt) ist nur ein  neuer Fall gefunden worden: Ritten; 
mit seinen 20 Todesfällen in 70 Tagen, bei ungefähr 55 Heimgästen. Nicht alle sind Covid bedingt,
aber in einer "normalen" Situation wären auch 6 Fälle schon viel.

Für Details siehe 
- [Liste der suspekten Heime](Traueranzeigen-Seniorenheime-Hotspots-Welle2-20210102.txt)
- [Grafiken](Traueranzeigen-SeniorenheimeCumuliert_2020-05-01_to_2020-12-30.pdf)

Auch wenn nirgendswo bekannt gegeben, gibt es Informationen, laut denen es im Seniorenheim der Stiftung St. Josef in Taufers (nördlich von Bruneck) es fast zwei Dutzend Positive gäbe, halbe-halbe Gäste und Personal.

Das Personal ist in Quarantäne, oder zumindest zu Hause, und die Gäste sind nach Bozen verlegt worden. 

Einer dieser Heimgäste ist kürzlich verstorben.



## Keep In Touch

- Mastodon: https://troet.cafe/@COVID_BZ_Statistics
