[covid-bz](../../../README.md) / [maps](../../README.md) / [2021-01](../../2021-01/README.md) / [12](../12)
# Dienstag, 12. Januar 2021

- [PDF, Daten und Text](../12)
  - [tägliche Pressemitteilung](20210112.txt)
  - Karten
    - [Summe der PCR- und AG-Positiven der letzten 7 Tagen](CovidCartinaPositiviPcrAgUltimi7giorni-2021-01-12.pdf)
    - [Summe der PCR- **und AG**-Positiven der letzten **21** Tagen](CovidCartinaPositiviPcrAgUltimi21giorni-2021-01-12.pdf)
    - [**Anstieg bzw. Rückgang** der PCR- und AG-Positiven der letzten 7 Tagen im Vergleich zu den letzten 21 Tagen](CovidCartinaAccelerazione7su21giorniPositiviPcrAgUltimi-2021-01-12.pdf)
    - [Karte mit aktueller Quarantäne](CovidCartinaQuarantena-2021-01-12.pdf)
  - [weiteres Material](../12)


## Seniorenheime

Heute sind für die als ["suspekt" aufgelisteten Seniorenheime](Traueranzeigen-Seniorenheime-Hotspots-Welle2-20210112.txt) keine Todesfälle gefunden worden.


Für Details siehe 
- [Liste der suspekten Heime](Traueranzeigen-Seniorenheime-Hotspots-Welle2-20210112.txt)
- [Grafiken](Traueranzeigen-SeniorenheimeCumuliert_2020-05-01_to_2021-01-09.pdf)

### 51 Covid-Tote in Bozner Seniorenheime
Bisher, an Hand verfügbaren Daten, wurden um 30 Covid-Tote für **Don Bosco (Bozen)** und 5 für **Villa Europa (Bozen)** [geschätzt](Traueranzeigen-Seniorenheime-Hotspots-Welle2-20210112.txt).
Leider war es eine Unterschätzung.
Laut einen Artikel im Alto Adige (12. Januar, Seite 18) gab es in Bozner Seniorenheime insgesamt 51 Covid-Tote. davon 47 während der 2. Welle.

Es handelt sich um die Antwort, die der Bozner Gemeindepolitiker Gabriele Giovanett von "Oltre" auf seine Anfrage erhalten hat.


In der Vergangen heit gab es pro Jahr folgende Anzahl an Todesfälle:
- 2020: 84
- 2019: 42
- 2018: 41
- 2017: 50 (meine Anmerkung: Winter 2016/2017 hatte eine schlimme Grippesaison)
- 2016: 43
- 2015: 38

Im Jahr 2020:
- Don Bosco: 41 Covid-Tote, davon 40 während der 2. Welle
  - Im November gab es 27 Covid-Tote, davon 4 mit einen "dubbio".
  - Im Dezember gab es 13 Covid-Tote, davon 2 mit einen "dubbio".
- Villa Europa 2020: 8+1=9 Covid-Tote, davon 6 im November
- Villa Armonia: 1 Covidtoter im Februar. Null 2. Welle
- Villa Serena: alle Covid-infizierte sind geheilt

- Heime Bozen: insgesamt 126 Gäste infiziert. über 50 Personal.
- Villa Europa: 30 infizierte
- Villa Serena und Villa Armonia: positives Personal, aber Gäste ok.

Aus den obigen Zahlen errechnet man, dass 1/4 der infizierten Gäste auch gestorben ist.
Und, dass die geschätzten Zahlen um 1/4 geringer sind als die offiziellen.

Als Zusatzinfo: "Don Bosco" hat um die 170 Gäste. "Villa Europa" um die 100. Mit anderen Worte: Im "Don Bosco" sind 1/4 aller Gäste an Covid gestorben, in weniger als 50 Tagen.


Ausserdem:
- Heute beginnen Impfungen in Villa Europa und Villa Armonia
- Protokoll: alle 14 Tage screening vom Personal. jede Woche falls es im Heim Fälle gibt. Wenn symptomatisch immer sofort AG-Test.


## Keep In Touch

- Mastodon: https://troet.cafe/@COVID_BZ_Statistics
  - Einladungslink (nicht unbedingt notwendig): https://troet.cafe/invite/goV294E5

