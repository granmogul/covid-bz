[covid-bz](../../../README.md) / [maps](../../README.md) / [2021-01](../../2021-01/README.md) / [06](../06)
# Mittwoch, 6. Januar 2021

- [PDF, Daten und Text](../06)
  - [tägliche Pressemitteilung](20210106.txt)
  - Karten
    - [Summe der PCR- und AG-Positiven der letzten 7 Tagen](CovidCartinaPositiviPcrAgUltimi7giorni-2021-01-06.pdf)
    - [Summe der PCR- **und AG**-Positiven der letzten **21** Tagen](CovidCartinaPositiviPcrAgUltimi21giorni-2021-01-06.pdf)
    - [**Anstieg bzw. Rückgang** der PCR- und AG-Positiven der letzten 7 Tagen im Vergleich zu den letzten 21 Tagen](CovidCartinaAccelerazione7su21giorniPositiviPcrAgUltimi-2021-01-06.pdf)
    - [Karte mit aktueller Quarantäne](CovidCartinaQuarantena-2021-01-06.pdf)
  - [weiteres Material](../06)


## Seniorenheime

Zu den oben aufgelisteten [suspekten Heime](Traueranzeigen-Seniorenheime-Hotspots-Welle2-20210106.txt) ist ein weiterer Fall hinzugekommen, im Seniorenheim **St. Sisinius in Laas.**
Wegen diesen "Fall zu viel" wird jetzt St. Sisinius von der unteren Gruppe der "weniger suspekten" in die obere bewegt.
Seit Ende November, also in weniger als 40 Tagen gab es 6 Todesfälle, während in den 12 Monaten davor es nur 12 waren.
St. Sisinius hat in normalen Zeiten 50 Heimgäste.


Ebenfalls wegen eine zusätzlichen Trauerfall zu viel ist das **Pflegeheim Firmian** (Bozen) in die Liste der "suspekten" Heime hinzugefügt worden.
In den letzten 75 Tagen gab es mindestens 12 Todesfälle. 9 davon in den letzten 30 Tagen.
So etwas (9 in 30 Tagen) gab es auch im April. In den anderen 10 Monaten gab es 32 Trauerfälle.
Wie immer, gibt es keine Informationen ob diese Personen wegen Covid gestorben sind.
Zusätzlich ist auch unklar, ob, falls überhaupt, sie angesteckt wurden während sie im Firmian waren
oder dort schon angesteckt aufgenommen wurden.


Für Details siehe 
- [Liste der suspekten Heime](Traueranzeigen-Seniorenheime-Hotspots-Welle2-20210106.txt)
- [Grafiken](Traueranzeigen-SeniorenheimeCumuliert_2020-05-01_to_2021-01-03.pdf)


### Pflegeheim Firmian (Bozen)

Die Pflegeinrichtung Firmian untersteht der SABES (es handelt sich irgendwie um eine externalisierte Abteilung des Spitals) und hat in der Regel 120 Gäste.

In den letzten 75 Tagen gab es mindestens 12 Todesfälle. 9 davon in den letzten 30 Tagen.
So etwas (9 in 30 Tagen) gab es auch im April. In den anderen 10 Monaten gab es 32 Trauerfälle.
Wie immer, gibt es keine Informationen ob diese Personen wegen Covid gestorben sind.
Zusätzlich ist auch unklar, ob, falls überhaupt, sie angesteckt wurden während sie im Firmian waren
oder dort schon angesteckt aufgenommen wurden.

Am 16. Dezember (vor 3 Wochen) wusste man, dass 110, zwischen Gäste und Mitarbeiter, sich mit dem Virus angesteckt hatten.
75 davon wurden zwischen dem 10. und 16. Dezember gefunden. Seit dem 23. Dezember weigert sich die SABES diese Daten herzugeben. 

Die hohe Anzahl an Todesfälle in einen Jahr darf aber als solche nicht wundern, da dort nur schwere Fälle aufgenommen werden.



## Keep In Touch

- Mastodon: https://troet.cafe/@COVID_BZ_Statistics
