[covid-bz](../../../README.md) / [maps](../../README.md) / [2021-01](../../2021-01/README.md) / [21](../21)
# Donnerstag, 21. Januar 2021

- [PDF, Daten und Text](../21)
  - [tägliche Pressemitteilung](20210121.txt)
  - Karten
    - [Summe der PCR- und AG-Positiven der letzten 7 Tagen](CovidCartinaPositiviPcrAgUltimi7giorni-2021-01-21.pdf)
    - [Summe der PCR- **und AG**-Positiven der letzten **21** Tagen](CovidCartinaPositiviPcrAgUltimi21giorni-2021-01-21.pdf)
    - [**Anstieg bzw. Rückgang** der PCR- und AG-Positiven der letzten 7 Tagen im Vergleich zu den letzten 21 Tagen](CovidCartinaAccelerazione7su21giorniPositiviPcrAgUltimi-2021-01-21.pdf)
    - [Karte mit aktueller Quarantäne](CovidCartinaQuarantena-2021-01-21.pdf)
  - [weiteres Material](../21)


## Seniorenheime

Heute sind für die als ["suspekt" aufgelisteten Seniorenheime](Traueranzeigen-Seniorenheime-Hotspots-Welle2-20210121.txt) zwei Todesfälle gefunden worden: im Lorenzerhof in Lana und in der Villa Eden in Meran.


Für Details siehe 
- [Liste der suspekten Heime](Traueranzeigen-Seniorenheime-Hotspots-Welle2-20210121.txt)
- [Grafiken](Traueranzeigen-SeniorenheimeCumuliert_2020-05-01_to_2021-01-18.pdf)


## Massentest am 23. und 24. in Moos und St. Leonhard im Passeier und in Jenesien

- [Pressemitteilung der SABES vom 21. Jan.](https://www.sabes.it/de/news.asp?aktuelles_action=4&aktuelles_article_id=651999)

### St. Leonhard im Passeier
- 3.600 Einw. 
- In den letzten 7 Tagen 59 positive (PCR+AG), in den 14 Tagen davor 52. 
- Die Summe der Positive der letzten 7 Tagen je 100.000 Einwohner beträgt: 1.700
- vom 11.10. bis 23.12. hatte sie keine offizielle Covid-Todefälle zu beklagen. Nach dem 23.12. wurden keine Daten mehr veröffentlicht.
- in St. Leonhard gibt es einen Seniorenheim (St. Barbara) mit 43 Betten. Dieser hat in der Regel vorwiegend Heimgäste aus der Gemeinde selber (50%) und den anderen zwei im Tal: Moos i.P. (25%) und St. Martin i.P. (20%)  aber auch Riffian.
- Für die Einwohner von St. Leonhard sind neben dem Seniorenheim St. Barbara (80%) auch die Seniorenheime in St. Martin i. P. (St. Benedikt, 15%) und in Riffian (Sternguet, 5%) interessant. Alle drei Heime sind zwischen 23.10.2020 und 21.1.2021 nicht auffällig geworden.

### Moos im Passeier
- 2.100 Einw. 
- In den letzten 7 Tagen 12 positive (PCR+AG), in den 14 Tagen davor 31. 
- Die Summe der Positive der letzten 7 Tagen je 100.000 Einwohner beträgt: 600
- vom 11.10. bis 23.12. hatte sie keine offizielle Covid-Todefälle zu beklagen. Nach dem 23.12. wurden keine Daten mehr veröffentlicht.
- in Moos i. P.  gibt es keinen Seniorenheim
- Für die Einwohner von Moos sind neben dem Seniorenheim in St. Leonhard i. P. (St. Barbara, 60%) auch die Seniorenheime in St. Martin i. P. (St. Benedikt, 20%) und Riffian (Sternguet, 10%) interessant. Alle drei Heime sind zwischen 23.10.2020 und 21.1.2021 nicht auffällig geworden.

### Jenesien
- 3.100 Einw. 
- In den letzten 7 Tagen 27 positive (PCR+AG), in den 14 Tagen davor 75. 
- Die Summe der Positive der letzten 7 Tagen je 100.000 Einwohner beträgt: 900
- vom 11.10. bis 23.12. hatte sie keine offizielle Covid-Todefälle zu beklagen. Nach dem 23.12. wurden keine Daten mehr veröffentlicht.
- in Jenesien gibt es keinen Seniorenheim
- Für die Einwohner von Jenesien ist der Seniorenheim in Mölten (Tschögglberg, 80%) interessant. Dieses ist zwischen 23.10.2020 und 21.1.2021 nicht auffällig geworden.

## Keep In Touch

- Mastodon: https://troet.cafe/@COVID_BZ_Statistics

