[covid-bz](../../../README.md) / [maps](../../README.md) / [2021-01](../../2021-01/README.md) / [15](../15)
# Freitag, 15. Januar 2021

- [PDF, Daten und Text](../15)
  - [tägliche Pressemitteilung](20210115.txt)
  - Karten
    - [Summe der PCR- und AG-Positiven der letzten 7 Tagen](CovidCartinaPositiviPcrAgUltimi7giorni-2021-01-15.pdf)
    - [Summe der PCR- **und AG**-Positiven der letzten **21** Tagen](CovidCartinaPositiviPcrAgUltimi21giorni-2021-01-15.pdf)
    - [**Anstieg bzw. Rückgang** der PCR- und AG-Positiven der letzten 7 Tagen im Vergleich zu den letzten 21 Tagen](CovidCartinaAccelerazione7su21giorniPositiviPcrAgUltimi-2021-01-15.pdf)
    - [Karte mit aktueller Quarantäne](CovidCartinaQuarantena-2021-01-15.pdf)
  - [weiteres Material](../15)


## Seniorenheime

Heute sind für die als ["suspekt" aufgelisteten Seniorenheime](Traueranzeigen-Seniorenheime-Hotspots-Welle2-20210115.txt) zwei Todesfälle gefunden worden, im Lorenzerhof in Lana.


Für Details siehe 
- [Liste der suspekten Heime](Traueranzeigen-Seniorenheime-Hotspots-Welle2-20210115.txt)
- [Grafiken](Traueranzeigen-SeniorenheimeCumuliert_2020-05-01_to_2021-01-12.pdf)

## Keep In Touch

- Mastodon: https://troet.cafe/@COVID_BZ_Statistics
  - Einladungslink (nicht unbedingt notwendig): https://troet.cafe/invite/goV294E5
