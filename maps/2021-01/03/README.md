[covid-bz](../../../README.md) / [maps](../../README.md) / [2021-01](../../2021-01/README.md) / [03](../03)
# Sonntag, 3. Januar 2021

- [PDF, Daten und Text](../03)
  - [tägliche Pressemitteilung](20210103.txt)
  - Karten
    - [Summe der PCR- und AG-Positiven der letzten 7 Tagen](CovidCartinaPositiviPcrAgUltimi7giorni-2021-01-03.pdf)
    - **NEW** [Summe der PCR- **und AG**-Positiven der letzten **21** Tagen](CovidCartinaPositiviPcrAgUltimi21giorni-2021-01-03.pdf)
    - **NEW** [**Anstieg bzw. Rückgang** der PCR- und AG-Positiven der letzten 7 Tagen im Vergleich zu den letzten 21 Tagen](CovidCartinaAccelerazione7su21giorniPositiviPcrAgUltimi-2021-01-03.pdf)
    - [Karte mit aktueller Quarantäne](CovidCartinaQuarantena-2021-01-03.pdf)
  - [weiteres Material](../03)

## 2. + 3. Januar: Lokaler Massentest in drei Pustrer Gemeinden: Bruneck, Mühlwald, Prettau

Laut [gestriger Pressemitteilung der SABES](https://www.sabes.it/de/news.asp?aktuelles_action=4&aktuelles_article_id=651463) 
wurden am Samstag (2. Jan.) in der Gemeinde Bruneck 3657 AG-Test gemacht (22% der Einwohner), davon 31 (0,85%) positiv. Heute soll in Bruneck weitergetestet werden. 

In den anderen zwei Gemeinden (Prettau und Mühlwald) wurde gestern nicht getestet, sondern nur heute (Sonntag, 3. Jan.).

Außerdem soll morgen (Montag, 4. Jan.) in einigen großen Brunecker Betrieben getestet werden, wo insgesamt an die 2.500 Mitarbeiterinnen und Mitarbeiter aufgerufen sind, sich testen zu lassen.

Soweit man verstehen kann, sind bei den Gemeinde-Tests nur die meldeamtliche Einwohner zugelassen, bei den Betriebstest wohl auch die nicht ansässigen.


Die [heutige Pressemitteilung am Abend](https://www.sabes.it/de/news.asp?aktuelles_action=4&aktuelles_article_id=651467)
berichtet von insgesamt 6703 Tests, davon 55 mit positiven Ergebnis, alle ini Bruneck.

Endergebnis der ersten zwei Tage (die Betriebstest folgen am Montag)

| Gemeinde  |  Einw. |  Tests | (je Einw) |   Pos  | (je Test) |
| --------- | ------ | ------ | --------- | ------ | --------- |
| Bruneck   | 16.805 | 6.216  | (37,0%)   |  55    |  (0,88%)  |
| Mühlwald  | 1.422 | 329  | (23,1%)   |  0    |  (0,00%)  |
| Prettau   |  543  | 185  | (34,1%)   |  0    |  (0,00%)  |
| **Insgesamt** | **18.770**  | **6703**  | **(35,7%)**   | **55**    |  **(0,82%)**  |


## Seniorenheime
Für die [suspekten Heime](Traueranzeigen-Seniorenheime-Hotspots-Welle2-20210103.txt) ist noch (at 14:12) ein  neuer Fall gefunden worden: **Don Bosco** in Bozen.
 
Don Bosco (Bozen) hat in "normalen" Zeiten um die 175 Heimgäste. In den letzten 70 Tagen sind mindestens 32 verstorben.
Die Meisten wohl in Zusammenhang mit Covid. Auf jeden Fall wären  in "normalen" Zeiten selbst 6 Fälle in 70 Tagen schon viel.

Einen Todesfall mit Covid gab es im **Seniorenheim von Olang**.

Für Details siehe 
- [Liste der suspekten Heime](Traueranzeigen-Seniorenheime-Hotspots-Welle2-20210103.txt)
- [Grafiken](Traueranzeigen-SeniorenheimeCumuliert_2020-05-01_to_2020-12-31.pdf)





## Keep In Touch

- Mastodon: https://troet.cafe/@COVID_BZ_Statistics
