[covid-bz](../../README.md) / [maps](../README.md) / [2021-01](../2021-01) 
# Cartine e altri grafici con i dati comunali trovati nei quotidiani file Excel e analisi dei necrologi

**[Februar 2021](../2021-02/README.md)**

## Januar 2021
- **[Sonntag,31. Jan.](31/README.md)**
- [Samstag, 30. Jan.](30/README.md), **NEW** [Residenti messi in quarantena negli ultimi 7 giorni](CovidCartinaQuarantenaMandatiUltimi7giornigiorniPositiviPcrAgUltimi-2021-01-30.pdf)
- [Freitag, 29. Jan.](29/README.md)
- [Giovedi, 28 gennaio](28/README.md) mit Ankündigung **Massentest** in **Tramin**
- [Mercoledi, 27 gennaio](27/README.md)
- [Martedi, 26 gennaio](26/README.md)
- [Lunedi, 25 gennaio](25/README.md)
- [Domenica, 24 gennaio](24/README.md) mit Endergebnisse 1.+2. Tag **Massentest** in **Moos** und **St. Leonahard** im Passeier und in **Jenesien**. Außerdem: **NEW** Links zu andere "Anbieter"
- [Sabato, 23 gennaio](23/README.md), mit Ergebnisse 1. Tag Massentest in Moos und St. Leonahard im Passeier und in Jenesien
- [Venerdi, 22 gennaio](22/README.md), mit **offiziellen** Daten zu den **Seniorenheime** an Hand Antwortschreiben der Landesrätin **Deeg**
- [Donnerstag, 21. Jan.](21/README.md), mit Grundinformationen zu Moos und St. Leonhard im Passeier und Jenesien ( **Massentest** am 23. und 24.)
- [Mittwoch, 20. Jan.](20/README.md)
- [Dienstag, 19. Jan.](19/README.md)
- [Montag, 18. Jan.](18/README.md)
- *[Sonntag, 17. Jan.](17)*
- [Samstag, 16. Jan.](16/README.md), mit Ergebnisse Massentest in **Toblach**: 15. und 16. Januar
- [Freitag, 15. Jan.](15/README.md)
- *[Giovedi, 14 gennaio](14)*
- *[Mercoledi, 13 gennaio](13)*
- [Martedi, 12 gennaio](12/README.md), mit Sonderinfo bzgl. 51 Covid-Tote in Bozner Seniorenheime
- [Lunedi, 11 gennaio](11/README.md), mit Informationen bzgl. Massentest am 9. und 10. in **Ahrntal** und **Gais** im Pustertal und in Graun im Vinschgau
- [Domenica, 10 gennaio](10/README.md), mit Sonderinfo bzgl. Covid-Verbreitung in 2 Brixner Seniorenheime. Grundinformationenzu lokalem Massentest
- [Sabato, 9 gennaio](09/README.md), mit Grundinformationen bzgl. Massentest am 9. und 10. in Ahrntal und Gais im Pustertal und in Graun im Vinschgau
- [Venerdi, 8 gennaio](08/README.md)
- [Donnerstag, 7. Jan.](07/README.md)
- [Mittwoch, 6. Jan.](06/README.md), mit Sonderinfo bzgl. Pflegeheim Firmian (Bozen)
- [Dienstag, 5. Jan.](05/README.md), mit Info bzgl. Massentests in Gais und Ahrntal und Betriebstests in Bruneck
- [Montag, 4. Jan.](04/README.md)
- [Sonntag, 3. Jan.](03/README.md), mit Ergebnisse bzgl. 2. + 3. Januar: Lokaler Massentest in drei Pustrer Gemeinden: **Bruneck**, **Mühlwald**, **Prettau**. Außerdem: **NEW** [Karte mit **Anstieg bzw. Rückgang** der PCR- und AG-Positiven der letzten 7 Tagen im Vergleich zu den letzten 21 Tagen](03/CovidCartinaAccelerazione7su21giorniPositiviPcrAgUltimi-2021-01-03.pdf)
- [Samstag, 2. Jan.](02/README.md), mit Kommentar bzgl. 2. + 3. Januar: Lokaler Massentest in drei Pustrer Gemeinden: Bruneck, Mühlwald, Prettau. Außerdem: **NEW** [Karte mit Summe der PCR- und AG-Positiven der letzten **21** Tagen](02/CovidCartinaPositiviPcrAgUltimi21giorni-2021-01-02.pdf)
- [Freitag, 1. Jan.](01/README.md), mit "Eine kleine Zeitreise" über Sterbezahlen in Italien und Südtirol in über 100 Jahren


## Keep In Touch

Für mehr Kommentare und evtl. Gedankenaustausch:
- Mastodon: https://troet.cafe/@COVID_BZ_Statistics
  - Einladungslink: https://troet.cafe/invite/goV294E5
