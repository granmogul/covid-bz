[covid-bz](../../../README.md) / [maps](../../README.md) / [2021-01](../../2021-01/README.md) / [24](../24)
# Sonntag, 24. Januar 2021
 
- [PDF, Daten und Text](../24)
  - [tägliche Pressemitteilung](20210124.txt)
  - Karten
    - [Summe der PCR- und AG-Positiven der letzten 7 Tagen](CovidCartinaPositiviPcrAgUltimi7giorni-2021-01-24.pdf)
    - [Summe der PCR- **und AG**-Positiven der letzten **21** Tagen](CovidCartinaPositiviPcrAgUltimi21giorni-2021-01-24.pdf)
    - [**Anstieg bzw. Rückgang** der PCR- und AG-Positiven der letzten 7 Tagen im Vergleich zu den letzten 21 Tagen](CovidCartinaAccelerazione7su21giorniPositiviPcrAgUltimi-2021-01-24.pdf)
    - [Karte mit aktueller Quarantäne](CovidCartinaQuarantena-2021-01-24.pdf)
  - [weiteres Material](../24)
 
 
## Seniorenheime
 
Für Details siehe 
- [Liste der suspekten Heime](Traueranzeigen-Seniorenheime-Hotspots-Welle2-20210124.txt)
- [Grafiken](Traueranzeigen-SeniorenheimeCumuliert_2020-05-01_to_2021-01-21.pdf)
 
## Massentest in Moos und St. Leonhard im Passeier und in Jenesien
Siehe Mitteilungen der Gemeinden:
- [Moos i. P.](https://www.gemeinde.moosinpasseier.bz.it/de/Cronoa-Test_-_Mitteilung_des_Buergermeisters_und_weitere_Informationen)
- [St. Leonhard i. P.](https://www.sankt-leonhard.eu/de/Cronoa-Test_-_Mitteilung_des_Buergermeisters_und_weitere_Informationen)
- [Jenesien](https://www.gemeinde.jenesien.bz.it/de/2_Covid-Massentest_in_Jenesien)


|         | Jenesien |Moos i. P.   |St. Leonhard i. P.  |
| :----    | -----: | ---------: | ---------: |
| **Einwohner**    | 3.066  | 2.086  | 3.558
| **16.1.-23.1.** |
| PCR+AG Pos.       | 28   | 8   | 42 |
| je 100 Einw.        | 0,9%   | 0,4%   | 1,2% |
| **Samstag**, 23. |
| Teilnahme    | 13,7%  | 8,5%  | 21,8% |
| Tests        | 421   | 177   | 775 |
| Positiv        | 3   | 2   | 21 |
| je 100 Tests        | 0,7%   | 1,1%   | 2,7% |
| **Sonntag**, 24. |
| Teilnahme    | 12,9%  | 9,2%  | 14,9% |
| Tests        | 395   | 191   | 530 |
| Positiv        | 4   | 11   | 15 |
| je 100 Tests        | 1,0%   | 5,8%   | 2,8% |
| **INSGESAMT** |
| Teilnahme    | 26,6%  | 17,6%  | 36,7% |
| Tests        | 816   | 368   | 1305 |
| Positiv        | 7   | 13   | 36 |
| je 100 Tests        | 0,9%   | 3,5%   | 2,8% |

Quelle: ISTAT, SABES, [Markus Falk on Telegram](https://t.me/c/1430539185/11147)
 

# Links zu anderen Südtirolbezogene Webseiten
 
- Dashboard von [Markus Falk](https://www.markusfalk.com/)
  - [Version 1](https://www.markusfalk.com/dashboard/#lang=de&dset=ST&reg=all&tab=PRED&from=2020-08-11&to=true)
  - [Version 2](https://www.markusfalk.com/dashboard-beta/#lang=de&dset=ST_ALL&reg=all&tab=PRED&from=2020-08-11&to=true)
- Tabellen von [Moritz Mair](https://moritzmair.info/)
  - [Impfzahlen in Südtirol nach Alter](https://covvac.moritzmair.info/?tab=1)
- Ivan Sieder
  - [Interaktive Karten](https://map.corona-bz.simedia.cloud/)
- Zivilschutz Südtirol
  - [Grafiken](http://www.provinz.bz.it/sicherheit-zivilschutz/zivilschutz/aktuelle-daten-zum-coronavirus.asp)
  - [Aktuelle Excel-Datei mit Positiven (PCR+AG)](https://afbs.provinz.bz.it/upload/coronavirus/CoronavirusPositivi.xlsx)
  - [Aktuelle Excel-Datei mit Quarantäne](https://afbs.provinz.bz.it/upload/coronavirus/CoronavirusQuarantaene.xlsx)
- [Protezione civile Italia](https://github.com/pcm-dpc/COVID-19/blob/master/README.md)
  - [dati (CSV)](https://github.com/pcm-dpc/COVID-19/tree/master/dati-regioni)
  - [dati (JSON)](https://github.com/pcm-dpc/COVID-19/tree/master/dati-json/dpc-covid19-ita-regioni.json)
  - [scheda riepilogativa (PDF)](https://github.com/pcm-dpc/COVID-19/raw/master/schede-riepilogative/regioni/dpc-covid19-ita-scheda-regioni-latest.pdf)
 
## Keep In Touch
 
- Mastodon: https://troet.cafe/@COVID_BZ_Statistics
- Matrix: https://matrix.to/#/@antoniogulino:matrix.org
