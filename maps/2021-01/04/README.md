[covid-bz](../../../README.md) / [maps](../../README.md) / [2021-01](../../2021-01/README.md) / [04](../04)
# Montag, 4. Januar 2021

- [PDF, Daten und Text](../04)
  - [tägliche Pressemitteilung](20210104.txt)
  - Karten
    - [Summe der PCR- und AG-Positiven der letzten 7 Tagen](CovidCartinaPositiviPcrAgUltimi7giorni-2021-01-04.pdf)
    - [Summe der PCR- **und AG**-Positiven der letzten **21** Tagen](CovidCartinaPositiviPcrAgUltimi21giorni-2021-01-04.pdf)
    - [**Anstieg bzw. Rückgang** der PCR- und AG-Positiven der letzten 7 Tagen im Vergleich zu den letzten 21 Tagen](CovidCartinaAccelerazione7su21giorniPositiviPcrAgUltimi-2021-01-04.pdf)
    - [Karte mit aktueller Quarantäne](CovidCartinaQuarantena-2021-01-04.pdf)
  - [weiteres Material](../04)

## Seniorenheime

Für die [suspekten Heime](Traueranzeigen-Seniorenheime-Hotspots-Welle2-20210104.txt) ist ein  neuer Fall gefunden worden: **Bezirksaltenheim Wipptal** in Sterzing.

Ab heute wird in der Liste der "weniger suspekten" das Altenheim Dorf Tirol geführt, mit 5 Fällen in den letzten 70 Tagen, davon 2 letzte Woche.

Das Seniorenheim Dorf Tirol hat in der Regel 35 Heimgäste. 
5 davon sind seit dem 3. Nov. verstorben.
Für die 10 Monaten davor lassen sich nur 4 Todesfälle finden.
Da es keine Informationen zu den Todesursachen gibt, könnte es klarerweise sein,
dass kein einziger dieser Todesfälle etwas mit dem Coronavirus zu tun hat.

Für Details siehe 
- [Liste der suspekten Heime](Traueranzeigen-Seniorenheime-Hotspots-Welle2-20210104.txt)
- [Grafiken](Traueranzeigen-SeniorenheimeCumuliert_2020-05-01_to_2021-01-01.pdf)


## Keep In Touch

- Mastodon: https://troet.cafe/@COVID_BZ_Statistics
 
