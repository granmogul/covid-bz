[covid-bz](../../../README.md) / [maps](../../README.md) / [2021-01](../../2021-01/README.md) / [16](../16)
# Freitag, 16. Januar 2021

- [PDF, Daten und Text](../16)
  - [tägliche Pressemitteilung](20210116.txt)
  - Karten
    - [Summe der PCR- und AG-Positiven der letzten 7 Tagen](CovidCartinaPositiviPcrAgUltimi7giorni-2021-01-16.pdf)
    - [Summe der PCR- **und AG**-Positiven der letzten **21** Tagen](CovidCartinaPositiviPcrAgUltimi21giorni-2021-01-16.pdf)
    - [**Anstieg bzw. Rückgang** der PCR- und AG-Positiven der letzten 7 Tagen im Vergleich zu den letzten 21 Tagen](CovidCartinaAccelerazione7su21giorniPositiviPcrAgUltimi-2021-01-16.pdf)
    - [Karte mit aktueller Quarantäne](CovidCartinaQuarantena-2021-01-16.pdf)
  - [weiteres Material](../16)

## Massentest in Toblach: 15. und 16. Januar 
Toblach hat 3.350 Einwohner. In den vergangenen 7 Tagen wurden 39 positive Fälle gemeldet (PCR+AG), die zwei Sochen davor 77.
Somit sin in den letzten 21 Tagen 3,5% der Bevölkerung positiv getestet worden. In Quarantäne sind zur Zeit offiziell 5,3%  (178) der Einwohner


|    |Tests|Pos| %|
|----|----|----|----|
|15. Jan., Freitag|780|15|1,9%|
|16. Jan., Samstag|464|7|1,5%|
|**Insgesamt**|**1.244**|**22**|**1,8%**|


## Seniorenheime

Heute ist wegen einen neuen Fall das **Langpflegeheim Firmian (Bozen)** wieder in der Liste der ["suspekten" Seniorenheime](Traueranzeigen-Seniorenheime-Hotspots-Welle2-20210116.txt) aufgenommen worden. Einen weiteren Todesfall gibt es für den Seniorenheim in **St. Pankraz** (im Ultental).


Für Details siehe 
- [Liste der suspekten Heime](Traueranzeigen-Seniorenheime-Hotspots-Welle2-20210116.txt)
- [Grafiken](Traueranzeigen-SeniorenheimeCumuliert_2020-05-01_to_2021-01-13.pdf)

## Keep In Touch

- Mastodon: https://troet.cafe/@COVID_BZ_Statistics
  - Einladungslink (nicht unbedingt notwendig): https://troet.cafe/invite/goV294E5

