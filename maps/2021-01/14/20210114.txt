https://www.sabes.it/de/news.asp?aktuelles_action=4&aktuelles_article_id=651769

Coronavirus – Mitteilungen des Südtiroler Sanitätsbetriebes | 14.01.2021 | 11:30

In den letzten 24 Stunden wurden 2.188 PCR-Tests untersucht und dabei 174 Neuinfektionen festgestellt. Zusätzlich gab es 199 positive Antigentests.

Foto: 123rf

Bisher (14. Jänner) wurden insgesamt 386.047 Abstriche untersucht, die von 168.946 Personen stammen.

Die Zahlen im Überblick:

PCR-Abstriche:

Untersuchte Abstriche gestern (13. Jänner): 2.188

Mittels PCR-Test neu positiv getestete Personen: 174

Gesamtzahl der mittels PCR-Test positiv getesteten Personen: 32.186

Gesamtzahl der untersuchten Abstriche: 386.047

Gesamtzahl der mit Abstrichen getesteten Personen: 168.946 (+489)

Antigentests:

Durchgeführte Antigentests gestern: 4.657

Mittels Antigentest neu positiv getestete Personen: 199

Weitere Daten:

Auf Normalstationen im Krankenhaus untergebrachte Covid-19-Patienten/-Patientinnen: 208

In Privatkliniken untergebrachte Covid-19-Patienten/-Patientinnen: 151

In Gossensaß und Sarns untergebrachte Covid-19-Patienten/-Patientinnen: 12 (12 in Gossensaß, 0 in Sarns)

Covid-19-Patientinnen und -Patienten in Intensivbetreuung: 21

Gesamtzahl der mit Covid-19 Verstorbenen: 796 (+8)

Personen in Quarantäne/häuslicher Isolation: 11.360

Personen, die Quarantäne/häusliche Isolation beendet haben: 72.367

Personen betroffen von verordneter Quarantäne/häuslicher Isolation: 83.727

Geheilte Personen: mit PCR Test 19.785 (+236); zusätzlich 1.564 (+5) Personen, die ein unklares Testergebnis hatten und in der Folge negativ getestet wurden; weiters geheilte Personen aufgrund von epidemiologischer Analyse: 7.410 (+68).

Positiv getestete Mitarbeiter und Mitarbeiterinnen des Sanitätsbetriebes: 1.437, davon 1.064 geheilt. Positiv getestete Basis-, Kinderbasisärzte und Bereitsschaftsärzte: 41, davon 29 geheilt. (Stand: 02.01.2021)



(VS)

