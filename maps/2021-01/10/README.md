[covid-bz](../../../README.md) / [maps](../../README.md) / [2021-01](../../2021-01/README.md) / [10](../10)
# Sonntag, 10. Januar 2021

- [PDF, Daten und Text](../10)
  - [tägliche Pressemitteilung](20210110.txt)
  - Karten
    - [Summe der PCR- und AG-Positiven der letzten 7 Tagen](CovidCartinaPositiviPcrAgUltimi7giorni-2021-01-10.pdf)
    - [Summe der PCR- **und AG**-Positiven der letzten **21** Tagen](CovidCartinaPositiviPcrAgUltimi21giorni-2021-01-10.pdf)
    - [**Anstieg bzw. Rückgang** der PCR- und AG-Positiven der letzten 7 Tagen im Vergleich zu den letzten 21 Tagen](CovidCartinaAccelerazione7su21giorniPositiviPcrAgUltimi-2021-01-10.pdf)
    - [Karte mit aktueller Quarantäne](CovidCartinaQuarantena-2021-01-10.pdf)
  - [weiteres Material](../10)


## Seniorenheime

Heute sind für die als ["suspekt" aufgelisteten Seniorenheime](Traueranzeigen-Seniorenheime-Hotspots-Welle2-20210110.txt) keine Todesfälle gefunden worden.

Aber die **Privatklinik St. Maria in Bozen** (mit 5 Fälle seit 23. Oktober) ist zu den "weniger suspekten" hinzugefügt worden.
Es ist zu sagen, dass es sich nicht um ein normales Seniorenheim handelt.
Mir ist unbekannt, ob die SABES dort auch Covid-Patienten unterbringt.

Für Details siehe 
- [Liste der suspekten Heime](Traueranzeigen-Seniorenheime-Hotspots-Welle2-20210110.txt)
- [Grafiken](Traueranzeigen-SeniorenheimeCumuliert_2020-05-01_to_2021-01-07.pdf)

### Covid-Verbreitung in 2 Brixner Seniorenheime
Laut Tageszeitung Alto Adige ([1](https://www.altoadige.it/cronaca/bressanone/covid-focolai-in-casa-di-riposo-43-casi-tra-anziani-e-operatori-1.2524061) und [2](https://www.altoadige.it/cronaca/bressanone/non-sappiamo-da-dove-sia-partita-la-catena-dei-contagi-1.2523968)) gab es in 2 Seniorenheime von Brixen instgesamt 43 positive Tests, davon 10 von Mitarbeiter. Der erste Fall ist am 28. Dezember bemerkt worden und erst nach ein paar Tagen das volle Ausmaß.

Ich führe zur Zeit das **Bürgerheim von Brixen** unter den "weniger suspekten". Leider könnte sich das bald ändern. Muss aber nicht.

## Massentest am 9. und 10. in Ahrntal und Gais im Pustertal und in Graun im Vinschgau

### Graun
- 2.400 Einw. 
- In den letzten 7 Tagen 31 positive (PCR+AG), in den 14 Tagen davor 13. 
  - Die Summe der Positive der letzten 7 Tagen je 100.000 Einwohner beträgt: 1300
- vom 11.10. bis 23.12. hatte sie 1 offizielle Covid-Todefälle zu beklagen. 
  - Je 100.000 Einwohner waren es 40.

### Ahrntal
- 6.000 Einw. 
- In den letzten 7 Tagen 10 Positive (PCR+AG), in den 14 Tagen davor 16. 
  - Die Summe der Positive der letzten 7 Tagen je 100.000 Einwohner beträgt: 170
- vom 11.10. bis 23.12. hatte sie 2 offizielle Covid-Todefälle zu beklagen. 
  - Je 100.000 Einwohner waren es 30.

## Gais
- 3.300 Einw. 
- In den letzten 7 Tagen 6 Positive (PCR+AG), in den 14 Tagen davor 10. 
  - Die Summe der Positive der letzten 7 Tagen je 100.000 Einwohner beträgt: 180
- vom 11.10. bis 23.12. hatte sie 1 offizielle Covid-Todefälle zu beklagen. 
  - Je 100.000 Einwohner waren es 30.


## Keep In Touch

- Mastodon: https://troet.cafe/@COVID_BZ_Statistics
  - Einladungslink: https://troet.cafe/invite/goV294E5
