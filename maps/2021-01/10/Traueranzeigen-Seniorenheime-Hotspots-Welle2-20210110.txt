dal 2020-10-23 a 2021-01-10 14:25                               Heim Welle2 attesoSemplice eccessoSemplice eccesso95   proba
Bolzano, Don Bosco                                 Bolzano, Don Bosco     32          3.038              28        26 0.00000
Ritten                                                         Ritten     20          3.038              16        14 0.00000
Meran, Villa Eden                                   Meran, Villa Eden     18          2.785              15        12 0.00000
Mals, Martinsheim                                   Mals, Martinsheim     17          3.545              13        10 0.00000
Schluderns                                                 Schluderns     14          2.026              11         9 0.00000
Klausen, Haus Eiseck                             Klausen, Haus Eiseck      9          1.772               7         4 0.00079
Meran, Martinsbrunn                               Meran, Martinsbrunn     11          4.811               6         2 0.01058
Sterzing, Bezirksaltenheim Wipptal Sterzing, Bezirksaltenheim Wipptal      9          3.038               5         3 0.00412
Kaltern                                                       Kaltern     10          4.051               5         2 0.00883
Bolzano, Villa Europa                           Bolzano, Villa Europa      7          1.772               5         2 0.01089
St. Ulrich, San Durich                         St. Ulrich, San Durich      8          3.038               4         2 0.01276
Lana, Lorenzerhof                                   Lana, Lorenzerhof      8          3.545               4         1 0.02851
Sarnthein, Sarner Stiftung                 Sarnthein, Sarner Stiftung      8          3.545               4         1 0.02851
Laas, St. Sisinius                                 Laas, St. Sisinius      6          1.772               4         1 0.03390

Bolzano, St. Maria                                 Bolzano, St. Maria      5          0.253               4         0 0.09214
Brixen, Bürgerheim                                 Brixen, Bürgerheim      5          0.253               4         0 0.09214
Dorf Tirol                                                 Dorf Tirol      5          0.760               4         0 0.09214
St. Pankraz                                               St. Pankraz      5          1.013               3         0 0.09214
Algund                                                         Algund      5          2.026               2         0 0.09214
Villanders, Josefsheim                         Villanders, Josefsheim      5          2.532               2         0 0.11315
INSIEME                                                  INSIEME (20)    207         48.615             158       147 0.71275

0 bis 2 sind vielleicht "Fehlalarme"

Privatklinik St. Maria in Bozen (mit 5 Fälle seit 23. Oktober) ist zu den "weniger suspekten" hinzugefügt worden.
Es ist zu sagen, dass es sich nicht um ein normales Seniorenheim handelt.
Mir ist unbekannt, ob die SABES dort auch Covid-Patienten unterbringt.

Laut Tageszeitung Alto Adige ([1] und [2]) gab es in 2 Seniorenheime von Brixen instgesamt 43 positive Tests, 
davon 10 von Mitarbeiter. Der erste Fall ist am 28. Dezember bemerkt worden und erst nach ein paar Tagen das volle Ausmaß.
Ich führe zur Zeit das Bürgerheim von Brixen unter den "weniger suspekten". Leider könnte sich das bald ändern. Muss aber nicht.

[1] https://www.altoadige.it/cronaca/bressanone/covid-focolai-in-casa-di-riposo-43-casi-tra-anziani-e-operatori-1.2524061
[2] https://www.altoadige.it/cronaca/bressanone/non-sappiamo-da-dove-sia-partita-la-catena-dei-contagi-1.2523968
