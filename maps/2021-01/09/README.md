[covid-bz](../../../README.md) / [maps](../../README.md) / [2021-01](../../2021-01/README.md) / [09](../09)
# Samstag, 9. Januar 2021

- [PDF, Daten und Text](../09)
  - [tägliche Pressemitteilung](20210109.txt)
  - Karten
    - [Summe der PCR- und AG-Positiven der letzten 7 Tagen](CovidCartinaPositiviPcrAgUltimi7giorni-2021-01-09.pdf)
    - [Summe der PCR- **und AG**-Positiven der letzten **21** Tagen](CovidCartinaPositiviPcrAgUltimi21giorni-2021-01-09.pdf)
    - [**Anstieg bzw. Rückgang** der PCR- und AG-Positiven der letzten 7 Tagen im Vergleich zu den letzten 21 Tagen](CovidCartinaAccelerazione7su21giorniPositiviPcrAgUltimi-2021-01-09.pdf)
    - [Karte mit aktueller Quarantäne](CovidCartinaQuarantena-2021-01-09.pdf)
  - [weiteres Material](../09)


## Seniorenheime

Heute sind für die als ["suspekt" aufgelisteten Seniorenheime](Traueranzeigen-Seniorenheime-Hotspots-Welle2-20210109.txt) keine Todesfälle gefunden worden.

Und **Firmian (Bolzano)** ist wieder aus der Liste geflogen. War vielleicht nur ein Fehlalarm.

Für Details siehe 
- [Liste der suspekten Heime](Traueranzeigen-Seniorenheime-Hotspots-Welle2-20210109.txt)
- [Grafiken](Traueranzeigen-SeniorenheimeCumuliert_2020-05-01_to_2021-01-06.pdf)


## Massentest am 9. und 10. in Ahrntal und Gais im Pustertal und in Graun im Vinschgau


### Graun
- 2.400 Einw. 
- In den letzten 7 Tagen 21 positive (PCR+AG), in den 14 Tagen davor 13. 
- Die Summe der Positive der letzten 7 Tagen je 100.000 Einwohner beträgt: 900
- vom 11.10. bis 23.12. hatte sie 1 offizielle Covid-Todefälle zu beklagen. Je 100.000 Einwohner waren es 40.

### Ahrntal
- 6.000 Einw. 
- In den letzten 7 Tagen 9 Positive (PCR+AG), in den 14 Tagen davor 23. 
- Die Summe der Positive der letzten 7 Tagen je 100.000 Einwohner beträgt: 150
- vom 11.10. bis 23.12. hatte sie 2 offizielle Covid-Todefälle zu beklagen. Je 100.000 Einwohner waren es 30.

## Gais
- 3.300 Einw. 
- In den letzten 7 Tagen 7 Positive (PCR+AG), in den 14 Tagen davor 11. 
- Die Summe der Positive der letzten 7 Tagen je 100.000 Einwohner beträgt: 210
- vom 11.10. bis 23.12. hatte sie 1 offizielle Covid-Todefälle zu beklagen. Je 100.000 Einwohner waren es 30.

## Keep In Touch

- Mastodon: https://troet.cafe/@COVID_BZ_Statistics
