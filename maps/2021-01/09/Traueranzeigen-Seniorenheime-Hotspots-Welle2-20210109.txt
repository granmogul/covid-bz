dal 2020-10-23 a 2021-01-09 22:00                                Heim Welle2 attesoSemplice eccessoSemplice eccesso95   proba
Bolzano, Don Bosco                                 Bolzano, Don Bosco     32           3.00              29        26 0.00000
Ritten                                                         Ritten     20           3.00              17        14 0.00000
Meran, Villa Eden                                   Meran, Villa Eden     18           2.75              15        12 0.00000
Mals, Martinsheim                                   Mals, Martinsheim     17           3.50              13        10 0.00000
Schluderns                                                 Schluderns     14           2.00              12         9 0.00000
Klausen, Haus Eiseck                             Klausen, Haus Eiseck      9           1.75               7         4 0.00072
Sterzing, Bezirksaltenheim Wipptal Sterzing, Bezirksaltenheim Wipptal      9           3.00               6         3 0.00380
Kaltern                                                       Kaltern     10           4.00               6         2 0.00813
Meran, Martinsbrunn                               Meran, Martinsbrunn     11           4.75               6         2 0.00970
Bolzano, Villa Europa                           Bolzano, Villa Europa      7           1.75               5         2 0.01021
St. Ulrich, San Durich                         St. Ulrich, San Durich      8           3.00               5         2 0.01190
Lana, Lorenzerhof                                   Lana, Lorenzerhof      8           3.50               4         1 0.02674
Sarnthein, Sarner Stiftung                 Sarnthein, Sarner Stiftung      8           3.50               4         1 0.02674
Laas, St. Sisinius                                 Laas, St. Sisinius      6           1.75               4         1 0.03218

Brixen, Bürgerheim                                 Brixen, Bürgerheim      5           0.25               4         0 0.08850
Dorf Tirol                                                 Dorf Tirol      5           0.75               4         0 0.08850
St. Pankraz                                               St. Pankraz      5           1.00               4         0 0.08850
Algund                                                         Algund      5           2.00               3         0 0.08850
Villanders, Josefsheim                         Villanders, Josefsheim      5           2.50               2         0 0.10882
INSIEME                                                  INSIEME (19)    202          47.75             154       143 0.59294

0 bis 2 sind vielleicht "Fehlalarme"


Für die oben aufgelisteten Seniorenheime sind keine weitere Todesfälle gefunden worden.

Firmian (Bolzano) ist wieder aus der Liste geflogen. War vielleicht nur ein Fehlalarm.
