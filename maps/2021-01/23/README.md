[covid-bz](../../../README.md) / [maps](../../README.md) / [2021-01](../../2021-01/README.md) / [23](../23)
# Samstag, 23. Januar 2021
 
- [PDF, Daten und Text](../23)
  - [tägliche Pressemitteilung](20210123.txt)
  - Karten
    - [Summe der PCR- und AG-Positiven der letzten 7 Tagen](CovidCartinaPositiviPcrAgUltimi7giorni-2021-01-23.pdf)
    - [Summe der PCR- **und AG**-Positiven der letzten **21** Tagen](CovidCartinaPositiviPcrAgUltimi21giorni-2021-01-23.pdf)
    - [**Anstieg bzw. Rückgang** der PCR- und AG-Positiven der letzten 7 Tagen im Vergleich zu den letzten 21 Tagen](CovidCartinaAccelerazione7su21giorniPositiviPcrAgUltimi-2021-01-23.pdf)
    - [Karte mit aktueller Quarantäne](CovidCartinaQuarantena-2021-01-23.pdf)
  - [weiteres Material](../23)
 
 
## Seniorenheime
 
Für Details siehe 
- [Liste der suspekten Heime](Traueranzeigen-Seniorenheime-Hotspots-Welle2-20210123.txt)
- [Grafiken](Traueranzeigen-SeniorenheimeCumuliert_2020-05-01_to_2021-01-20.pdf)
 
## Massentest in Moos und St. Leonhard im Passeier und in Jenesien
Siehe Mitteilungen der Gemeinden:
- [Moos i. P.](https://www.gemeinde.moosinpasseier.bz.it/de/Cronoa-Test_-_Mitteilung_des_Buergermeisters_und_weitere_Informationen)
- [St. Leonhard i. P.](https://www.sankt-leonhard.eu/de/Cronoa-Test_-_Mitteilung_des_Buergermeisters_und_weitere_Informationen)
- [Jenesien](https://www.gemeinde.jenesien.bz.it/de/2_Covid-Massentest_in_Jenesien)


|         | Jenesien |Moos i. P.   |St. Leonhard i. P.  |
| :----    | -----: | ---------: | ---------: |
| Einwohner    | 3.066  | 2.086  | 3.558
| 16.1.-23.1. |
| PCR+AG Pos.       | 28   | 8   | 42 |
| je 100 Einw.        | 0,9%   | 0,4%   | 1,2% |
| Samstag, 23. |
| Teilnahme    | 13,7%  | 8,5%  | 21,8% |
| Tests        | 421   | 177   | 775 |
| Positiv        | 3   | 2   | 21 |
| je 100 Tests        | 0,7%   | 1,1%   | 2,7% |

Quelle: ISTAT, SABES, [Markus Falk on Telegram](https://t.me/c/1430539185/11124)


## Keep In Touch
 
- Mastodon: https://troet.cafe/@COVID_BZ_Statistics
- Matrix: https://matrix.to/#/@antoniogulino:matrix.org
