dal 2020-10-23 a 2021-01-01 14:21                                Heim Welle2 attesoSemplice eccessoSemplice eccesso95   proba
Bolzano, Don Bosco                                 Bolzano, Don Bosco     31          2.692              28        25 0.00000
Ritten                                                         Ritten     19          2.692              16        13 0.00000
Meran, Villa Eden                                   Meran, Villa Eden     17          2.468              14        12 0.00000
Mals, Martinsheim                                   Mals, Martinsheim     17          3.141              13        11 0.00000
Schluderns                                                 Schluderns     14          1.795              12         9 0.00000
Klausen, Haus Eiseck                             Klausen, Haus Eiseck      9          1.571               7         4 0.00034
Kaltern                                                       Kaltern     10          3.590               6         3 0.00395
St. Ulrich, San Durich                         St. Ulrich, San Durich      8          2.692               5         2 0.00651
Sterzing, Bezirksaltenheim Wipptal Sterzing, Bezirksaltenheim Wipptal      8          2.692               5         2 0.00651
Lana, Lorenzerhof                                   Lana, Lorenzerhof      8          3.141               4         2 0.01525
Sarnthein, Sarner Stiftung                 Sarnthein, Sarner Stiftung      8          3.141               4         2 0.01525
Bolzano, Villa Europa                           Bolzano, Villa Europa      6          1.571               4         1 0.02045
Meran, Martinsbrunn                               Meran, Martinsbrunn      9          4.263               4         1 0.03026

Brixen, Bürgerheim                                 Brixen, Bürgerheim      5          0.224               4         0 0.06213
St. Pankraz                                               St. Pankraz      5          0.897               4         0 0.06213
Algund                                                         Algund      5          1.795               3         0 0.06213
Laas, St. Sisinius                                 Laas, St. Sisinius      5          1.571               3         0 0.06213
Villanders, Josefsheim                         Villanders, Josefsheim      5          2.244               2         0 0.07729
INSIEME                                                  INSIEME (18)    189         42.179             146       136 0.42432

0 bis 1 sind "Fehlalarme"

Für oben aufgelistete Heime ist (noch) kein neuer Fall gefunden worden.
