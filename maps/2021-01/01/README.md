[covid-bz](../../../README.md) / [maps](../../README.md) / [2021-01](../../2021-01/README.md) / [01](../01)
# Freitag, 1. Januar 2021

- [PDF, Daten und Text](../01)
  - [tägliche Pressemitteilung](20210101.txt)
  - [Karte mit Summe der PCR- und AG-Positiven der letzten 7 Tagen](CovidCartinaPositiviPcrAgUltimi7giorni-2021-01-01.pdf)
  - [Karte mit aktueller Quarantäne](CovidCartinaQuarantena-2021-01-01.pdf)
  - [weiteres Material](../01)




## Seniorenheime

Leider ist die allererste Traueranzeige, die ich gefunden hab und das neue Jahr 2021 betrifft, eine mit Bezug auf ein Seniorenheim, das aber nicht "suspekt" ist.

Für die suspekten Heime ist keine Traueranzeige gefunden worden.

Für Details siehe 
- [Liste der Suspekten](Traueranzeigen-Seniorenheime-Hotspots-Welle2-20210101.txt)
- [Grafiken](Traueranzeigen-SeniorenheimeCumuliert_2020-05-01_to_2020-12-29.pdf)

## Eine kleine Zeitreise

Wir werden für das Jahr 2020 vielleicht mehr als 10 Tote je 1000 Einwohner haben.

Das letzte mal, dass die 10-er Marke überschritten wurde in Südtirol, war im Jahr 1952.

Das ISTAT  schätzt , dass Italienweit das Jahr 2020 das bisherige Recordjahr 1944(!) schlagen wird.

Wenn wir für Südtirol die offiziellen COVID-Tote 2020 mit den Einwohner dividieren, erhalten wir das Verhältnis 140 je 100.000.

Italienweit, ALLE Todesfälle wegen *"Malattie infettive e parassitarie"* je 100 Tausend Einwohner, 
war das letzte mal nur in den Jahren 1941-1946 höher als 140. 
1949 war diese Zahl schon unter 100 und im "glorreichen" Jahr 1965 schon unter 20. 
Alle "malattie infettive und parassitarie" zusammen, wohl gemerkt.

Quellen: 
- ASTAT: Südtirols Bevölkerung gestern, heute, morgen - von 1936 bis 2010 [PDF](https://astat.provincia.bz.it/it/news-pubblicazioni-info.asp?news_action=300&news_image_id=898474)
- ISTAT: Sommario di statistiche storiche dell'Italia. 1861 - 1965 [PDF](https://www.istat.it/it/files/2011/03/sommariostatistichestoriche1861-1965.pdf)




## Keep In Touch

- Mastodon: https://troet.cafe/@COVID_BZ_Statistics
