https://www.sabes.it/de/news.asp?aktuelles_action=4&aktuelles_article_id=657592

Coronavirus – Mitteilung des Sanitätsbetriebes | 14.07.2021 | 11:56

In den letzten 24 Stunden wurden 442 PCR-Tests untersucht und dabei 8 Neuinfektionen festgestellt. Zusätzlich gab es 4 positive Antigentests.

Foto: 123rf

Bisher (14. Juli) wurden insgesamt 601.625 Abstriche untersucht, die von 225.528 Personen stammen.

Die Zahlen im Überblick:

PCR-Abstriche:

Untersuchte Abstriche gestern (13. Juli): 442Mittels PCR-Test neu positiv getestete Personen: 8

Gesamtzahl der mittels PCR-Test positiv getesteten Personen: 48.757

Gesamtzahl der untersuchten Abstriche: 601.625

Gesamtzahl der mit Abstrichen getesteten Personen: 225.528 (+143)

Antigentests:

Gesamtzahl der durchgeführten Antigentests: 1.596.392

Gesamtzahl der positiven Antigentests: 26.269

Durchgeführte Antigentests gestern: 1.981

Mittels Antigentest neu positiv getestete Personen: 4

Nasenflügeltests Stand 13.07.2021: 782.245 Tests gesamt, 919 positive Ergebnisse, davon 390 bestätigt, 346 PCR-negativ, 183 ausständig/zu überprüfen.

Weitere Daten:

Auf Normalstationen im Krankenhaus untergebrachte Covid-19-Patienten/-Patientinnen: 5

In Gossensaß und Sarns untergebrachte Covid-19-Patienten/-Patientinnen: 1

Anzahl der auf Intensivstationen aufgenommenen Covid-Patienten/Patientinnen: 2 (davon 1 als ICU-Covid klassifiziert)

Gesamtzahl der mit Covid-19 Verstorbenen: 1.182 (+0)

Personen in Quarantäne/häuslicher Isolation: 364

Personen, die Quarantäne/häusliche Isolation beendet haben: 136.911

Personen betroffen von verordneter Quarantäne/häuslicher Isolation: 137.275

Geheilte Personen insgesamt: 73.704 (+2)

Positiv getestete Mitarbeiter und Mitarbeiterinnen des Sanitätsbetriebes: 2.043, davon 1.681 geheilt. Positiv getestete Basis-, Kinderbasisärzte und Bereitschaftsärzte: 58, davon 46 geheilt. (Stand: 13.05.2021)

Informationen zur Pflichtimpfung des Gesundheitspersonals

NB: Aufgrund der vielen beteiligten Akteure (Autonome Provinz Bozen, Südtiroler Sanitätsbetrieb, Berufskammern, verschiedene Arbeitgeber, u.a.m.) und der Tatsache, dass die betroffenen Personen sich jederzeit zur Impfung anmelden können, sind die Daten als vorläufig zu betrachten.

(VS)

