[covid-bz](../../../README.md) / [maps](../../README.md) / [2021-07](../../2021-07/README.md) / [14](../14)
 
# Mittwoch, 14. Juli 2021
 
<img src="CovidCartinaPositiviPcrAgUltimi7giorni-2021-07-14-1.jpg" alt="7-Tage-Inzidenz" width="50%" align="right">
 
Die **7-Tage-Inzidenz** (um die 20 auf 100.000) ist,
in den letzten 5 Wochen im Durchschnitt um -3.3% täglich gesunken, aber seit einigen Tagen wieder beim steigen.


- [Die 7-Tage-Inzidenz](#die-7-tage-inzidenz)
- [Die Lage in den Gemeinden](#die-lage-in-den-gemeinden)
- [Seniorenheime](#seniorenheime)
- [Links zu anderen Südtirolbezogene Webseiten](#links-zu-anderen-südtirolbezogene-webseiten)
- [Keep In Touch](#keep-in-touch)


## Die 7-Tage-Inzidenz


Die 7-Tage-Inzidenz (54 PCR + 33 AG) ist 16 auf 100.000.
Vor einer Woche (am 07.07.) war sie 10.
Also +6.8% täglich.


## Die Lage in den Gemeinden
 

<img src="CovidCartinaCovidFree-2021-07-14-1.jpg" alt="Cartina con comuni covid-free" width="50%" align="right">
 
Folgende 58 Gemeinden könnte man als **Covid-Free** betrachten, denn sie haben schon seit 21 Tagen keinen Fall mehr gehabt, niemand befindet sich in Quarantäne oder Isolation und die SABES zählt zur Zeit keinen "aktiv Positiven".
 
1. Kastelruth (6.855)
1. Schlanders (6.215)
1. Latsch (5.214)
1. St.Ulrich (4.869)
1. Deutschnofen (3.937)
1. Salurn (3.827)
1. Auer (3.825)
1. Prad am Stilfser Joch (3.624)
1. Völs am Schlern (3.579)
1. St.Leonhard i.P. (3.558)
1. Vintl (3.339)
1. St.Martin i.P. (3.255)
1. Mühlbach (3.151)
1. Jenesien (3.066)
1. Welsberg-Taisten (2.914)
1. Schenna (2.912)
1. Kiens (2.887)
1. Pfalzen (2.823)
1. Villnöss (2.629)
1. Graun im Vinschgau (2.389)
1. Gsies (2.320)
1. Kastelbell-Tschars (2.298)
1. Moos i.P. (2.086)
1. Nals (2.017)
1. St.Christina in Gröden (1.974)
1. Tisens (1.965)
1. Sexten (1.880)
1. Schluderns (1.838)
1. Barbian (1.755)
1. Montan (1.701)
1. Mölten (1.692)
1. Aldein (1.656)
1. Percha (1.565)
1. St.Pankraz (1.544)
1. Müehlwald (1.432)
1. Riffian (1.362)
1. Magreid a.d.W. (1.274)
1. Rodeneck (1.240)
1. Schnals (1.228)
1. Stilfs (1.141)
1. Pfatten (1.057)
1. Truden im Naturpark (1.040)
1. Andrian (1.030)
1. Tiers (1.007)
1. Franzensfeste (1.000)
1. Vöran (959)
1. Glurns (900)
1. Martell (841)
1. Hafling (778)
1. U.L.Frau I.W.-St.Felix (762)
1. Kurtinig a.d.W. (665)
1. Prags (655)
1. Prettau (546)
1. Altrei (398)
1. Kuens (395)
1. Laurein (342)
1. Proveis (265)
1. Waidbruck (195)
 
Weitere Informationen: 
- [PDF, Daten und Text](../14) - in tedesco e italiano, se non indicato diversamente
  - [tägliche Pressemitteilung](20210714.txt) - solo tedesco
  - Karten - Cartine con dettaglio comunale
    - [**Covid-freie** Gemeinden](CovidCartinaCovidFree-2021-07-14.pdf)
    - [Cartina con evidenziati i comuni con almeno un nuovo caso](CovidCartinaPositiviPcrAgOggi-2021-07-14.pdf)
    - [Summe der PCR- und AG-Positiven der letzten 7 Tagen](CovidCartinaPositiviPcrAgUltimi7giorni-2021-07-14.pdf)
    - [Summe der PCR- **und AG**-Positiven der letzten **21** Tagen](CovidCartinaPositiviPcrAgUltimi21giorni-2021-07-14.pdf)
    - [**Anstieg bzw. Rückgang** der PCR- und AG-Positiven der letzten 7 Tagen im Vergleich zu den letzten 21 Tagen](CovidCartinaAccelerazione7su21giorniPositiviPcrAgUltimi-2021-07-14.pdf)
    - [Persone trovate positive con test PCR rispetto tutti i positivi (PCR e AG). Ultimi 21 giorni](CovidCartinaPositivi21PctPcr-2021-07-14.pdf)
    - [Karte mit aktueller Quarantäne](CovidCartinaQuarantena-2021-07-14.pdf)
    - [Residenti messi in quarantena negli ultimi 7 giorni](CovidCartinaQuarantenaMandatiUltimi7giorni-2021-07-14.pdf)
    - [Persone con almento un test positivo (PCR o AG), da 1° ottobre in poi](CovidCartinaPositiviTotaleWelle2-2021-07-14.pdf)
  - [weiteres Material](../14)
 
 
## Seniorenheime
 
Für Details siehe 
- [Liste der suspekten Heime](Traueranzeigen-Seniorenheime-Hotspots-Welle2-20210714.txt)
- [Grafiken](Traueranzeigen-SeniorenheimeCumuliert_2020-05-01_to_2021-07-11.pdf)
 
 
# Links zu anderen Südtirolbezogene Webseiten
 
- Dashboard von [Markus Falk](https://www.markusfalk.com/)
  - [Version 1](https://www.markusfalk.com/dashboard/#lang=de&dset=ST&reg=all&tab=PRED&from=2020-08-11&to=true)
  - [Version 2](https://www.markusfalk.com/dashboard-beta/#lang=de&dset=ST_ALL&reg=all&tab=PRED&from=2020-08-11&to=true)
- Tabellen von [Moritz Mair](https://moritzmair.info/)
  - [Impfzahlen in Südtirol nach Alter](https://covvac.moritzmair.info/?tab=1)
- Ivan Sieder
  - [Interaktive Karten](https://map.corona-bz.simedia.cloud/)
- Zivilschutz Südtirol
  - Ohne Gemeindedetails
    - [Grafiken](http://www.provinz.bz.it/sicherheit-zivilschutz/zivilschutz/aktuelle-daten-zum-coronavirus.asp)
    - [dati provinciali con serie storica a partire da 15 marzo 2020](https://afbs.provinz.bz.it/upload/coronavirus/Corona.Data.Total.csv)
  - Mit Gemeindedetails
    - [Grafici e cartine con dettaglio comunale](http://www.provincia.bz.it/sicurezza-protezione-civile/protezione-civile/aktuelle-daten-zum-coronavirus-in-den-gemeinden.asp)
    - [Aktuelle Excel-Datei mit Positiven (PCR+AG)](https://afbs.provinz.bz.it/upload/coronavirus/CoronavirusPositivi.xlsx)
    - [Aktuelle Excel-Datei mit Quarantäne](https://afbs.provinz.bz.it/upload/coronavirus/CoronavirusQuarantaene.xlsx)
    - [dati comunali con serie storica a partire da 18 dicembre](https://afbs.provinz.bz.it/upload/coronavirus/Corona.Data.Detail.csv)
- [Protezione civile Italia](https://github.com/pcm-dpc/COVID-19/blob/master/README.md)
  - diffusione del Virus
    - [dati (CSV)](https://github.com/pcm-dpc/COVID-19/tree/master/dati-regioni)
    - [dati (JSON)](https://github.com/pcm-dpc/COVID-19/tree/master/dati-json/dpc-covid19-ita-regioni.json)
    - [scheda riepilogativa (PDF)](https://github.com/pcm-dpc/COVID-19/raw/master/schede-riepilogative/regioni/dpc-covid19-ita-scheda-regioni-latest.pdf)
  - [Vaccinazioni](https://github.com/italia/covid19-opendata-vaccini/tree/master/dati)
    - [dati provinciali per classi di età, serie storica (CSV)](https://github.com/italia/covid19-opendata-vaccini/blob/master/dati/somministrazioni-vaccini-latest.csv)
 
## Keep In Touch
 
- Mastodon: https://troet.cafe/@COVID_BZ_Statistics
- Matrix: https://matrix.to/#/@antoniogulino:matrix.org
