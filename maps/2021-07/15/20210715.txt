https://www.sabes.it/de/news.asp?aktuelles_action=4&aktuelles_article_id=657628

Coronavirus – Mitteilung des Sanitätsbetriebes | 15.07.2021 | 11:36

In den letzten 24 Stunden wurden 615 PCR-Tests untersucht und dabei 13 Neuinfektionen festgestellt. Zusätzlich gab es 10 positive Antigentests.

Foto: 123rf

Bisher (15. Juli) wurden insgesamt 602.240 Abstriche untersucht, die von 225.724 Personen stammen.

Die Zahlen im Überblick:

PCR-Abstriche:

Untersuchte Abstriche gestern (14. Juli): 615Mittels PCR-Test neu positiv getestete Personen: 13

Gesamtzahl der mittels PCR-Test positiv getesteten Personen: 48.770

Gesamtzahl der untersuchten Abstriche: 602.240

Gesamtzahl der mit Abstrichen getesteten Personen: 225.724 (+196)

Antigentests:

Gesamtzahl der durchgeführten Antigentests: 1.598.048

Gesamtzahl der positiven Antigentests: 26.279

Durchgeführte Antigentests gestern: 1.656

Mittels Antigentest neu positiv getestete Personen: 10

Nasenflügeltests Stand 14.07.2021: 784.560 Tests gesamt, 921 positive Ergebnisse, davon 392 bestätigt, 346 PCR-negativ, 183 ausständig/zu überprüfen.

Weitere Daten:

Anzahl der auf Intensivstationen aufgenommenen Covid-Patienten/Patientinnen: 0

Gesamtzahl der mit Covid-19 Verstorbenen: 1.182 (+0)

Personen in Quarantäne/häuslicher Isolation: 394

Personen, die Quarantäne/häusliche Isolation beendet haben: 136.924

Personen betroffen von verordneter Quarantäne/häuslicher Isolation: 137.318

Geheilte Personen insgesamt: 73.718 (+14)

Positiv getestete Mitarbeiter und Mitarbeiterinnen des Sanitätsbetriebes: 2.043, davon 1.681 geheilt. Positiv getestete Basis-, Kinderbasisärzte und Bereitschaftsärzte: 58, davon 46 geheilt. (Stand: 13.05.2021)

(VS)

