https://www.sabes.it/de/news.asp?aktuelles_action=4&aktuelles_article_id=657742

Coronavirus – Mitteilung des Sanitätsbetriebes | 21.07.2021 | 11:48

In den letzten 24 Stunden wurden 670 PCR-Tests untersucht und dabei 14 Neuinfektionen festgestellt. Zusätzlich gab es 13 positive Antigentests.

Coronavirus: 14 Neuinfektionen nachgewiesen durch PCR-Test und 13 positive Antigentests (Foto: 123rf)

Bisher (21. Juli) wurden insgesamt 605.322 Abstriche untersucht, die von 226.802 Personen stammen.

Die Zahlen im Überblick:

PCR-Abstriche:

Untersuchte Abstriche gestern (20. Juli): 670Mittels PCR-Test neu positiv getestete Personen: 14

Gesamtzahl der mittels PCR-Test positiv getesteten Personen: 48.846

Gesamtzahl der untersuchten Abstriche: 605.322

Gesamtzahl der mit Abstrichen getesteten Personen: 226.802 (+204)

Antigentests:

Gesamtzahl der durchgeführten Antigentests: 1.611.455

Gesamtzahl der positiven Antigentests: 26.354

Durchgeführte Antigentests gestern: 2.307

Mittels Antigentest neu positiv getestete Personen: 13

Nasenflügeltests Stand 20.07.2021: 792.975 Tests gesamt, 931 positive Ergebnisse, davon 397 bestätigt, 347 PCR-negativ, 187 ausständig/zu überprüfen.

Weitere Daten:

Auf Normalstationen im Krankenhaus untergebrachte Covid-19-Patienten/-Patientinnen: 5

In Gossensaß und Sarns untergebrachte Covid-19-Patienten/-Patientinnen: 7

Anzahl der auf Intensivstationen aufgenommenen Covid-Patienten/Patientinnen: 0

Gesamtzahl der mit Covid-19 Verstorbenen: 1.183 (+1)

Personen in Quarantäne/häuslicher Isolation: 576

Personen, die Quarantäne/häusliche Isolation beendet haben: 137.051

Personen betroffen von verordneter Quarantäne/häuslicher Isolation: 137.627

Geheilte Personen insgesamt: 73.787 (+9)

Positiv getestete Mitarbeiter und Mitarbeiterinnen des Sanitätsbetriebes: 2.043, davon 1.681 geheilt. Positiv getestete Basis-, Kinderbasisärzte und Bereitschaftsärzte: 58, davon 46 geheilt. (Stand: 13.05.2021) Hinweis: Die Anzahl der Abstriche wurde gestern (20.07.) aufgrund einer Neuberechnung durch die IT von 604.653 auf 604.652 korrigiert.

(MK)

