dal 2020-10-23 a 2021-07-23 21:43  Welle2 attesoSemplice eccessoSemplice eccesso95   proba
Bolzano, Don Bosco                     35           10.5              24        19 0.00000
Mals, Martinsheim                      27           12.2              14         9 0.00018
Meran, Villa Eden                      22            9.6              12         7 0.00043
Ritten                                 23           10.5              12         7 0.00057
Schluderns                             19            7.0              12         6 0.00085
Tisens, St. Josef                      16            4.4              11         3 0.01009
Sterzing, Bezirksaltenheim Wipptal     20           10.5               9         4 0.00579

Niederdorf, Von-Kurz                   17            9.6               7         2 0.01978
Lana, Lorenzerhof                      19           12.2               6         1 0.04421

Partschins                             13            4.4               8         0 0.07338
Klausen, Haus Eiseck                   13            6.1               6         0 0.07338
Schlanders, St. Nikolaus v.d. Flüe     20           14.0               6         0 0.07650
INSIEME (12)                          244          111.1             132       115 0.30517

0 bis 1 sind "Fehlalarme"

