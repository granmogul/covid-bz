https://www.sabes.it/de/news.asp?aktuelles_action=4&aktuelles_article_id=657651

Coronavirus – Mitteilung des Sanitätsbetriebes | 16.07.2021 | 11:26

In den letzten 24 Stunden wurden 449 PCR-Tests untersucht und dabei 7 Neuinfektionen festgestellt. Zusätzlich gab es 13 positive Antigentests.

Foto: 123rf

Bisher (16. Juli) wurden insgesamt 602.690 Abstriche untersucht, die von 225.866 Personen stammen.

Die Zahlen im Überblick:

PCR-Abstriche:

Untersuchte Abstriche gestern (15. Juli): 449Mittels PCR-Test neu positiv getestete Personen: 7

Gesamtzahl der mittels PCR-Test positiv getesteten Personen: 48.777

Gesamtzahl der untersuchten Abstriche: 602.690

Gesamtzahl der mit Abstrichen getesteten Personen: 225.866 (+142)

Antigentests:

Gesamtzahl der durchgeführten Antigentests: 1.600.242

Gesamtzahl der positiven Antigentests: 26.292

Durchgeführte Antigentests gestern: 2.194

Mittels Antigentest neu positiv getestete Personen: 13

Nasenflügeltests Stand 15.07.2021: 786.136 Tests gesamt, 924 positive Ergebnisse, davon 395 bestätigt, 346 PCR-negativ, 183 ausständig/zu überprüfen.

Weitere Daten:

Anzahl der auf Intensivstationen aufgenommenen Covid-Patienten/Patientinnen: 0

Gesamtzahl der mit Covid-19 Verstorbenen: 1.182 (+0)

Personen in Quarantäne/häuslicher Isolation: 396

Personen, die Quarantäne/häusliche Isolation beendet haben: 136.954

Personen betroffen von verordneter Quarantäne/häuslicher Isolation: 137.350

Geheilte Personen insgesamt: 73.731 (+13)

Positiv getestete Mitarbeiter und Mitarbeiterinnen des Sanitätsbetriebes: 2.043, davon 1.681 geheilt. Positiv getestete Basis-, Kinderbasisärzte und Bereitschaftsärzte: 58, davon 46 geheilt. (Stand: 13.05.2021)

(VS)

