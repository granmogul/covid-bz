https://www.sabes.it/de/news.asp?aktuelles_action=4&aktuelles_article_id=657466

Coronavirus – Mitteilung des Sanitätsbetriebes | 09.07.2021 | 12:03

In den letzten 24 Stunden wurden 493 PCR-Tests untersucht und dabei 6 Neuinfektionen festgestellt. Zusätzlich gab es 2 positive Antigentests.

Foto: 123rf

Bisher (08. Juli) wurden insgesamt 598.987 Abstriche untersucht, die von 224.657 Personen stammen.

Die Zahlen im Überblick:

PCR-Abstriche:

Untersuchte Abstriche gestern (07. Juli): 493

Mittels PCR-Test neu positiv getestete Personen: 6

Gesamtzahl der mittels PCR-Test positiv getesteten Personen: 48.709

Gesamtzahl der untersuchten Abstriche: 598.987

Gesamtzahl der mit Abstrichen getesteten Personen: 224.657 (+175)

Antigentests:

Gesamtzahl der durchgeführten Antigentests: 1.585.264

Gesamtzahl der positiven Antigentests: 26.238

Durchgeführte Antigentests gestern: 1.743

Mittels Antigentest neu positiv getestete Personen: 2

Nasenflügeltests Stand 06.07.2021: 763.462 Tests gesamt, 900 positive Ergebnisse, davon 379 bestätigt, 343 PCR-negativ, 178 ausständig/zu überprüfen.

Weitere Daten:

Auf Normalstationen im Krankenhaus untergebrachte Covid-19-Patienten/-Patientinnen: 6

In Gossensaß und Sarns untergebrachte Covid-19-Patienten/-Patientinnen: 2

Anzahl der auf Intensivstationen aufgenommenen Covid-Patienten/Patientinnen: 1 (davon 1 als ICU-Covid klassifiziert)Gesamtzahl der mit Covid-19 Verstorbenen: 1.180 (+0)

Personen in Quarantäne/häuslicher Isolation: 297

Personen, die Quarantäne/häusliche Isolation beendet haben: 136.794

Personen betroffen von verordneter Quarantäne/häuslicher Isolation: 137.091

Geheilte Personen insgesamt: 73.669 (+11)

Positiv getestete Mitarbeiter und Mitarbeiterinnen des Sanitätsbetriebes: 2.043, davon 1.681 geheilt. Positiv getestete Basis-, Kinderbasisärzte und Bereitschaftsärzte: 58, davon 46 geheilt. (Stand: 13.05.2021)

(PAS)

