[covid-bz](../../../README.md) / [maps](../../README.md) / [2021-04](../../2021-04/README.md) / [18](../18)
# Sonntag, 18. April 2021
 
## Die Lage in den Gemeinden
 
Heute (18.04.) wurden +43 Positive mit Wohnsitz in Südtirol mitgeteilt, in 21 verschiedene Gemeinden (von 116). In 45 Gemeinden (gleich viele wie gestern) gibt es schon seit mindestens 7 Tagen keinen neuen Fall mehr, in 22 davon (gleich viele wie gestern) sogar schon seit 21 Tagen oder mehr.

Bemerkenswerte (im negative Sinn) Gemeinden sind:
- **Gsies** (2.320 Einw.): Heute = +1, vorhergehende 6 Tage = +9, vorhergehende 14 Tage = +13, 7-Tage-Inzidenz = 400/100.000
- **Truden** (1.040 Einw.): Heute = +1, vorhergehende 6 Tage = +3, vorhergehende 14 Tage = +5, 7-Tage-Inzidenz = 400/100.000
 
Weitere nennenswerte Gemeinden (im negativen Sinn) sind: **Aldein**, **Tscherms** und **Laurein**.

Folgende 17 Gemeinden (1 mehr als gestern: **Stilfs**) könnte man als **Covid-Free** betrachten, denn sie haben schon seit 21 Tagen keinen Fall mehr gehabt, niemand befindet sich in Quarantäne oder Isolation und die SABES zählt zur Zeit keinen "aktiv Positiven".
- Marling (2.791 Einwohner)
- Moos i.P. (2.086)
- Schluderns (1.838)
- Montan (1.701)
- Corvara (1.378)
- Riffian (1.362)
- Schnals (1.228)
- **Stilfs (1.141)**
- Tiers (1007)
- Taufers im Münstertal (969)
- Vöran (959)
- Glurns (900)
- Martell (841)
- Kurtinig a.d.W. (665)
- Prags (655)
- Proveis (265)
- Waidbruck (195)


Weitere Infos:

- [PDF, Daten und Text](../18) - in tedesco e italiano, se non indicato diversamente
  - [tägliche Pressemitteilung](20210418.txt) - solo tedesco
  - Karten - Cartine con dettaglio comunale
    - **NEW** [**Covid-freie** Gemeinden](CovidCartinaCovidFree-2021-04-18.pdf)
    - [Cartina con evidenziati i comuni con almeno un nuovo caso](CovidCartinaPositiviPcrAgOggi-2021-04-18.pdf)
    - [Summe der PCR- und AG-Positiven der letzten 7 Tagen](CovidCartinaPositiviPcrAgUltimi7giorni-2021-04-18.pdf)
    - [Summe der PCR- **und AG**-Positiven der letzten **21** Tagen](CovidCartinaPositiviPcrAgUltimi21giorni-2021-04-18.pdf)
    - [**Anstieg bzw. Rückgang** der PCR- und AG-Positiven der letzten 7 Tagen im Vergleich zu den letzten 21 Tagen](CovidCartinaAccelerazione7su21giorniPositiviPcrAgUltimi-2021-04-18.pdf)
    - [Persone trovate positive con test PCR rispetto tutti i positivi (PCR e AG). Ultimi 21 giorni](CovidCartinaPositivi21PctPcr-2021-04-18.pdf)
    - [Karte mit aktueller Quarantäne](CovidCartinaQuarantena-2021-04-18.pdf)
    - [Residenti messi in quarantena negli ultimi 7 giorni](CovidCartinaQuarantenaMandatiUltimi7giorni-2021-04-18.pdf)
    - [Persone con almento un test positivo (PCR o AG), da 1° ottobre in poi](CovidCartinaPositiviTotaleWelle2-2021-04-18.pdf)
  - [weiteres Material](../18)
 
 
## Seniorenheime
 
Für Details siehe 
- [Liste der suspekten Heime](Traueranzeigen-Seniorenheime-Hotspots-Welle2-20210418.txt)
- [Grafiken](Traueranzeigen-SeniorenheimeCumuliert_2020-05-01_to_2021-04-15.pdf)
 
Il grafici nel pdf, mostrano la curva cumulata degli annunci funebri che hanno un riferimento ad una casa di riposo, a partire dal primo di maggio 2020 (ovvero dopo la strage di primavera).

Le diverse rette colorate rappresentano l'andamento medio lineare durante un particolare periodo.

In basso a destra di ciascun grafico si legge il numero medio di decessi settimanali in ciascuno di questi periodi.

Osservando il grafico in alto a sinistra nel pdf, si vede che attualmente il numero medio di decessi settinamanli è quasi uguale come in estate 2020. Sembra una buona notizia, ma in realtà oltre il 10% di tutti gli ospiti sono deceduti (oltre la metà per il virus) e in buona parte questi, da novembre in poi, non sono stati "sostituiti".

Questo significa che il numero di morti attuale è ancora leggermente eccessivo. Forse dovuto al Long-Covid. Ad occhio si può stimare che per ciascun decesso per Covid corrispondano uno o più ospiti sopravvissuti all'infezione ma che subiscono il Long-Covid.
 
# Links zu anderen Südtirolbezogene Webseiten
 
- Dashboard von [Markus Falk](https://www.markusfalk.com/)
  - [Version 1](https://www.markusfalk.com/dashboard/#lang=de&dset=ST&reg=all&tab=PRED&from=2020-08-11&to=true)
  - [Version 2](https://www.markusfalk.com/dashboard-beta/#lang=de&dset=ST_ALL&reg=all&tab=PRED&from=2020-08-11&to=true)
- Tabellen von [Moritz Mair](https://moritzmair.info/)
  - [Impfzahlen in Südtirol nach Alter](https://covvac.moritzmair.info/?tab=1)
- Ivan Sieder
  - [Interaktive Karten](https://map.corona-bz.simedia.cloud/)
- Zivilschutz Südtirol
  - Ohne Gemeindedetails
    - [Grafiken](http://www.provinz.bz.it/sicherheit-zivilschutz/zivilschutz/aktuelle-daten-zum-coronavirus.asp)
    - [dati provinciali con serie storica a partire da 15 marzo 2020](https://afbs.provinz.bz.it/upload/coronavirus/Corona.Data.Total.csv)
  - Mit Gemeindedetails
    - [Grafici e cartine con dettaglio comunale](http://www.provincia.bz.it/sicurezza-protezione-civile/protezione-civile/aktuelle-daten-zum-coronavirus-in-den-gemeinden.asp)
    - [Aktuelle Excel-Datei mit Positiven (PCR+AG)](https://afbs.provinz.bz.it/upload/coronavirus/CoronavirusPositivi.xlsx)
    - [Aktuelle Excel-Datei mit Quarantäne](https://afbs.provinz.bz.it/upload/coronavirus/CoronavirusQuarantaene.xlsx)
    - [dati comunali con serie storica a partire da 18 dicembre](https://afbs.provinz.bz.it/upload/coronavirus/Corona.Data.Detail.csv)
- [Protezione civile Italia](https://github.com/pcm-dpc/COVID-19/blob/master/README.md)
  - diffusione del Virus
    - [dati (CSV)](https://github.com/pcm-dpc/COVID-19/tree/master/dati-regioni)
    - [dati (JSON)](https://github.com/pcm-dpc/COVID-19/tree/master/dati-json/dpc-covid19-ita-regioni.json)
    - [scheda riepilogativa (PDF)](https://github.com/pcm-dpc/COVID-19/raw/master/schede-riepilogative/regioni/dpc-covid19-ita-scheda-regioni-latest.pdf)
  - [Vaccinazioni](https://github.com/italia/covid19-opendata-vaccini/tree/master/dati)
    - [dati provinciali per classi di età, serie storica (CSV)](https://github.com/italia/covid19-opendata-vaccini/blob/master/dati/somministrazioni-vaccini-latest.csv)
 
## Keep In Touch
 
- Mastodon: https://troet.cafe/@COVID_BZ_Statistics
- Matrix: https://matrix.to/#/@antoniogulino:matrix.org
