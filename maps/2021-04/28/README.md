[covid-bz](../../../README.md) / [maps](../../README.md) / [2021-04](../../2021-04/README.md) / [28](../28)
# Mittwoch, 28. April 2021
 
## Die Lage in den Gemeinden
 
Heute (28.04.) wurden
+90 Positive mit Wohnsitz in Südtirol mitgeteilt, in 35 verschiedene Gemeinden (von 116).
In 41 Gemeinden (5 mehr als [gestern](../27/README.md)) gibt es schon seit mindestens 7 Tagen keinen neuen Fall mehr,
in 17 davon (eine weniger als [gestern](../27/README.md)) sogar schon seit 21 Tagen oder mehr.
 
Hingewiesen wird auf
- **Welsberg-Taisten** (2.914 Einw.): Heute = +6, vorhergehende 6 Tage = +10, vorhergehende 14 Tage = +8, 7-Tage-Inzidenz = 500/100.000
- **Welschnofen** (1.973 Einw.): Heute = +4, vorhergehende 6 Tage = +5, vorhergehende 14 Tage = +7, 7-Tage-Inzidenz = 500/100.000  
- **Truden** im Naturpark (1.040 Einw.): Heute = +1, vorhergehende 6 Tage = +4, vorhergehende 14 Tage = +5, 7-Tage-Inzidenz = 500/100.000 
- **U.L.Frau I.W.-St.Felix** (762 Einw.): Heute = +5, vorhergehende 6 Tage = +2, vorhergehende 14 Tage = +4, 7-Tage-Inzidenz = 900/100.000

Zu nennen wären noch: **St.Leonhard i.P.** (3.558 Einw.), **St.Martin in Thurn** (1.767 Einw.) und **Gargazon** (1.726 Einw.)

<img src="CovidCartinaCovidFree-2021-04-28-1.jpg" alt="Cartina con comuni covid-free" width="50%" align="right">
 
Folgende 9 Gemeinden (1 weniger als [gestern](../27/README.md)) könnte man als **Covid-Free** betrachten, denn sie haben schon seit 21 Tagen keinen Fall mehr gehabt, niemand befindet sich in Quarantäne oder Isolation und die SABES zählt zur Zeit keinen "aktiv Positiven".
 
1. Abtei (3.505)
1. Tirol (2.450)
1. Schluderns (1.838)
1. Corvara (1.378)
1. Andrian (1.030)
1. Glurns (900)
1. Altrei (398)
1. Kuens (395)
1. Waidbruck (195)
- <del>_Stilfs (1.141)_</del>
 
Weitere Informationen: 
- [PDF, Daten und Text](../28) - in tedesco e italiano, se non indicato diversamente
  - [tägliche Pressemitteilung](20210428.txt) - solo tedesco
  - Karten - Cartine con dettaglio comunale
    - [**Covid-freie** Gemeinden](CovidCartinaCovidFree-2021-04-28.pdf)
    - [Cartina con evidenziati i comuni con almeno un nuovo caso](CovidCartinaPositiviPcrAgOggi-2021-04-28.pdf)
    - [Summe der PCR- und AG-Positiven der letzten 7 Tagen](CovidCartinaPositiviPcrAgUltimi7giorni-2021-04-28.pdf)
    - [Summe der PCR- **und AG**-Positiven der letzten **21** Tagen](CovidCartinaPositiviPcrAgUltimi21giorni-2021-04-28.pdf)
    - [**Anstieg bzw. Rückgang** der PCR- und AG-Positiven der letzten 7 Tagen im Vergleich zu den letzten 21 Tagen](CovidCartinaAccelerazione7su21giorniPositiviPcrAgUltimi-2021-04-28.pdf)
    - [Persone trovate positive con test PCR rispetto tutti i positivi (PCR e AG). Ultimi 21 giorni](CovidCartinaPositivi21PctPcr-2021-04-28.pdf)
    - [Karte mit aktueller Quarantäne](CovidCartinaQuarantena-2021-04-28.pdf)
    - [Residenti messi in quarantena negli ultimi 7 giorni](CovidCartinaQuarantenaMandatiUltimi7giorni-2021-04-28.pdf)
    - [Persone con almento un test positivo (PCR o AG), da 1° ottobre in poi](CovidCartinaPositiviTotaleWelle2-2021-04-28.pdf)
  - [weiteres Material](../28)
 
 
## Seniorenheime

Für **Seniorenheim Partschins** ist ein weiterer Todefall bekannt: der 4. in 8 Tagen. Im Seniorenheim Partschins gab es einen Ausbruch. Laut offiziellen Daten waren es 11 Heimgäste und 4 Mitarbeiter. Zwischen dem 19. und 26. April sind 4 Heimgäste gestorben. Positivität und Todesursachen ist nicht bekannt gegeben worden.

Für Details siehe 
- [Liste der suspekten Heime](Traueranzeigen-Seniorenheime-Hotspots-Welle2-20210428.txt)
- [Grafiken](Traueranzeigen-SeniorenheimeCumuliert_2020-05-01_to_2021-04-25.pdf)
 
 
# Links zu anderen Südtirolbezogene Webseiten
 
- Dashboard von [Markus Falk](https://www.markusfalk.com/)
  - [Version 1](https://www.markusfalk.com/dashboard/#lang=de&dset=ST&reg=all&tab=PRED&from=2020-08-11&to=true)
  - [Version 2](https://www.markusfalk.com/dashboard-beta/#lang=de&dset=ST_ALL&reg=all&tab=PRED&from=2020-08-11&to=true)
- Tabellen von [Moritz Mair](https://moritzmair.info/)
  - [Impfzahlen in Südtirol nach Alter](https://covvac.moritzmair.info/?tab=1)
- Ivan Sieder
  - [Interaktive Karten](https://map.corona-bz.simedia.cloud/)
- Zivilschutz Südtirol
  - Ohne Gemeindedetails
    - [Grafiken](http://www.provinz.bz.it/sicherheit-zivilschutz/zivilschutz/aktuelle-daten-zum-coronavirus.asp)
    - [dati provinciali con serie storica a partire da 15 marzo 2020](https://afbs.provinz.bz.it/upload/coronavirus/Corona.Data.Total.csv)
  - Mit Gemeindedetails
    - [Grafici e cartine con dettaglio comunale](http://www.provincia.bz.it/sicurezza-protezione-civile/protezione-civile/aktuelle-daten-zum-coronavirus-in-den-gemeinden.asp)
    - [Aktuelle Excel-Datei mit Positiven (PCR+AG)](https://afbs.provinz.bz.it/upload/coronavirus/CoronavirusPositivi.xlsx)
    - [Aktuelle Excel-Datei mit Quarantäne](https://afbs.provinz.bz.it/upload/coronavirus/CoronavirusQuarantaene.xlsx)
    - [dati comunali con serie storica a partire da 18 dicembre](https://afbs.provinz.bz.it/upload/coronavirus/Corona.Data.Detail.csv)
- [Protezione civile Italia](https://github.com/pcm-dpc/COVID-19/blob/master/README.md)
  - diffusione del Virus
    - [dati (CSV)](https://github.com/pcm-dpc/COVID-19/tree/master/dati-regioni)
    - [dati (JSON)](https://github.com/pcm-dpc/COVID-19/tree/master/dati-json/dpc-covid19-ita-regioni.json)
    - [scheda riepilogativa (PDF)](https://github.com/pcm-dpc/COVID-19/raw/master/schede-riepilogative/regioni/dpc-covid19-ita-scheda-regioni-latest.pdf)
  - [Vaccinazioni](https://github.com/italia/covid19-opendata-vaccini/tree/master/dati)
    - [dati provinciali per classi di età, serie storica (CSV)](https://github.com/italia/covid19-opendata-vaccini/blob/master/dati/somministrazioni-vaccini-latest.csv)
 
## Keep In Touch
 
- Mastodon: https://troet.cafe/@COVID_BZ_Statistics
- Matrix: https://matrix.to/#/@antoniogulino:matrix.org
