[covid-bz](../../../README.md) / [maps](../../README.md) / [2021-04](../../2021-04/README.md) / [20](../20)
# Dienstag, 20. April 2021
 
## Die Lage in den Gemeinden
 
### Zusammenfassend
Heute (20.04.) wurden +104 Positive mit Wohnsitz in Südtirol mitgeteilt, in 33 verschiedene Gemeinden (von 116).
In 45 Gemeinden (eine weniger als gestern) gibt es schon seit mindestens 7 Tagen keinen neuen Fall mehr,
in 22 davon (gleich viele wie gestern) sogar schon seit 21 Tagen oder mehr.

Bemerkenswert ist **Laurein** (342 Einw.) mit seiner aktuellen 7-Tage-Inszidenz von 1.200 (auf 100.000), **tendenz steigend**. Wobei es aber in den letzten 7 Tage es nur 4 Fälle waren, 2 davon heute. In den 2 Wochen davor hatten sie einen einzigen Fall. Nun sind 8 in Quarantäne/Isolation.

Weitere Gemeinden, die man sich anschauen kann, sind: **Ahrntal**, **Partschins**, **Mühlbach** und **Welsberg-Taisten**


### Covid-Free? ist keine Garantie!

16 Gemeinden, eine weniger als gestern, könnte man als **Covid-Free** betrachten, denn sie haben schon seit 21 Tagen keinen Fall mehr gehabt, niemand befindet sich in Quarantäne oder Isolation und die SABES zählt zur Zeit keinen "aktiv Positiven".

Neu hinzugekommen ist die Gemeinde **Kuens** (395 Einwohner). Dafür sind **Martell** (841 Einw.) und **Marling** (2.791 Einw.), die bis gestern als Covid-Free geführt wurden, aus der Liste gestrichen worden, weil sie heute entweder einen neuen Fall hatten (Martell) oder weil jemand in Quarantäne bzw. Isolation geschickt wurde (Marling).

Zur Zeit sind also folgende Gemeinden sozusagen "Covid-Free":
- <del>_**Marling** (2.791 Einw.)_</del>
- Moos i.P. (2.086 Einwohner)
- Schluderns (1.838)
- Montan (1.701)
- Corvara (1.378)
- Riffian (1.362)
- Schnals (1.228)
- Stilfs (1.141)
- Tiers (1.007)
- Taufers im Münstertal (969)
- Vöran (959)
- Glurns (900)
- <s>_**Martell** (841 Einw.)_</s>
- Kurtinig a.d.W. (665)
- Prags (655)
- **Kuens (395)**
- Proveis (265)
- Waidbruck (195)

### Weitere Daten, Karten, ...

- [PDF, Daten und Text](../20) - in tedesco e italiano, se non indicato diversamente
  - [tägliche Pressemitteilung](20210420.txt) - solo tedesco
  - Karten - Cartine con dettaglio comunale
    - **NEW** [**Covid-freie** Gemeinden](CovidCartinaCovidFree-2021-04-20.pdf)
    - [Cartina con evidenziati i comuni con almeno un nuovo caso](CovidCartinaPositiviPcrAgOggi-2021-04-20.pdf)
    - [Summe der PCR- und AG-Positiven der letzten 7 Tagen](CovidCartinaPositiviPcrAgUltimi7giorni-2021-04-20.pdf)
    - [Summe der PCR- **und AG**-Positiven der letzten **21** Tagen](CovidCartinaPositiviPcrAgUltimi21giorni-2021-04-20.pdf)
    - [**Anstieg bzw. Rückgang** der PCR- und AG-Positiven der letzten 7 Tagen im Vergleich zu den letzten 21 Tagen](CovidCartinaAccelerazione7su21giorniPositiviPcrAgUltimi-2021-04-20.pdf)
    - [Persone trovate positive con test PCR rispetto tutti i positivi (PCR e AG). Ultimi 21 giorni](CovidCartinaPositivi21PctPcr-2021-04-20.pdf)
    - [Karte mit aktueller Quarantäne](CovidCartinaQuarantena-2021-04-20.pdf)
    - [Residenti messi in quarantena negli ultimi 7 giorni](CovidCartinaQuarantenaMandatiUltimi7giorni-2021-04-20.pdf)
    - [Persone con almento un test positivo (PCR o AG), da 1° ottobre in poi](CovidCartinaPositiviTotaleWelle2-2021-04-20.pdf)
  - [weiteres Material](../20)
 
 
## Seniorenheime
 
Für Details siehe 
- [Liste der suspekten Heime](Traueranzeigen-Seniorenheime-Hotspots-Welle2-20210420.txt)
- [Grafiken](Traueranzeigen-SeniorenheimeCumuliert_2020-05-01_to_2021-04-17.pdf)
 
 
# Links zu anderen Südtirolbezogene Webseiten
 
- Dashboard von [Markus Falk](https://www.markusfalk.com/)
  - [Version 1](https://www.markusfalk.com/dashboard/#lang=de&dset=ST&reg=all&tab=PRED&from=2020-08-11&to=true)
  - [Version 2](https://www.markusfalk.com/dashboard-beta/#lang=de&dset=ST_ALL&reg=all&tab=PRED&from=2020-08-11&to=true)
- Tabellen von [Moritz Mair](https://moritzmair.info/)
  - [Impfzahlen in Südtirol nach Alter](https://covvac.moritzmair.info/?tab=1)
- Ivan Sieder
  - [Interaktive Karten](https://map.corona-bz.simedia.cloud/)
- Zivilschutz Südtirol
  - Ohne Gemeindedetails
    - [Grafiken](http://www.provinz.bz.it/sicherheit-zivilschutz/zivilschutz/aktuelle-daten-zum-coronavirus.asp)
    - [dati provinciali con serie storica a partire da 15 marzo 2020](https://afbs.provinz.bz.it/upload/coronavirus/Corona.Data.Total.csv)
  - Mit Gemeindedetails
    - [Grafici e cartine con dettaglio comunale](http://www.provincia.bz.it/sicurezza-protezione-civile/protezione-civile/aktuelle-daten-zum-coronavirus-in-den-gemeinden.asp)
    - [Aktuelle Excel-Datei mit Positiven (PCR+AG)](https://afbs.provinz.bz.it/upload/coronavirus/CoronavirusPositivi.xlsx)
    - [Aktuelle Excel-Datei mit Quarantäne](https://afbs.provinz.bz.it/upload/coronavirus/CoronavirusQuarantaene.xlsx)
    - [dati comunali con serie storica a partire da 18 dicembre](https://afbs.provinz.bz.it/upload/coronavirus/Corona.Data.Detail.csv)
- [Protezione civile Italia](https://github.com/pcm-dpc/COVID-19/blob/master/README.md)
  - diffusione del Virus
    - [dati (CSV)](https://github.com/pcm-dpc/COVID-19/tree/master/dati-regioni)
    - [dati (JSON)](https://github.com/pcm-dpc/COVID-19/tree/master/dati-json/dpc-covid19-ita-regioni.json)
    - [scheda riepilogativa (PDF)](https://github.com/pcm-dpc/COVID-19/raw/master/schede-riepilogative/regioni/dpc-covid19-ita-scheda-regioni-latest.pdf)
  - [Vaccinazioni](https://github.com/italia/covid19-opendata-vaccini/tree/master/dati)
    - [dati provinciali per classi di età, serie storica (CSV)](https://github.com/italia/covid19-opendata-vaccini/blob/master/dati/somministrazioni-vaccini-latest.csv)
 
## Keep In Touch
 
- Mastodon: https://troet.cafe/@COVID_BZ_Statistics
- Matrix: https://matrix.to/#/@antoniogulino:matrix.org
