[covid-bz](../../../README.md) / [maps](../../README.md) / [2021-04](../../2021-04/README.md) / [22](../22)
# Donnerstag, 22. April 2021
 
## Die Lage in den Gemeinden
 
### Zusammenfassend 
Heute (22.04.) wurden +109 Positive mit Wohnsitz in Südtirol mitgeteilt, in 40 verschiedene Gemeinden (von 116). In 38 Gemeinden (eine weniger als gestern) gibt es schon seit mindestens 7 Tagen keinen neuen Fall mehr, in 18 davon (zwei weniger als gestern) sogar schon seit 21 Tagen oder mehr.

Bemerkenswert sind: 
- **Laurein** (342 Einw.): Heute = +2, vorhergehende 6 Tage = +5, vorhergehende 14 Tage = +1, 7-Tage-Inzidenz = 2.000/100.000
- **Plaus** (724 Einw.): Heute = +1, vorhergehende 6 Tage = +2, vorhergehende 14 Tage = +2, 7-Tage-Inzidenz = 400/100.000

Nennenswert sind auch **Kastelbell-Tschars**, **Percha** und **Ulten** (mittlere/hohe Inzidenz, und steigend) und **Mühlbach** (noch mittelhohe Inzidenz, aber fallend)
 
### Covid-Free? und da waren's nur noch zehn
Folgende 10 Gemeinden (4 weniger als gestern) könnte man als **Covid-Free** betrachten, denn sie haben schon seit 21 Tagen keinen Fall mehr gehabt, niemand befindet sich in Quarantäne oder Isolation und die SABES zählt zur Zeit keinen "aktiv Positiven".
- Moos i.P. (2086)
- Schluderns (1838)
- Montan (1701)
- Corvara (1378)
- Riffian (1362)
- Stilfs (1141)
- Tiers (1007)
- <del>_Taufers im Münstertal (969)_</del> 1 neuer Fall
- Glurns (900)
- <del>_Kurtinig a.d.W. (665)_</del> 1 neuer Fall
- <del>_Prags (655)_</del> 1 neuer Fall, +4 in Quarantäne
- Kuens (395)
- <del>_Proveis (265)_</del>  +2 in Quarantäne
- Waidbruck (195)

### Weitere Informationen

- [PDF, Daten und Text](../22) - in tedesco e italiano, se non indicato diversamente
  - [tägliche Pressemitteilung](20210422.txt) - solo tedesco
  - Karten - Cartine con dettaglio comunale
    - **NEW** [**Covid-freie** Gemeinden](CovidCartinaCovidFree-2021-04-22.pdf)
    - [Cartina con evidenziati i comuni con almeno un nuovo caso](CovidCartinaPositiviPcrAgOggi-2021-04-22.pdf)
    - [Summe der PCR- und AG-Positiven der letzten 7 Tagen](CovidCartinaPositiviPcrAgUltimi7giorni-2021-04-22.pdf)
    - [Summe der PCR- **und AG**-Positiven der letzten **21** Tagen](CovidCartinaPositiviPcrAgUltimi21giorni-2021-04-22.pdf)
    - [**Anstieg bzw. Rückgang** der PCR- und AG-Positiven der letzten 7 Tagen im Vergleich zu den letzten 21 Tagen](CovidCartinaAccelerazione7su21giorniPositiviPcrAgUltimi-2021-04-22.pdf)
    - [Persone trovate positive con test PCR rispetto tutti i positivi (PCR e AG). Ultimi 21 giorni](CovidCartinaPositivi21PctPcr-2021-04-22.pdf)
    - [Karte mit aktueller Quarantäne](CovidCartinaQuarantena-2021-04-22.pdf)
    - [Residenti messi in quarantena negli ultimi 7 giorni](CovidCartinaQuarantenaMandatiUltimi7giorni-2021-04-22.pdf)
    - [Persone con almento un test positivo (PCR o AG), da 1° ottobre in poi](CovidCartinaPositiviTotaleWelle2-2021-04-22.pdf)
  - [weiteres Material](../22)
 
 
## Seniorenheime

Im Seniorenheim Partschins (50 Betten) gab es einen Ausbruch, 3 Frauen sind innerhalb von 3 Tagen gestorben.

Für Details über Seniorenheime im Allgemeinen, siehe 
- [Liste der suspekten Heime](Traueranzeigen-Seniorenheime-Hotspots-Welle2-20210422.txt)
- [Grafiken](Traueranzeigen-SeniorenheimeCumuliert_2020-05-01_to_2021-04-19.pdf)
 
 
# Links zu anderen Südtirolbezogene Webseiten
 
- Dashboard von [Markus Falk](https://www.markusfalk.com/)
  - [Version 1](https://www.markusfalk.com/dashboard/#lang=de&dset=ST&reg=all&tab=PRED&from=2020-08-11&to=true)
  - [Version 2](https://www.markusfalk.com/dashboard-beta/#lang=de&dset=ST_ALL&reg=all&tab=PRED&from=2020-08-11&to=true)
- Tabellen von [Moritz Mair](https://moritzmair.info/)
  - [Impfzahlen in Südtirol nach Alter](https://covvac.moritzmair.info/?tab=1)
- Ivan Sieder
  - [Interaktive Karten](https://map.corona-bz.simedia.cloud/)
- Zivilschutz Südtirol
  - Ohne Gemeindedetails
    - [Grafiken](http://www.provinz.bz.it/sicherheit-zivilschutz/zivilschutz/aktuelle-daten-zum-coronavirus.asp)
    - [dati provinciali con serie storica a partire da 15 marzo 2020](https://afbs.provinz.bz.it/upload/coronavirus/Corona.Data.Total.csv)
  - Mit Gemeindedetails
    - [Grafici e cartine con dettaglio comunale](http://www.provincia.bz.it/sicurezza-protezione-civile/protezione-civile/aktuelle-daten-zum-coronavirus-in-den-gemeinden.asp)
    - [Aktuelle Excel-Datei mit Positiven (PCR+AG)](https://afbs.provinz.bz.it/upload/coronavirus/CoronavirusPositivi.xlsx)
    - [Aktuelle Excel-Datei mit Quarantäne](https://afbs.provinz.bz.it/upload/coronavirus/CoronavirusQuarantaene.xlsx)
    - [dati comunali con serie storica a partire da 18 dicembre](https://afbs.provinz.bz.it/upload/coronavirus/Corona.Data.Detail.csv)
- [Protezione civile Italia](https://github.com/pcm-dpc/COVID-19/blob/master/README.md)
  - diffusione del Virus
    - [dati (CSV)](https://github.com/pcm-dpc/COVID-19/tree/master/dati-regioni)
    - [dati (JSON)](https://github.com/pcm-dpc/COVID-19/tree/master/dati-json/dpc-covid19-ita-regioni.json)
    - [scheda riepilogativa (PDF)](https://github.com/pcm-dpc/COVID-19/raw/master/schede-riepilogative/regioni/dpc-covid19-ita-scheda-regioni-latest.pdf)
  - [Vaccinazioni](https://github.com/italia/covid19-opendata-vaccini/tree/master/dati)
    - [dati provinciali per classi di età, serie storica (CSV)](https://github.com/italia/covid19-opendata-vaccini/blob/master/dati/somministrazioni-vaccini-latest.csv)
 
## Keep In Touch
 
- Mastodon: https://troet.cafe/@COVID_BZ_Statistics
- Matrix: https://matrix.to/#/@antoniogulino:matrix.org
