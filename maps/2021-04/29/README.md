[covid-bz](../../../README.md) / [maps](../../README.md) / [2021-04](../../2021-04/README.md) / [29](../29)
# Donnerstag, 29. April 2021

## Zu den Nasenflügeltest in den Schulen 
Bisher wurden in ingesamt 536 Schulen (Grund-, Mittel-, Ober- und Landesberufsschulen) 266.483 Nasenflügeltest durchgeführt.

davon waren zwar 295 positiv (1,1 pro mille) aber
- nur 136 PCR-positiv
- 89 PCR-negativ
- 70 noch zu überprüfen

Dies bedeutet, dass die Nasenflügeltests, im Durchschnitt 3 bis 6 Falsch positive auf 10.000 "real negativen" Personen produzieren.

Wie viele "real positive" diese Test verpassen (also die "Falsch Negativen" je 100 "real Positive"), kann man aus diesen Werten nicht ausrechnen.

## Die Lage in den Gemeinden
 
Heute (29.04.) wurden
+58 Positive mit Wohnsitz in Südtirol mitgeteilt, in 26 verschiedene Gemeinden (von 116).
In 46 Gemeinden (5 mehr als [gestern](../28/README.md)) gibt es schon seit mindestens 7 Tagen keinen neuen Fall mehr,
in 17 davon (gleich viele wie [gestern](../28/README.md)) sogar schon seit 21 Tagen oder mehr.

Bemerkenswert sind heute
- **Gsies** (2.320 Einw.): Heute = +2, vorhergehende 6 Tage = +9, vorhergehende 14 Tage = +19, 7-Tage-Inzidenz = 500/100.000. Alle Gruppen von einen Kindergarten sind seit einigen Tagen zu Hause.
- **Gargazon** (1.726 Einw.): Heute = +1, vorhergehende 6 Tage = +6, vorhergehende 14 Tage = +8, 7-Tage-Inzidenz = 400/100.000"

Nenneswert sind heute auch: **Ahrntal** (6.022 Einw.), **Mühlbach** (3.151), **Welsberg-Taisten** (2.914), **Truden** im Naturpark (1.040), **U.L.Frau I.W.-St.Felix** (762) und **Laurein** (342)
 

<img src="CovidCartinaCovidFree-2021-04-29-1.jpg" alt="Cartina con comuni covid-free" width="50%" align="right">
 
Folgende 9 Gemeinden (die selben wie [gestern](../28/README.md)) könnte man als **Covid-Free** betrachten, denn sie haben schon seit 21 Tagen keinen Fall mehr gehabt, niemand befindet sich in Quarantäne oder Isolation und die SABES zählt zur Zeit keinen "aktiv Positiven".
 
1. Abtei (3.505)
1. Tirol (2.450)
1. Schluderns (1.838)
1. Corvara (1.378)
1. Andrian (1.030)
1. Glurns (900)
1. Altrei (398)
1. Kuens (395)
1. Waidbruck (195)
 
Weitere Informationen: 
- [PDF, Daten und Text](../29) - in tedesco e italiano, se non indicato diversamente
  - [tägliche Pressemitteilung](20210429.txt) - solo tedesco
  - Karten - Cartine con dettaglio comunale
    - [**Covid-freie** Gemeinden](CovidCartinaCovidFree-2021-04-29.pdf)
    - [Cartina con evidenziati i comuni con almeno un nuovo caso](CovidCartinaPositiviPcrAgOggi-2021-04-29.pdf)
    - [Summe der PCR- und AG-Positiven der letzten 7 Tagen](CovidCartinaPositiviPcrAgUltimi7giorni-2021-04-29.pdf)
    - [Summe der PCR- **und AG**-Positiven der letzten **21** Tagen](CovidCartinaPositiviPcrAgUltimi21giorni-2021-04-29.pdf)
    - [**Anstieg bzw. Rückgang** der PCR- und AG-Positiven der letzten 7 Tagen im Vergleich zu den letzten 21 Tagen](CovidCartinaAccelerazione7su21giorniPositiviPcrAgUltimi-2021-04-29.pdf)
    - [Persone trovate positive con test PCR rispetto tutti i positivi (PCR e AG). Ultimi 21 giorni](CovidCartinaPositivi21PctPcr-2021-04-29.pdf)
    - [Karte mit aktueller Quarantäne](CovidCartinaQuarantena-2021-04-29.pdf)
    - [Residenti messi in quarantena negli ultimi 7 giorni](CovidCartinaQuarantenaMandatiUltimi7giorni-2021-04-29.pdf)
    - [Persone con almento un test positivo (PCR o AG), da 1° ottobre in poi](CovidCartinaPositiviTotaleWelle2-2021-04-29.pdf)
  - [weiteres Material](../29)
 
 
## Seniorenheime
 
Für Details siehe 
- [Liste der suspekten Heime](Traueranzeigen-Seniorenheime-Hotspots-Welle2-20210429.txt)
- [Grafiken](Traueranzeigen-SeniorenheimeCumuliert_2020-05-01_to_2021-04-26.pdf)
 
 
# Links zu anderen Südtirolbezogene Webseiten
 
- Dashboard von [Markus Falk](https://www.markusfalk.com/)
  - [Version 1](https://www.markusfalk.com/dashboard/#lang=de&dset=ST&reg=all&tab=PRED&from=2020-08-11&to=true)
  - [Version 2](https://www.markusfalk.com/dashboard-beta/#lang=de&dset=ST_ALL&reg=all&tab=PRED&from=2020-08-11&to=true)
- Tabellen von [Moritz Mair](https://moritzmair.info/)
  - [Impfzahlen in Südtirol nach Alter](https://covvac.moritzmair.info/?tab=1)
- Ivan Sieder
  - [Interaktive Karten](https://map.corona-bz.simedia.cloud/)
- Zivilschutz Südtirol
  - Ohne Gemeindedetails
    - [Grafiken](http://www.provinz.bz.it/sicherheit-zivilschutz/zivilschutz/aktuelle-daten-zum-coronavirus.asp)
    - [dati provinciali con serie storica a partire da 15 marzo 2020](https://afbs.provinz.bz.it/upload/coronavirus/Corona.Data.Total.csv)
  - Mit Gemeindedetails
    - [Grafici e cartine con dettaglio comunale](http://www.provincia.bz.it/sicurezza-protezione-civile/protezione-civile/aktuelle-daten-zum-coronavirus-in-den-gemeinden.asp)
    - [Aktuelle Excel-Datei mit Positiven (PCR+AG)](https://afbs.provinz.bz.it/upload/coronavirus/CoronavirusPositivi.xlsx)
    - [Aktuelle Excel-Datei mit Quarantäne](https://afbs.provinz.bz.it/upload/coronavirus/CoronavirusQuarantaene.xlsx)
    - [dati comunali con serie storica a partire da 18 dicembre](https://afbs.provinz.bz.it/upload/coronavirus/Corona.Data.Detail.csv)
- [Protezione civile Italia](https://github.com/pcm-dpc/COVID-19/blob/master/README.md)
  - diffusione del Virus
    - [dati (CSV)](https://github.com/pcm-dpc/COVID-19/tree/master/dati-regioni)
    - [dati (JSON)](https://github.com/pcm-dpc/COVID-19/tree/master/dati-json/dpc-covid19-ita-regioni.json)
    - [scheda riepilogativa (PDF)](https://github.com/pcm-dpc/COVID-19/raw/master/schede-riepilogative/regioni/dpc-covid19-ita-scheda-regioni-latest.pdf)
  - [Vaccinazioni](https://github.com/italia/covid19-opendata-vaccini/tree/master/dati)
    - [dati provinciali per classi di età, serie storica (CSV)](https://github.com/italia/covid19-opendata-vaccini/blob/master/dati/somministrazioni-vaccini-latest.csv)
 
## Keep In Touch
 
- Mastodon: https://troet.cafe/@COVID_BZ_Statistics
- Matrix: https://matrix.to/#/@antoniogulino:matrix.org
