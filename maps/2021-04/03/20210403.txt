https://www.sabes.it/de/news.asp?aktuelles_action=4&aktuelles_article_id=654596

Coronavirus – Mitteilung des Sanitätsbetriebes | 03.04.2021 | 11:39

In den letzten 24 Stunden wurden 1.164 PCR-Tests untersucht und dabei 102 Neuinfektionen festgestellt. Zusätzlich gab es 43 positive Antigentests.

Foto: 123rf

Bisher (03. April) wurden insgesamt 532.737 Abstriche untersucht, die von 205.646 Personen stammen.

Die Zahlen im Überblick:

PCR-Abstriche:

Untersuchte Abstriche gestern (02. April): 1.164

Mittels PCR-Test neu positiv getestete Personen: 102

Gesamtzahl der mittels PCR-Test positiv getesteten Personen: 45.829

Gesamtzahl der untersuchten Abstriche: 532.737

Gesamtzahl der mit Abstrichen getesteten Personen: 205.646 (+292)

Antigentests:

Gesamtzahl der durchgeführten Antigentests:  1.152.357

Gesamtzahl der positiven Antigentests: 24.589

Durchgeführte Antigentests gestern: 8.288

Mittels Antigentest neu positiv getestete Personen: 43

Weitere Daten:

Auf Normalstationen im Krankenhaus untergebrachte Covid-19-Patienten/-Patientinnen: 69

In Privatkliniken untergebrachte Covid-19-Patienten/-Patientinnen (post-akut bzw. aus Seniorenwohnheimen übernommen): 61

In Gossensaß und Sarns untergebrachte Covid-19-Patienten/-Patientinnen: 75 (58 in Gossensaß, 17 in Sarns)

Anzahl der auf Intensivstationen aufgenommenen Covid-Patienten/Patientinnen: 21 (davon 16 als ICU-Covid klassifiziert, kein/e Covid-Patientin/Patient in Intensivbetreuung Ausland)

Gesamtzahl der mit Covid-19 Verstorbenen: 1.135 (+2)

Personen in Quarantäne/häuslicher Isolation: 3.438

Personen, die Quarantäne/häusliche Isolation beendet haben: 125.502

Personen betroffen von verordneter Quarantäne/häuslicher Isolation: 128.940

Geheilte Personen insgesamt: 67.707 (+167)

Positiv getestete Mitarbeiter und Mitarbeiterinnen des Sanitätsbetriebes: 2.013, davon 1.618 geheilt. Positiv getestete Basis-, Kinderbasisärzte und Bereitschaftsärzte: 58, davon 44 geheilt. (Stand: 17.03.2021)

(LR)

