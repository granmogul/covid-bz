https://www.sabes.it/de/news.asp?aktuelles_action=4&aktuelles_article_id=654996

Coronavirus – Mitteilung des Sanitätsbetriebes | 17.04.2021 | 11:23

In den letzten 24 Stunden wurden 1.258 PCR-Tests untersucht und dabei 60 Neuinfektionen festgestellt. Zusätzlich gab es 38 positive Antigentests.

foto: 123rf

Bisher (17. April) wurden insgesamt 544.743 Abstriche untersucht, die von 208.612 Personen stammen.

Die Zahlen im Überblick:

PCR-Abstriche:

Untersuchte Abstriche gestern (16. April): 1.258

Mittels PCR-Test neu positiv getestete Personen 60

Gesamtzahl der mittels PCR-Test positiv getesteten Personen: 46.503

Gesamtzahl der untersuchten Abstriche: 544.743

Gesamtzahl der mit Abstrichen getesteten Personen: 208.612 (+310)

Antigentests:

Gesamtzahl der durchgeführten Antigentests:  1.255.095

Gesamtzahl der positiven Antigentests: 25.159

Durchgeführte Antigentests gestern: 8.322

Mittels Antigentest neu positiv getestete Personen: 38

Weitere Daten:

Auf Normalstationen im Krankenhaus untergebrachte Covid-19-Patienten/-Patientinnen: 60

In Privatkliniken untergebrachte Covid-19-Patienten/-Patientinnen (post-akut bzw. aus Seniorenwohnheimen übernommen): 54

In Gossensaß und Sarns untergebrachte Covid-19-Patienten/-Patientinnen: 6 (5 in Gossensaß, 1 in Sarns)

Anzahl der auf Intensivstationen aufgenommenen Covid-Patienten/Patientinnen: 15 (davon 12 als ICU-Covid klassifiziert)

Gesamtzahl der mit Covid-19 Verstorbenen: 1.155 (+0)

Personen in Quarantäne/häuslicher Isolation: 2.881

Personen, die Quarantäne/häusliche Isolation beendet haben: 128.263

Personen betroffen von verordneter Quarantäne/häuslicher Isolation: 131.144

Geheilte Personen insgesamt: 69.230 (+123)

Positiv getestete Mitarbeiter und Mitarbeiterinnen des Sanitätsbetriebes: 2.016, davon 1.632 geheilt. Positiv getestete Basis-, Kinderbasisärzte und Bereitschaftsärzte: 60, davon 45 geheilt. (Stand: 11.04.2021)

(TDB)

