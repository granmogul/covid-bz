[covid-bz](../../../README.md) / [maps](../../README.md) / [2021-04](../../2021-04/README.md) / [17](../17)
# Samstag, 17. April 2021
 
## Die Lage in den Gemeinden
 
Heute (17.04.) wurden
+82 Positive mit Wohnsitz in Südtirol mitgeteilt, in 33 verschiedene Gemeinden (von 116).
In 45 Gemeinden (1 mehr als gestern) gibt es schon seit mindestens 7 Tagen keinen neuen Fall mehr,
in 22 davon (1 mehr als gestern) sogar schon seit 21 Tagen oder mehr.

Nenneswerte Gemeinden sind heute: **Ahrntal**, **Sand in Taufers**, **Gsies**, **Tscherms**, **Percha**, **St.Pankraz** und **Truden**.

Folgende 16 Gemeinden (4 mehr als gestern) könnte man als **Covid-Free** betrachten. Es sind die selben Gemeinden wie gestern, plus Marling, Moos in Passeier, Montan, Schnals.

- **Marling (2.791 Einw.)**
- **Moos i.P. (2.086)**
- Schluderns (1.838)
- **Montan (1.701)**
- Corvara (1.378)
- Riffian (1.362)
- **Schnals (1.228)**
- Tiers (1.007)
- Taufers im Münstertal (969)
- Vöran (959)
- Glurns (900)
- Martell (841)
- Kurtinig a.d.W. (665)
- Prags (655)
- Proveis (265)
- Waidbruck (195)

Weitere Infos:
 
- [PDF, Daten und Text](../17) - in tedesco e italiano, se non indicato diversamente
  - [tägliche Pressemitteilung](20210417.txt) - solo tedesco
  - Karten - Cartine con dettaglio comunale
    - **NEW** [**Covid-freie** Gemeinden](CovidCartinaCovidFree-2021-04-17.pdf)
    - [Cartina con evidenziati i comuni con almeno un nuovo caso](CovidCartinaPositiviPcrAgOggi-2021-04-17.pdf)
    - [Summe der PCR- und AG-Positiven der letzten 7 Tagen](CovidCartinaPositiviPcrAgUltimi7giorni-2021-04-17.pdf)
    - [Summe der PCR- **und AG**-Positiven der letzten **21** Tagen](CovidCartinaPositiviPcrAgUltimi21giorni-2021-04-17.pdf)
    - [**Anstieg bzw. Rückgang** der PCR- und AG-Positiven der letzten 7 Tagen im Vergleich zu den letzten 21 Tagen](CovidCartinaAccelerazione7su21giorniPositiviPcrAgUltimi-2021-04-17.pdf)
    - [Persone trovate positive con test PCR rispetto tutti i positivi (PCR e AG). Ultimi 21 giorni](CovidCartinaPositivi21PctPcr-2021-04-17.pdf)
    - [Karte mit aktueller Quarantäne](CovidCartinaQuarantena-2021-04-17.pdf)
    - [Residenti messi in quarantena negli ultimi 7 giorni](CovidCartinaQuarantenaMandatiUltimi7giorni-2021-04-17.pdf)
    - [Persone con almento un test positivo (PCR o AG), da 1° ottobre in poi](CovidCartinaPositiviTotaleWelle2-2021-04-17.pdf)
  - [weiteres Material](../17)
 
 
## Seniorenheime
 
Für Details siehe 
- [Liste der suspekten Heime](Traueranzeigen-Seniorenheime-Hotspots-Welle2-20210417.txt)
- [Grafiken](Traueranzeigen-SeniorenheimeCumuliert_2020-05-01_to_2021-04-14.pdf)
 
 
# Links zu anderen Südtirolbezogene Webseiten
 
- Dashboard von [Markus Falk](https://www.markusfalk.com/)
  - [Version 1](https://www.markusfalk.com/dashboard/#lang=de&dset=ST&reg=all&tab=PRED&from=2020-08-11&to=true)
  - [Version 2](https://www.markusfalk.com/dashboard-beta/#lang=de&dset=ST_ALL&reg=all&tab=PRED&from=2020-08-11&to=true)
- Tabellen von [Moritz Mair](https://moritzmair.info/)
  - [Impfzahlen in Südtirol nach Alter](https://covvac.moritzmair.info/?tab=1)
- Ivan Sieder
  - [Interaktive Karten](https://map.corona-bz.simedia.cloud/)
- Zivilschutz Südtirol
  - Ohne Gemeindedetails
    - [Grafiken](http://www.provinz.bz.it/sicherheit-zivilschutz/zivilschutz/aktuelle-daten-zum-coronavirus.asp)
    - [dati provinciali con serie storica a partire da 15 marzo 2020](https://afbs.provinz.bz.it/upload/coronavirus/Corona.Data.Total.csv)
  - Mit Gemeindedetails
    - [Grafici e cartine con dettaglio comunale](http://www.provincia.bz.it/sicurezza-protezione-civile/protezione-civile/aktuelle-daten-zum-coronavirus-in-den-gemeinden.asp)
    - [Aktuelle Excel-Datei mit Positiven (PCR+AG)](https://afbs.provinz.bz.it/upload/coronavirus/CoronavirusPositivi.xlsx)
    - [Aktuelle Excel-Datei mit Quarantäne](https://afbs.provinz.bz.it/upload/coronavirus/CoronavirusQuarantaene.xlsx)
    - [dati comunali con serie storica a partire da 18 dicembre](https://afbs.provinz.bz.it/upload/coronavirus/Corona.Data.Detail.csv)
- [Protezione civile Italia](https://github.com/pcm-dpc/COVID-19/blob/master/README.md)
  - diffusione del Virus
    - [dati (CSV)](https://github.com/pcm-dpc/COVID-19/tree/master/dati-regioni)
    - [dati (JSON)](https://github.com/pcm-dpc/COVID-19/tree/master/dati-json/dpc-covid19-ita-regioni.json)
    - [scheda riepilogativa (PDF)](https://github.com/pcm-dpc/COVID-19/raw/master/schede-riepilogative/regioni/dpc-covid19-ita-scheda-regioni-latest.pdf)
  - [Vaccinazioni](https://github.com/italia/covid19-opendata-vaccini/tree/master/dati)
    - [dati provinciali per classi di età, serie storica (CSV)](https://github.com/italia/covid19-opendata-vaccini/blob/master/dati/somministrazioni-vaccini-latest.csv)
 
## Keep In Touch
 
- Mastodon: https://troet.cafe/@COVID_BZ_Statistics
- Matrix: https://matrix.to/#/@antoniogulino:matrix.org
