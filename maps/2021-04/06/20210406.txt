https://www.sabes.it/de/news.asp?aktuelles_action=4&aktuelles_article_id=654609

Coronavirus – Mitteilung des Sanitätsbetriebes | 06.04.2021 | 10:57

In den letzten 24 Stunden wurden 446 PCR-Tests untersucht und dabei 22 Neuinfektionen festgestellt. Zusätzlich gab es 13 positive Antigentests.

Coronavirus: 22 Neuinfektionen durch PCR-Test und 13 positive Antigentests

Bisher (06. April) wurden insgesamt 534.099 Abstriche untersucht, die von 206.008 Personen stammen.

Die Zahlen im Überblick:

PCR-Abstriche:

Untersuchte Abstriche gestern (05. April): 446

Mittels PCR-Test neu positiv getestete Personen: 22

Gesamtzahl der mittels PCR-Test positiv getesteten Personen: 45.925

Gesamtzahl der untersuchten Abstriche: 534.099

Gesamtzahl der mit Abstrichen getesteten Personen: 206.008 (+112)

Antigentests:

Gesamtzahl der durchgeführten Antigentests:  1.158.929

Gesamtzahl der positiven Antigentests: 24.657

Durchgeführte Antigentests gestern: 2.089



Mittels Antigentest neu positiv getestete Personen: 13

Weitere Daten:

Auf Normalstationen im Krankenhaus untergebrachte Covid-19-Patienten/-Patientinnen: 85

In Privatkliniken untergebrachte Covid-19-Patienten/-Patientinnen (post-akut bzw. aus Seniorenwohnheimen übernommen): 61

In Gossensaß und Sarns untergebrachte Covid-19-Patienten/-Patientinnen: 70 (55 in Gossensaß, 15 in Sarns)

Anzahl der auf Intensivstationen aufgenommenen Covid-Patienten/Patientinnen: 23 (davon 17 als ICU-Covid klassifiziert, kein/e Covid-Patientin/Patient in Intensivbetreuung Ausland)

Gesamtzahl der mit Covid-19 Verstorbenen: 1.146 (+5)

Personen in Quarantäne/häuslicher Isolation: 3.207

Personen, die Quarantäne/häusliche Isolation beendet haben: 126.147

Personen betroffen von verordneter Quarantäne/häuslicher Isolation: 129.354

Geheilte Personen insgesamt: 67.973 (+86)

Positiv getestete Mitarbeiter und Mitarbeiterinnen des Sanitätsbetriebes: 2.016, davon 1.624 geheilt. Positiv getestete Basis-, Kinderbasisärzte und Bereitschaftsärzte: 59, davon 45 geheilt. (Stand: 04.04.2021)

(MK)

