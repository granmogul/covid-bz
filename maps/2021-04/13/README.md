[covid-bz](../../../README.md) / [maps](../../README.md) / [2021-04](../../2021-04/README.md) / [13](../13)
# Dienstag, 13. April 2021
 
## Gemeinden

Heute (13.4.) wurden +144 Positive gemeldet, in 41 verschiedene Gemeinden (von 116). In 40 Gemeinden (4 mehr als gestern) wurden in den letzten 7 Tagen keine Fälle mehr gemeldet. In 19 davon (eine mehr als gestern) schon seit mindestens 21 Tagen.

Bemerkenswerte Gemeinde ist **Gsies** (2.320 Einwohner): heute +6 Fälle, in den vorhergehenden 6 Tagen ebenfalls +6 Fälle und in den vorhergehenden 14 Tagen +8 Fälle. Also **Tendenz steigend**.

Nennenswert sind auch **Terenten** (tendenz steigend) und die mit nicht mehr steigender Tendenz **Mühlbach**, **Pfalzen** und **Pfatten**.

## Mühlbach? Meransen!

[9 Tage alte Meldung](https://www.video33.it/2021/04/03/coronavirus-focolaio-a-maranza/) sagt: 

_Video 33_ - **Coronavirus: focolaio a Maranza**
> Preoccupazione nella zona di Rio Pusteria per un focolaio del virus Covid 19 emerso in questi giorni in località Maranza. In una settimana sono stati registrati 35 contagi su un totale di 900 residenti. Quasi 130 le persone poste in quarantena, tante quante se ne registrano attualmente in centri ben più popolosi come Bressanone e Laives. Alla chiusa di Rio Pusteria è stato allestito un centro per i test rapidi che sarà attivo martedì e giovedì sera. 

## Covid-Free 

Folgende 12 Gemeinden haben schon seit 21 Tagen keinen Fall mehr gehabt, niemand befindet sich in Quarantäne oder Isolation und die SABES zählt zur Zeit keinen "aktiv Positiven".

- Schluderns (1.838)
- Corvara  (1.378)
- Riffian  (1.362)
- Tiers  (1.007)
- Taufers im Münstertal  (969)
- Vöran  (959)
- Glurns  (900)
- Martell  (841)
- Kurtinig a.d.W. (665)
- Prags  (655)
- Proveis  (265)
- Waidbruck (195)


## Weitere Infos

- [PDF, Daten und Text](../13) - in tedesco e italiano, se non indicato diversamente
  - [tägliche Pressemitteilung](20210413.txt) - solo tedesco
  - Karten - Cartine con dettaglio comunale
    - [Summe der PCR- und AG-Positiven der letzten 7 Tagen](CovidCartinaPositiviPcrAgUltimi7giorni-2021-04-13.pdf)
    - [Summe der PCR- **und AG**-Positiven der letzten **21** Tagen](CovidCartinaPositiviPcrAgUltimi21giorni-2021-04-13.pdf)
    - [**Anstieg bzw. Rückgang** der PCR- und AG-Positiven der letzten 7 Tagen im Vergleich zu den letzten 21 Tagen](CovidCartinaAccelerazione7su21giorniPositiviPcrAgUltimi-2021-04-13.pdf)
    - [Persone trovate positive con test PCR rispetto tutti i positivi (PCR e AG). Ultimi 21 giorni](CovidCartinaPositivi21PctPcr-2021-04-13.pdf)
    - [Karte mit aktueller Quarantäne](CovidCartinaQuarantena-2021-04-13.pdf)
    - [Residenti messi in quarantena negli ultimi 7 giorni](CovidCartinaQuarantenaMandatiUltimi7giorni-2021-04-13.pdf)
    - [Persone con almento un test positivo (PCR o AG), da 1° ottobre in poi](CovidCartinaPositiviTotaleWelle2-2021-04-13.pdf)
    - [Cartina con evidenziati i comuni con almeno un nuovo caso](CovidCartinaPositiviPcrAgOggi-2021-04-13.pdf)
  - [weiteres Material](../13)
 
 
## Seniorenheime
 
Für Details siehe 
- [Liste der suspekten Heime](Traueranzeigen-Seniorenheime-Hotspots-Welle2-20210413.txt)
- [Grafiken](Traueranzeigen-SeniorenheimeCumuliert_2020-05-01_to_2021-04-10.pdf)
 
 
# Links zu anderen Südtirolbezogene Webseiten
 
- Dashboard von [Markus Falk](https://www.markusfalk.com/)
  - [Version 1](https://www.markusfalk.com/dashboard/#lang=de&dset=ST&reg=all&tab=PRED&from=2020-08-11&to=true)
  - [Version 2](https://www.markusfalk.com/dashboard-beta/#lang=de&dset=ST_ALL&reg=all&tab=PRED&from=2020-08-11&to=true)
- Tabellen von [Moritz Mair](https://moritzmair.info/)
  - [Impfzahlen in Südtirol nach Alter](https://covvac.moritzmair.info/?tab=1)
- Ivan Sieder
  - [Interaktive Karten](https://map.corona-bz.simedia.cloud/)
- Zivilschutz Südtirol
  - Ohne Gemeindedetails
    - [Grafiken](http://www.provinz.bz.it/sicherheit-zivilschutz/zivilschutz/aktuelle-daten-zum-coronavirus.asp)
    - [dati provinciali con serie storica a partire da 15 marzo 2020](https://afbs.provinz.bz.it/upload/coronavirus/Corona.Data.Total.csv)
  - Mit Gemeindedetails
    - [Grafici e cartine con dettaglio comunale](http://www.provincia.bz.it/sicurezza-protezione-civile/protezione-civile/aktuelle-daten-zum-coronavirus-in-den-gemeinden.asp)
    - [Aktuelle Excel-Datei mit Positiven (PCR+AG)](https://afbs.provinz.bz.it/upload/coronavirus/CoronavirusPositivi.xlsx)
    - [Aktuelle Excel-Datei mit Quarantäne](https://afbs.provinz.bz.it/upload/coronavirus/CoronavirusQuarantaene.xlsx)
    - [dati comunali con serie storica a partire da 18 dicembre](https://afbs.provinz.bz.it/upload/coronavirus/Corona.Data.Detail.csv)
- [Protezione civile Italia](https://github.com/pcm-dpc/COVID-19/blob/master/README.md)
  - diffusione del Virus
    - [dati (CSV)](https://github.com/pcm-dpc/COVID-19/tree/master/dati-regioni)
    - [dati (JSON)](https://github.com/pcm-dpc/COVID-19/tree/master/dati-json/dpc-covid19-ita-regioni.json)
    - [scheda riepilogativa (PDF)](https://github.com/pcm-dpc/COVID-19/raw/master/schede-riepilogative/regioni/dpc-covid19-ita-scheda-regioni-latest.pdf)
  - [Vaccinazioni](https://github.com/italia/covid19-opendata-vaccini/tree/master/dati)
    - [dati provinciali per classi di età, serie storica (CSV)](https://github.com/italia/covid19-opendata-vaccini/blob/master/dati/somministrazioni-vaccini-latest.csv)
 
## Keep In Touch
 
- Mastodon: https://troet.cafe/@COVID_BZ_Statistics
- Matrix: https://matrix.to/#/@antoniogulino:matrix.org
