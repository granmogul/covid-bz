[covid-bz](../../../README.md) / [maps](../../README.md) / [2021-04](../../2021-04/README.md) / [21](../21)
# Mittwoch, 21. April 2021
 
## Die Lage in den Gemeinden
 
### Zusammenfassung

Heute (21.04.) wurden
+110 Positive mit Wohnsitz in Südtirol mitgeteilt, in 40 verschiedene Gemeinden (von 116).
In 39 Gemeinden (6 weniger als gestern) gibt es schon seit mindestens 7 Tagen keinen neuen Fall mehr,
in 20 davon (zwei weniger) sogar schon seit 21 Tagen oder mehr.
 
Bemerkenswert sind 
- **Laurein** (342 Einw.): Heute = +1, vorhergehende 6 Tage = +4, vorhergehende 14 Tage = +1, 7-Tage-Inzidenz = 1500/100.000"
- **Plaus** (724 Einw.): Heute = +1, vorhergehende 6 Tage = +1, vorhergehende 14 Tage = +3, 7-Tage-Inzidenz = 300/100.000"   

Nennenswert sind auch **Gargazon**, **Partschins** (Ausbruch im **Seniorenheim**), **Percha**, **Mühlbach**, **Ahrntal** und **Gsies**

### Covid-Free? aber ohne Garantie!

In 14 Gemeinden (2 weniger als gestern) könnte man als **Covid-Free** betrachten, denn sie haben schon seit 21 Tagen keinen Fall mehr gehabt, niemand befindet sich in Quarantäne oder Isolation und die SABES zählt zur Zeit keinen "aktiv Positiven".

- Moos i.P. (2086)
- Schluderns (1838)
- Montan (1701)
- Corvara (1378)
- Riffian (1362)
- <del>_Schnals (1.228)_</del> (heute ein "aktiv positiver" hinzugekommen)
- Stilfs (1141)
- Tiers (1007)
- Taufers im Münstertal (969)
- <del>_Vöran (959)_</del> (heute ein Fall)
- Glurns (900)
- Kurtinig a.d.W. (665)
- Prags (655)
- Kuens (395)
- Proveis (265)
- Waidbruck (195)

### Weitere Informationen


- [PDF, Daten und Text](../21) - in tedesco e italiano, se non indicato diversamente
  - [tägliche Pressemitteilung](20210421.txt) - solo tedesco
  - Karten - Cartine con dettaglio comunale
    - **NEW** [**Covid-freie** Gemeinden](CovidCartinaCovidFree-2021-04-21.pdf)
    - [Cartina con evidenziati i comuni con almeno un nuovo caso](CovidCartinaPositiviPcrAgOggi-2021-04-21.pdf)
    - [Summe der PCR- und AG-Positiven der letzten 7 Tagen](CovidCartinaPositiviPcrAgUltimi7giorni-2021-04-21.pdf)
    - [Summe der PCR- **und AG**-Positiven der letzten **21** Tagen](CovidCartinaPositiviPcrAgUltimi21giorni-2021-04-21.pdf)
    - [**Anstieg bzw. Rückgang** der PCR- und AG-Positiven der letzten 7 Tagen im Vergleich zu den letzten 21 Tagen](CovidCartinaAccelerazione7su21giorniPositiviPcrAgUltimi-2021-04-21.pdf)
    - [Persone trovate positive con test PCR rispetto tutti i positivi (PCR e AG). Ultimi 21 giorni](CovidCartinaPositivi21PctPcr-2021-04-21.pdf)
    - [Karte mit aktueller Quarantäne](CovidCartinaQuarantena-2021-04-21.pdf)
    - [Residenti messi in quarantena negli ultimi 7 giorni](CovidCartinaQuarantenaMandatiUltimi7giorni-2021-04-21.pdf)
    - [Persone con almento un test positivo (PCR o AG), da 1° ottobre in poi](CovidCartinaPositiviTotaleWelle2-2021-04-21.pdf)
  - [weiteres Material](../21)
 
 
## Seniorenheime

Nella **casa di riposo di Parcines** è in corso un **focolaio Covid**. **Una** persona è **deceduta**. **13 ospiti e 4 collaboratori** sono infetti. La casa di riposo è stata, assieme a quella di San Candido, la prima ad effettuare le vaccinazioni.

[Alto Adige, 21 aprile 2021](https://www.altoadige.it/cronaca/merano/merano-focolaio-covid-in-una-rsa-vaccinata-1.2892872)
> In una Rsa della zona di Merano - nonostante la vaccinazione sia già avvenuta - sono ora risultati positivi al Covid 13 anziani e 4 collaboratori.

> La notizia riportata dal quotidiano Dolomiten è stata confermata all'Ansa dall'Azienda sanitaria dell'Alto Adige che ribadisce però che in quasi tutti i casi hanno un decorso lieve.

> Due anziani sono stati ricoverati in ospedale, uno dei quali è deceduto ma per una grave patologia pregressa, precisa l'Asl. Non è ancora chiaro se tutte le 17 persone risultate positive sono effettivamente state vaccinate e se hanno ricevuto entrambe le dosi, visto che in Alto Adige tra il personale della case di riposo la percentuale di no vax è piuttosto elevata.  
 
[STOL, 21 aprile 2021](https://www.stol.it/artikel/chronik/wirbel-um-promi-frau-und-re-infektion-in-heim)
> Derzeit läuft die Kontaktnachverfolgung in einem Burggräfler Seniorenheim, wo sich 13 komplett geimpfte Bewohner und 4 Mitarbeiter erneut mit Covid infiziert haben. Ein Senior und eine Seniorin wurden ins Meraner Spital gebracht.

> Die Frau ist gestorben, doch soll dies nicht Covid-19, sondern einer gastrointestinalen Infektion zuzuschreiben sein. Ersten Informationen zufolge könnte das Virus im Heim von einer nicht geimpften Pflegekraft verbreitet worden sein.

Für Details siehe 
- [Liste der suspekten Heime](Traueranzeigen-Seniorenheime-Hotspots-Welle2-20210421.txt)
- [Grafiken](Traueranzeigen-SeniorenheimeCumuliert_2020-05-01_to_2021-04-18.pdf)
 
 
# Links zu anderen Südtirolbezogene Webseiten
 
- Dashboard von [Markus Falk](https://www.markusfalk.com/)
  - [Version 1](https://www.markusfalk.com/dashboard/#lang=de&dset=ST&reg=all&tab=PRED&from=2020-08-11&to=true)
  - [Version 2](https://www.markusfalk.com/dashboard-beta/#lang=de&dset=ST_ALL&reg=all&tab=PRED&from=2020-08-11&to=true)
- Tabellen von [Moritz Mair](https://moritzmair.info/)
  - [Impfzahlen in Südtirol nach Alter](https://covvac.moritzmair.info/?tab=1)
- Ivan Sieder
  - [Interaktive Karten](https://map.corona-bz.simedia.cloud/)
- Zivilschutz Südtirol
  - Ohne Gemeindedetails
    - [Grafiken](http://www.provinz.bz.it/sicherheit-zivilschutz/zivilschutz/aktuelle-daten-zum-coronavirus.asp)
    - [dati provinciali con serie storica a partire da 15 marzo 2020](https://afbs.provinz.bz.it/upload/coronavirus/Corona.Data.Total.csv)
  - Mit Gemeindedetails
    - [Grafici e cartine con dettaglio comunale](http://www.provincia.bz.it/sicurezza-protezione-civile/protezione-civile/aktuelle-daten-zum-coronavirus-in-den-gemeinden.asp)
    - [Aktuelle Excel-Datei mit Positiven (PCR+AG)](https://afbs.provinz.bz.it/upload/coronavirus/CoronavirusPositivi.xlsx)
    - [Aktuelle Excel-Datei mit Quarantäne](https://afbs.provinz.bz.it/upload/coronavirus/CoronavirusQuarantaene.xlsx)
    - [dati comunali con serie storica a partire da 18 dicembre](https://afbs.provinz.bz.it/upload/coronavirus/Corona.Data.Detail.csv)
- [Protezione civile Italia](https://github.com/pcm-dpc/COVID-19/blob/master/README.md)
  - diffusione del Virus
    - [dati (CSV)](https://github.com/pcm-dpc/COVID-19/tree/master/dati-regioni)
    - [dati (JSON)](https://github.com/pcm-dpc/COVID-19/tree/master/dati-json/dpc-covid19-ita-regioni.json)
    - [scheda riepilogativa (PDF)](https://github.com/pcm-dpc/COVID-19/raw/master/schede-riepilogative/regioni/dpc-covid19-ita-scheda-regioni-latest.pdf)
  - [Vaccinazioni](https://github.com/italia/covid19-opendata-vaccini/tree/master/dati)
    - [dati provinciali per classi di età, serie storica (CSV)](https://github.com/italia/covid19-opendata-vaccini/blob/master/dati/somministrazioni-vaccini-latest.csv)
 
## Keep In Touch
 
- Mastodon: https://troet.cafe/@COVID_BZ_Statistics
- Matrix: https://matrix.to/#/@antoniogulino:matrix.org
