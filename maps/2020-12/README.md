[covid-bz](../../README.md) / [maps](../README.md) / [2020-12](../2020-12) 
# Cartine e altri grafici con i dati comunali trovati nei quotidiani file Excel e analisi dei necrologi
## Dezember 2020
- [Donnerstag, 31. Dez.](31/README.md)
- [Mittwoch, 30. Dez.](30)
- [Dienstag, 29. Dez.](29)
- [Montag, 28. Dez.](28)
- [domenica, 27  Dic.](27)
- [sabato, 26  Dic.](26), con [riassunto settimanale ospizi](26/Traueranzeigen-Seniorenheime-Hotspots-Welle2-20201226.txt)
- [venerdi, 25  Dic.](25), **NEW**: [CovidCartinaPositivi**PcrAgUltimi7giorni**-YYYY-MM-DD.pdf](25/CovidCartinaPositiviPcrAgUltimi7giorni-2020-12-25.pdf) 
- [giovedi, 24  Dic.](24)
- [mercoledi, 23  Dic.](23), compresa [cartina decessi](23/CovidCartinaDecessiWelle2-2020-12-23.pdf)
- [martedi, 22  Dic.](22)
- [lunedi, 21  Dic.](21)
- [Sonntag, 20. Dez.](20)
- [Samstag, 19. Dez.](19), mit [Karte der Todesfälle](19/CovidCartinaDecessiWelle2-2020-12-19.pdf) und [wöchentliche Zusammenfassung Seniorenheime](19/Traueranzeigen-Seniorenheime-Hotspots-Welle2-20201219.txt)
- [Freitag, 18. Dez.](18)
- [Donnerstag, 17. Dez.](17), mit [Karte der Todesfälle](17/CovidCartinaDecessiWelle2-2020-12-17.pdf)
- [Mittwoch, 16. Dez.](16)
- [Dienstag, 15. Dez.](15)
- [Montag, 14. Dez.](14)
- [domenica, 13  Dic.](13), compresa [cartina decessi](13/CovidCartinaDecessiWelle2-2020-12-13.pdf)
- [sabato, 12  Dic.](12)
- [venerdi, 11  Dic.](11)
- [giovedi, 10  Dic.](10)
- [mercoledi, 9  Dic.](09), compresa [cartina decessi](09/CovidCartinaDecessiWelle2-2020-12-09.pdf)
- [martedi, 8  Dic.](08)
- [lunedi, 7  Dic.](07)
- [Sonntag, 6. Dez.](06)
- [Samstag, 5. Dez.](05)
- [Freitag, 4. Dez.](04)
- [Donnerstag, 3. Dez.](03)
- [Mittwoch, 2. Dez.](02)
- [Dienstag, 1. Dez.](01)

## Keep In Touch

- Mastodon: https://troet.cafe/@COVID_BZ_Statistics
- Telegram: https://t.me/COVID_BZ_Statistics
