https://www.sabes.it/de/news.asp?aktuelles_action=4&aktuelles_article_id=651018

Coronavirus – Mitteilungen des Südtiroler Sanitätsbetriebes | 17.12.2020 | 11:09

In den letzten 24 Stunden wurden 1.986 Abstriche untersucht und 287 Neuinfektionen festgestellt. 190 Covid-19-Positive sind stationär aufgenommen, weitere 22 in intensivmedizinischer Betreuung. 5 Todesfälle

Foto: 123rf

Bisher (17. Dezember) wurden insgesamt 341.608 Abstriche untersucht, die von 156.988 Personen stammen.

Die Zahlen im Überblick:

PCR-Abstriche:

Untersuchte Abstriche gestern (16. Dezember): 1.986

Mittels PCR-Test neu positiv getestete Personen: 287

Gesamtzahl der mittels PCR-Test positiv getesteten Personen: 27.512

Gesamtzahl der untersuchten Abstriche: 341.608

Gesamtzahl der mit Abstrichen getesteten Personen: 156.988 (+614)

Antigentests:

Durchgeführte Antigentests gestern: 1.365

Mittels Antigentest neu positiv getestete Personen: 88

Weitere Daten:

Auf Normalstationen im Krankenhaus untergebrachte Covid-19-Patienten/-Patientinnen: 190

In Privatkliniken untergebrachte Covid-19-Patienten/-Patientinnen: 146

In Gossensaß und Sarns untergebrachte Covid-19-Patienten/-Patientinnen: 31 (23 in Gossensaß, 8 in Sarns)

Covid-19-Patientinnen und -Patienten in Intensivbetreuung: 22

Gesamtzahl der mit Covid-19 Verstorbenen: 673 (+5)

Personen in Quarantäne/häuslicher Isolation: 6.180

Personen, die Quarantäne/häusliche Isolation beendet haben: 61.256

Personen betroffen von verordneter Quarantäne/häuslicher Isolation: 67.436

Geheilte Personen: 16.127 (+224); zusätzlich 1.465 (+3) Personen, die ein unklares Testergebnis hatten und in der Folge negativ getestet wurden. Insgesamt: 17.592 (+227)

Positiv getestete Mitarbeiter und Mitarbeiterinnen des Sanitätsbetriebes: 1.218, davon 780 geheilt. Positiv getestete Basis-, Kinderbasisärzte und Bereitsschaftsärzte: 37, davon 26 geheilt. (Stand: 05.12.)

(PAS)

