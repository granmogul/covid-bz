[covid-bz](../../../README.md) / [maps](../../README.md) / [2020-12](../../2020-12/README.md) / [31](../31)
# Donnerstag, 31. Dezember 2020

- [PDF, Daten und Text](../31)

## Pustertal

Am ersten Wochenende des Jahres, Sam. 2. und Sonn. 3. Januar 2021 soll ein Massentest in #Bruneck #Prettau und #Mühlwald durchgeführt werden.
[Pressemitteilung SABES vom .12.2020](../30/20201230-MassentestBruneckAnkuendigung.txt)

Auffallend ist das oberste Pustertal. Während nordlich von Bruneck alles "ruhig" ist, 
betrachtet man die Anzahl der neuen Positiven in den letzten 7 Tagen ([PDF](CovidCartinaPositiviPcrAgUltimi7giorni-2020-12-31.pdf))

Die selbe heikle Situation gilt für das selbe Gebiet, wenn man die offiziellen Quarantäne zahlen sieht ([PDF](CovidCartinaQuarantena-2020-12-31.pdf)).

Folgende 5 Gemeinden (Toblach, Niederdorf, Prags,  Welsberg-Taisten, Gsies) haben insegsamt fast 11 Tausend Einwohner. 
85 davon (8 je 1000 Einwohner) waren in den letzten 7 Tagen positiv (#PCR oder #AG). 
In Quarantäne sind 303, also 28 je 1000 Einwohner.

In der Nachbargemeinde Innichen sind 9 von 1000 in Quarantäne (0.9%=31/3367) 
und 1 von Tausend in den letzten 7 Tagen mit Positive Testergebnisse. 
Die anderen Nachbargemeinden Olang und Rasen haben ebenfalls so niedrige Werte.


## Seniorenheime

Es sind 5 neue Traueranzeigen mit Bezug auf Seniorenheime gefunden worden:  
- Don Bosco (Bolzano)
- Ritten
- Kaltern
- Meran, Martinsbrunn. Wird jetzt als "suspekt" statt "weniger suspekt" aufgelistet.
- Mals, Martinsheim

Für mehr Details siehe eigene [Datei](Traueranzeigen-Seniorenheime-Hotspots-Welle2-20201231.txt)


## Schöne Nachrichten

Buone notizie ci accompagnano alla fine dell'anno:

A 106 anni, ospite presso la Don Bosco di Bolzano, sconfigge il Covid! 

[Alto Adige, 31 dic. 2020](https://www.altoadige.it/cronaca/bolzano/bolzano-a-106-anni-la-signora-pina-riesce-a-sconfiggere-il-covid-1.2514236)

NB: nella casa di riposo Don Bosco di Bolzano, nel giro di poco più di un mese è morto un anziano su 6. Quasi tutti dovuti al Covid.


## Keep In Touch

- Mastodon: https://troet.cafe/@COVID_BZ_Statistics
