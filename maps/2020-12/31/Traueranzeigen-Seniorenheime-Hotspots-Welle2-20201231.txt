dal 2020-10-23 a 2020-12-31 12:20                                Heim Welle2 attesoSemplice eccessoSemplice eccesso95   proba
Bolzano, Don Bosco                                 Bolzano, Don Bosco     31          2.654              28        25 0.00000
Ritten                                                         Ritten     19          2.654              16        13 0.00000
Meran, Villa Eden                                   Meran, Villa Eden     17          2.433              14        12 0.00000
Schluderns                                                 Schluderns     14          1.769              12         9 0.00000
Mals, Martinsheim                                   Mals, Martinsheim     17          3.096              17        11 0.00000
Klausen, Haus Eiseck                             Klausen, Haus Eiseck      9          1.548               7         4 0.00030
Kaltern                                                       Kaltern     10          3.538               6         3 0.00357
St. Ulrich, San Durich                         St. Ulrich, San Durich      8          2.654               5         2 0.00600
Sterzing, Bezirksaltenheim Wipptal Sterzing, Bezirksaltenheim Wipptal      8          2.654               5         2 0.00600
Lana, Lorenzerhof                                   Lana, Lorenzerhof      8          3.096               4         2 0.01412
Sarnthein, Sarner Stiftung                 Sarnthein, Sarner Stiftung      8          3.096               4         2 0.01412
Bolzano, Villa Europa                           Bolzano, Villa Europa      6          1.548               4         1 0.01922
Meran, Martinsbrunn                               Meran, Martinsbrunn      9          4.202               4         1 0.02800

Brixen, Bürgerheim                                 Brixen, Bürgerheim      5          0.221               4         0 0.05919
St. Pankraz                                               St. Pankraz      5          0.885               4         0 0.05919
Algund                                                         Algund      5          1.769               3         0 0.05919
Laas, St. Sisinius                                 Laas, St. Sisinius      5          1.548               3         0 0.05919
Villanders, Josefsheim                         Villanders, Josefsheim      5          2.212               2         0 0.07375
INSIEME                                                  INSIEME (18)    189         41.577             147       137 0.40185

0 bis 1 sind "Fehlalarme

Meran, Martinsbrunn ist von den "weniger suspekten" wieder zu den "suspekten" hinauf gestuft worden.
Anmerkung: Die "weniger suspkten" Heime sind jene mit dem Wert größer als 0.05 in der Spalte "proba".

Es sind 5 neue Fälle gefunden worden:
 Bolzano, Don Bosco: +1 
             Ritten: +1
            Kaltern: +1
Meran, Martinsbrunn: +1
  Mals, Martinsheim: +1

Allgemeiner Hinweis zu den Veränderungen: 
Die Veränderungen sind im Vergleich zur letzten Version des Vortages. 
Sie können sich auch auf Sterbefälle beziehen, die mehrere Tage (manchmal auch Wochen) zurück liegen.
In den Grafiken bezieht sich das Datum auf dem tatsächlichen Todestag.

