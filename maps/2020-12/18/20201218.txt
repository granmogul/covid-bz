https://www.sabes.it/de/news.asp?aktuelles_action=4&aktuelles_article_id=651084

Coronavirus – Mitteilungen des Südtiroler Sanitätsbetriebes | 18.12.2020 | 11:43

In den letzten 24 Stunden wurden 1.197 Abstriche untersucht und 169 Neuinfektionen festgestellt. 175 Covid-19-Positive sind stationär aufgenommen, weitere 23 in intensivmedizinischer Betreuung. 6 Todesfälle

Foto: 123rf

Bisher (18. Dezember) wurden insgesamt 342.805 Abstriche untersucht, die von 157.406 Personen stammen.

Die Zahlen im Überblick:

PCR-Abstriche:

Untersuchte Abstriche gestern (17. Dezember): 1.197

Mittels PCR-Test neu positiv getestete Personen: 169

Gesamtzahl der mittels PCR-Test positiv getesteten Personen: 27.681

Gesamtzahl der untersuchten Abstriche: 342.805

Gesamtzahl der mit Abstrichen getesteten Personen: 157.406 (+418)

Antigentests:

Durchgeführte Antigentests gestern: 4.840

Mittels Antigentest neu positiv getestete Personen: 119

Weitere Daten:

Auf Normalstationen im Krankenhaus untergebrachte Covid-19-Patienten/-Patientinnen: 175

In Privatkliniken untergebrachte Covid-19-Patienten/-Patientinnen: 145

In Gossensaß und Sarns untergebrachte Covid-19-Patienten/-Patientinnen: 33 (23 in Gossensaß, 10 in Sarns)

Covid-19-Patientinnen und -Patienten in Intensivbetreuung: 23

Gesamtzahl der mit Covid-19 Verstorbenen: 679 (+6)

Personen in Quarantäne/häuslicher Isolation: 6.066

Personen, die Quarantäne/häusliche Isolation beendet haben: 61.951

Personen betroffen von verordneter Quarantäne/häuslicher Isolation: 68.017

Geheilte Personen mit PCR Test: 16.259 (+132); zusätzlich 1.467 (+2) Personen, die ein unklares Testergebnis hatten und in der Folge negativ getestet wurden. Insgesamt: 17.726 (+134)

Geheilte Personen aufgrund von epidemiologischer Analyse: 6.073*

Gesamtzahl der Geheilten: 22.332*

Positiv getestete Mitarbeiter und Mitarbeiterinnen des Sanitätsbetriebes: 1.218, davon 780 geheilt. Positiv getestete Basis-, Kinderbasisärzte und Bereitsschaftsärzte: 37, davon 26 geheilt. (Stand: 05.12.)

* Mit heutigem Tag wird ein Nachtrag bei der "Anzahl der Geheilten" mitgeteilt (+ 6.073). In Absprache mit dem Dienst der Epidemiologischen Überwachung wurde eine Neuberechnung dieser Information durchgeführt, indem insbesondere jene Personen, die aufgrund epidemiologischer Analyse schon länger als "geheilt" gelten und für die, die Isolationszeit schon beendet worden ist, in die Berechnung miteinfließen (Isolationszeit über 21 Tage, mindestens 7 Tage symptomfrei).

(VS)

