[covid-bz](../../README.md) / [maps](../README.md) / [2021-03](../2021-03) 
# Cartine e altri grafici con i dati comunali trovati nei quotidiani file Excel e analisi dei necrologi

**[Februar 2021](../2021-02/README.md)**

## März 2021
- [domenica, 28 marzo](28/README.md)
- [sabato, 27 marzo](27/README.md)
- [venerdi, 26 marzo](26/README.md)
- [giovedi, 25 marzo](25/README.md)
- [mercoledi, 24  marzo](24/README.md)
- [martedi, 23 marzo](23/README.md)
- [lunedi, 22 marzo](22/README.md)
- [Sonntag, 21 März](21/README.md)
- [Samstag, 20 März](20/README.md)
- [Freitag, 19 März](19/README.md)
- [Donnerstag, 18 März](18/README.md)
- [Mittwoch, 17 März](17/README.md)
- [Dienstag, 16 März](16/README.md)
- [Montag, 15 März](15/README.md)
- [domenica, 14 marzo](14/README.md)
- [sabato, 13 marzo](13/README.md)
- [venerdi, 12 marzo](12/README.md)
- [giovedi, 11 marzo](11/README.md)
- [mercoledi, 10 marzo](10/README.md)
- [martedi, 9 marzo](09/README.md)
- [lunedi, 8 marzo](08/README.md)
- [Sonntag, 7. März](07/README.md)
- [Samstag, 6. März](06/README.md)
- [Freitag, 5. März](05/README.md)
- [Donnerstag, 4. März](04/README.md)
- [Mittwoch, 3. März](03/README.md)
- [Dienstag, 2. März](02/README.md)
- [Montag, 1. März](01/README.md)


 
# Links zu anderen Südtirolbezogene Webseiten
 
- Dashboard von [Markus Falk](https://www.markusfalk.com/) (siehe auch [Telegramgruppe](https://t.me/joinchat/Q4y5yRtSsh2rk4tvioBAjg))
  - [Version 1](https://www.markusfalk.com/dashboard/#lang=de&dset=ST&reg=all&tab=PRED&from=2020-08-11&to=true)
  - [Version 2](https://www.markusfalk.com/dashboard-beta/#lang=de&dset=ST_ALL&reg=all&tab=PRED&from=2020-08-11&to=true)
- Tabellen von [Moritz Mair](https://moritzmair.info/)
  - [Impfzahlen in Südtirol nach Alter](https://covvac.moritzmair.info/?tab=1)
- Ivan Sieder
  - [Interaktive Karten](https://map.corona-bz.simedia.cloud/)
- Zivilschutz Südtirol
  - [Grafiken](http://www.provinz.bz.it/sicherheit-zivilschutz/zivilschutz/aktuelle-daten-zum-coronavirus.asp)
  - [Aktuelle Excel-Datei mit Positiven (PCR+AG)](https://afbs.provinz.bz.it/upload/coronavirus/CoronavirusPositivi.xlsx)
  - [Aktuelle Excel-Datei mit Quarantäne](https://afbs.provinz.bz.it/upload/coronavirus/CoronavirusQuarantaene.xlsx)
- [Protezione civile Italia](https://github.com/pcm-dpc/COVID-19/blob/master/README.md)
  - [dati (CSV)](https://github.com/pcm-dpc/COVID-19/tree/master/dati-regioni)
  - [dati (JSON)](https://github.com/pcm-dpc/COVID-19/tree/master/dati-json/dpc-covid19-ita-regioni.json)
  - [scheda riepilogativa (PDF)](https://github.com/pcm-dpc/COVID-19/raw/master/schede-riepilogative/regioni/dpc-covid19-ita-scheda-regioni-latest.pdf)


# Keep In Touch

Für mehr Kommentare und evtl. Gedankenaustausch:
- Mastodon: https://troet.cafe/@COVID_BZ_Statistics
  - Einladungslink: https://troet.cafe/invite/goV294E5
