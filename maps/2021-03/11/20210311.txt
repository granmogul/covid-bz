https://www.sabes.it/de/news.asp?aktuelles_action=4&aktuelles_article_id=653788

Coronavirus – Mitteilungen des Südtiroler Sanitätsbetriebes | 11.03.2021 | 11:51

In den letzten 24 Stunden wurden 1.486 PCR-Tests untersucht und dabei 133 Neuinfektionen festgestellt. Zusätzlich gab es 71 positive Antigentests.

Foto: 123rf

Bisher (11. März) wurden insgesamt 508.986 Abstriche untersucht, die von 200.257 Personen stammen.

Die Zahlen im Überblick:

PCR-Abstriche:

Untersuchte Abstriche gestern (10. März): 1.486

Mittels PCR-Test neu positiv getestete Personen: 133

Gesamtzahl der mittels PCR-Test positiv getesteten Personen: 44.280

Gesamtzahl der untersuchten Abstriche: 508.986

Gesamtzahl der mit Abstrichen getesteten Personen: 200.257 (+307)

Antigentests:

Gesamtzahl der durchgeführten Antigentests: 939.618

Gesamtzahl der positiven Antigentests: 23.327

Durchgeführte Antigentests gestern: 10.195

Mittels Antigentest neu positiv getestete Personen: 71

Weitere Daten:

Auf Normalstationen im Krankenhaus untergebrachte Covid-19-Patienten/-Patientinnen: 169

In Privatkliniken untergebrachte Covid-19-Patienten/-Patientinnen (post-akut bzw. aus Seniorenwohnheimen übernommen): 151

In Gossensaß und Sarns untergebrachte Covid-19-Patienten/-Patientinnen: 132 (110 in Gossensaß, 22 in Sarns)

Anzahl der auf Intensivstationen aufgenommenen Covid-Patienten/Patientinnen: 41 (davon 38 als ICU-Covid klassifiziert, zusätzlich 3 Patientinnen/Patienten in Intensivbetreuung Ausland)

Gesamtzahl der mit Covid-19 Verstorbenen: 1.071 (+1)

Personen in Quarantäne/häuslicher Isolation: 5.978

Personen, die Quarantäne/häusliche Isolation beendet haben: 118.772

Personen betroffen von verordneter Quarantäne/häuslicher Isolation: 124.750

Geheilte Personen insgesamt: 63.184

Positiv getestete Mitarbeiter und Mitarbeiterinnen des Sanitätsbetriebes: 1.846, davon 1.408 geheilt. Positiv getestete Basis-, Kinderbasisärzte und Bereitschaftsärzte: 53, davon 40 geheilt. (Stand: 10.02.2021)

(PAS)

