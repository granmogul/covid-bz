[covid-bz](../../../README.md) / [maps](../../README.md) / [2021-02](../../2021-02/README.md) / [10](../10)

[09](../09/README.md) < [Februar](../README.md) > [11](../11/README.md)

# Mittwoch, 10. Februar 2021
 
- [PDF, Daten und Text](../10)
  - [tägliche Pressemitteilung](20210210.txt)
  - Karten
    - [Karte mit aktueller Quarantäne](CovidCartinaQuarantena-2021-02-10.pdf)
    - [Residenti messi in quarantena negli ultimi 7 giorni](CovidCartinaQuarantenaMandatiUltimi7giorni-2021-02-10.pdf)
  - [weiteres Material](../10)
 
 
## Klarstellungen der SABES zur täglichen Presseaussendung "Aktuelle Corona-Zahlen"

_Das Gesundheitsministerium in Rom hat kürzlich auf der Grundlage der ECDC-Richtlinien die Definition des bestätigten Covid-19-Falles aktualisiert. Die wesentliche Neuerung besteht darin, dass auch Antigentests in die Berichterstattung der bestätigten Corona-Virus-Fälle einfließen müssen._

_In einer Reihe von Gesprächen mit Experten des Gesundheitsministeriums und des Istituto Superiore della Sanitá wurden in den vergangenen Wochen, vor dem Hintergrund einer durchaus unterschiedlichen Handhabung der Berichterstattung in den verschiedenen Regionen, eine Reihe von Details geklärt. Mit heutigen Datum wird deshalb auch die Gesamtzahl der durchgeführten Antigentests, der positiven Antigentest-Ergebnisse und die Anzahl der mittlerweile Geheilten mitgeteilt. Dadurch entsteht ein vollständigeres Bild des Infektionsgeschehens, das zum einen eine höhere Anzahl an Geheilten (49875) zeigt, andrerseits auch eine höhere Anzahl an aktuell Infizierten (7057), weil sich die Berücksichtigung der Antigentestergebnisse in der Berechnung auch dieser Kennzahlen auswirkt._

_In Südtirol werden, wie auch in anderen Regionen, schon länger Antigentests, insbesondere bei Screening-Aktionen wie "Südtirol testet", eingesetzt. Die Neuinfektionen auch aufgrund von Antigentests wurden täglich kommuniziert. Auch für das Contact-Tracing wurden die Antigentests von Anfang an als vollwertige Tests verwendet._

_Eine weitere Klarstellung, die sich aus den genannten Rücksprachen ergeben hat, betrifft die Aufnahmen in den Intensivstationen. Diesbezüglich wird nunmehr neben der objektiv gegebenen Bettenauslastung der Covid-Intensiv-Stationen, auch mitgeteilt, welche Patienten auf der Grundlage der Leitlinien des Ministerium nicht mehr als Covid-Intensivpatienten nach Rom zu melden sind. Weil diese Patienten aber weiterhin auf den Covid-Stationen betreut werden, wurde in Südtirol immer die effektive Belegung veröffentlicht._
 
## Seniorenheime
 
Für Details siehe 
- [Liste der suspekten Heime](Traueranzeigen-Seniorenheime-Hotspots-Welle2-20210210.txt)
- [Grafiken](Traueranzeigen-SeniorenheimeCumuliert_2020-05-01_to_2021-02-07.pdf)
 
 
# Links zu anderen Südtirolbezogene Webseiten
 
- Dashboard von [Markus Falk](https://www.markusfalk.com/)
  - [Version 1](https://www.markusfalk.com/dashboard/#lang=de&dset=ST&reg=all&tab=PRED&from=2020-08-11&to=true)
  - [Version 2](https://www.markusfalk.com/dashboard-beta/#lang=de&dset=ST_ALL&reg=all&tab=PRED&from=2020-08-11&to=true)
- Tabellen von [Moritz Mair](https://moritzmair.info/)
  - [Impfzahlen in Südtirol nach Alter](https://covvac.moritzmair.info/?tab=1)
- Ivan Sieder
  - [Interaktive Karten](https://map.corona-bz.simedia.cloud/)
- Zivilschutz Südtirol
  - [Grafiken](http://www.provinz.bz.it/sicherheit-zivilschutz/zivilschutz/aktuelle-daten-zum-coronavirus.asp)
  - [Aktuelle Excel-Datei mit Positiven (PCR+AG)](https://afbs.provinz.bz.it/upload/coronavirus/CoronavirusPositivi.xlsx)
  - [Aktuelle Excel-Datei mit Quarantäne](https://afbs.provinz.bz.it/upload/coronavirus/CoronavirusQuarantaene.xlsx)
- [Protezione civile Italia](https://github.com/pcm-dpc/COVID-19/blob/master/README.md)
  - [dati (CSV)](https://github.com/pcm-dpc/COVID-19/tree/master/dati-regioni)
  - [dati (JSON)](https://github.com/pcm-dpc/COVID-19/tree/master/dati-json/dpc-covid19-ita-regioni.json)
  - [scheda riepilogativa (PDF)](https://github.com/pcm-dpc/COVID-19/raw/master/schede-riepilogative/regioni/dpc-covid19-ita-scheda-regioni-latest.pdf)
 
## Keep In Touch
 
- Mastodon: https://troet.cafe/@COVID_BZ_Statistics
- Matrix: https://matrix.to/#/@antoniogulino:matrix.org
