[covid-bz](../../README.md) / [maps](../README.md) / [2021-02](../2021-02) 
# Cartine e altri grafici con i dati comunali trovati nei quotidiani file Excel e analisi dei necrologi

**[Januar 2021](../2021-01/README.md)** <-> **[März 2021](../2021-03/README.md)** 

## Februar 2021
- [domenica, 28 febbraio](28)
- [sabato, 27 febbraio](27)
- [venerdi, 26 febbraio](26)
- [giovedi, 25 febbraio](25/README.md)
- [mercoledi, 24  febbraio](24/README.md)
- [martedi, 23 febbraio](23/README.md)
- [lunedi, 22 febbraio](22/README.md)
- [Sonntag, 21 Feb.](21/README.md)
- [Samstag, 20 Feb.](20/README.md)
- [Freitag, 19 Feb.](19/README.md)
- [Donnerstag, 18 Feb.](18/README.md)
- [Mittwoch, 17 Feb.](17/README.md)
- [Dienstag, 16 Feb.](16/README.md)
- [Montag, 15 Feb.](15/README.md)
- [domenica, 14 febbraio](14/README.md)
- **[sabato, 13 febbraio](13/README.md)** mit Insgesamt offizielle **Covid−Todesfälle** je 10.000 Einwohner ajourniert am 10.02.2021
  - **NEW**: Persone trovate positive con test PCR rispetto tutti i positivi (PCR e AG). Ultimi 21 giorni
  - **NEW**: Persone con almento un test positivo (PCR o AG), da marzo in poi
- [venerdi, 12 febbraio](12)
- [giovedi, 11 febbraio](11)
- [mercoledi, 10 febbraio](10/README.md), mit Klarstellungen der SABES zur täglichen Presseaussendung "Aktuelle Corona-Zahlen"
- [martedi, 9 febbraio](09/README.md), mit Kurzstatistik **Ojöp Frëinademetz von St. Martin i. T.** (Gadertal) und **Griesfeld von Neumarkt**,
- [lunedi, 8 febbraio](08/README.md)
- [Sonntag, 7. Feb.](07/README.md)
- [Samstag, 6. Feb.](06/README.md)
- [Freitag, 5. Feb.](05/README.md), mit Ankündigung Antigen-Massentest in Barbian und Lana
- [Donnerstag, 4. Feb.](04/README.md)
- [Mittwoch, 3. Feb.](03/README.md), mit Infos über positive Fälle in **Villa Serena (Bozen)**, **Ojöp Frëinademetz (St. Martin i. T.)**, **Georgianum (St. Johann im Ahrntal)**
- [Dienstag, 2. Feb.](02/README.md)
- [Montag, 1. Feb.](01/README.md)


 
# Links zu anderen Südtirolbezogene Webseiten
 
- Dashboard von [Markus Falk](https://www.markusfalk.com/) (siehe auch [Telegramgruppe](https://t.me/joinchat/Q4y5yRtSsh2rk4tvioBAjg))
  - [Version 1](https://www.markusfalk.com/dashboard/#lang=de&dset=ST&reg=all&tab=PRED&from=2020-08-11&to=true)
  - [Version 2](https://www.markusfalk.com/dashboard-beta/#lang=de&dset=ST_ALL&reg=all&tab=PRED&from=2020-08-11&to=true)
- Tabellen von [Moritz Mair](https://moritzmair.info/)
  - [Impfzahlen in Südtirol nach Alter](https://covvac.moritzmair.info/?tab=1)
- Ivan Sieder
  - [Interaktive Karten](https://map.corona-bz.simedia.cloud/)
- Zivilschutz Südtirol
  - [Grafiken](http://www.provinz.bz.it/sicherheit-zivilschutz/zivilschutz/aktuelle-daten-zum-coronavirus.asp)
  - [Aktuelle Excel-Datei mit Positiven (PCR+AG)](https://afbs.provinz.bz.it/upload/coronavirus/CoronavirusPositivi.xlsx)
  - [Aktuelle Excel-Datei mit Quarantäne](https://afbs.provinz.bz.it/upload/coronavirus/CoronavirusQuarantaene.xlsx)
- [Protezione civile Italia](https://github.com/pcm-dpc/COVID-19/blob/master/README.md)
  - [dati (CSV)](https://github.com/pcm-dpc/COVID-19/tree/master/dati-regioni)
  - [dati (JSON)](https://github.com/pcm-dpc/COVID-19/tree/master/dati-json/dpc-covid19-ita-regioni.json)
  - [scheda riepilogativa (PDF)](https://github.com/pcm-dpc/COVID-19/raw/master/schede-riepilogative/regioni/dpc-covid19-ita-scheda-regioni-latest.pdf)


# Keep In Touch

Für mehr Kommentare und evtl. Gedankenaustausch:
- Mastodon: https://troet.cafe/@COVID_BZ_Statistics
  - Einladungslink: https://troet.cafe/invite/goV294E5
