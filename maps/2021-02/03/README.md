[covid-bz](../../../README.md) / [maps](../../README.md) / [2021-02](../../2021-02/README.md) / [03](../03)
# Mittwoch,  3. Februar 2021
 
- [PDF, Daten und Text](../03)
  - [tägliche Pressemitteilung](20210203.txt)
  - Karten
    - [Summe der PCR- und AG-Positiven der letzten 7 Tagen](CovidCartinaPositiviPcrAgUltimi7giorni-2021-02-03.pdf)
    - [Summe der PCR- **und AG**-Positiven der letzten **21** Tagen](CovidCartinaPositiviPcrAgUltimi21giorni-2021-02-03.pdf)
    - [**Anstieg bzw. Rückgang** der PCR- und AG-Positiven der letzten 7 Tagen im Vergleich zu den letzten 21 Tagen](CovidCartinaAccelerazione7su21giorniPositiviPcrAgUltimi-2021-02-03.pdf)
    - [Karte mit aktueller Quarantäne](CovidCartinaQuarantena-2021-02-03.pdf)
    - [Residenti messi in quarantena negli ultimi 7 giorni](CovidCartinaQuarantenaMandatiUltimi7giorni-2021-02-03.pdf)
  - [weiteres Material](../03)
 
 
## Seniorenheime
 
### Villa Serena (Bozen)

Laut [Alto Adige von heute](https://www.altoadige.it/cronaca/bolzano/a-villa-serena-infettati-26-ospiti-e-9-operatori-1.2825024) sind zur Zeit in einen der #Seniorenheime in Bozen, Villa Serena Fagenstrasse, 26 Gäste und 9 Mitarbeiter positiv gefunden worden.

Villa Serena hat um die 75 Gäste (und wahrscheinlich ebensoviele Mitarbeiter).

Eine dieser Gäste ist schon an Covid gestorben.

Vor drei Wochen wurden fast alle Gäste geimpft. Und 4/5 der Mitarbeiter. Morgen (Donnerstag) müsste die zweite Dosis verabreicht werden.

### Ojöp Frëinademetz (St. Martin i. T.)

Laut "[La usc di ladins](https://www.lausc.it/valedes-ladines/val-badia/14696-corona-te-ciasa-de-palsa)" sind vor wenigen Tagen im #Seniorenheime  von St. Martin i. T. (Ojöp Frëinademetz) 29 Gäste (auf 80) und 12 Mitarbeiter #Positive gefunden worden. Am 25. Januar waren aber noch alle Negativ.

Ungefähr 2/3 der 80 Gäste und 75 Mitarbeiter hatten die 1. #Impfung. Die zweite soll in dieser Woche geschehen.

Es wird gesagt, dass alle keine starte Sympthome hätten.

### Georgianum (St. Johann im Ahrntal)

Mindestens 1 Mitarbeiter wurde positiv getestet. 

Diesen Freitag kommt die 2. Impfdosis



Dieses Heim hat in der Regel fast 55 Betten.

### Weiter Details

Für Details siehe 
- [Liste der suspekten Heime](Traueranzeigen-Seniorenheime-Hotspots-Welle2-20210203.txt)
- [Grafiken](Traueranzeigen-SeniorenheimeCumuliert_2020-05-01_to_2021-01-31.pdf)
 
 
# Links zu anderen Südtirolbezogene Webseiten
 
- Dashboard von [Markus Falk](https://www.markusfalk.com/)
  - [Version 1](https://www.markusfalk.com/dashboard/#lang=de&dset=ST&reg=all&tab=PRED&from=2020-08-11&to=true)
  - [Version 2](https://www.markusfalk.com/dashboard-beta/#lang=de&dset=ST_ALL&reg=all&tab=PRED&from=2020-08-11&to=true)
- Tabellen von [Moritz Mair](https://moritzmair.info/)
  - [Impfzahlen in Südtirol nach Alter](https://covvac.moritzmair.info/?tab=1)
- Ivan Sieder
  - [Interaktive Karten](https://map.corona-bz.simedia.cloud/)
- Zivilschutz Südtirol
  - [Grafiken](http://www.provinz.bz.it/sicherheit-zivilschutz/zivilschutz/aktuelle-daten-zum-coronavirus.asp)
  - [Aktuelle Excel-Datei mit Positiven (PCR+AG)](https://afbs.provinz.bz.it/upload/coronavirus/CoronavirusPositivi.xlsx)
  - [Aktuelle Excel-Datei mit Quarantäne](https://afbs.provinz.bz.it/upload/coronavirus/CoronavirusQuarantaene.xlsx)
- [Protezione civile Italia](https://github.com/pcm-dpc/COVID-19/blob/master/README.md)
  - [dati (CSV)](https://github.com/pcm-dpc/COVID-19/tree/master/dati-regioni)
  - [dati (JSON)](https://github.com/pcm-dpc/COVID-19/tree/master/dati-json/dpc-covid19-ita-regioni.json)
  - [scheda riepilogativa (PDF)](https://github.com/pcm-dpc/COVID-19/raw/master/schede-riepilogative/regioni/dpc-covid19-ita-scheda-regioni-latest.pdf)
 
## Keep In Touch
 
- Mastodon: https://troet.cafe/@COVID_BZ_Statistics
- Matrix: https://matrix.to/#/@antoniogulino:matrix.org
