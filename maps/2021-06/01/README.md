[covid-bz](../../../README.md) / [maps](../../README.md) / [2021-06](../../2021-06/README.md) / [01](../01)
 
# Dienstag,  1. Juni 2021
 
<img src="CovidCartinaPositiviPcrAgUltimi7giorni-2021-06-01-1.jpg" alt="7-Tage-Inzidenz" width="50%" align="right">
 
Die **7-Tage-Inzidenz** (um die 60 auf 100.000) ist zwar weiterhin beim sinken,
aber in den letzten 5 Wochen im Durchschnitt nur um -1.6% täglich,
was die **50-er Marke erst in 1 Wochen** "möglich" machen würde.


- [Die 7-Tage-Inzidenz](#die-7-tage-inzidenz)
- [Nasenflügeltests in den Schulen](#nasenflügeltests-in-den-schulen)
- [Die Lage in den Gemeinden](#die-lage-in-den-gemeinden)
- [Seniorenheime](#seniorenheime)
- [Links zu anderen Südtirolbezogene Webseiten](#links-zu-anderen-südtirolbezogene-webseiten)
- [Keep In Touch](#keep-in-touch)  _(feedback is welcome)_


## Die 7-Tage-Inzidenz


Die 7-Tage-Inzidenz (225 PCR + 86 AG) ist 59 auf 100.000.
Vor einer Woche (am 25.05.) war sie 56.
Also +0.6% täglich.



## Nasenflügeltests in den Schulen 
_Hinweis: die von der SABES gelieferten Daten sind nicht ganz kohärent. Hier werden sie so wie die Daten gemeldet wurden wiedergegeben und verglichen._

> Nasenflügeltests in den Schulen (bis 31.05.2021): 646.709 Tests gesamt an 550 Schulen, 524 positive Ergebnisse, davon 256 bestätigt, 226 PCR-negativ, 42  ausständig/zu überprüfen

Auswertung von 646.709 (+xx.xxx mehr als [gestern](../../2021-05/31/README.md)) Nasenflügeltests an xxx Schulen (-xxx)
 
davon waren zwar   524 (+xxx) positiv (x,xx pro mille) aber
- 256 xxx davon PCR-positiv (+xxx) 
-  226 PCR-negativ (xxx) 
-  42  noch zu überprüfen (xxx)
 
Dies bedeutet, dass die Nasenflügeltests, im Durchschnitt x,x bis x,x **Falsch positive** auf 10.000 "real negativen" Personen produziert haben.
 
_Wie viele "real positive" bei diesen Test "falsch negativ" sind, kann man aus diesen Werten nicht ausrechnen._
 

 
## Die Lage in den Gemeinden
 
<img src="CovidCartinaPositiviPcrAgOggi-2021-06-01-1.jpg" alt="Cartina con comuni con nuovi positivi oggi" width="50%" align="right">
 
Heute (01.06.) wurden
+23 Positive mit Wohnsitz in Südtirol mitgeteilt, in 11 verschiedene Gemeinden (von 116).
In 58 Gemeinden (gleich viele [gestern](../../2021-05/31/README.md)) gibt es schon seit mindestens 7 Tagen keinen neuen Fall mehr,
in 21 davon (1 weniger als [gestern](../../2021-05/31/README.md))  sogar schon seit 21 Tagen oder mehr.
 
Bemerkenswert sind heute:
- **Ahrntal** (6.022 Einw.): **Heute = +4**, vorhergehende 6 Tage = +18, vorhergehende 14 Tage = +17, **7-Tage-Inzidenz = 400**/100.000
- **Toblach** (3.351 Einw.): **Heute = +2**, vorhergehende 6 Tage = +7, vorhergehende 14 Tage = +13, 7-Tage-Inzidenz = 300/100.000

Nicht viel besser dran sind:
- **Klausen** (5.215 Einw.): Heute = +0, vorhergehende 6 Tage = +14, vorhergehende 14 Tage = +26, 7-Tage-Inzidenz = 270
- **Natz-Schabs** (3.230 Einw.): **Heute = +1**, vorhergehende 6 Tage = +10, vorhergehende 14 Tage = +41, 7-Tage-Inzidenz = **340**
- **Freienfeld** (2.656 Einw.): **Heute = +2**, vorhergehende 6 Tage = +6, vorhergehende 14 Tage = +2, 7-Tage-Inzidenz = 300
- **Schluderns** (1.838 Einw.): Heute = +0, vorhergehende 6 Tage = +6, vorhergehende 14 Tage = +5, 7-Tage-Inzidenz = **330**
 
<img src="CovidCartinaCovidFree-2021-06-01-1.jpg" alt="Cartina con comuni covid-free" width="50%" align="right">
 
Folgende 16 Gemeinden könnte man als **Covid-Free** betrachten, denn sie haben schon seit 21 Tagen keinen Fall mehr gehabt, niemand befindet sich in Quarantäne oder Isolation und die SABES zählt zur Zeit keinen "aktiv Positiven".
 
1. St.Leonhard i.P. (3.558)
1. Tisens (1.965)
1. Montan (1.701)
1. Mölten (1.692)
1. Aldein (1.656)
1. Riffian (1.362)
1. Rodeneck (1.240)
1. Schnals (1.228)
1. Pfatten (1.057)
1. Truden im Naturpark (1.040)
1. Hafling (778)
1. U.L.Frau I.W.-St.Felix (762)
1. Plaus (724)
1. Kurtinig a.d.W. (665)
1. Prettau (546)
1. Waidbruck (195)
 
Weitere Informationen: 
- [PDF, Daten und Text](../01) - in tedesco e italiano, se non indicato diversamente
  - [tägliche Pressemitteilung](20210601.txt) - solo tedesco
  - Karten - Cartine con dettaglio comunale
    - [**Covid-freie** Gemeinden](CovidCartinaCovidFree-2021-06-01.pdf)
    - [Cartina con evidenziati i comuni con almeno un nuovo caso](CovidCartinaPositiviPcrAgOggi-2021-06-01.pdf)
    - [Summe der PCR- und AG-Positiven der letzten 7 Tagen](CovidCartinaPositiviPcrAgUltimi7giorni-2021-06-01.pdf)
    - [Summe der PCR- **und AG**-Positiven der letzten **21** Tagen](CovidCartinaPositiviPcrAgUltimi21giorni-2021-06-01.pdf)
    - [**Anstieg bzw. Rückgang** der PCR- und AG-Positiven der letzten 7 Tagen im Vergleich zu den letzten 21 Tagen](CovidCartinaAccelerazione7su21giorniPositiviPcrAgUltimi-2021-06-01.pdf)
    - [Persone trovate positive con test PCR rispetto tutti i positivi (PCR e AG). Ultimi 21 giorni](CovidCartinaPositivi21PctPcr-2021-06-01.pdf)
    - [Karte mit aktueller Quarantäne](CovidCartinaQuarantena-2021-06-01.pdf)
    - [Residenti messi in quarantena negli ultimi 7 giorni](CovidCartinaQuarantenaMandatiUltimi7giorni-2021-06-01.pdf)
    - [Persone con almento un test positivo (PCR o AG), da 1° ottobre in poi](CovidCartinaPositiviTotaleWelle2-2021-06-01.pdf)
  - [weiteres Material](../01)
 
 
## Seniorenheime
 
Für Details siehe 
- [Liste der suspekten Heime](Traueranzeigen-Seniorenheime-Hotspots-Welle2-20210601.txt)
- [Grafiken](Traueranzeigen-SeniorenheimeCumuliert_2020-05-01_to_2021-05-29.pdf)
 
 
# Links zu anderen Südtirolbezogene Webseiten
 
- Dashboard von [Markus Falk](https://www.markusfalk.com/)
  - [Version 1](https://www.markusfalk.com/dashboard/#lang=de&dset=ST&reg=all&tab=PRED&from=2020-08-11&to=true)
  - [Version 2](https://www.markusfalk.com/dashboard-beta/#lang=de&dset=ST_ALL&reg=all&tab=PRED&from=2020-08-11&to=true)
- Tabellen von [Moritz Mair](https://moritzmair.info/)
  - [Impfzahlen in Südtirol nach Alter](https://covvac.moritzmair.info/?tab=1)
- Ivan Sieder
  - [Interaktive Karten](https://map.corona-bz.simedia.cloud/)
- Zivilschutz Südtirol
  - Ohne Gemeindedetails
    - [Grafiken](http://www.provinz.bz.it/sicherheit-zivilschutz/zivilschutz/aktuelle-daten-zum-coronavirus.asp)
    - [dati provinciali con serie storica a partire da 15 marzo 2020](https://afbs.provinz.bz.it/upload/coronavirus/Corona.Data.Total.csv)
  - Mit Gemeindedetails
    - [Grafici e cartine con dettaglio comunale](http://www.provincia.bz.it/sicurezza-protezione-civile/protezione-civile/aktuelle-daten-zum-coronavirus-in-den-gemeinden.asp)
    - [Aktuelle Excel-Datei mit Positiven (PCR+AG)](https://afbs.provinz.bz.it/upload/coronavirus/CoronavirusPositivi.xlsx)
    - [Aktuelle Excel-Datei mit Quarantäne](https://afbs.provinz.bz.it/upload/coronavirus/CoronavirusQuarantaene.xlsx)
    - [dati comunali con serie storica a partire da 18 dicembre](https://afbs.provinz.bz.it/upload/coronavirus/Corona.Data.Detail.csv)
- [Protezione civile Italia](https://github.com/pcm-dpc/COVID-19/blob/master/README.md)
  - diffusione del Virus
    - [dati (CSV)](https://github.com/pcm-dpc/COVID-19/tree/master/dati-regioni)
    - [dati (JSON)](https://github.com/pcm-dpc/COVID-19/tree/master/dati-json/dpc-covid19-ita-regioni.json)
    - [scheda riepilogativa (PDF)](https://github.com/pcm-dpc/COVID-19/raw/master/schede-riepilogative/regioni/dpc-covid19-ita-scheda-regioni-latest.pdf)
  - [Vaccinazioni](https://github.com/italia/covid19-opendata-vaccini/tree/master/dati)
    - [dati provinciali per classi di età, serie storica (CSV)](https://github.com/italia/covid19-opendata-vaccini/blob/master/dati/somministrazioni-vaccini-latest.csv)
 
## Keep In Touch
 
- Mastodon: https://troet.cafe/@COVID_BZ_Statistics
- Matrix: https://matrix.to/#/@antoniogulino:matrix.org
