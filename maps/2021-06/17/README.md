[covid-bz](../../../README.md) / [maps](../../README.md) / [2021-06](../../2021-06/README.md) / [17](../17)
 
# Donnerstag, 17. Juni 2021
 
<img src="CovidCartinaPositiviPcrAgUltimi7giorni-2021-06-17-1.jpg" alt="7-Tage-Inzidenz" width="50%" align="right">
 
Die **7-Tage-Inzidenz** (um die 20 auf 100.000) ist weiterhin beim sinken,
in den letzten 5 Wochen im Durchschnitt um -4.8% täglich.


- [Die 7-Tage-Inzidenz](#die-7-tage-inzidenz)
- [Nasenflügeltests in den Schulen](#nasenflügeltests-in-den-schulen)
- [Die Lage in den Gemeinden](#die-lage-in-den-gemeinden)
- [Seniorenheime](#seniorenheime)
- [Links zu anderen Südtirolbezogene Webseiten](#links-zu-anderen-südtirolbezogene-webseiten)
- [Keep In Touch](#keep-in-touch)  _(feedback is welcome)_


## Die 7-Tage-Inzidenz


Die 7-Tage-Inzidenz (73 PCR + 17 AG) ist 17 auf 100.000.
Vor einer Woche (am 10.06.) war sie 34.
Also -9.3% täglich.


## Nasenflügeltests in den Schulen 
_Hinweis: die von der SABES gelieferten Daten sind nicht ganz kohärent. Hier werden sie so wie die Daten gemeldet wurden wiedergegeben und verglichen._
 
Auswertung von xxx.xxx (+xx.xxx mehr als [gestern](../16/README.md)) Nasenflügeltests an xxx Schulen (-xxx)
 
davon waren zwar  xxx (+xxx) positiv (x,xx pro mille) aber
- nur xxx davon PCR-positiv (+xxx) 
- xxx PCR-negativ (xxx) 
- xx noch zu überprüfen (xxx)
 
Dies bedeutet, dass die Nasenflügeltests, im Durchschnitt x,x bis x,x **Falsch positive** auf 10.000 "real negativen" Personen produziert haben.
 
_Wie viele "real positive" bei diesen Test "falsch negativ" sind, kann man aus diesen Werten nicht ausrechnen._
 
Im Vergleich zu [gestern](../16/README.md) gab es +xx positive auf +xx.xxx, dies bedeutet 0,xx pro mille, also "_im Durchschnitt_" alle "**Falsch Positiv**".
 
## Die Lage in den Gemeinden
 
<img src="CovidCartinaPositiviPcrAgOggi-2021-06-17-1.jpg" alt="Cartina con comuni con nuovi positivi oggi" width="50%" align="right">
 
Heute (17.06.) wurden
+10 Positive mit Wohnsitz in Südtirol mitgeteilt, in 7 verschiedene Gemeinden (von 116).
In 89 Gemeinden (x mehr als [gestern](../16/README.md)) gibt es schon seit mindestens 7 Tagen keinen neuen Fall mehr,
in 47 davon sogar schon seit 21 Tagen oder mehr.
 
Bemerkenswert ist 
- **Lüsen** (1.574 Einw.): **Heute = +1**, vorhergehende 6 Tage = +3, vorhergehende 14 Tage = +5, **7-Tage-Inzidenz = 300**/100.000

Nenneswert sind
- **Ahrntal** (6.022 Einw.): **Heute = +1**, vorhergehende 6 Tage = +4, vorhergehende 14 Tage = +39, 7-Tage-Inzidenz =  80
- **Innichen** (3.367 Einw.): **Heute = +3**, vorhergehende 6 Tage = +2, vorhergehende 14 Tage = +2, 7-Tage-Inzidenz = 150
- **Branzoll** (2.808 Einw.): Heute = +0, vorhergehende 6 Tage = +4, vorhergehende 14 Tage = +6, 7-Tage-Inzidenz = 140


<img src="CovidCartinaCovidFree-2021-06-17-1.jpg" alt="Cartina con comuni covid-free" width="50%" align="right">
 
Folgende 35 Gemeinden könnte man als **Covid-Free** betrachten, denn sie haben schon seit 21 Tagen keinen Fall mehr gehabt, niemand befindet sich in Quarantäne oder Isolation und die SABES zählt zur Zeit keinen "aktiv Positiven".
 
1. Kaltern a.d.W. (8.104)
1. Sarntal (7.145)
1. Neumarkt (5.384)
1. Algund (5.005)
1. Salurn (3.827)
1. St.Leonhard i.P. (3.558)
1. Jenesien (3.066)
1. Ulten (2.896)
1. Marling (2.791)
1. Lajen (2.715)
1. Wolkenstein in Gröden (2.629)
1. Kastelbell-Tschars (2.298)
1. Welschnofen (1.973)
1. Sexten (1.880)
1. Gargazon (1.726)
1. Montan (1.701)
1. Mölten (1.692)
1. Niederdorf (1.597)
1. St.Pankraz (1.544)
1. Tscherms (1.527)
1. Magreid a.d.W. (1.274)
1. Rodeneck (1.240)
1. Truden im Naturpark (1.040)
1. Franzensfeste (1.000)
1. Vöran (959)
1. Glurns (900)
1. Martell (841)
1. Hafling (778)
1. U.L.Frau I.W.-St.Felix (762)
1. Plaus (724)
1. Prags (655)
1. Altrei (398)
1. Laurein (342)
1. Proveis (265)
1. Waidbruck (195)
 
Weitere Informationen: 
- [PDF, Daten und Text](../17) - in tedesco e italiano, se non indicato diversamente
  - [tägliche Pressemitteilung](20210617.txt) - solo tedesco
  - Karten - Cartine con dettaglio comunale
    - [**Covid-freie** Gemeinden](CovidCartinaCovidFree-2021-06-17.pdf)
    - [Cartina con evidenziati i comuni con almeno un nuovo caso](CovidCartinaPositiviPcrAgOggi-2021-06-17.pdf)
    - [Summe der PCR- und AG-Positiven der letzten 7 Tagen](CovidCartinaPositiviPcrAgUltimi7giorni-2021-06-17.pdf)
    - [Summe der PCR- **und AG**-Positiven der letzten **21** Tagen](CovidCartinaPositiviPcrAgUltimi21giorni-2021-06-17.pdf)
    - [**Anstieg bzw. Rückgang** der PCR- und AG-Positiven der letzten 7 Tagen im Vergleich zu den letzten 21 Tagen](CovidCartinaAccelerazione7su21giorniPositiviPcrAgUltimi-2021-06-17.pdf)
    - [Persone trovate positive con test PCR rispetto tutti i positivi (PCR e AG). Ultimi 21 giorni](CovidCartinaPositivi21PctPcr-2021-06-17.pdf)
    - [Karte mit aktueller Quarantäne](CovidCartinaQuarantena-2021-06-17.pdf)
    - [Residenti messi in quarantena negli ultimi 7 giorni](CovidCartinaQuarantenaMandatiUltimi7giorni-2021-06-17.pdf)
    - [Persone con almento un test positivo (PCR o AG), da 1° ottobre in poi](CovidCartinaPositiviTotaleWelle2-2021-06-17.pdf)
  - [weiteres Material](../17)
 
 
## Seniorenheime
 
Für Details siehe 
- [Liste der suspekten Heime](Traueranzeigen-Seniorenheime-Hotspots-Welle2-20210617.txt)
- [Grafiken](Traueranzeigen-SeniorenheimeCumuliert_2020-05-01_to_2021-06-14.pdf)
 
 
# Links zu anderen Südtirolbezogene Webseiten
 
- Dashboard von [Markus Falk](https://www.markusfalk.com/)
  - [Version 1](https://www.markusfalk.com/dashboard/#lang=de&dset=ST&reg=all&tab=PRED&from=2020-08-11&to=true)
  - [Version 2](https://www.markusfalk.com/dashboard-beta/#lang=de&dset=ST_ALL&reg=all&tab=PRED&from=2020-08-11&to=true)
- Tabellen von [Moritz Mair](https://moritzmair.info/)
  - [Impfzahlen in Südtirol nach Alter](https://covvac.moritzmair.info/?tab=1)
- Ivan Sieder
  - [Interaktive Karten](https://map.corona-bz.simedia.cloud/)
- Zivilschutz Südtirol
  - Ohne Gemeindedetails
    - [Grafiken](http://www.provinz.bz.it/sicherheit-zivilschutz/zivilschutz/aktuelle-daten-zum-coronavirus.asp)
    - [dati provinciali con serie storica a partire da 15 marzo 2020](https://afbs.provinz.bz.it/upload/coronavirus/Corona.Data.Total.csv)
  - Mit Gemeindedetails
    - [Grafici e cartine con dettaglio comunale](http://www.provincia.bz.it/sicurezza-protezione-civile/protezione-civile/aktuelle-daten-zum-coronavirus-in-den-gemeinden.asp)
    - [Aktuelle Excel-Datei mit Positiven (PCR+AG)](https://afbs.provinz.bz.it/upload/coronavirus/CoronavirusPositivi.xlsx)
    - [Aktuelle Excel-Datei mit Quarantäne](https://afbs.provinz.bz.it/upload/coronavirus/CoronavirusQuarantaene.xlsx)
    - [dati comunali con serie storica a partire da 18 dicembre](https://afbs.provinz.bz.it/upload/coronavirus/Corona.Data.Detail.csv)
- [Protezione civile Italia](https://github.com/pcm-dpc/COVID-19/blob/master/README.md)
  - diffusione del Virus
    - [dati (CSV)](https://github.com/pcm-dpc/COVID-19/tree/master/dati-regioni)
    - [dati (JSON)](https://github.com/pcm-dpc/COVID-19/tree/master/dati-json/dpc-covid19-ita-regioni.json)
    - [scheda riepilogativa (PDF)](https://github.com/pcm-dpc/COVID-19/raw/master/schede-riepilogative/regioni/dpc-covid19-ita-scheda-regioni-latest.pdf)
  - [Vaccinazioni](https://github.com/italia/covid19-opendata-vaccini/tree/master/dati)
    - [dati provinciali per classi di età, serie storica (CSV)](https://github.com/italia/covid19-opendata-vaccini/blob/master/dati/somministrazioni-vaccini-latest.csv)
 
## Keep In Touch
 
- Mastodon: https://troet.cafe/@COVID_BZ_Statistics
- Matrix: https://matrix.to/#/@antoniogulino:matrix.org
