[covid-bz](../../../README.md) / [maps](../../README.md) / [2021-06](../../2021-06/README.md) / [07](../07)
 
# Montag,  7. Juni 2021
 
<img src="CovidCartinaPositiviPcrAgUltimi7giorni-2021-06-07-1.jpg" alt="7-Tage-Inzidenz" width="50%" align="right">
 
Die **7-Tage-Inzidenz** (um die 40 auf 100.000) ist weiterhin beim sinken,
in den letzten 5 Wochen im Durchschnitt um -3.5% täglich.


- [Die 7-Tage-Inzidenz](#die-7-tage-inzidenz)
- [Nasenflügeltests in den Schulen](#nasenflügeltests-in-den-schulen)
- [Die Lage in den Gemeinden](#die-lage-in-den-gemeinden)
- [Seniorenheime](#seniorenheime)
- [Links zu anderen Südtirolbezogene Webseiten](#links-zu-anderen-südtirolbezogene-webseiten)
- [Keep In Touch](#keep-in-touch)  _(feedback is welcome)_


## Die 7-Tage-Inzidenz


Die 7-Tage-Inzidenz (152 PCR + 38 AG) ist 36 auf 100.000.
Vor einer Woche (am 31.05.) war sie 57.
Also -6.3% täglich.



## Nasenflügeltests in den Schulen 
_Hinweis: die von der SABES gelieferten Daten sind nicht ganz kohärent. Hier werden sie so wie die Daten gemeldet wurden wiedergegeben und verglichen._
 
Auswertung von xxx.xxx (+xx.xxx mehr als [gestern](../06/README.md)) Nasenflügeltests an xxx Schulen (-xxx)
 
davon waren zwar  xxx (+xxx) positiv (x,xx pro mille) aber
- nur xxx davon PCR-positiv (+xxx) 
- xxx PCR-negativ (xxx) 
- xx noch zu überprüfen (xxx)
 
Dies bedeutet, dass die Nasenflügeltests, im Durchschnitt x,x bis x,x **Falsch positive** auf 10.000 "real negativen" Personen produziert haben.
 
_Wie viele "real positive" bei diesen Test "falsch negativ" sind, kann man aus diesen Werten nicht ausrechnen._
 
Im Vergleich zu [gestern](../06/README.md) gab es +xx positive auf +xx.xxx, dies bedeutet 0,xx pro mille, also "_im Durchschnitt_" alle "**Falsch Positiv**".
 
## Die Lage in den Gemeinden
 
<img src="CovidCartinaPositiviPcrAgOggi-2021-06-07-1.jpg" alt="Cartina con comuni con nuovi positivi oggi" width="50%" align="right">
 
Heute (07.06.) wurden
+3 Positive mit Wohnsitz in Südtirol mitgeteilt, in 3 verschiedene Gemeinden (von 116).
In 71 Gemeinden (x mehr als [gestern](../06/README.md)) gibt es schon seit mindestens 7 Tagen keinen neuen Fall mehr,
in 33 davon sogar schon seit 21 Tagen oder mehr.

Bei nur 3 positive Fälle gibt es heute fast nichts neues zu melden. Folgende Gemeinden sind aber trotzdem nennenswert, wegen der Inzidenz und der Beschleunigung:
- Ahrntal (6.022 Einw.): Heute = +0, vorhergehende 6 Tage = +16, vorhergehende 14 Tage = +31, 7-Tage-Inzidenz = 270/100.000" 
- Freienfeld (2.656 Einw.): Heute = +0, vorhergehende 6 Tage = +5, vorhergehende 14 Tage = +7, 7-Tage-Inzidenz = 190/100.000"
- Mühlwald (1.432 Einw.): Heute = +0, vorhergehende 6 Tage = +3, vorhergehende 14 Tage = +4, 7-Tage-Inzidenz = 210

<img src="CovidCartinaCovidFree-2021-06-07-1.jpg" alt="Cartina con comuni covid-free" width="50%" align="right">
 
Folgende 24 Gemeinden könnte man als **Covid-Free** betrachten, denn sie haben schon seit 21 Tagen keinen Fall mehr gehabt, niemand befindet sich in Quarantäne oder Isolation und die SABES zählt zur Zeit keinen "aktiv Positiven".
 
1. Auer (3.825)
1. Völs am Schlern (3.579)
1. St.Leonhard i.P. (3.558)
1. St.Martin i.P. (3.255)
1. Kastelbell-Tschars (2.298)
1. Welschnofen (1.973)
1. Tisens (1.965)
1. Sexten (1.880)
1. Montan (1.701)
1. Mölten (1.692)
1. Aldein (1.656)
1. Tscherms (1.527)
1. Wengen (1.389)
1. Truden im Naturpark (1.040)
1. Tiers (1.007)
1. Franzensfeste (1.000)
1. Glurns (900)
1. Hafling (778)
1. U.L.Frau I.W.-St.Felix (762)
1. Plaus (724)
1. Kurtinig a.d.W. (665)
1. Altrei (398)
1. Laurein (342)
1. Waidbruck (195)
 
Weitere Informationen: 
- [PDF, Daten und Text](../07) - in tedesco e italiano, se non indicato diversamente
  - [tägliche Pressemitteilung](20210607.txt) - solo tedesco
  - Karten - Cartine con dettaglio comunale
    - [**Covid-freie** Gemeinden](CovidCartinaCovidFree-2021-06-07.pdf)
    - [Cartina con evidenziati i comuni con almeno un nuovo caso](CovidCartinaPositiviPcrAgOggi-2021-06-07.pdf)
    - [Summe der PCR- und AG-Positiven der letzten 7 Tagen](CovidCartinaPositiviPcrAgUltimi7giorni-2021-06-07.pdf)
    - [Summe der PCR- **und AG**-Positiven der letzten **21** Tagen](CovidCartinaPositiviPcrAgUltimi21giorni-2021-06-07.pdf)
    - [**Anstieg bzw. Rückgang** der PCR- und AG-Positiven der letzten 7 Tagen im Vergleich zu den letzten 21 Tagen](CovidCartinaAccelerazione7su21giorniPositiviPcrAgUltimi-2021-06-07.pdf)
    - [Persone trovate positive con test PCR rispetto tutti i positivi (PCR e AG). Ultimi 21 giorni](CovidCartinaPositivi21PctPcr-2021-06-07.pdf)
    - [Karte mit aktueller Quarantäne](CovidCartinaQuarantena-2021-06-07.pdf)
    - [Residenti messi in quarantena negli ultimi 7 giorni](CovidCartinaQuarantenaMandatiUltimi7giorni-2021-06-07.pdf)
    - [Persone con almento un test positivo (PCR o AG), da 1° ottobre in poi](CovidCartinaPositiviTotaleWelle2-2021-06-07.pdf)
  - [weiteres Material](../07)
 
 
## Seniorenheime
 
Für Details siehe 
- [Liste der suspekten Heime](Traueranzeigen-Seniorenheime-Hotspots-Welle2-20210607.txt)
- [Grafiken](Traueranzeigen-SeniorenheimeCumuliert_2020-05-01_to_2021-06-04.pdf)
 
 
# Links zu anderen Südtirolbezogene Webseiten
 
- Dashboard von [Markus Falk](https://www.markusfalk.com/)
  - [Version 1](https://www.markusfalk.com/dashboard/#lang=de&dset=ST&reg=all&tab=PRED&from=2020-08-11&to=true)
  - [Version 2](https://www.markusfalk.com/dashboard-beta/#lang=de&dset=ST_ALL&reg=all&tab=PRED&from=2020-08-11&to=true)
- Tabellen von [Moritz Mair](https://moritzmair.info/)
  - [Impfzahlen in Südtirol nach Alter](https://covvac.moritzmair.info/?tab=1)
- Ivan Sieder
  - [Interaktive Karten](https://map.corona-bz.simedia.cloud/)
- Zivilschutz Südtirol
  - Ohne Gemeindedetails
    - [Grafiken](http://www.provinz.bz.it/sicherheit-zivilschutz/zivilschutz/aktuelle-daten-zum-coronavirus.asp)
    - [dati provinciali con serie storica a partire da 15 marzo 2020](https://afbs.provinz.bz.it/upload/coronavirus/Corona.Data.Total.csv)
  - Mit Gemeindedetails
    - [Grafici e cartine con dettaglio comunale](http://www.provincia.bz.it/sicurezza-protezione-civile/protezione-civile/aktuelle-daten-zum-coronavirus-in-den-gemeinden.asp)
    - [Aktuelle Excel-Datei mit Positiven (PCR+AG)](https://afbs.provinz.bz.it/upload/coronavirus/CoronavirusPositivi.xlsx)
    - [Aktuelle Excel-Datei mit Quarantäne](https://afbs.provinz.bz.it/upload/coronavirus/CoronavirusQuarantaene.xlsx)
    - [dati comunali con serie storica a partire da 18 dicembre](https://afbs.provinz.bz.it/upload/coronavirus/Corona.Data.Detail.csv)
- [Protezione civile Italia](https://github.com/pcm-dpc/COVID-19/blob/master/README.md)
  - diffusione del Virus
    - [dati (CSV)](https://github.com/pcm-dpc/COVID-19/tree/master/dati-regioni)
    - [dati (JSON)](https://github.com/pcm-dpc/COVID-19/tree/master/dati-json/dpc-covid19-ita-regioni.json)
    - [scheda riepilogativa (PDF)](https://github.com/pcm-dpc/COVID-19/raw/master/schede-riepilogative/regioni/dpc-covid19-ita-scheda-regioni-latest.pdf)
  - [Vaccinazioni](https://github.com/italia/covid19-opendata-vaccini/tree/master/dati)
    - [dati provinciali per classi di età, serie storica (CSV)](https://github.com/italia/covid19-opendata-vaccini/blob/master/dati/somministrazioni-vaccini-latest.csv)
 
## Keep In Touch
 
- Mastodon: https://troet.cafe/@COVID_BZ_Statistics
- Matrix: https://matrix.to/#/@antoniogulino:matrix.org
