[covid-bz](../../../README.md) / [maps](../../README.md) / [2021-06](../../2021-06/README.md) / [16](../16)
 
# Mittwoch, 16. Juni 2021
 
<img src="CovidCartinaPositiviPcrAgUltimi7giorni-2021-06-16-1.jpg" alt="7-Tage-Inzidenz" width="50%" align="right">
 
Die **7-Tage-Inzidenz** (um die 20 auf 100.000) ist weiterhin beim sinken,
in den letzten 5 Wochen im Durchschnitt um -4.6% täglich.

- [Betriebe und Varianten](#betriebe-und-varianten)
- [Die 7-Tage-Inzidenz](#die-7-tage-inzidenz)
- [Nasenflügeltests in den Schulen](#nasenflügeltests-in-den-schulen)
- [Die Lage in den Gemeinden](#die-lage-in-den-gemeinden)
- [Seniorenheime](#seniorenheime)
- [Links zu anderen Südtirolbezogene Webseiten](#links-zu-anderen-südtirolbezogene-webseiten)
- [Keep In Touch](#keep-in-touch)  _(feedback is welcome)_


## Betriebe und Varianten

> _In den vergangenen Tagen wurden in einigen Südtiroler Betrieben Infektionsfälle unter Mitarbeitenden festgestellt.
Betroffen sind insgesamt **acht Betriebe** im **Großraum Brixen**, in **Bozen und Umgebung**, im **Burggrafenamt** und im **Pustertal**.
Bei dem Betrieb im Burggrafenamt wurden mehrere Mitarbeiter positiv getestet, ein Mitarbeiter war positiv auf die hochansteckende **„Delta-Variante“**. Ein weiteres Infektionsgeschehen betrifft einen Betrieb in **Überetsch**, in dem die **britische** Variante festgestellt worden ist._ 
 Quelle: [SABES, 16.06.2021 10:42](https://www.sabes.it/de/news.asp?aktuelles_action=4&aktuelles_article_id=656816)

Ich glaube, wenn in einen Betrieb  gleichzeitig mehrere positiv sind, dann wird wohl nicht nur einer sondern alle die Delta-Variante haben. Ich nehme an, dass sie nur bei einer Person die Variante untersucht haben.



## Die 7-Tage-Inzidenz


Die 7-Tage-Inzidenz (81 PCR + 19 AG) ist 19 auf 100.000.
Vor einer Woche (am 09.06.) war sie 33.
Also -7.8% täglich.


## Nasenflügeltests in den Schulen 
_Hinweis: die von der SABES gelieferten Daten sind nicht ganz kohärent. Hier werden sie so wie die Daten gemeldet wurden wiedergegeben und verglichen._

> Nasenflügeltests in den Schulen (Stand 14.06.2021): 794.035 Tests gesamt an 554 Schulen, 587 positive Ergebnisse, davon 275 bestätigt, 268 PCR-negativ, 44 ausständig/zu überprüfen.



Auswertung von 794.035  Nasenflügeltests an an 554 Schulen 
 
davon waren zwar   587 positiv (0.74 pro mille) aber
- nur 275 davon PCR-positiv 
-  268 PCR-negativ 
-  44 noch zu überprüfen 
 
Dies bedeutet, dass die Nasenflügeltests, im Durchschnitt 3,4 bis 3,9 **Falsch positive** auf 10.000 "real negativen" Personen produziert haben.
 
_Wie viele "real positive" bei diesen Test "falsch negativ" sind, kann man aus diesen Werten nicht ausrechnen._
 

## Die Lage in den Gemeinden
 
<img src="CovidCartinaPositiviPcrAgOggi-2021-06-16-1.jpg" alt="Cartina con comuni con nuovi positivi oggi" width="50%" align="right">
 
Heute (16.06.) wurden
+10 Positive mit Wohnsitz in Südtirol mitgeteilt, in 9 verschiedene Gemeinden (von 116).
In 88 Gemeinden (x mehr als [gestern](../15/README.md)) gibt es schon seit mindestens 7 Tagen keinen neuen Fall mehr,
in 44 davon sogar schon seit 21 Tagen oder mehr.

Bemerkenswert ist heute
- **Sand in Taufers** (5.496 Einw.): **Heute = +2**, vorhergehende 6 Tage = +4, vorhergehende 14 Tage = +10, 7-Tage-Inzidenz = 100/100.000

Nennenswert sind heute
- **Ahrntal** (6.022 Einw.): **Heute = +1**, vorhergehende 6 Tage = +5, vorhergehende 14 Tage = +38, 7-Tage-Inzidenz = 100
- **Gais** (3.311 Einw.): **Heute = +1**, vorhergehende 6 Tage = +1, vorhergehende 14 Tage = +3, 7-Tage-Inzidenz =  60
- **Branzoll** (2.808 Einw.): Heute = +0, vorhergehende 6 Tage = +4, vorhergehende 14 Tage = +6, 7-Tage-Inzidenz = 140
- **Lüsen** (1.574 Einw.): Heute = +0, vorhergehende 6 Tage = +3, vorhergehende 14 Tage = +5, 7-Tage-Inzidenz = 190

<img src="CovidCartinaCovidFree-2021-06-16-1.jpg" alt="Cartina con comuni covid-free" width="50%" align="right">
 
Folgende 35 Gemeinden könnte man als **Covid-Free** betrachten, denn sie haben schon seit 21 Tagen keinen Fall mehr gehabt, niemand befindet sich in Quarantäne oder Isolation und die SABES zählt zur Zeit keinen "aktiv Positiven".
 
1. Kaltern a.d.W. (8.104)
1. Naturns (5.869)
1. Neumarkt (5.384)
1. Salurn (3.827)
1. St.Leonhard i.P. (3.558)
1. St.Martin i.P. (3.255)
1. Jenesien (3.066)
1. Ulten (2.896)
1. Marling (2.791)
1. Lajen (2.715)
1. Wolkenstein in Gröden (2.629)
1. Kastelbell-Tschars (2.298)
1. Welschnofen (1.973)
1. Sexten (1.880)
1. Gargazon (1.726)
1. Montan (1.701)
1. Mölten (1.692)
1. St.Pankraz (1.544)
1. Tscherms (1.527)
1. Magreid a.d.W. (1.274)
1. Rodeneck (1.240)
1. Truden im Naturpark (1.040)
1. Tiers (1.007)
1. Franzensfeste (1.000)
1. Vöran (959)
1. Glurns (900)
1. Martell (841)
1. Hafling (778)
1. U.L.Frau I.W.-St.Felix (762)
1. Plaus (724)
1. Prags (655)
1. Altrei (398)
1. Laurein (342)
1. Proveis (265)
1. Waidbruck (195)
 
Weitere Informationen: 
- [PDF, Daten und Text](../16) - in tedesco e italiano, se non indicato diversamente
  - [tägliche Pressemitteilung](20210616.txt) - solo tedesco
  - Karten - Cartine con dettaglio comunale
    - [**Covid-freie** Gemeinden](CovidCartinaCovidFree-2021-06-16.pdf)
    - [Cartina con evidenziati i comuni con almeno un nuovo caso](CovidCartinaPositiviPcrAgOggi-2021-06-16.pdf)
    - [Summe der PCR- und AG-Positiven der letzten 7 Tagen](CovidCartinaPositiviPcrAgUltimi7giorni-2021-06-16.pdf)
    - [Summe der PCR- **und AG**-Positiven der letzten **21** Tagen](CovidCartinaPositiviPcrAgUltimi21giorni-2021-06-16.pdf)
    - [**Anstieg bzw. Rückgang** der PCR- und AG-Positiven der letzten 7 Tagen im Vergleich zu den letzten 21 Tagen](CovidCartinaAccelerazione7su21giorniPositiviPcrAgUltimi-2021-06-16.pdf)
    - [Persone trovate positive con test PCR rispetto tutti i positivi (PCR e AG). Ultimi 21 giorni](CovidCartinaPositivi21PctPcr-2021-06-16.pdf)
    - [Karte mit aktueller Quarantäne](CovidCartinaQuarantena-2021-06-16.pdf)
    - [Residenti messi in quarantena negli ultimi 7 giorni](CovidCartinaQuarantenaMandatiUltimi7giorni-2021-06-16.pdf)
    - [Persone con almento un test positivo (PCR o AG), da 1° ottobre in poi](CovidCartinaPositiviTotaleWelle2-2021-06-16.pdf)
  - [weiteres Material](../16)
 
 
## Seniorenheime
 
Für Details siehe 
- [Liste der suspekten Heime](Traueranzeigen-Seniorenheime-Hotspots-Welle2-20210616.txt)
- [Grafiken](Traueranzeigen-SeniorenheimeCumuliert_2020-05-01_to_2021-06-13.pdf)
 
 
# Links zu anderen Südtirolbezogene Webseiten
 
- Dashboard von [Markus Falk](https://www.markusfalk.com/)
  - [Version 1](https://www.markusfalk.com/dashboard/#lang=de&dset=ST&reg=all&tab=PRED&from=2020-08-11&to=true)
  - [Version 2](https://www.markusfalk.com/dashboard-beta/#lang=de&dset=ST_ALL&reg=all&tab=PRED&from=2020-08-11&to=true)
- Tabellen von [Moritz Mair](https://moritzmair.info/)
  - [Impfzahlen in Südtirol nach Alter](https://covvac.moritzmair.info/?tab=1)
- Ivan Sieder
  - [Interaktive Karten](https://map.corona-bz.simedia.cloud/)
- Zivilschutz Südtirol
  - Ohne Gemeindedetails
    - [Grafiken](http://www.provinz.bz.it/sicherheit-zivilschutz/zivilschutz/aktuelle-daten-zum-coronavirus.asp)
    - [dati provinciali con serie storica a partire da 15 marzo 2020](https://afbs.provinz.bz.it/upload/coronavirus/Corona.Data.Total.csv)
  - Mit Gemeindedetails
    - [Grafici e cartine con dettaglio comunale](http://www.provincia.bz.it/sicurezza-protezione-civile/protezione-civile/aktuelle-daten-zum-coronavirus-in-den-gemeinden.asp)
    - [Aktuelle Excel-Datei mit Positiven (PCR+AG)](https://afbs.provinz.bz.it/upload/coronavirus/CoronavirusPositivi.xlsx)
    - [Aktuelle Excel-Datei mit Quarantäne](https://afbs.provinz.bz.it/upload/coronavirus/CoronavirusQuarantaene.xlsx)
    - [dati comunali con serie storica a partire da 18 dicembre](https://afbs.provinz.bz.it/upload/coronavirus/Corona.Data.Detail.csv)
- [Protezione civile Italia](https://github.com/pcm-dpc/COVID-19/blob/master/README.md)
  - diffusione del Virus
    - [dati (CSV)](https://github.com/pcm-dpc/COVID-19/tree/master/dati-regioni)
    - [dati (JSON)](https://github.com/pcm-dpc/COVID-19/tree/master/dati-json/dpc-covid19-ita-regioni.json)
    - [scheda riepilogativa (PDF)](https://github.com/pcm-dpc/COVID-19/raw/master/schede-riepilogative/regioni/dpc-covid19-ita-scheda-regioni-latest.pdf)
  - [Vaccinazioni](https://github.com/italia/covid19-opendata-vaccini/tree/master/dati)
    - [dati provinciali per classi di età, serie storica (CSV)](https://github.com/italia/covid19-opendata-vaccini/blob/master/dati/somministrazioni-vaccini-latest.csv)
 
## Keep In Touch
 
- Mastodon: https://troet.cafe/@COVID_BZ_Statistics
- Matrix: https://matrix.to/#/@antoniogulino:matrix.org
