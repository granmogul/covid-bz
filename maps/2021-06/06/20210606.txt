https://www.sabes.it/de/news.asp?aktuelles_action=4&aktuelles_article_id=656464

Coronavirus – Mitteilung des Sanitätsbetriebes | 06.06.2021 | 11:15

In den letzten 24 Stunden wurden 584 PCR-Tests untersucht und dabei 36 Neuinfektionen festgestellt. Zusätzlich gab es 2 positive Antigentests.

Foto: 123rf

Bisher (06. Juni) wurden insgesamt 581.948 Abstriche untersucht, die von 219.179 Personen stammen.

Die Zahlen im Überblick:

PCR-Abstriche:

Untersuchte Abstriche gestern (05. Juni): 584

Mittels PCR-Test neu positiv getestete Personen: 36

Gesamtzahl der mittels PCR-Test positiv getesteten Personen: 48.473

Gesamtzahl der untersuchten Abstriche: 581.948

Gesamtzahl der mit Abstrichen getesteten Personen: 219.179 (+154)

Antigentests:

Gesamtzahl der durchgeführten Antigentests: 1.516.932

Gesamtzahl der positiven Antigentests: 26.155

Durchgeführte Antigentests gestern: 1.959

Mittels Antigentest neu positiv getestete Personen: 2

Nasenflügeltests Stand 05.06.2021 (ohne Schulen) 608.331 Tests, davon 803 positiv

Nasenflügeltests in den Schulen (Stand 05.06.2021)720.660 Tests gesamt an 536 Schulen, 566 positive Ergebnisse, davon 266 bestätigt, 246 PCR-negativ, 54 ausständig/zu überprüfen

Weitere Daten:

Auf Normalstationen im Krankenhaus untergebrachte Covid-19-Patienten/-Patientinnen: 13

In Gossensaß und Sarns untergebrachte Covid-19-Patienten/-Patientinnen: 15

Anzahl der auf Intensivstationen aufgenommenen Covid-Patienten/Patientinnen: 3 (davon 2 als ICU-Covid klassifiziert)Gesamtzahl der mit Covid-19 Verstorbenen: 1.177 (+1)

Personen in Quarantäne/häuslicher Isolation: 1.147

Personen, die Quarantäne/häusliche Isolation beendet haben: 135.428

Personen betroffen von verordneter Quarantäne/häuslicher Isolation: 136.575

Geheilte Personen insgesamt: 72.850 (+41)

Positiv getestete Mitarbeiter und Mitarbeiterinnen des Sanitätsbetriebes: 2.043, davon 1.681 geheilt. Positiv getestete Basis-, Kinderbasisärzte und Bereitschaftsärzte: 58, davon 46 geheilt. (Stand: 13.05.2021)

(PAS)

