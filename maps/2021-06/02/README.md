[covid-bz](../../../README.md) / [maps](../../README.md) / [2021-06](../../2021-06/README.md) / [02](../02)
 
# Mittwoch,  2. Juni 2021
 
In der Gemeinde **Ahrntal** werden sie weiterhin fündig. Im **Unterland** ist ein über 8.000 Einwohner **Covid-Free**-Gebiet entstanden, bei **Auer** Berg aufwärts. Im **Obervinschgau** ist die Lage noch nicht ganz ruhig. Bei **Klausen** auch nicht. Das Landesweitegeschehen ist immer mehr in wenigen Gemeinden konzentriert. Dank des heutigen Feiertag könnte die 50-er Grenze der **7-Tage-Inzidenz** schon mit den Daten, die morgen publiziert werden, erreicht sein.

<img src="CovidCartinaPositiviPcrAgUltimi7giorni-2021-06-02-1.jpg" alt="7-Tage-Inzidenz" width="50%" align="right">
 
Die **7-Tage-Inzidenz** (knapp über 50 auf 100.000) ist zwar weiterhin beim sinken,
aber in den letzten 5 Wochen im Durchschnitt nur um -2.1% täglich,
was die **50-er Marke in wenigen Tagen** "möglich" machen würde.


- [Die 7-Tage-Inzidenz](#die-7-tage-inzidenz)
- [Nasenflügeltests in den Schulen](#nasenflügeltests-in-den-schulen)
- [Die Lage in den Gemeinden](#die-lage-in-den-gemeinden)
- [Seniorenheime](#seniorenheime)
- [Links zu anderen Südtirolbezogene Webseiten](#links-zu-anderen-südtirolbezogene-webseiten)
- [Keep In Touch](#keep-in-touch)  _(feedback is welcome)_


## Die 7-Tage-Inzidenz


Die 7-Tage-Inzidenz (216 PCR + 66 AG) ist 53 auf 100.000.
Vor einer Woche (am 26.05.) war sie 56.
Also -0.8% täglich.
Bei genau _dieser_ Entschleunigung braucht es noch 1 Wochen um auf 50 zu kommen (9. Juni um "genau" zu sein).


## Nasenflügeltests in den Schulen 
_Hinweis: die von der SABES gelieferten Daten sind nicht ganz kohärent. Hier werden sie so wie die Daten gemeldet wurden wiedergegeben und verglichen._

> Nasenflügeltests in den Schulen (bis 01.06.2021): 677.855 Tests gesamt an 550 Schulen, 546 positive Ergebnisse, davon 258 bestätigt, 227 PCR-negativ, 61  ausständig/zu überprüfen

Auswertung von 677.855 (+107.344 mehr als [7 Tage davor](../../2021-05/26/README.md)) Nasenflügeltests 
 
davon waren zwar  546 (+103 in [7 Tagen](../../2021-05/26/README.md)) positiv (0,80 pro mille) aber
- nur 258 davon PCR-positiv (0,38 pro mille); +17
- 227 PCR-negativ (0,33 pro mille); +63
- 61 noch zu überprüfen; +32
 
Dies bedeutet, dass die Nasenflügeltests, im Durchschnitt 3,3 bis 4,2 **Falsch positive** auf 10.000 "real negativen" Personen produziert haben.

In diesen [7 Tagen](../../2021-05/26/README.md) gab es also 17/107.344 = 0,16 pro mille PCR-Positive-Tests und 0,59 pro mille Falsch-Positive-Test. Auf 75.000 Schüler gerechnet (sie machen meistens 2 Test pro Woche) sind es 0,22 pro mille PCR-Positiv.
 
_NB: Wie viele "real positive" bei diesen Test "falsch negativ" sind, kann man aus diesen Werten nicht herausrechnen._
 
## Die Lage in den Gemeinden
 
<img src="CovidCartinaPositiviPcrAgOggi-2021-06-02-1.jpg" alt="Cartina con comuni con nuovi positivi oggi" width="50%" align="right">
 
Heute (02.06.) wurden
+25 Positive mit Wohnsitz in Südtirol mitgeteilt, in 15 verschiedene Gemeinden (von 116).
In 62 Gemeinden (x mehr als [gestern](../01/README.md)) gibt es schon seit mindestens 7 Tagen keinen neuen Fall mehr,
in 24 davon sogar schon seit 21 Tagen oder mehr.
 
Bemerkenswert sind heute:
- **Ahrntal** (6.022 Einw.): **Heute = +5**, vorhergehende 6 Tage = +16, vorhergehende 14 Tage = +23, 7-Tage-Inzidenz = 300/100.000
- **Toblach** (3.351 Einw.): **Heute = +1**, vorhergehende 6 Tage = +7, vorhergehende 14 Tage = +15, 7-Tage-Inzidenz = 200/100.000

Einen Blick wert sind auch:
- **Sand in Taufers** (5.496 Einw.): **Heute = +2**, vorhergehende 6 Tage = +6, vorhergehende 14 Tage = +7, 7-Tage-Inzidenz = 150
- **Klausen** (5.215 Einw.): **Heute = +2**, vorhergehende 6 Tage = +10, vorhergehende 14 Tage = +28, 7-Tage-Inzidenz = 230
- **Schluderns** (1.838 Einw.): Heute = +0, vorhergehende 6 Tage = +6, vorhergehende 14 Tage = +5, 7-Tage-Inzidenz = **330**
- **Lüsen** (1.574 Einw.): **Heute = +1**, vorhergehende 6 Tage = +2, vorhergehende 14 Tage = +3, 7-Tage-Inzidenz = 190
- **Taufers im Münstertal** (969 Einw.): **Heute = +1**, vorhergehende 6 Tage = +5, vorhergehende 14 Tage = +16, 7-Tage-Inzidenz = **620**

<img src="CovidCartinaCovidFree-2021-06-02-1.jpg" alt="Cartina con comuni covid-free" width="50%" align="right">
 
Folgende 18 Gemeinden könnte man als **Covid-Free** betrachten, denn sie haben schon seit 21 Tagen keinen Fall mehr gehabt, niemand befindet sich in Quarantäne oder Isolation und die SABES zählt zur Zeit keinen "aktiv Positiven".

 
1. **Auer (3.825)** (zusammen mit Montan, Aldein und Truden sind es **8.222 Einwohner**)
1. St.Leonhard i.P. (3.558)
1. Tisens (1.965)
1. Montan (1.701)
1. Mölten (1.692)
1. Aldein (1.656)
1. Tscherms (1.527)
1. Wengen (1.389)
1. Riffian (1.362)
1. Rodeneck (1.240)
1. Schnals (1.228)
1. Truden im Naturpark (1.040)
1. Hafling (778)
1. U.L.Frau I.W.-St.Felix (762)
1. Plaus (724)
1. Kurtinig a.d.W. (665)
1. Prettau (546)
1. Waidbruck (195)
 
Weitere Informationen: 
- [PDF, Daten und Text](../02) - in tedesco e italiano, se non indicato diversamente
  - [tägliche Pressemitteilung](20210602.txt) - solo tedesco
  - Karten - Cartine con dettaglio comunale
    - [**Covid-freie** Gemeinden](CovidCartinaCovidFree-2021-06-02.pdf)
    - [Cartina con evidenziati i comuni con almeno un nuovo caso](CovidCartinaPositiviPcrAgOggi-2021-06-02.pdf)
    - [Summe der PCR- und AG-Positiven der letzten 7 Tagen](CovidCartinaPositiviPcrAgUltimi7giorni-2021-06-02.pdf)
    - [Summe der PCR- **und AG**-Positiven der letzten **21** Tagen](CovidCartinaPositiviPcrAgUltimi21giorni-2021-06-02.pdf)
    - [**Anstieg bzw. Rückgang** der PCR- und AG-Positiven der letzten 7 Tagen im Vergleich zu den letzten 21 Tagen](CovidCartinaAccelerazione7su21giorniPositiviPcrAgUltimi-2021-06-02.pdf)
    - [Persone trovate positive con test PCR rispetto tutti i positivi (PCR e AG). Ultimi 21 giorni](CovidCartinaPositivi21PctPcr-2021-06-02.pdf)
    - [Karte mit aktueller Quarantäne](CovidCartinaQuarantena-2021-06-02.pdf)
    - [Residenti messi in quarantena negli ultimi 7 giorni](CovidCartinaQuarantenaMandatiUltimi7giorni-2021-06-02.pdf)
    - [Persone con almento un test positivo (PCR o AG), da 1° ottobre in poi](CovidCartinaPositiviTotaleWelle2-2021-06-02.pdf)
  - [weiteres Material](../02)
 
 
## Seniorenheime
 
Für Details siehe 
- [Liste der suspekten Heime](Traueranzeigen-Seniorenheime-Hotspots-Welle2-20210602.txt)
- [Grafiken](Traueranzeigen-SeniorenheimeCumuliert_2020-05-01_to_2021-05-30.pdf)
 
 
# Links zu anderen Südtirolbezogene Webseiten
 
- Dashboard von [Markus Falk](https://www.markusfalk.com/)
  - [Version 1](https://www.markusfalk.com/dashboard/#lang=de&dset=ST&reg=all&tab=PRED&from=2020-08-11&to=true)
  - [Version 2](https://www.markusfalk.com/dashboard-beta/#lang=de&dset=ST_ALL&reg=all&tab=PRED&from=2020-08-11&to=true)
- Tabellen von [Moritz Mair](https://moritzmair.info/)
  - [Impfzahlen in Südtirol nach Alter](https://covvac.moritzmair.info/?tab=1)
- Ivan Sieder
  - [Interaktive Karten](https://map.corona-bz.simedia.cloud/)
- Zivilschutz Südtirol
  - Ohne Gemeindedetails
    - [Grafiken](http://www.provinz.bz.it/sicherheit-zivilschutz/zivilschutz/aktuelle-daten-zum-coronavirus.asp)
    - [dati provinciali con serie storica a partire da 15 marzo 2020](https://afbs.provinz.bz.it/upload/coronavirus/Corona.Data.Total.csv)
  - Mit Gemeindedetails
    - [Grafici e cartine con dettaglio comunale](http://www.provincia.bz.it/sicurezza-protezione-civile/protezione-civile/aktuelle-daten-zum-coronavirus-in-den-gemeinden.asp)
    - [Aktuelle Excel-Datei mit Positiven (PCR+AG)](https://afbs.provinz.bz.it/upload/coronavirus/CoronavirusPositivi.xlsx)
    - [Aktuelle Excel-Datei mit Quarantäne](https://afbs.provinz.bz.it/upload/coronavirus/CoronavirusQuarantaene.xlsx)
    - [dati comunali con serie storica a partire da 18 dicembre](https://afbs.provinz.bz.it/upload/coronavirus/Corona.Data.Detail.csv)
- [Protezione civile Italia](https://github.com/pcm-dpc/COVID-19/blob/master/README.md)
  - diffusione del Virus
    - [dati (CSV)](https://github.com/pcm-dpc/COVID-19/tree/master/dati-regioni)
    - [dati (JSON)](https://github.com/pcm-dpc/COVID-19/tree/master/dati-json/dpc-covid19-ita-regioni.json)
    - [scheda riepilogativa (PDF)](https://github.com/pcm-dpc/COVID-19/raw/master/schede-riepilogative/regioni/dpc-covid19-ita-scheda-regioni-latest.pdf)
  - [Vaccinazioni](https://github.com/italia/covid19-opendata-vaccini/tree/master/dati)
    - [dati provinciali per classi di età, serie storica (CSV)](https://github.com/italia/covid19-opendata-vaccini/blob/master/dati/somministrazioni-vaccini-latest.csv)
 
## Keep In Touch
 
- Mastodon: https://troet.cafe/@COVID_BZ_Statistics
- Matrix: https://matrix.to/#/@antoniogulino:matrix.org
