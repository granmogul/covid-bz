[covid-bz](../../../README.md) / [maps](../../README.md) / [2021-06](../../2021-06/README.md) / [03](../03)
 
# Donnerstag,  3. Juni 2021

**Die 50-er Marke der 7-Tage-Inzidenz auf 100.000 wurde erstmals unterschritten.** Und das **Ahrntal** und **Taufers im Münstertal** rudern weiterhin dagegen. Und ein Gebiet im **Unterland** mit **8.620** Einwohner (Auer, Montan, Aldein, Truden und Altrei) ist laut meiner strengen Definition **Covid-free**.

<img src="CovidCartinaPositiviPcrAgUltimi7giorni-2021-06-03-1.jpg" alt="7-Tage-Inzidenz" width="50%" align="right">
 
Die **7-Tage-Inzidenz** (um die 40 auf 100.000) ist zwar weiterhin beim sinken,
in den letzten 5 Wochen im Durchschnitt um -2.8% täglich. Heute wurde **erstmals**
die **50-er Marke** unterschritten.


- [Die 7-Tage-Inzidenz](#die-7-tage-inzidenz)
- [Nasenflügeltests in den Schulen](#nasenflügeltests-in-den-schulen)
- [Die Lage in den Gemeinden](#die-lage-in-den-gemeinden)
- [Seniorenheime](#seniorenheime)
- [Links zu anderen Südtirolbezogene Webseiten](#links-zu-anderen-südtirolbezogene-webseiten)
- [Keep In Touch](#keep-in-touch)  _(feedback is welcome)_


## Die 7-Tage-Inzidenz


Die 7-Tage-Inzidenz (184 PCR + 54 AG) ist 45 auf 100.000.
Vor einer Woche (am 27.05.) war sie 60.
Also -4.0% täglich.



## Nasenflügeltests in den Schulen 
_Hinweis: die von der SABES gelieferten Daten sind nicht ganz kohärent. Hier werden sie so wie die Daten gemeldet wurden wiedergegeben und verglichen._

> Nasenflügeltests in den Schulen (Stand 02.06.2021): 684.041 Tests gesamt an 550 Schulen, 550 positive Ergebnisse, davon 264 bestätigt, 239 PCR-negativ, 47 ausständig/zu überprüfen

Auswertung von 684.041 (+xx.xxx mehr als [gestern](../02/README.md)) Nasenflügeltests an xxx Schulen (-xxx)
 
davon waren zwar  550 (+xxx) positiv (x,xx pro mille) aber
- nur 264 davon PCR-positiv (+xxx) 
- 239 PCR-negativ (xxx) 
- 47 noch zu überprüfen (xxx)
 
Dies bedeutet, dass die Nasenflügeltests, im Durchschnitt x,x bis x,x **Falsch positive** auf 10.000 "real negativen" Personen produziert haben.
 
_Wie viele "real positive" bei diesen Test "falsch negativ" sind, kann man aus diesen Werten nicht ausrechnen._
 
Im Vergleich zu [gestern](../02/README.md) gab es +xx positive auf +xx.xxx, dies bedeutet 0,xx pro mille, also "_im Durchschnitt_" alle "**Falsch Positiv**".
 
## Die Lage in den Gemeinden
 
<img src="CovidCartinaPositiviPcrAgOggi-2021-06-03-1.jpg" alt="Cartina con comuni con nuovi positivi oggi" width="50%" align="right">
 
Heute (03.06.) wurden
+16 Positive mit Wohnsitz in Südtirol mitgeteilt, in 13 verschiedene Gemeinden (von 116).
In 64 Gemeinden (x mehr als [gestern](../02/README.md)) gibt es schon seit mindestens 7 Tagen keinen neuen Fall mehr,
in 25 davon sogar schon seit 21 Tagen oder mehr.

Trotz den wenigen Fällen bleibt bemerkenswert
- **Ahrntal** (6.022 Einw.): **Heute = +1**, vorhergehende 6 Tage = +20, vorhergehende 14 Tage = +23, 7-Tage-Inzidenz = 300/100.000"

Nicht viel besser dran stehen:
- **Sand in Taufers** (5.496 Einw.): **Heute = +1**, vorhergehende 6 Tage = +7, vorhergehende 14 Tage = +7, 7-Tage-Inzidenz = 150
- **Schluderns** (1.838 Einw.): Heute = +0, vorhergehende 6 Tage = +5, vorhergehende 14 Tage = +6, 7-Tage-Inzidenz = 270
- **Lüsen** (1.574 Einw.): **Heute = +1**, vorhergehende 6 Tage = +3, vorhergehende 14 Tage = +1, 7-Tage-Inzidenz = 250
- **Taufers im Münstertal** (969 Einw.): **Heute = +1**, vorhergehende 6 Tage = +2, vorhergehende 14 Tage = +19, **7-Tage-Inzidenz = 310**
 
<img src="CovidCartinaCovidFree-2021-06-03-1.jpg" alt="Cartina con comuni covid-free" width="50%" align="right">
 
Folgende 19 Gemeinden könnte man als **Covid-Free** betrachten, denn sie haben schon seit 21 Tagen keinen Fall mehr gehabt, niemand befindet sich in Quarantäne oder Isolation und die SABES zählt zur Zeit keinen "aktiv Positiven".
 
1. **Auer** (3.825), zusammen mit Montan, Aldein, Truden und Altrei sind es **8.620 Einwohner**
1. St.Leonhard i.P. (3.558)
1. Tisens (1.965)
1. Montan (1.701)
1. Mölten (1.692)
1. Aldein (1.656)
1. Tscherms (1.527)
1. Wengen (1.389)
1. Riffian (1.362)
1. Rodeneck (1.240)
1. Truden im Naturpark (1.040)
1. **Franzensfeste** (1.000)
1. Hafling (778)
1. U.L.Frau I.W.-St.Felix (762)
1. Plaus (724)
1. Kurtinig a.d.W. (665)
1. Prettau (546)
1. **Altrei** (398)
1. Waidbruck (195)
 
Weitere Informationen: 
- [PDF, Daten und Text](../03) - in tedesco e italiano, se non indicato diversamente
  - [tägliche Pressemitteilung](20210603.txt) - solo tedesco
  - Karten - Cartine con dettaglio comunale
    - [**Covid-freie** Gemeinden](CovidCartinaCovidFree-2021-06-03.pdf)
    - [Cartina con evidenziati i comuni con almeno un nuovo caso](CovidCartinaPositiviPcrAgOggi-2021-06-03.pdf)
    - [Summe der PCR- und AG-Positiven der letzten 7 Tagen](CovidCartinaPositiviPcrAgUltimi7giorni-2021-06-03.pdf)
    - [Summe der PCR- **und AG**-Positiven der letzten **21** Tagen](CovidCartinaPositiviPcrAgUltimi21giorni-2021-06-03.pdf)
    - [**Anstieg bzw. Rückgang** der PCR- und AG-Positiven der letzten 7 Tagen im Vergleich zu den letzten 21 Tagen](CovidCartinaAccelerazione7su21giorniPositiviPcrAgUltimi-2021-06-03.pdf)
    - [Persone trovate positive con test PCR rispetto tutti i positivi (PCR e AG). Ultimi 21 giorni](CovidCartinaPositivi21PctPcr-2021-06-03.pdf)
    - [Karte mit aktueller Quarantäne](CovidCartinaQuarantena-2021-06-03.pdf)
    - [Residenti messi in quarantena negli ultimi 7 giorni](CovidCartinaQuarantenaMandatiUltimi7giorni-2021-06-03.pdf)
    - [Persone con almento un test positivo (PCR o AG), da 1° ottobre in poi](CovidCartinaPositiviTotaleWelle2-2021-06-03.pdf)
  - [weiteres Material](../03)
 
 
## Seniorenheime
 
Für Details siehe 
- [Liste der suspekten Heime](Traueranzeigen-Seniorenheime-Hotspots-Welle2-20210603.txt)
- [Grafiken](Traueranzeigen-SeniorenheimeCumuliert_2020-05-01_to_2021-05-31.pdf)
 
 
# Links zu anderen Südtirolbezogene Webseiten
 
- Dashboard von [Markus Falk](https://www.markusfalk.com/)
  - [Version 1](https://www.markusfalk.com/dashboard/#lang=de&dset=ST&reg=all&tab=PRED&from=2020-08-11&to=true)
  - [Version 2](https://www.markusfalk.com/dashboard-beta/#lang=de&dset=ST_ALL&reg=all&tab=PRED&from=2020-08-11&to=true)
- Tabellen von [Moritz Mair](https://moritzmair.info/)
  - [Impfzahlen in Südtirol nach Alter](https://covvac.moritzmair.info/?tab=1)
- Ivan Sieder
  - [Interaktive Karten](https://map.corona-bz.simedia.cloud/)
- Zivilschutz Südtirol
  - Ohne Gemeindedetails
    - [Grafiken](http://www.provinz.bz.it/sicherheit-zivilschutz/zivilschutz/aktuelle-daten-zum-coronavirus.asp)
    - [dati provinciali con serie storica a partire da 15 marzo 2020](https://afbs.provinz.bz.it/upload/coronavirus/Corona.Data.Total.csv)
  - Mit Gemeindedetails
    - [Grafici e cartine con dettaglio comunale](http://www.provincia.bz.it/sicurezza-protezione-civile/protezione-civile/aktuelle-daten-zum-coronavirus-in-den-gemeinden.asp)
    - [Aktuelle Excel-Datei mit Positiven (PCR+AG)](https://afbs.provinz.bz.it/upload/coronavirus/CoronavirusPositivi.xlsx)
    - [Aktuelle Excel-Datei mit Quarantäne](https://afbs.provinz.bz.it/upload/coronavirus/CoronavirusQuarantaene.xlsx)
    - [dati comunali con serie storica a partire da 18 dicembre](https://afbs.provinz.bz.it/upload/coronavirus/Corona.Data.Detail.csv)
- [Protezione civile Italia](https://github.com/pcm-dpc/COVID-19/blob/master/README.md)
  - diffusione del Virus
    - [dati (CSV)](https://github.com/pcm-dpc/COVID-19/tree/master/dati-regioni)
    - [dati (JSON)](https://github.com/pcm-dpc/COVID-19/tree/master/dati-json/dpc-covid19-ita-regioni.json)
    - [scheda riepilogativa (PDF)](https://github.com/pcm-dpc/COVID-19/raw/master/schede-riepilogative/regioni/dpc-covid19-ita-scheda-regioni-latest.pdf)
  - [Vaccinazioni](https://github.com/italia/covid19-opendata-vaccini/tree/master/dati)
    - [dati provinciali per classi di età, serie storica (CSV)](https://github.com/italia/covid19-opendata-vaccini/blob/master/dati/somministrazioni-vaccini-latest.csv)
 
## Keep In Touch
 
- Mastodon: https://troet.cafe/@COVID_BZ_Statistics
- Matrix: https://matrix.to/#/@antoniogulino:matrix.org
