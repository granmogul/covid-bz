[covid-bz](../README.md) / [maps](../maps)

# MAPS

## Directory
- Cartine (pdf) con i dati comunali trovati nei quotidiani file Excel e analisi dei necrologi
  - [Giugno 2021](2021-06/)
  - [Maggio 2021](2021-05/README.md)
  - [Aprile 2021](2021-04/README.md)
  - [Marzo 2021](2021-03/README.md)
  - [Febbraio 2021](2021-02/README.md)
  - [Gennaio 2021](2021-01/README.md)
  - [Dicembre 2020](2020-12/README.md)
- Cartine (pdf) con i dati comunali trovati nei quotidiani file Excel
  - Lunedi, [30 novembre 2020](2020-11-30)
  - Domenica, [29 novembre 2020](2020-11-29)
  - Sabato, [28 novembre 2020](2020-11-28)
  - Venerdi, [27 novembre 2020](2020-11-27)
  - Giovedi, [26 novembre 2020](2020-11-26)
- Cartine (pdf) con i dati comunali  trovati nei quotidiani file Excel con i test PCR e nei file csv con i dati del test di massa  di novembre
  - Mercoledi, [25 novembre 2020](2020-11-25) (test di massa; test PCR)
  - Martedi, [24 novembre 2020](2020-11-24) (test di massa; test PCR)
  - Lunedi, [23 novembre 2020](2020-11-23) (test di massa; test PCR)
  - Domenica, [22 novembre 2020](2020-11-22) (test di massa; test PCR)
- Cartine (pdf) con i dati comunali trovati nei quotidiani file Excel
  - Sabato, [21 novembre 2020](2020-11-21)
  - fino al [20 novembre 2020](maps)


## PDF - descrizione

Si trovano i seguenti **file PDF**, che hanno tutti nel nome del file la data nel formato `-YYYY-MM-DD`. Se sono presenti gli allegati con i **tre elenchi dei comuni**, questo sono rispettivamente: alfabetico tedesco, alfabetico italiano, per valore dell'indicatore. Negli elenchi viene indicato il valore dell'indicatore, il numeratore e il denominatore.
- con i dati comunali trovati nei quotidiani file Excel, che fanno riferimento a **test PCR**
  - `CovidCartinaQuarantena` - **Numero casi di QUARANTENE/ISOLAMENTI IN CORSO ogni 100 residenti**
  - `CovidCartinaPositivi` - **Numero casi di POSITIVI ANCORA ATTIVI ogni 100 residenti**
  - `CovidCartinaAccelerazioneBisettimanalePositivi` - **Incremento test positivi (somma di due settimane)**
  - `CovidCartinaPositiviUltimi21giorni` - **Somma dei nuovi positivi negli ultimi 21 giorni ogni 100 residenti**
    - con allegati i tre elenchi dei comuni
- con i dati del **test di massa** di novembre. Nel nome del file è compreso pure l'aggiornamento (10h, 16h, 20h) nel formato `-YYYY-MM-DD_HHh`
  - `MassentestCartinaPositiviPostazione` - **Percentuale test positivi. Comune di effettuazione test**
  - `MassentestCartinaPositiviResidenza` - **Percentuale test positivi. Comune di residenza**
  - `MassentestCartinaPositiviPostazione_no_MMG_PLS` - **Percentuale test positivi. Comune di effettuazione test. Esclusi test presso medici di base, pediatri, ...**
    - con allegati i tre elenchi dei comuni
  - `MassentestCartinaTestatiResidenza` - **Percentuale residenti testati** - divide il numero di persone testate per comune di residenza, con il totale persone residenti in quel comune.
    - con allegati i tre elenchi dei comuni
- entrambe le fonti
  - `CovidCartinaPositiviPcrMassentestResidenza` - **Test di massa AND Positivi PCR ancora attivi. Positivi ogni 100 residenti. Comune di residenza**

## Keep In Touch

- Mastodon: <a rel="me" href="https://troet.cafe/@COVID_BZ_Statistics">https://troet.cafe/@COVID_BZ_Statistics</a> 
- Telegram: https://t.me/COVID_BZ_Statistics


