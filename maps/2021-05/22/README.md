[covid-bz](../../../README.md) / [maps](../../README.md) / [2021-05](../../2021-05/README.md) / [22](../22)
 
# Samstag, 22. Mai 2021
 
<img src="CovidCartinaPositiviPcrAgUltimi7giorni-2021-05-22-1.jpg" alt="7-Tage-Inzidenz" width="50%" align="right">
 
Die **7-Tage-Inzidenz** (um die 70 auf 100.000) ist zwar weiterhin beim sinken,
aber in den letzten 5 Wochen im Durchschnitt nur um -1.5% täglich,
was die **50-er Marke erst in 3 Wochen** "möglich" machen würde.


- [Die 7-Tage-Inzidenz](#die-7-tage-inzidenz)
- [Nasenflügeltests in den Schulen](#nasenflügeltests-in-den-schulen)
- [Die Lage in den Gemeinden](#die-lage-in-den-gemeinden)
- [Seniorenheime](#seniorenheime)
- [Links zu anderen Südtirolbezogene Webseiten](#links-zu-anderen-südtirolbezogene-webseiten)
- [Keep In Touch](#keep-in-touch)  _(feedback is welcome)_


## Die 7-Tage-Inzidenz


Die 7-Tage-Inzidenz (241 PCR + 116 AG) ist 67 auf 100.000.
Vor einer Woche (am 15.05.) war sie 82.
Also -2.8% täglich.
Bei genau _dieser_ Entschleunigung braucht es noch 2 Wochen um auf 50 zu kommen (1. Juni um "genau" zu sein).


## Nasenflügeltests in den Schulen 
_Hinweis: die von der SABES gelieferten Daten sind nicht ganz kohärent. Hier werden sie so wie die Daten gemeldet wurden wiedergegeben und verglichen._
 
> Nasenflügeltests in den Schulen (bis 21.05.2021) 558.950 Tests gesamt an 544 Schulen, 436 positive Ergebnisse, davon 237 bestätigt, 146 PCR-negativ, 53 ausständig/zu überprüfen



Auswertung von 558.950 (+29.301 mehr als [gestern](../21/README.md)) Nasenflügeltests an an 544 Schulen 
 
davon waren zwar  436 (+20) positiv (x,xx pro mille) aber
- nur 237 davon PCR-positiv (+7) 
- 146 PCR-negativ (+5) 
-  53 noch zu überprüfen (+8)
 
Dies bedeutet, dass die Nasenflügeltests, im Durchschnitt 2,6 bis 3,6 **Falsch positive** auf 10.000 "real negativen" Personen produziert haben.
 
_Wie viele "real positive" bei diesen Test "falsch negativ" sind, kann man aus diesen Werten nicht ausrechnen._

 
## Die Lage in den Gemeinden
 
<img src="CovidCartinaPositiviPcrAgOggi-2021-05-22-1.jpg" alt="Cartina con comuni con nuovi positivi oggi" width="50%" align="right">
 
Heute (22.05.) wurden
+43 Positive mit Wohnsitz in Südtirol mitgeteilt, in 18 verschiedene Gemeinden (von 116).
In 50 Gemeinden (1 mehr als [gestern](../21/README.md)) gibt es schon seit mindestens 7 Tagen keinen neuen Fall mehr,
in 19 davon (2 mehr als [gestern](../21/README.md)) sogar schon seit 21 Tagen oder mehr.
 
Besonders negativ waren heute
- **Gais** (3.311 Einw.): **Heute = +3**, vorhergehende 6 Tage = +7, vorhergehende 14 Tage = +12, 7-Tage-Inzidenz = **300**/100.000       
- **Natz-Schabs** (3.230 Einw.): **Heute = +1, vorhergehende 6 Tage = +24**, vorhergehende 14 Tage = +18, 7-Tage-Inzidenz = **800**/100.000

Nicht viel besser sind:
- **Klausen** (5.215 Einw.): **Heute = +6**, vorhergehende 6 Tage = +6, vorhergehende 14 Tage = +15, 7-Tage-Inzidenz = 230/100.000
- **Terenten** (1.766 Einw.): Heute = +0, vorhergehende 6 Tage = +4, vorhergehende 14 Tage = +6, 7-Tage-Inzidenz = 230/100.000
- **Prags** (655 Einw.): Heute = +0, vorhergehende 6 Tage = +4, vorhergehende 14 Tage = +1, 7-Tage-Inzidenz = **610**/100.000

<img src="CovidCartinaCovidFree-2021-05-22-1.jpg" alt="Cartina con comuni covid-free" width="50%" align="right">
 
Folgende 15 Gemeinden könnte man als **Covid-Free** betrachten, denn sie haben schon seit 21 Tagen keinen Fall mehr gehabt, niemand befindet sich in Quarantäne oder Isolation und die SABES zählt zur Zeit keinen "aktiv Positiven".
 
1. Kurtasch a.d.W. (2.239)
1. Tisens (1.965)
1. Montan (1.701)
1. Mölten (1.692)
1. Rodeneck (1.240)
1. Schnals (1.228)
1. Pfatten (1.057)
1. Andrian (1.030)
1. Hafling (778)
1. Plaus (724)
1. Kurtinig a.d.W. (665)
1. Altrei (398)
1. Kuens (395)
1. Proveis (265)
1. Waidbruck (195)
 
Weitere Informationen: 
- [PDF, Daten und Text](../22) - in tedesco e italiano, se non indicato diversamente
  - [tägliche Pressemitteilung](20210522.txt) - solo tedesco
  - Karten - Cartine con dettaglio comunale
    - [**Covid-freie** Gemeinden](CovidCartinaCovidFree-2021-05-22.pdf)
    - [Cartina con evidenziati i comuni con almeno un nuovo caso](CovidCartinaPositiviPcrAgOggi-2021-05-22.pdf)
    - [Summe der PCR- und AG-Positiven der letzten 7 Tagen](CovidCartinaPositiviPcrAgUltimi7giorni-2021-05-22.pdf)
    - [Summe der PCR- **und AG**-Positiven der letzten **21** Tagen](CovidCartinaPositiviPcrAgUltimi21giorni-2021-05-22.pdf)
    - [**Anstieg bzw. Rückgang** der PCR- und AG-Positiven der letzten 7 Tagen im Vergleich zu den letzten 21 Tagen](CovidCartinaAccelerazione7su21giorniPositiviPcrAgUltimi-2021-05-22.pdf)
    - [Persone trovate positive con test PCR rispetto tutti i positivi (PCR e AG). Ultimi 21 giorni](CovidCartinaPositivi21PctPcr-2021-05-22.pdf)
    - [Karte mit aktueller Quarantäne](CovidCartinaQuarantena-2021-05-22.pdf)
    - [Residenti messi in quarantena negli ultimi 7 giorni](CovidCartinaQuarantenaMandatiUltimi7giorni-2021-05-22.pdf)
    - [Persone con almento un test positivo (PCR o AG), da 1° ottobre in poi](CovidCartinaPositiviTotaleWelle2-2021-05-22.pdf)
  - [weiteres Material](../22)
 
 
## Seniorenheime
 
Für Details siehe 
- [Liste der suspekten Heime](Traueranzeigen-Seniorenheime-Hotspots-Welle2-20210522.txt)
- [Grafiken](Traueranzeigen-SeniorenheimeCumuliert_2020-05-01_to_2021-05-19.pdf)
 
 
# Links zu anderen Südtirolbezogene Webseiten
 
- Dashboard von [Markus Falk](https://www.markusfalk.com/)
  - [Version 1](https://www.markusfalk.com/dashboard/#lang=de&dset=ST&reg=all&tab=PRED&from=2020-08-11&to=true)
  - [Version 2](https://www.markusfalk.com/dashboard-beta/#lang=de&dset=ST_ALL&reg=all&tab=PRED&from=2020-08-11&to=true)
- Tabellen von [Moritz Mair](https://moritzmair.info/)
  - [Impfzahlen in Südtirol nach Alter](https://covvac.moritzmair.info/?tab=1)
- Ivan Sieder
  - [Interaktive Karten](https://map.corona-bz.simedia.cloud/)
- Zivilschutz Südtirol
  - Ohne Gemeindedetails
    - [Grafiken](http://www.provinz.bz.it/sicherheit-zivilschutz/zivilschutz/aktuelle-daten-zum-coronavirus.asp)
    - [dati provinciali con serie storica a partire da 15 marzo 2020](https://afbs.provinz.bz.it/upload/coronavirus/Corona.Data.Total.csv)
  - Mit Gemeindedetails
    - [Grafici e cartine con dettaglio comunale](http://www.provincia.bz.it/sicurezza-protezione-civile/protezione-civile/aktuelle-daten-zum-coronavirus-in-den-gemeinden.asp)
    - [Aktuelle Excel-Datei mit Positiven (PCR+AG)](https://afbs.provinz.bz.it/upload/coronavirus/CoronavirusPositivi.xlsx)
    - [Aktuelle Excel-Datei mit Quarantäne](https://afbs.provinz.bz.it/upload/coronavirus/CoronavirusQuarantaene.xlsx)
    - [dati comunali con serie storica a partire da 18 dicembre](https://afbs.provinz.bz.it/upload/coronavirus/Corona.Data.Detail.csv)
- [Protezione civile Italia](https://github.com/pcm-dpc/COVID-19/blob/master/README.md)
  - diffusione del Virus
    - [dati (CSV)](https://github.com/pcm-dpc/COVID-19/tree/master/dati-regioni)
    - [dati (JSON)](https://github.com/pcm-dpc/COVID-19/tree/master/dati-json/dpc-covid19-ita-regioni.json)
    - [scheda riepilogativa (PDF)](https://github.com/pcm-dpc/COVID-19/raw/master/schede-riepilogative/regioni/dpc-covid19-ita-scheda-regioni-latest.pdf)
  - [Vaccinazioni](https://github.com/italia/covid19-opendata-vaccini/tree/master/dati)
    - [dati provinciali per classi di età, serie storica (CSV)](https://github.com/italia/covid19-opendata-vaccini/blob/master/dati/somministrazioni-vaccini-latest.csv)
 
## Keep In Touch
 
- Mastodon: https://troet.cafe/@COVID_BZ_Statistics
- Matrix: https://matrix.to/#/@antoniogulino:matrix.org
