https://www.sabes.it/de/news.asp?aktuelles_action=4&aktuelles_article_id=656082

Coronavirus – Mitteilung des Sanitätsbetriebes | 25.05.2021 | 10:26

In den letzten 24 Stunden wurden 290 PCR-Tests untersucht und dabei 7 Neuinfektionen festgestellt. Zusätzlich gab es 13 positive Antigentests.

Coronavirus: 7 Neuinfektionen durch PCR-Test und 13 positive Antigentests

Bisher (25. Mai) wurden insgesamt 573.267 Abstriche untersucht, die von 216.606 Personen stammen.

Die Zahlen im Überblick:

PCR-Abstriche:

Untersuchte Abstriche gestern (24. Mai): 290

Mittels PCR-Test neu positiv getestete Personen: 7

Gesamtzahl der mittels PCR-Test positiv getesteten Personen: 48.115

Gesamtzahl der untersuchten Abstriche: 573.267

Gesamtzahl der mit Abstrichen getesteten Personen: 216.606 (+82)

Antigentests:

Gesamtzahl der durchgeführten Antigentests: 1.469.003

Gesamtzahl der positiven Antigentests: 26.046

Durchgeführte Antigentests gestern: 1.648

Mittels Antigentest neu positiv getestete Personen: 13

Nasenflügeltests bis 24.05.2021 (ohne Schulen) 429.893 Tests, davon 594 positiv

Nasenflügeltests in den Schulen (bis 22.05.2021)570.511 Tests gesamt an 544 Schulen, 443 positive Ergebnisse, davon 238 bestätigt, 157 PCR-negativ, 48  ausständig/zu überprüfen

Weitere Daten:

Auf Normalstationen im Krankenhaus untergebrachte Covid-19-Patienten/-Patientinnen: 15

In Privatkliniken untergebrachte Covid-19-Patienten/-Patientinnen (post-akut bzw. aus Seniorenwohnheimen übernommen): 8

In Gossensaß und Sarns untergebrachte Covid-19-Patienten/-Patientinnen: 1

Anzahl der auf Intensivstationen aufgenommenen Covid-Patienten/Patientinnen: 5 (davon 4 als ICU-Covid klassifiziert) Gesamtzahl der mit Covid-19 Verstorbenen: 1.175 (+0)

Personen in Quarantäne/häuslicher Isolation: 1.573

Personen, die Quarantäne/häusliche Isolation beendet haben: 134.308

Personen betroffen von verordneter Quarantäne/häuslicher Isolation: 135.881

Geheilte Personen insgesamt: 72.225 (+98)

Positiv getestete Mitarbeiter und Mitarbeiterinnen des Sanitätsbetriebes: 2.043, davon 1.681 geheilt. Positiv getestete Basis-, Kinderbasisärzte und Bereitschaftsärzte: 58, davon 46 geheilt. (Stand: 13.05.2021)

(TDB)

