[covid-bz](../../../README.md) / [maps](../../README.md) / [2021-05](../../2021-05/README.md) / [16](../16)
 
# Sonntag, 16. Mai 2021
 
<img src="CovidCartinaPositiviPcrAgUltimi7giorni-2021-05-16-1.jpg" alt="7-Tage-Inzidenz" width="50%" align="right">
 
Die **7-Tage-Inzidenz** (um die 80 auf 100.000) ist zwar weiterhin beim sinken,
aber in den letzten 7 Wochen im Durchschnitt nur um -1.4% täglich,
was die **50-er Marke erst in 5 Wochen** "möglich" machen würde.

Selbst bei niedrigen Zahlen wie heute, schafft es das Pärchen **Stilfs**, **Taufers im Münstertal** im **Obervinschgau** auffällig zu sein, mit je +2. Die 7-Tage-Inzidenz vom Obervinschgau ist jetzt schon seit 3 Tagen auf einen Niveau, das es schon seit Mitte März nicht mehr gab.

Am Eingang des **Pustertales** ist **Natz-Schabs** (+2), das gleich neben dem vor kurzem abgeschlossenen(?) Hotspot von Meransen/**Mühlbach** (+1) liegt, auch weiterhin heiss. Innerhalb 6 Tagen wurden in Natz-Schabs 15 Fälle gefunden (In Mühlbach wurden innerhalb von 2 Wochen 70 Fälle gefunden, aber das ist schon ein Monat her).

- [Die 7-Tage-Inzidenz](#die-7-tage-inzidenz)
- [Die Lage in den Gemeinden](#die-lage-in-den-gemeinden)
- [Nasenflügeltests in den Schulen](#nasenflügeltests-in-den-schulen)
- [Seniorenheime](#seniorenheime)
- [Links zu anderen Südtirolbezogene Webseiten](#links-zu-anderen-südtirolbezogene-webseiten)
- [Keep In Touch](#keep-in-touch)  _(feedback is welcome)_


## Die 7-Tage-Inzidenz


Die 7-Tage-Inzidenz (300 PCR + 113 AG) ist 78 auf 100.000.
Vor einer Woche (am 09.05.) war sie 88.
Also -1.8% täglich.
Bei genau _dieser_ Entschleunigung braucht es noch 3 Wochen um auf 50 zu kommen (9. Juni um "genau" zu sein).



 
## Die Lage in den Gemeinden
 
<img src="CovidCartinaPositiviPcrAgOggi-2021-05-16-1.jpg" alt="Cartina con comuni con nuovi positivi oggi" width="50%" align="right">
 
Heute (16.05.) wurden
+40 Positive mit Wohnsitz in Südtirol mitgeteilt, in 23 verschiedene Gemeinden (von 116).
In 37 Gemeinden (2 mehr als [gestern](../15/README.md)) gibt es schon seit mindestens 7 Tagen keinen neuen Fall mehr,
in 17 davon (gleich viele wie [gestern](../15/README.md)) sogar schon seit 21 Tagen oder mehr.
 
Bemerkenswerte Gemeinden sind heute
- **Natz-Schabs** (3.230 Einw.): Heute = +2, vorhergehende 6 Tage = +13, vorhergehende 14 Tage = +6, 7-Tage-Inzidenz = 500/100.000       
- **Stilfs** (1.141 Einw.): Heute = +2, vorhergehende 6 Tage = +6, vorhergehende 14 Tage = +13, 7-Tage-Inzidenz = 700/100.000       
- **Taufers im Münstertal** (969 Einw.): Heute = +2, vorhergehende 6 Tage = +7, vorhergehende 14 Tage = +10, 7-Tage-Inzidenz = 900/100.000


Nenneswert ist auch **Sexten** (1.880 Einw.): Heute = +1


<img src="CovidCartinaCovidFree-2021-05-16-1.jpg" alt="Cartina con comuni covid-free" width="50%" align="right">
 
Folgende 14 Gemeinden (3 mehr als [gestern](../15/README.md)) könnte man als **Covid-Free** betrachten, denn sie haben schon seit 21 Tagen keinen Fall mehr gehabt, niemand befindet sich in Quarantäne oder Isolation und die SABES zählt zur Zeit keinen "aktiv Positiven".
 
1. Kurtasch a.d.W. (2.239), letzter positiver Fall am 21. April
1. **Brenner** (2.232), letzter positiver Fall am 22. April
1. **Tisens** (1.965), letzter positiver Fall am 19. April
1. Mölten (1.692), letzter positiver Fall am 6. April
1. Magreid a.d.W. (1.274), letzter positiver Fall am 15. April
1. **Schnals** (1.228), letzter positiver Fall am 27. März
1. Pfatten (1.057), letzter positiver Fall am 13. April
1. Andrian (1.030), letzter positiver Fall am 4. April
1. Hafling (778), letzter positiver Fall am 18. April
1. Plaus (724), letzter positiver Fall am 22. April
1. Altrei (398), letzter positiver Fall am 1. April
1. Kuens (395), letzter positiver Fall am 10. März
1. Proveis (265), letzter positiver Fall am 3. März
1. Waidbruck (195), letzter positiver Fall am 6. Februar

Weitere Informationen: 
- [PDF, Daten und Text](../16) - in tedesco e italiano, se non indicato diversamente
  - [tägliche Pressemitteilung](20210516.txt) - solo tedesco
  - Karten - Cartine con dettaglio comunale
    - [**Covid-freie** Gemeinden](CovidCartinaCovidFree-2021-05-16.pdf)
    - [Cartina con evidenziati i comuni con almeno un nuovo caso](CovidCartinaPositiviPcrAgOggi-2021-05-16.pdf)
    - [Summe der PCR- und AG-Positiven der letzten 7 Tagen](CovidCartinaPositiviPcrAgUltimi7giorni-2021-05-16.pdf)
    - [Summe der PCR- **und AG**-Positiven der letzten **21** Tagen](CovidCartinaPositiviPcrAgUltimi21giorni-2021-05-16.pdf)
    - [**Anstieg bzw. Rückgang** der PCR- und AG-Positiven der letzten 7 Tagen im Vergleich zu den letzten 21 Tagen](CovidCartinaAccelerazione7su21giorniPositiviPcrAgUltimi-2021-05-16.pdf)
    - [Persone trovate positive con test PCR rispetto tutti i positivi (PCR e AG). Ultimi 21 giorni](CovidCartinaPositivi21PctPcr-2021-05-16.pdf)
    - [Karte mit aktueller Quarantäne](CovidCartinaQuarantena-2021-05-16.pdf)
    - [Residenti messi in quarantena negli ultimi 7 giorni](CovidCartinaQuarantenaMandatiUltimi7giorni-2021-05-16.pdf)
    - [Persone con almento un test positivo (PCR o AG), da 1° ottobre in poi](CovidCartinaPositiviTotaleWelle2-2021-05-16.pdf)
  - [weiteres Material](../16)
 
## Nasenflügeltests in den Schulen 
_Hinweis: die von der SABES gelieferten Daten sind nicht ganz kohärent. Hier werden sie so wie die Daten gemeldet wurden wiedergegeben und verglichen._

> Nasenflügeltests in den Schulen (bis 15.05.2021) 478.593 Tests gesamt an 548 Schulen, 386 positive Ergebnisse, davon 214 bestätigt, 126 PCR-negativ, 46 ausständig/zu überprüfen



Auswertung von 478.593 (+348 mehr als [gestern](../15/README.md)) Nasenflügeltests an 548 Schulen (+5)
 
davon waren zwar  386 (+8) positiv (0,81 pro mille) aber
- nur davon 214 davon PCR-positiv (+4) 
-  126 PCR-negativ (+/-0) 
-  46 noch zu überprüfen (+4)
 
Dies bedeutet, dass die Nasenflügeltests, im Durchschnitt 2,6 bis 3,6 **Falsch positive** auf 10.000 "real negativen" Personen produziert haben.
 
_Wie viele "real positive" bei diesen Test "falsch negativ" sind, kann man aus diesen Werten nicht ausrechnen._
 



## Seniorenheime
 
Für Details siehe 
- [Liste der suspekten Heime](Traueranzeigen-Seniorenheime-Hotspots-Welle2-20210516.txt)
- [Grafiken](Traueranzeigen-SeniorenheimeCumuliert_2020-05-01_to_2021-05-13.pdf)
 
 
# Links zu anderen Südtirolbezogene Webseiten
 
- Dashboard von [Markus Falk](https://www.markusfalk.com/)
  - [Version 1](https://www.markusfalk.com/dashboard/#lang=de&dset=ST&reg=all&tab=PRED&from=2020-08-11&to=true)
  - [Version 2](https://www.markusfalk.com/dashboard-beta/#lang=de&dset=ST_ALL&reg=all&tab=PRED&from=2020-08-11&to=true)
- Tabellen von [Moritz Mair](https://moritzmair.info/)
  - [Impfzahlen in Südtirol nach Alter](https://covvac.moritzmair.info/?tab=1)
- Ivan Sieder
  - [Interaktive Karten](https://map.corona-bz.simedia.cloud/)
- Zivilschutz Südtirol
  - Ohne Gemeindedetails
    - [Grafiken](http://www.provinz.bz.it/sicherheit-zivilschutz/zivilschutz/aktuelle-daten-zum-coronavirus.asp)
    - [dati provinciali con serie storica a partire da 15 marzo 2020](https://afbs.provinz.bz.it/upload/coronavirus/Corona.Data.Total.csv)
  - Mit Gemeindedetails
    - [Grafici e cartine con dettaglio comunale](http://www.provincia.bz.it/sicurezza-protezione-civile/protezione-civile/aktuelle-daten-zum-coronavirus-in-den-gemeinden.asp)
    - [Aktuelle Excel-Datei mit Positiven (PCR+AG)](https://afbs.provinz.bz.it/upload/coronavirus/CoronavirusPositivi.xlsx)
    - [Aktuelle Excel-Datei mit Quarantäne](https://afbs.provinz.bz.it/upload/coronavirus/CoronavirusQuarantaene.xlsx)
    - [dati comunali con serie storica a partire da 18 dicembre](https://afbs.provinz.bz.it/upload/coronavirus/Corona.Data.Detail.csv)
- [Protezione civile Italia](https://github.com/pcm-dpc/COVID-19/blob/master/README.md)
  - diffusione del Virus
    - [dati (CSV)](https://github.com/pcm-dpc/COVID-19/tree/master/dati-regioni)
    - [dati (JSON)](https://github.com/pcm-dpc/COVID-19/tree/master/dati-json/dpc-covid19-ita-regioni.json)
    - [scheda riepilogativa (PDF)](https://github.com/pcm-dpc/COVID-19/raw/master/schede-riepilogative/regioni/dpc-covid19-ita-scheda-regioni-latest.pdf)
  - [Vaccinazioni](https://github.com/italia/covid19-opendata-vaccini/tree/master/dati)
    - [dati provinciali per classi di età, serie storica (CSV)](https://github.com/italia/covid19-opendata-vaccini/blob/master/dati/somministrazioni-vaccini-latest.csv)
 
## Keep In Touch
 
- Mastodon: https://troet.cafe/@COVID_BZ_Statistics
- Matrix: https://matrix.to/#/@antoniogulino:matrix.org
