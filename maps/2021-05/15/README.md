[covid-bz](../../../README.md) / [maps](../../README.md) / [2021-05](../../2021-05/README.md) / [15](../15)
 
# Samstag, 15. Mai 2021

Im Oberen Vinschgau gab es heute wieder +4 Fälle und wieder waren es Taufers im Münstertal (+3) und Stilfs (+1). In beiden gleichgroßen Gemeinden gab es 8 Fälle in 7 Tagen: 0,8% der Wohnbevölkerung.

Das Gebiet im Unteren Eisacktal meldet auch heute +7 Fälle.

<img src="CovidCartinaPositiviPcrAgUltimi7giorni-2021-05-15-1.jpg" alt="7-Tage-Inzidenz" width="50%" align="right">
 
Die **7-Tage-Inzidenz** (um die 80 auf 100.000) ist zwar weiterhin beim sinken,
aber in den letzten 9 Wochen im Durchschnitt nur um -1.6% täglich,
was die **50-er Marke erst in 4 Wochen** "möglich" machen würde.


- [Die 7-Tage-Inzidenz](#die-7-tage-inzidenz)
- [Nasenflügeltests in den Schulen](#nasenflügeltests-in-den-schulen)
- [Die Lage in den Gemeinden](#die-lage-in-den-gemeinden)
- [Seniorenheime](#seniorenheime) heute mit Info über Covid-Tote in St. Pauls (Eppan)
- [Links zu anderen Südtirolbezogene Webseiten](#links-zu-anderen-südtirolbezogene-webseiten)
- [Keep In Touch](#keep-in-touch)  _(feedback is welcome)_


## Die 7-Tage-Inzidenz


Die 7-Tage-Inzidenz (314 PCR + 121 AG) ist 82 auf 100.000.
Vor einer Woche (am 08.05.) war sie 88.
Also -1.0% täglich.
Bei genau _dieser_ Entschleunigung braucht es noch 7 Wochen um auf 50 zu kommen (4. Juli um "genau" zu sein).


## Nasenflügeltests in den Schulen 
_Hinweis: die von der SABES gelieferten Daten sind nicht ganz kohärent. Hier werden sie so wie die Daten gemeldet wurden wiedergegeben und verglichen._
 
> Nasenflügeltests in den Schulen (bis 14.05.2021)478.245 Tests gesamt an 543 Schulen, 378 positive Ergebnisse, davon 210 bestätigt, 126 PCR-negativ, 42 ausständig/zu überprüfen



Auswertung von 478.245 (+31.999 mehr als [gestern](../14/README.md)) Nasenflügeltests an 543 Schulen (+6)
 
davon waren zwar  378 (+10) positiv (0,79 pro mille) aber
- nur davon 210 davon PCR-positiv (+3) 
-  126 PCR-negativ (+1) 
-  42 noch zu überprüfen (+6)
 
Dies bedeutet, dass die Nasenflügeltests, im Durchschnitt 2,6 bis 3,5 **Falsch positive** auf 10.000 "real negativen" Personen produziert haben.
 
_Wie viele "real positive" bei diesen Test "falsch negativ" sind, kann man aus diesen Werten nicht ausrechnen._

 
## Die Lage in den Gemeinden
 
<img src="CovidCartinaPositiviPcrAgOggi-2021-05-15-1.jpg" alt="Cartina con comuni con nuovi positivi oggi" width="50%" align="right">
 
Heute (15.05.) wurden
+48 Positive mit Wohnsitz in Südtirol mitgeteilt, in 20 verschiedene Gemeinden (von 116).
In 35 Gemeinden (1 weniger als [gestern](../14/README.md)) gibt es schon seit mindestens 7 Tagen keinen neuen Fall mehr,
in 17 davon (1 weniger als [gestern](../14/README.md)) sogar schon seit 21 Tagen oder mehr.
 
Bemerkenswert sind heute:
- **Natz-Schabs** (3.230 Einw.): Heute = +1, vorhergehende 6 Tage = +12, vorhergehende 14 Tage = +7, 7-Tage-Inzidenz = 400/100.000       
- **Stilfs** (1.141 Einw.): Heute = +1, vorhergehende 6 Tage = +7, vorhergehende 14 Tage = +11, 7-Tage-Inzidenz = 700/100.000             
- **Taufers im Münstertal** (969 Einw.): Heute = +3, vorhergehende 6 Tage = +5, vorhergehende 14 Tage = +10, 7-Tage-Inzidenz = 800/100.000

Nicht viel besser steht es in den Gemeinden **Vintl** (3.339 Einw., heute = +2), **Sexten** (1.880 Einw.) und **Laurein** (342 Einw., heute = +1)



<img src="CovidCartinaCovidFree-2021-05-15-1.jpg" alt="Cartina con comuni covid-free" width="50%" align="right">
 
Folgende 11 Gemeinden (die selben wie [gestern](../14/README.md) und [vorgestern](../13/README.md)) könnte man als **Covid-Free** betrachten, denn sie haben schon seit 21 Tagen keinen Fall mehr gehabt, niemand befindet sich in Quarantäne oder Isolation und die SABES zählt zur Zeit keinen "aktiv Positiven".
 
1. Kurtasch a.d.W. (2.239), letzter positiver Fall am 21. April
1. Mölten (1.692), letzter positiver Fall am 6. April
1. Magreid a.d.W. (1.274), letzter positiver Fall am 15. April
1. Pfatten (1.057), letzter positiver Fall am 13. April
1. Andrian (1.030), letzter positiver Fall am 4. April
1. Hafling (778), letzter positiver Fall am 18. April
1. Plaus (724), letzter positiver Fall am 22. April
1. Altrei (398), letzter positiver Fall am 1. April
1. Kuens (395), letzter positiver Fall am 10. März
1. Proveis (265), letzter positiver Fall am 3. März
1. Waidbruck (195), letzter positiver Fall am 6. Februar
 
Weitere Informationen: 
- [PDF, Daten und Text](../15) - in tedesco e italiano, se non indicato diversamente
  - [tägliche Pressemitteilung](20210515.txt) - solo tedesco
  - Karten - Cartine con dettaglio comunale
    - [**Covid-freie** Gemeinden](CovidCartinaCovidFree-2021-05-15.pdf)
    - [Cartina con evidenziati i comuni con almeno un nuovo caso](CovidCartinaPositiviPcrAgOggi-2021-05-15.pdf)
    - [Summe der PCR- und AG-Positiven der letzten 7 Tagen](CovidCartinaPositiviPcrAgUltimi7giorni-2021-05-15.pdf)
    - [Summe der PCR- **und AG**-Positiven der letzten **21** Tagen](CovidCartinaPositiviPcrAgUltimi21giorni-2021-05-15.pdf)
    - [**Anstieg bzw. Rückgang** der PCR- und AG-Positiven der letzten 7 Tagen im Vergleich zu den letzten 21 Tagen](CovidCartinaAccelerazione7su21giorniPositiviPcrAgUltimi-2021-05-15.pdf)
    - [Persone trovate positive con test PCR rispetto tutti i positivi (PCR e AG). Ultimi 21 giorni](CovidCartinaPositivi21PctPcr-2021-05-15.pdf)
    - [Karte mit aktueller Quarantäne](CovidCartinaQuarantena-2021-05-15.pdf)
    - [Residenti messi in quarantena negli ultimi 7 giorni](CovidCartinaQuarantenaMandatiUltimi7giorni-2021-05-15.pdf)
    - [Persone con almento un test positivo (PCR o AG), da 1° ottobre in poi](CovidCartinaPositiviTotaleWelle2-2021-05-15.pdf)
  - [weiteres Material](../15)
 
 
## Seniorenheime

Der Direktor vom **Seniorenheim St. Pauls in Eppan (94 Betten) bestätigt 16-Covid-Tote** seit Beginn der Pandemie ([Alto Adige](https://www.altoadige.it/cronaca/bolzano/case-di-riposo-allarme-personale-in-600-rifiutano-la-vaccinazione-1.2912756))
> [...] dice Erwin Lorenzini, direttore della casa di riposo di San Paolo -[...] da noi su 80 operatori addetti all’assistenza diretta degli anziani circa 21 non vogliano farsi vaccinare. 
> [...] anche se dovrebbe essere sufficiente ricordare che in un anno abbiamo avuto 16 ospiti portati via dal covid

Mit meiner Methode hatte ich für den Seniorenheim St.Pauls in Eppan für den **Zeitraum 28. März 2020 bis 19. April 2020 (23 Tage)** 15 Totesfälle gezählt, davon 3 mit direkten Bezug auf #Covid als Ursache

Und, immer mit meiner Methode, scheint auf, dass ein halbes Dutzend Heime (auf 77 Insgesamt) noch mehr Corona-Tote hatten (bei größeren aber auch bei kleineren Heime)

Für Details über die allgemeine Lage in Südtirols Seniorenheime siehe 
- [Liste der suspekten Heime](Traueranzeigen-Seniorenheime-Hotspots-Welle2-20210515.txt)
- [Grafiken](Traueranzeigen-SeniorenheimeCumuliert_2020-05-01_to_2021-05-12.pdf)
 
 
# Links zu anderen Südtirolbezogene Webseiten
 
- Dashboard von [Markus Falk](https://www.markusfalk.com/)
  - [Version 1](https://www.markusfalk.com/dashboard/#lang=de&dset=ST&reg=all&tab=PRED&from=2020-08-11&to=true)
  - [Version 2](https://www.markusfalk.com/dashboard-beta/#lang=de&dset=ST_ALL&reg=all&tab=PRED&from=2020-08-11&to=true)
- Tabellen von [Moritz Mair](https://moritzmair.info/)
  - [Impfzahlen in Südtirol nach Alter](https://covvac.moritzmair.info/?tab=1)
- Ivan Sieder
  - [Interaktive Karten](https://map.corona-bz.simedia.cloud/)
- Zivilschutz Südtirol
  - Ohne Gemeindedetails
    - [Grafiken](http://www.provinz.bz.it/sicherheit-zivilschutz/zivilschutz/aktuelle-daten-zum-coronavirus.asp)
    - [dati provinciali con serie storica a partire da 15 marzo 2020](https://afbs.provinz.bz.it/upload/coronavirus/Corona.Data.Total.csv)
  - Mit Gemeindedetails
    - [Grafici e cartine con dettaglio comunale](http://www.provincia.bz.it/sicurezza-protezione-civile/protezione-civile/aktuelle-daten-zum-coronavirus-in-den-gemeinden.asp)
    - [Aktuelle Excel-Datei mit Positiven (PCR+AG)](https://afbs.provinz.bz.it/upload/coronavirus/CoronavirusPositivi.xlsx)
    - [Aktuelle Excel-Datei mit Quarantäne](https://afbs.provinz.bz.it/upload/coronavirus/CoronavirusQuarantaene.xlsx)
    - [dati comunali con serie storica a partire da 18 dicembre](https://afbs.provinz.bz.it/upload/coronavirus/Corona.Data.Detail.csv)
- [Protezione civile Italia](https://github.com/pcm-dpc/COVID-19/blob/master/README.md)
  - diffusione del Virus
    - [dati (CSV)](https://github.com/pcm-dpc/COVID-19/tree/master/dati-regioni)
    - [dati (JSON)](https://github.com/pcm-dpc/COVID-19/tree/master/dati-json/dpc-covid19-ita-regioni.json)
    - [scheda riepilogativa (PDF)](https://github.com/pcm-dpc/COVID-19/raw/master/schede-riepilogative/regioni/dpc-covid19-ita-scheda-regioni-latest.pdf)
  - [Vaccinazioni](https://github.com/italia/covid19-opendata-vaccini/tree/master/dati)
    - [dati provinciali per classi di età, serie storica (CSV)](https://github.com/italia/covid19-opendata-vaccini/blob/master/dati/somministrazioni-vaccini-latest.csv)
 
## Keep In Touch
 
- Mastodon: https://troet.cafe/@COVID_BZ_Statistics
- Matrix: https://matrix.to/#/@antoniogulino:matrix.org
