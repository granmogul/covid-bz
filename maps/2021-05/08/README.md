[covid-bz](../../../README.md) / [maps](../../README.md) / [2021-05](../../2021-05/README.md) / [08](../08)
 
# Samstag,  8. Mai 2021
 

In **Mühlbach** bricht die Serie weiterhin nicht ab und im kleinen **Stilfs** gibt es letzthin auch täglich einen Fall, wie auch in **Gsies**, **Sand in Taufers** und in **Taufers im Münstertal**. **Ahrntal** hat zwar immer wieder Fälle,  aber scheint sich zu beruhigen, dafür meldet sich letzthin **Mühlwald** öfters und lauter. Zu den zwei kleinen Gemeinden an der Schweizer Grenze - Taufers im Münstertal und Stilfs - hat sich heute **Glurns** gesellt.

Die **7-Tage-Inzidenz** (um die 90 auf 100.000) ist zwar weiterhin beim sinken, aber in den letzten 9 Wochen im Durchschnitt nur um -1.9% täglich,
was die **50-er Marke erst in 4 Wochen** "möglich" machen würde.

- [Die 7-Tage-Inzidenz](#die-7-tage-inzidenz)
- [Nasenflügeltests in den Schulen](#nasenfl203MaiCgeltests-in-den-schulen)
- [Die Lage in den Gemeinden](#die-lage-in-den-gemeinden)
- [Seniorenheime](#seniorenheime) **<- heute mit Bericht**
- [Links zu anderen Südtirolbezogene Webseiten](#links-zu-anderen-s203MaiCdtirolbezogene-webseiten)
- [Keep In Touch](#keep-in-touch)  _(feedback is welcome)_


## Die 7-Tage-Inzidenz

<img src="CovidCartinaPositiviPcrAgUltimi7giorni-2021-05-08-1.jpg" alt="7-Tage-Inzidenz" width="50%" align="right">

 
Die 7-Tage-Inzidenz (307 PCR + 159 AG) ist **88 auf 100.000**.
Vor einer Woche (am 01.05.) war sie 98.
Also -1.6% täglich.
Bei genau _dieser_ Entschleunigung braucht es _noch genau 35 Tage um auf 50_ zu kommen (11. Juni).


## Nasenflügeltests in den Schulen 
_Hinweis: die von der SABES gelieferten Daten sind nicht ganz kohärent. Hier werden sie so wie die Daten gemeldet wurden wiedergegeben und verglichen._
 
> Nasenflügeltests in den Schulen (bis 07.05.2021)392.533 Tests gesamt an 533 Schulen, 350 positive Ergebnisse, davon 177 PCR-positiv, 118 PCR-negativ, 43 ausständig/zu überprüfen



Auswertung von 392.533 (+34.755 mehr als [gestern](../07/README.md)) Nasenflügeltests an 533 Schulen (+1)
 
davon waren zwar  350 (+20) positiv (0,89 pro mille) aber
- nur 177 davon PCR-positiv (-12) 
- 118 PCR-negativ (-3) 
-  43 noch zu überprüfen (+23)
 
Dies bedeutet, dass die Nasenflügeltests, im Durchschnitt 3,0 bis 4,1 **Falsch positive** auf 10.000 "real negativen" Personen produziert haben.
 
_Wie viele "real positive" bei diesen Test "falsch negativ" sind, kann man aus diesen Werten nicht ausrechnen._
 
 
## Die Lage in den Gemeinden
 
<img src="CovidCartinaPositiviPcrAgOggi-2021-05-08-1.jpg" alt="Cartina con comuni con nuovi positivi oggi" width="50%" align="right">
 
Heute (08.05.) wurden
+51 Positive mit Wohnsitz in Südtirol mitgeteilt, in 25 verschiedene Gemeinden (von 116).
In 39 Gemeinden (1 mehr als [gestern](../07/README.md)) gibt es schon seit mindestens 7 Tagen keinen neuen Fall mehr,
in 13 davon (1 weniger als [gestern](../07/README.md)) sogar schon seit 21 Tagen oder mehr.
 
Besonders negativ fallen heute auf:
- **Gsies** (2.320 Einw.): Heute = +4, vorhergehende 6 Tage = +12, vorhergehende 14 Tage = +20, 7-Tage-Inzidenz = 700/100.000
- **Stilfs** (1.141 Einw.): Heute = +1, vorhergehende 6 Tage = +6, vorhergehende 14 Tage = +4, 7-Tage-Inzidenz = 600/100.000
- **Taufers im Münstertal** (969 Einw.): Heute = +1, vorhergehende 6 Tage = +4, vorhergehende 14 Tage = +6, 7-Tage-Inzidenz = 500/100.000

Nicht viel besser sind: **Mühlbach** (3.151 Einw.) und **Burgstall** (1.872 Einw.)



<img src="CovidCartinaCovidFree-2021-05-08-1.jpg" alt="Cartina con comuni covid-free" width="50%" align="right">
 
Folgende 11 Gemeinden (1 weniger als [gestern](../07/README.md)) könnte man als **Covid-Free** betrachten, denn sie haben schon seit 21 Tagen keinen Fall mehr gehabt, niemand befindet sich in Quarantäne oder Isolation und die SABES zählt zur Zeit keinen "aktiv Positiven".
 
1. Marling (2.791), letzter positiver Fall am 24. März
1. Freienfeld (2.656), letzter positiver Fall am 12. April
1. Schluderns (1.838), letzter positiver Fall am 20. März
1. Mölten (1.692), letzter positiver Fall am 6. April
1. Magreid a.d.W. (1.274), letzter positiver Fall am 15. April
1. Pfatten (1.057), letzter positiver Fall am 13. April
1. Andrian (1.030), letzter positiver Fall am 4. April
1. Altrei (398), letzter positiver Fall am 1. April
1. Kuens (395), letzter positiver Fall am 10. März
1. Proveis (265), letzter positiver Fall am 3. März
1. Waidbruck (195), letzter positiver Fall am 6. Februar
- <del>_Glurns (900)_</del>, heute +1 Fall, vorletzter positiver Fall war am 2. April

Weitere Informationen: 
- [PDF, Daten und Text](../08) - in tedesco e italiano, se non indicato diversamente
  - [tägliche Pressemitteilung](20210508.txt) - solo tedesco
  - Karten - Cartine con dettaglio comunale
    - [**Covid-freie** Gemeinden](CovidCartinaCovidFree-2021-05-08.pdf)
    - [Cartina con evidenziati i comuni con almeno un nuovo caso](CovidCartinaPositiviPcrAgOggi-2021-05-08.pdf)
    - [Summe der PCR- und AG-Positiven der letzten 7 Tagen](CovidCartinaPositiviPcrAgUltimi7giorni-2021-05-08.pdf)
    - [Summe der PCR- **und AG**-Positiven der letzten **21** Tagen](CovidCartinaPositiviPcrAgUltimi21giorni-2021-05-08.pdf)
    - [**Anstieg bzw. Rückgang** der PCR- und AG-Positiven der letzten 7 Tagen im Vergleich zu den letzten 21 Tagen](CovidCartinaAccelerazione7su21giorniPositiviPcrAgUltimi-2021-05-08.pdf)
    - [Persone trovate positive con test PCR rispetto tutti i positivi (PCR e AG). Ultimi 21 giorni](CovidCartinaPositivi21PctPcr-2021-05-08.pdf)
    - [Karte mit aktueller Quarantäne](CovidCartinaQuarantena-2021-05-08.pdf)
    - [Residenti messi in quarantena negli ultimi 7 giorni](CovidCartinaQuarantenaMandatiUltimi7giorni-2021-05-08.pdf)
    - [Persone con almento un test positivo (PCR o AG), da 1° ottobre in poi](CovidCartinaPositiviTotaleWelle2-2021-05-08.pdf)
  - [weiteres Material](../08)
 
 
## Seniorenheime

- Es gab innerhalb von 4 Tagen 3 Trauerfälle mit Bezug auf den Seniorenheim **Lajen**, das 42 Betten hat.
- Innerhalb von 4 Wochen gab es auch 3 Trauerfälle im  **Ojöp Frëinademetz von St. Martin i. T.**, mit 90 Betten, aber hatte diesen Winter schon 15 Todesfälle. AUf jeden Fall, wird seit heute "Ojöp Frëinademetz" wieder aufgelistet.
- Und ebenfalls innerhalb von 4 Wochen gab es 5 Trauerfälle im **Martinsheim von Mals**, das zwar 83 Betten hätte, aber von Oktober bis Januar schon 20 Todesfälle hatte.
- 2 Todesfälle im kleinen (24 Betten) **"Claraheim" in Steinegg (Karneid)**, am selben Tag vor 2 Wochen, sind auch "interessant".

Auch weitere Seniorenheime haben eine eher erhöhte Sterberate seit dem 1. März: z.B. **Schlanders** und **Niederdorf**.

Insgesamt über alle Heime finden sich in den letzten 10 Wochen 5% mehr Traueranzeigen als im Sommer.
Wäre eigentlich nicht viel, aber in den Heimen haben sie jetzt auch um die 15% weniger Betreute als im Sommer.

Dass Insgesamt pro Woche, trotz geringerer Bettenauslastung, die Traueranzeigen pro Woche im Durchschnitt höher sind als im Sommer, trotz aller Maßnahmen, deutet auf jeden Fall auf eine allgemeine "Unregelmäßigkeit" hin. Dank Impfung sollte es aber keine derart große "Feuer" geben, dass man sie als solche wahrnehmen kann. **Partschins** war, hoffentlich, die Ausnahme.


Für Details siehe 
- [Liste der suspekten Heime](Traueranzeigen-Seniorenheime-Hotspots-Welle2-20210508.txt)
- [Grafiken](Traueranzeigen-SeniorenheimeCumuliert_2020-05-01_to_2021-05-05.pdf)
 
 
# Links zu anderen Südtirolbezogene Webseiten
 
- Dashboard von [Markus Falk](https://www.markusfalk.com/)
  - [Version 1](https://www.markusfalk.com/dashboard/#lang=de&dset=ST&reg=all&tab=PRED&from=2020-08-11&to=true)
  - [Version 2](https://www.markusfalk.com/dashboard-beta/#lang=de&dset=ST_ALL&reg=all&tab=PRED&from=2020-08-11&to=true)
- Tabellen von [Moritz Mair](https://moritzmair.info/)
  - [Impfzahlen in Südtirol nach Alter](https://covvac.moritzmair.info/?tab=1)
- Ivan Sieder
  - [Interaktive Karten](https://map.corona-bz.simedia.cloud/)
- Zivilschutz Südtirol
  - Ohne Gemeindedetails
    - [Grafiken](http://www.provinz.bz.it/sicherheit-zivilschutz/zivilschutz/aktuelle-daten-zum-coronavirus.asp)
    - [dati provinciali con serie storica a partire da 15 marzo 2020](https://afbs.provinz.bz.it/upload/coronavirus/Corona.Data.Total.csv)
  - Mit Gemeindedetails
    - [Grafici e cartine con dettaglio comunale](http://www.provincia.bz.it/sicurezza-protezione-civile/protezione-civile/aktuelle-daten-zum-coronavirus-in-den-gemeinden.asp)
    - [Aktuelle Excel-Datei mit Positiven (PCR+AG)](https://afbs.provinz.bz.it/upload/coronavirus/CoronavirusPositivi.xlsx)
    - [Aktuelle Excel-Datei mit Quarantäne](https://afbs.provinz.bz.it/upload/coronavirus/CoronavirusQuarantaene.xlsx)
    - [dati comunali con serie storica a partire da 18 dicembre](https://afbs.provinz.bz.it/upload/coronavirus/Corona.Data.Detail.csv)
- [Protezione civile Italia](https://github.com/pcm-dpc/COVID-19/blob/master/README.md)
  - diffusione del Virus
    - [dati (CSV)](https://github.com/pcm-dpc/COVID-19/tree/master/dati-regioni)
    - [dati (JSON)](https://github.com/pcm-dpc/COVID-19/tree/master/dati-json/dpc-covid19-ita-regioni.json)
    - [scheda riepilogativa (PDF)](https://github.com/pcm-dpc/COVID-19/raw/master/schede-riepilogative/regioni/dpc-covid19-ita-scheda-regioni-latest.pdf)
  - [Vaccinazioni](https://github.com/italia/covid19-opendata-vaccini/tree/master/dati)
    - [dati provinciali per classi di età, serie storica (CSV)](https://github.com/italia/covid19-opendata-vaccini/blob/master/dati/somministrazioni-vaccini-latest.csv)
 
## Keep In Touch
 
- Mastodon: https://troet.cafe/@COVID_BZ_Statistics
- Matrix: https://matrix.to/#/@antoniogulino:matrix.org
