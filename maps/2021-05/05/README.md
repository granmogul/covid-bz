[covid-bz](../../../README.md) / [maps](../../README.md) / [2021-05](../../2021-05/README.md) / [05](../05)
# Mittwoch,  5. Mai 2021

Es sieht so aus, als hätte man die Verbreitung in **Mühlbach**, **Gsies** und **Welsberg-Taisten** nicht im Griff. Gais, Sand in Taufers und Sexten, die nicht weit entfernt von diesen zwei letzten Gemeinden liegen, werden auch leicht auffällig.

Die 7-Tage-Inzidenz (um die 100 auf 100.000) ist zwar weiterhin beim sinken, aber in den letzten Wochen im Durchschnitt nur um -1,0% täglich (die Gastronomie hat erst seit einer Woche wieder offen), was die 50-er Marke erst für den Sommer "möglich" macht.

- [Die 7-Tage-Inzidenz](#die-7-tage-inzidenz)
- [Nasenflügeltests in den Schulen](#nasenfl%C3%BCgeltests-in-den-schulen)
- [Die Lage in den Gemeinden](#die-lage-in-den-gemeinden)
- [Seniorenheime](#seniorenheime)
- [Links zu anderen Südtirolbezogene Webseiten](#links-zu-anderen-s%C3%BCdtirolbezogene-webseiten)
- [Keep In Touch](#keep-in-touch)

## Die 7-Tage-Inzidenz

<img src="CovidCartinaPositiviPcrAgUltimi7giorni-2021-05-05-1.jpg" alt="7-Tage-Inzidenz" width="50%" align="right">

Die 7-Tage-Inzidenz (335 PCR + 188 AG) ist 99 auf 100.000. Vor einer Woche (am 28.4.) war sie 111. Also -1,6% täglich. Bei genau dieser Entschleunigung braucht es noch genau 42 Tage um auf 50 zu kommen (16. Juni).

## Nasenflügeltests in den Schulen 
_Hinweis: die von der SABES gelieferten Daten sind nicht ganz kohärent. Hier werden sie so wie die Daten gemeldet wurden wiedergegeben und verglichen._

Auswertung von 347.469 (+35.804 mehr als [gestern](../04/README.md)) Nasenflügeltests an 528 Schulen (-8)

davon waren zwar  329 (+10) positiv (0,95 pro mille) aber
- nur 172 PCR-positiv (+11) 
- 116 PCR-negativ (+5) 
- 41 noch zu überprüfen (-6)

Dies bedeutet, dass die Nasenflügeltests, im Durchschnitt 3,3 bis 4,5 **Falsch positive** auf 10.000 "real negativen" Personen produziert haben.

Wie viele "real positive" bei diesen Test "falsch negativ" sind, kann man aus diesen Werten nicht ausrechnen.

Im Vergleich zu [gestern](../04/README.md) gab es +10 positive auf +35.804, dies bedeutet 0,28 pro mille, also "_im Durchschnitt_" alle "**Falsch Positiv**".

## Die Lage in den Gemeinden

<img src="CovidCartinaPositiviPcrAgOggi-2021-05-05-1.jpg" alt="Cartina con comuni con nuovi positivi oggi" width="50%" align="right">
 

Heute (05.05.) wurden
+75 Positive mit Wohnsitz in Südtirol mitgeteilt, in 31 verschiedene Gemeinden (von 116).
In 40 Gemeinden (1 mehr als [gestern](../04/README.md)) gibt es schon seit mindestens 7 Tagen keinen neuen Fall mehr,
in 15 davon (gleich viele wie [gestern](../04/README.md)) sogar schon seit 21 Tagen oder mehr.
 
Besonders negativ fallen heute auf:
- **Mühlbach** (3.151 Einw.): Heute = **+4**, vorhergehende 6 Tage = +15, vorhergehende 14 Tage = +24, 7-Tage-Inzidenz = 600/100.000
- **Gsies** (2.320 Einw.): Heute = **+2**, vorhergehende 6 Tage = +14, vorhergehende 14 Tage = +17, 7-Tage-Inzidenz = 700/100.000
- **Stilfs** (1.141 Einw.): Heute = **+1**, vorhergehende 6 Tage = +4, vorhergehende 14 Tage = +4, 7-Tage-Inzidenz = 400/100.000
- **Taufers im Münstertal** (969 Einw.): Heute = **+2**, vorhergehende 6 Tage = +2, vorhergehende 14 Tage = +6, 7-Tage-Inzidenz = 400/100.000
- **U.L.Frau I.W.-St.Felix** (762 Einw.): Heute = **+3**, vorhergehende 6 Tage = +2, vorhergehende 14 Tage = +8, 7-Tage-Inzidenz = 700/100.000

Nicht viel besser sind: **Gais** (3.311 Einw.), **Sexten** (1.880 Einw.) und **Truden** im Naturpark (1.040 Einw.)
 
<img src="CovidCartinaCovidFree-2021-05-05-1.jpg" alt="Cartina con comuni covid-free" width="50%" align="right">
 
Folgende 10 Gemeinden (1 mehr als [gestern](../04/README.md)) könnte man als **Covid-Free** betrachten, denn sie haben schon seit 21 Tagen keinen Fall mehr gehabt, niemand befindet sich in Quarantäne oder Isolation und die SABES zählt zur Zeit keinen "aktiv Positiven".
 
1. **Abtei** (3.505), letzter positiver Fall am 3. April
1. Marling (2.791), letzter positiver Fall am 24. März
1. Schluderns (1.838), letzter positiver Fall am 20. März
1. Pfatten (1.057), letzter positiver Fall am 13. April
1. Andrian (1.030), letzter positiver Fall am 4. April
1. Glurns (900), letzter positiver Fall am 2. April
1. Altrei (398), letzter positiver Fall am 1. April
1. Kuens (395), letzter positiver Fall am 10. März
1. Proveis (265), letzter positiver Fall am 3. März
1. Waidbruck (195), letzter positiver Fall am 6. Februar
 
Weitere Informationen: 
- [PDF, Daten und Text](../05) - in tedesco e italiano, se non indicato diversamente
  - [tägliche Pressemitteilung](20210505.txt) - solo tedesco
  - Karten - Cartine con dettaglio comunale
    - [**Covid-freie** Gemeinden](CovidCartinaCovidFree-2021-05-05.pdf)
    - [Cartina con evidenziati i comuni con almeno un nuovo caso](CovidCartinaPositiviPcrAgOggi-2021-05-05.pdf)
    - [Summe der PCR- und AG-Positiven der letzten 7 Tagen](CovidCartinaPositiviPcrAgUltimi7giorni-2021-05-05.pdf)
    - [Summe der PCR- **und AG**-Positiven der letzten **21** Tagen](CovidCartinaPositiviPcrAgUltimi21giorni-2021-05-05.pdf)
    - [**Anstieg bzw. Rückgang** der PCR- und AG-Positiven der letzten 7 Tagen im Vergleich zu den letzten 21 Tagen](CovidCartinaAccelerazione7su21giorniPositiviPcrAgUltimi-2021-05-05.pdf)
    - [Persone trovate positive con test PCR rispetto tutti i positivi (PCR e AG). Ultimi 21 giorni](CovidCartinaPositivi21PctPcr-2021-05-05.pdf)
    - [Karte mit aktueller Quarantäne](CovidCartinaQuarantena-2021-05-05.pdf)
    - [Residenti messi in quarantena negli ultimi 7 giorni](CovidCartinaQuarantenaMandatiUltimi7giorni-2021-05-05.pdf)
    - [Persone con almento un test positivo (PCR o AG), da 1° ottobre in poi](CovidCartinaPositiviTotaleWelle2-2021-05-05.pdf)
  - [weiteres Material](../05)
 
 
## Seniorenheime
 
Für Details siehe 
- [Liste der suspekten Heime](Traueranzeigen-Seniorenheime-Hotspots-Welle2-20210505.txt)
- [Grafiken](Traueranzeigen-SeniorenheimeCumuliert_2020-05-01_to_2021-05-02.pdf)
 
 
# Links zu anderen Südtirolbezogene Webseiten
 
- Dashboard von [Markus Falk](https://www.markusfalk.com/)
  - [Version 1](https://www.markusfalk.com/dashboard/#lang=de&dset=ST&reg=all&tab=PRED&from=2020-08-11&to=true)
  - [Version 2](https://www.markusfalk.com/dashboard-beta/#lang=de&dset=ST_ALL&reg=all&tab=PRED&from=2020-08-11&to=true)
- Tabellen von [Moritz Mair](https://moritzmair.info/)
  - [Impfzahlen in Südtirol nach Alter](https://covvac.moritzmair.info/?tab=1)
- Ivan Sieder
  - [Interaktive Karten](https://map.corona-bz.simedia.cloud/)
- Zivilschutz Südtirol
  - Ohne Gemeindedetails
    - [Grafiken](http://www.provinz.bz.it/sicherheit-zivilschutz/zivilschutz/aktuelle-daten-zum-coronavirus.asp)
    - [dati provinciali con serie storica a partire da 15 marzo 2020](https://afbs.provinz.bz.it/upload/coronavirus/Corona.Data.Total.csv)
  - Mit Gemeindedetails
    - [Grafici e cartine con dettaglio comunale](http://www.provincia.bz.it/sicurezza-protezione-civile/protezione-civile/aktuelle-daten-zum-coronavirus-in-den-gemeinden.asp)
    - [Aktuelle Excel-Datei mit Positiven (PCR+AG)](https://afbs.provinz.bz.it/upload/coronavirus/CoronavirusPositivi.xlsx)
    - [Aktuelle Excel-Datei mit Quarantäne](https://afbs.provinz.bz.it/upload/coronavirus/CoronavirusQuarantaene.xlsx)
    - [dati comunali con serie storica a partire da 18 dicembre](https://afbs.provinz.bz.it/upload/coronavirus/Corona.Data.Detail.csv)
- [Protezione civile Italia](https://github.com/pcm-dpc/COVID-19/blob/master/README.md)
  - diffusione del Virus
    - [dati (CSV)](https://github.com/pcm-dpc/COVID-19/tree/master/dati-regioni)
    - [dati (JSON)](https://github.com/pcm-dpc/COVID-19/tree/master/dati-json/dpc-covid19-ita-regioni.json)
    - [scheda riepilogativa (PDF)](https://github.com/pcm-dpc/COVID-19/raw/master/schede-riepilogative/regioni/dpc-covid19-ita-scheda-regioni-latest.pdf)
  - [Vaccinazioni](https://github.com/italia/covid19-opendata-vaccini/tree/master/dati)
    - [dati provinciali per classi di età, serie storica (CSV)](https://github.com/italia/covid19-opendata-vaccini/blob/master/dati/somministrazioni-vaccini-latest.csv)
 
## Keep In Touch
 
- Mastodon: https://troet.cafe/@COVID_BZ_Statistics
- Matrix: https://matrix.to/#/@antoniogulino:matrix.org
