[covid-bz](../../../README.md) / [maps](../../README.md) / [2021-05](../../2021-05/README.md) / [09](../09)
 
# Sonntag,  9. Mai 2021
 
<img src="CovidCartinaPositiviPcrAgUltimi7giorni-2021-05-09-1.jpg" alt="7-Tage-Inzidenz" width="50%" align="right">

In **Mühlbach** bricht die Serie weiterhin nicht ab, **Gsies** und **Sexten** werden auch immer "lauter" und **Olang** hat vielleicht auch etwas "besonderes". Auf der ganz anderen Seite vom Land, in  **Taufers im Münstertal** und **Stilfs**, werden auch (fast) täglich neue Fälle gefunden. **Schluderns**, **Mals** und **Glurns** sind vielleicht jetzt auch involviert, aber zur Zeit nur mit je einen Fall.

Die **7-Tage-Inzidenz** (um die 90 auf 100.000) ist zwar weiterhin beim sinken,
aber in den letzten 9 Wochen im Durchschnitt nur um -1.8% täglich,
was die **50-er Marke erst in 4 Wochen** "möglich" machen würde.


- [Die 7-Tage-Inzidenz](#die-7-tage-inzidenz)
- [Die Lage in den Gemeinden](#die-lage-in-den-gemeinden)
- [Seniorenheime](#seniorenheime)
- [Links zu anderen Südtirolbezogene Webseiten](#links-zu-anderen-südtirolbezogene-webseiten)
- [Keep In Touch](#keep-in-touch)  _(feedback is welcome)_


## Die 7-Tage-Inzidenz


Die 7-Tage-Inzidenz (302 PCR + 167 AG) ist 88 auf 100.000.
Vor einer Woche (am 02.05.) war sie 97.
Also -1.3% täglich.
Bei genau _dieser_ Entschleunigung braucht es noch 6 Wochen um auf 50 zu kommen (22. Juni um "genau" zu sein).


## Die Lage in den Gemeinden
 
<img src="CovidCartinaPositiviPcrAgOggi-2021-05-09-1.jpg" alt="Cartina con comuni con nuovi positivi oggi" width="50%" align="right">
 
Heute (09.05.) wurden
+64 Positive mit Wohnsitz in Südtirol mitgeteilt, in 25 verschiedene Gemeinden (von 116).
In 39 Gemeinden (gleich viele wie [gestern](../08/README.md)) gibt es schon seit mindestens 7 Tagen keinen neuen Fall mehr,
in 14 davon (1 mehr als [gestern](../08/README.md)) sogar schon seit 21 Tagen oder mehr.
 
Besonders negativ fallen heute auf:
- **Gsies** (2.320 Einw.): Heute = +4, vorhergehende 6 Tage = +11, vorhergehende 14 Tage = +24, 7-Tage-Inzidenz = 600/100.000
- **Sexten** (1.880 Einw.): Heute = +3, vorhergehende 6 Tage = +8, vorhergehende 14 Tage = +1, 7-Tage-Inzidenz = 600/100.000
- **Stilfs** (1.141 Einw.): Heute = +2, vorhergehende 6 Tage = +7, vorhergehende 14 Tage = +4, 7-Tage-Inzidenz = 800/100.000
- **Taufers im Münstertal** (969 Einw.): Heute = +1, vorhergehende 6 Tage = +5, vorhergehende 14 Tage = +6, 7-Tage-Inzidenz = 600/100.000

Nicht viel besser ist **Mühlbach** (3.151 Einw.)



<img src="CovidCartinaCovidFree-2021-05-09-1.jpg" alt="Cartina con comuni covid-free" width="50%" align="right">
 
Folgende 11 Gemeinden (gleich viele, aber nicht die selben wie [gestern](../08/README.md)) könnte man als **Covid-Free** betrachten, denn sie haben schon seit 21 Tagen keinen Fall mehr gehabt, niemand befindet sich in Quarantäne oder Isolation und die SABES zählt zur Zeit keinen "aktiv Positiven".
 
1. Marling (2.791), letzter positiver Fall am 24. März
1. Freienfeld (2.656), letzter positiver Fall am 12. April
1. Mölten (1.692), letzter positiver Fall am 6. April
1. Magreid a.d.W. (1.274), letzter positiver Fall am 15. April
1. Pfatten (1.057), letzter positiver Fall am 13. April
1. Andrian (1.030), letzter positiver Fall am 4. April
1. **Hafling** (778), letzter positiver Fall am 18. April
1. Altrei (398), letzter positiver Fall am 1. April
1. Kuens (395), letzter positiver Fall am 10. März
1. Proveis (265), letzter positiver Fall am 3. März
1. Waidbruck (195), letzter positiver Fall am 6. Februar
- <del>_Schluderns (1.838)_</del>, heute +1 Fall, vorletzter positiver Fall am 20. März


Weitere Informationen: 
- [PDF, Daten und Text](../09) - in tedesco e italiano, se non indicato diversamente
  - [tägliche Pressemitteilung](20210509.txt) - solo tedesco
  - Karten - Cartine con dettaglio comunale
    - [**Covid-freie** Gemeinden](CovidCartinaCovidFree-2021-05-09.pdf)
    - [Cartina con evidenziati i comuni con almeno un nuovo caso](CovidCartinaPositiviPcrAgOggi-2021-05-09.pdf)
    - [Summe der PCR- und AG-Positiven der letzten 7 Tagen](CovidCartinaPositiviPcrAgUltimi7giorni-2021-05-09.pdf)
    - [Summe der PCR- **und AG**-Positiven der letzten **21** Tagen](CovidCartinaPositiviPcrAgUltimi21giorni-2021-05-09.pdf)
    - [**Anstieg bzw. Rückgang** der PCR- und AG-Positiven der letzten 7 Tagen im Vergleich zu den letzten 21 Tagen](CovidCartinaAccelerazione7su21giorniPositiviPcrAgUltimi-2021-05-09.pdf)
    - [Persone trovate positive con test PCR rispetto tutti i positivi (PCR e AG). Ultimi 21 giorni](CovidCartinaPositivi21PctPcr-2021-05-09.pdf)
    - [Karte mit aktueller Quarantäne](CovidCartinaQuarantena-2021-05-09.pdf)
    - [Residenti messi in quarantena negli ultimi 7 giorni](CovidCartinaQuarantenaMandatiUltimi7giorni-2021-05-09.pdf)
    - [Persone con almento un test positivo (PCR o AG), da 1° ottobre in poi](CovidCartinaPositiviTotaleWelle2-2021-05-09.pdf)
  - [weiteres Material](../09)
 
 
## Seniorenheime
 
Für Details siehe 
- [Liste der suspekten Heime](Traueranzeigen-Seniorenheime-Hotspots-Welle2-20210509.txt)
- [Grafiken](Traueranzeigen-SeniorenheimeCumuliert_2020-05-01_to_2021-05-06.pdf)
 
 
# Links zu anderen Südtirolbezogene Webseiten
 
- Dashboard von [Markus Falk](https://www.markusfalk.com/)
  - [Version 1](https://www.markusfalk.com/dashboard/#lang=de&dset=ST&reg=all&tab=PRED&from=2020-08-11&to=true)
  - [Version 2](https://www.markusfalk.com/dashboard-beta/#lang=de&dset=ST_ALL&reg=all&tab=PRED&from=2020-08-11&to=true)
- Tabellen von [Moritz Mair](https://moritzmair.info/)
  - [Impfzahlen in Südtirol nach Alter](https://covvac.moritzmair.info/?tab=1)
- Ivan Sieder
  - [Interaktive Karten](https://map.corona-bz.simedia.cloud/)
- Zivilschutz Südtirol
  - Ohne Gemeindedetails
    - [Grafiken](http://www.provinz.bz.it/sicherheit-zivilschutz/zivilschutz/aktuelle-daten-zum-coronavirus.asp)
    - [dati provinciali con serie storica a partire da 15 marzo 2020](https://afbs.provinz.bz.it/upload/coronavirus/Corona.Data.Total.csv)
  - Mit Gemeindedetails
    - [Grafici e cartine con dettaglio comunale](http://www.provincia.bz.it/sicurezza-protezione-civile/protezione-civile/aktuelle-daten-zum-coronavirus-in-den-gemeinden.asp)
    - [Aktuelle Excel-Datei mit Positiven (PCR+AG)](https://afbs.provinz.bz.it/upload/coronavirus/CoronavirusPositivi.xlsx)
    - [Aktuelle Excel-Datei mit Quarantäne](https://afbs.provinz.bz.it/upload/coronavirus/CoronavirusQuarantaene.xlsx)
    - [dati comunali con serie storica a partire da 18 dicembre](https://afbs.provinz.bz.it/upload/coronavirus/Corona.Data.Detail.csv)
- [Protezione civile Italia](https://github.com/pcm-dpc/COVID-19/blob/master/README.md)
  - diffusione del Virus
    - [dati (CSV)](https://github.com/pcm-dpc/COVID-19/tree/master/dati-regioni)
    - [dati (JSON)](https://github.com/pcm-dpc/COVID-19/tree/master/dati-json/dpc-covid19-ita-regioni.json)
    - [scheda riepilogativa (PDF)](https://github.com/pcm-dpc/COVID-19/raw/master/schede-riepilogative/regioni/dpc-covid19-ita-scheda-regioni-latest.pdf)
  - [Vaccinazioni](https://github.com/italia/covid19-opendata-vaccini/tree/master/dati)
    - [dati provinciali per classi di età, serie storica (CSV)](https://github.com/italia/covid19-opendata-vaccini/blob/master/dati/somministrazioni-vaccini-latest.csv)
 
## Keep In Touch
 
- Mastodon: https://troet.cafe/@COVID_BZ_Statistics
- Matrix: https://matrix.to/#/@antoniogulino:matrix.org
