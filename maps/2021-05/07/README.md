[covid-bz](../../../README.md) / [maps](../../README.md) / [2021-05](../../2021-05/README.md) / [07](../07)
 
# Freitag,  7. Mai 2021

<img src="CovidCartinaPositiviPcrAgUltimi7giorni-2021-05-07-1.jpg" alt="7-Tage-Inzidenz" width="50%" align="right">

In **Mühlbach** bricht die Serie weiterhin nicht ab und im kleinen **Stilfs** gibt es letzthin auch fast täglich einen Fall, wie auch in **Gsies** und **Sand in Taufers**. Niederdorf hat nach 4 Wochen Ruhe wieder einen Fall. Ahrntal hat zwar immer wieder Fälle,  aber scheint sich zu beruhigen.

Die **7-Tage-Inzidenz** (um die 90-100 auf 100.000) ist zwar weiterhin beim **sinken**, aber in den letzten Wochen im Durchschnitt nur um -1,0% täglich (die Gastronomie hat erst seit weniger als zwei Woche wieder offen), was die **50-er Marke erst für den Sommer** "möglich" macht.

- [Die 7-Tage-Inzidenz](#die-7-tage-inzidenz)
- [Nasenflügeltests in den Schulen](#nasenfl203MaiCgeltests-in-den-schulen)
- [Die Lage in den Gemeinden](#die-lage-in-den-gemeinden)
- [Seniorenheime](#seniorenheime)
- [Links zu anderen Südtirolbezogene Webseiten](#links-zu-anderen-s203MaiCdtirolbezogene-webseiten)
- [Keep In Touch](#keep-in-touch)  _(feedback is welcome)_


## Die 7-Tage-Inzidenz

Die 7-Tage-Inzidenz (307 PCR + 169 AG) ist 90 auf 100.000.
Vor einer Woche (am 30.04.) war sie 101.
Also -1.6% täglich.
Bei genau dieser Entschleunigung braucht es noch genau 36 Tage um auf 50 zu kommen (12. Juni).


## Nasenflügeltests in den Schulen 
_Hinweis: die von der SABES gelieferten Daten sind nicht ganz kohärent. Hier werden sie so wie die Daten gemeldet wurden wiedergegeben und verglichen._
 
> Nasenflügeltests in den Schulen (bis 06.05.2021)357.778 Tests gesamt an 532 Schulen, 330 positive Ergebnisse, davon 189 PCR-positiv, 121 PCR-negativ, 20 ausständig/zu überprüfen

Auswertung von 357.778  (+1.281 mehr als [gestern](../06/README.md)) Nasenflügeltests an 532 Schulen (+/- 0)
 
davon waren zwar  330 (-8) positiv (0,92 pro mille) aber
- nur 189 davon PCR-positiv (+20) 
- 121 PCR-negativ (+9) 
- 20 noch zu überprüfen (-37)
 
Dies bedeutet, dass die Nasenflügeltests, im Durchschnitt 3,4 bis 3,9 **Falsch positive** auf 10.000 "real negativen" Personen produziert haben.
 
_Wie viele "real positive" bei diesen Test "falsch negativ" sind, kann man aus diesen Werten nicht ausrechnen._
 

 
## Die Lage in den Gemeinden
 
<img src="CovidCartinaPositiviPcrAgOggi-2021-05-07-1.jpg" alt="Cartina con comuni con nuovi positivi oggi" width="50%" align="right">
 
Heute (07.05.) wurden
+64 Positive mit Wohnsitz in Südtirol mitgeteilt, in 28 verschiedene Gemeinden (von 116).
In 38 Gemeinden (1 weniger als [gestern](../06/README.md)) gibt es schon seit mindestens 7 Tagen keinen neuen Fall mehr,
in 14 davon (2 weniger als [gestern](../06/README.md)) sogar schon seit 21 Tagen oder mehr.

Besonders negativ fallen heute auf:
- **Sand in Taufers** (5.496 Einw.): Heute = +2, vorhergehende 6 Tage = +14, vorhergehende 14 Tage = +20, 7-Tage-Inzidenz = 300/100.000
- **Gsies** (2.320 Einw.): Heute = +2, vorhergehende 6 Tage = +13, vorhergehende 14 Tage = +17, 7-Tage-Inzidenz = 600/100.000

Nicht viel besser sind: **Gais** (3.311 Einw.), **Mühlbach** (3.151 Einw.), **Sexten** (1.880 Einw.) **Stilfs** (1.141 Einw.), **Taufers im Münstertal** (969 Einw.) und **U.L.Frau I.W.-St.Felix** (762 Einw.)


<img src="CovidCartinaCovidFree-2021-05-07-1.jpg" alt="Cartina con comuni covid-free" width="50%" align="right">
 
Folgende 12 Gemeinden (gleich viele aber nicht die selben von [gestern](../06/README.md)) könnte man als **Covid-Free** betrachten, denn sie haben schon seit 21 Tagen keinen Fall mehr gehabt, niemand befindet sich in Quarantäne oder Isolation und die SABES zählt zur Zeit keinen "aktiv Positiven".
 
1. Marling (2.791), letzter positiver Fall am 24. März
1. Freienfeld (2.656), letzter positiver Fall am 12. April
1. Schluderns (1.838), letzter positiver Fall am 20. März
1. **Mölten** (1.692), letzter positiver Fall am 6. April
1. Magreid a.d.W. (1.274), letzter positiver Fall am 15. April
1. Pfatten (1.057), letzter positiver Fall am 13. April
1. Andrian (1.030), letzter positiver Fall am 4. April
1. Glurns (900), letzter positiver Fall am 2. April
1. Altrei (398), letzter positiver Fall am 1. April
1. Kuens (395), letzter positiver Fall am 10. März
1. Proveis (265), letzter positiver Fall am 3. März
1. Waidbruck (195), letzter positiver Fall am 6. Februar
- <del>_Abtei_</del> (3.505), +3 neue Fälle, vorletzter positiver Fall war am 3. April

Weitere Informationen: 
- [PDF, Daten und Text](../07) - in tedesco e italiano, se non indicato diversamente
  - [tägliche Pressemitteilung](20210507.txt) - solo tedesco
  - Karten - Cartine con dettaglio comunale
    - [**Covid-freie** Gemeinden](CovidCartinaCovidFree-2021-05-07.pdf)
    - [Cartina con evidenziati i comuni con almeno un nuovo caso](CovidCartinaPositiviPcrAgOggi-2021-05-07.pdf)
    - [Summe der PCR- und AG-Positiven der letzten 7 Tagen](CovidCartinaPositiviPcrAgUltimi7giorni-2021-05-07.pdf)
    - [Summe der PCR- **und AG**-Positiven der letzten **21** Tagen](CovidCartinaPositiviPcrAgUltimi21giorni-2021-05-07.pdf)
    - [**Anstieg bzw. Rückgang** der PCR- und AG-Positiven der letzten 7 Tagen im Vergleich zu den letzten 21 Tagen](CovidCartinaAccelerazione7su21giorniPositiviPcrAgUltimi-2021-05-07.pdf)
    - [Persone trovate positive con test PCR rispetto tutti i positivi (PCR e AG). Ultimi 21 giorni](CovidCartinaPositivi21PctPcr-2021-05-07.pdf)
    - [Karte mit aktueller Quarantäne](CovidCartinaQuarantena-2021-05-07.pdf)
    - [Residenti messi in quarantena negli ultimi 7 giorni](CovidCartinaQuarantenaMandatiUltimi7giorni-2021-05-07.pdf)
    - [Persone con almento un test positivo (PCR o AG), da 1° ottobre in poi](CovidCartinaPositiviTotaleWelle2-2021-05-07.pdf)
  - [weiteres Material](../07)
 
 
## Seniorenheime
 
Für Details siehe 
- [Liste der suspekten Heime](Traueranzeigen-Seniorenheime-Hotspots-Welle2-20210507.txt)
- [Grafiken](Traueranzeigen-SeniorenheimeCumuliert_2020-05-01_to_2021-05-04.pdf)
 
 
# Links zu anderen Südtirolbezogene Webseiten
 
- Dashboard von [Markus Falk](https://www.markusfalk.com/)
  - [Version 1](https://www.markusfalk.com/dashboard/#lang=de&dset=ST&reg=all&tab=PRED&from=2020-08-11&to=true)
  - [Version 2](https://www.markusfalk.com/dashboard-beta/#lang=de&dset=ST_ALL&reg=all&tab=PRED&from=2020-08-11&to=true)
- Tabellen von [Moritz Mair](https://moritzmair.info/)
  - [Impfzahlen in Südtirol nach Alter](https://covvac.moritzmair.info/?tab=1)
- Ivan Sieder
  - [Interaktive Karten](https://map.corona-bz.simedia.cloud/)
- Zivilschutz Südtirol
  - Ohne Gemeindedetails
    - [Grafiken](http://www.provinz.bz.it/sicherheit-zivilschutz/zivilschutz/aktuelle-daten-zum-coronavirus.asp)
    - [dati provinciali con serie storica a partire da 15 marzo 2020](https://afbs.provinz.bz.it/upload/coronavirus/Corona.Data.Total.csv)
  - Mit Gemeindedetails
    - [Grafici e cartine con dettaglio comunale](http://www.provincia.bz.it/sicurezza-protezione-civile/protezione-civile/aktuelle-daten-zum-coronavirus-in-den-gemeinden.asp)
    - [Aktuelle Excel-Datei mit Positiven (PCR+AG)](https://afbs.provinz.bz.it/upload/coronavirus/CoronavirusPositivi.xlsx)
    - [Aktuelle Excel-Datei mit Quarantäne](https://afbs.provinz.bz.it/upload/coronavirus/CoronavirusQuarantaene.xlsx)
    - [dati comunali con serie storica a partire da 18 dicembre](https://afbs.provinz.bz.it/upload/coronavirus/Corona.Data.Detail.csv)
- [Protezione civile Italia](https://github.com/pcm-dpc/COVID-19/blob/master/README.md)
  - diffusione del Virus
    - [dati (CSV)](https://github.com/pcm-dpc/COVID-19/tree/master/dati-regioni)
    - [dati (JSON)](https://github.com/pcm-dpc/COVID-19/tree/master/dati-json/dpc-covid19-ita-regioni.json)
    - [scheda riepilogativa (PDF)](https://github.com/pcm-dpc/COVID-19/raw/master/schede-riepilogative/regioni/dpc-covid19-ita-scheda-regioni-latest.pdf)
  - [Vaccinazioni](https://github.com/italia/covid19-opendata-vaccini/tree/master/dati)
    - [dati provinciali per classi di età, serie storica (CSV)](https://github.com/italia/covid19-opendata-vaccini/blob/master/dati/somministrazioni-vaccini-latest.csv)
 
## Keep In Touch
 
- Mastodon: https://troet.cafe/@COVID_BZ_Statistics
- Matrix: https://matrix.to/#/@antoniogulino:matrix.org
