[covid-bz](../../../README.md) / [maps](../../README.md) / [2021-05](../../2021-05/README.md) / [14](../14)
 
# Freitag, 14. Mai 2021
 
<img src="CovidCartinaPositiviPcrAgUltimi7giorni-2021-05-14-1.jpg" alt="7-Tage-Inzidenz" width="50%" align="right">
 
Heute steht das **Obere Vinschgau** weiterhin im Fokus, mit **Stilfs** und **Taufers im Münstertal**, plus **Glurns** und **Laas** gab es heute insgesamt +6 Fälle (das Gebiet bis Laas hat 19.900 Einwohner). In den drei **Seniorenheime** Mals, Schluderns und Laas, mit ingesamt 180 Betten, gab es seit dem 1. April 8 Todesfälle. Auf das Jahr hochgerechnet sind es 36% der Bettenanzahl, gegenüber einem Landesdurschnitt von 25%.

Auch das Gebiet im **Unteren Eisacktal** (**Klausen**, **Lajen**, **Villanders**, **Barbian**, **Kastelruth**, **St. Ulrich**) ist weiterhin aktiv mit +5 Fälle auf 23.284 Einwohner. In den fünf **Seniorenheime** (mit insgesamt 260 Betten) in diesen Gemeinden gab es seit dem 1. April bis jetzt insgesamt 13 Trauerfälle. Auf das Jahr  hochgerechnet entspricht das über 40% der Bettenanzahl, was eindeutig über dem Landesdurchschnitt von 25% liegt.



Südtirolweit ist die **7-Tage-Inzidenz** (um die 80 auf 100.000) zwar weiterhin beim sinken, aber in den letzten 9 Wochen im Durchschnitt nur um -1.6% täglich, was die **50-er Marke erst in 5 Wochen** "möglich" machen würde.


- [Die 7-Tage-Inzidenz](#die-7-tage-inzidenz)
- [Nasenflügeltests in den Schulen](#nasenflügeltests-in-den-schulen)
- [Die Lage in den Gemeinden](#die-lage-in-den-gemeinden)
- [Seniorenheime](#seniorenheime)
- [Links zu anderen Südtirolbezogene Webseiten](#links-zu-anderen-südtirolbezogene-webseiten)
- [Keep In Touch](#keep-in-touch)  _(feedback is welcome)_


## Die 7-Tage-Inzidenz


Die 7-Tage-Inzidenz (324 PCR + 125 AG) ist 85 auf 100.000.
Vor einer Woche (am 07.05.) war sie 90.
Also -0.8% täglich.
Bei genau _dieser_ Entschleunigung braucht es noch 9 Wochen um auf 50 zu kommen (16. Juli um "genau" zu sein).


## Nasenflügeltests in den Schulen 
_Hinweis: die von der SABES gelieferten Daten sind nicht ganz kohärent. Hier werden sie so wie die Daten gemeldet wurden wiedergegeben und verglichen._
 
> Nasenflügeltests in den Schulen (bis 13.05.2021) 446.246 Tests gesamt an 537 Schulen, 368 positive Ergebnisse, davon 207 bestätigt, 125 PCR-negativ, 36 ausständig/zu überprüfen



Auswertung von 446.246 (+799 mehr als [gestern](../13/README.md)) Nasenflügeltests an 537 Schulen (+/-0)
 
davon waren zwar  368 (+/-0) positiv (0,82 pro mille) aber
- nur davon 207 davon PCR-positiv (+2) 
-  125 PCR-negativ (+/-0) 
-  36 noch zu überprüfen (-2)
 
Dies bedeutet, dass die Nasenflügeltests, im Durchschnitt 2,8 bis 3,6 **Falsch positive** auf 10.000 "real negativen" Personen produziert haben.
 
_Wie viele "real positive" bei diesen Test "falsch negativ" sind, kann man aus diesen Werten nicht ausrechnen._
 
## Die Lage in den Gemeinden
 
<img src="CovidCartinaPositiviPcrAgOggi-2021-05-14-1.jpg" alt="Cartina con comuni con nuovi positivi oggi" width="50%" align="right">
 
Heute (14.05.) wurden
+66 Positive mit Wohnsitz in Südtirol mitgeteilt, in 35 verschiedene Gemeinden (von 116).
In 36 Gemeinden (3 weniger als [gestern](../13/README.md)) gibt es schon seit mindestens 7 Tagen keinen neuen Fall mehr,
in 18 davon (1 weniger als [gestern](../13/README.md)) sogar schon seit 21 Tagen oder mehr.
 
Besondere Aufmerksamkeit gilt den zwei Obervinschger Gemeinden
- **Stilfs** (1.141 Einw.): Heute = +2, vorhergehende 6 Tage = +6, vorhergehende 14 Tage = +10, 7-Tage-Inzidenz = 700/100.000            
- **Taufers im Münstertal** (969 Einw.): Heute = +2, vorhergehende 6 Tage = +4, vorhergehende 14 Tage = +9, 7-Tage-Inzidenz = 600/100.000

Nicht aus den Augen verlieren: **Vintl** (3.339 Einw., heute = +1), **Natz-Schabs** (3.230 Einw.,**+8**), **Gsies** (2.320 Einw.), **Sexten** (1.880 Einw.), **Gargazon** (1.726 Einw., +1), **Mühlwald** (1.432 Einw., +1) und **Laurein** (342 Einw., +1)


<img src="CovidCartinaCovidFree-2021-05-14-1.jpg" alt="Cartina con comuni covid-free" width="50%" align="right">
 
Folgende 11 Gemeinden (die selben von [gestern](../13/README.md)) könnte man als **Covid-Free** betrachten, denn sie haben schon seit 21 Tagen keinen Fall mehr gehabt, niemand befindet sich in Quarantäne oder Isolation und die SABES zählt zur Zeit keinen "aktiv Positiven".
 
1. Kurtasch a.d.W. (2.239), letzter positiver Fall am 21. April
1. Mölten (1.692), letzter positiver Fall am 6. April
1. Magreid a.d.W. (1.274), letzter positiver Fall am 15. April
1. Pfatten (1.057), letzter positiver Fall am 13. April
1. Andrian (1.030), letzter positiver Fall am 4. April
1. Hafling (778), letzter positiver Fall am 18. April
1. Plaus (724), letzter positiver Fall am 22. April
1. Altrei (398), letzter positiver Fall am 1. April
1. Kuens (395), letzter positiver Fall am 10. März
1. Proveis (265), letzter positiver Fall am 3. März
1. Waidbruck (195), letzter positiver Fall am 6. Februar
 
Weitere Informationen: 
- [PDF, Daten und Text](../14) - in tedesco e italiano, se non indicato diversamente
  - [tägliche Pressemitteilung](20210514.txt) - solo tedesco
  - Karten - Cartine con dettaglio comunale
    - [**Covid-freie** Gemeinden](CovidCartinaCovidFree-2021-05-14.pdf)
    - [Cartina con evidenziati i comuni con almeno un nuovo caso](CovidCartinaPositiviPcrAgOggi-2021-05-14.pdf)
    - [Summe der PCR- und AG-Positiven der letzten 7 Tagen](CovidCartinaPositiviPcrAgUltimi7giorni-2021-05-14.pdf)
    - [Summe der PCR- **und AG**-Positiven der letzten **21** Tagen](CovidCartinaPositiviPcrAgUltimi21giorni-2021-05-14.pdf)
    - [**Anstieg bzw. Rückgang** der PCR- und AG-Positiven der letzten 7 Tagen im Vergleich zu den letzten 21 Tagen](CovidCartinaAccelerazione7su21giorniPositiviPcrAgUltimi-2021-05-14.pdf)
    - [Persone trovate positive con test PCR rispetto tutti i positivi (PCR e AG). Ultimi 21 giorni](CovidCartinaPositivi21PctPcr-2021-05-14.pdf)
    - [Karte mit aktueller Quarantäne](CovidCartinaQuarantena-2021-05-14.pdf)
    - [Residenti messi in quarantena negli ultimi 7 giorni](CovidCartinaQuarantenaMandatiUltimi7giorni-2021-05-14.pdf)
    - [Persone con almento un test positivo (PCR o AG), da 1° ottobre in poi](CovidCartinaPositiviTotaleWelle2-2021-05-14.pdf)
  - [weiteres Material](../14)
 
 
## Seniorenheime
 
Für Details siehe 
- [Liste der suspekten Heime](Traueranzeigen-Seniorenheime-Hotspots-Welle2-20210514.txt)
- [Grafiken](Traueranzeigen-SeniorenheimeCumuliert_2020-05-01_to_2021-05-11.pdf)
 
 
# Links zu anderen Südtirolbezogene Webseiten
 
- Dashboard von [Markus Falk](https://www.markusfalk.com/)
  - [Version 1](https://www.markusfalk.com/dashboard/#lang=de&dset=ST&reg=all&tab=PRED&from=2020-08-11&to=true)
  - [Version 2](https://www.markusfalk.com/dashboard-beta/#lang=de&dset=ST_ALL&reg=all&tab=PRED&from=2020-08-11&to=true)
- Tabellen von [Moritz Mair](https://moritzmair.info/)
  - [Impfzahlen in Südtirol nach Alter](https://covvac.moritzmair.info/?tab=1)
- Ivan Sieder
  - [Interaktive Karten](https://map.corona-bz.simedia.cloud/)
- Zivilschutz Südtirol
  - Ohne Gemeindedetails
    - [Grafiken](http://www.provinz.bz.it/sicherheit-zivilschutz/zivilschutz/aktuelle-daten-zum-coronavirus.asp)
    - [dati provinciali con serie storica a partire da 15 marzo 2020](https://afbs.provinz.bz.it/upload/coronavirus/Corona.Data.Total.csv)
  - Mit Gemeindedetails
    - [Grafici e cartine con dettaglio comunale](http://www.provincia.bz.it/sicurezza-protezione-civile/protezione-civile/aktuelle-daten-zum-coronavirus-in-den-gemeinden.asp)
    - [Aktuelle Excel-Datei mit Positiven (PCR+AG)](https://afbs.provinz.bz.it/upload/coronavirus/CoronavirusPositivi.xlsx)
    - [Aktuelle Excel-Datei mit Quarantäne](https://afbs.provinz.bz.it/upload/coronavirus/CoronavirusQuarantaene.xlsx)
    - [dati comunali con serie storica a partire da 18 dicembre](https://afbs.provinz.bz.it/upload/coronavirus/Corona.Data.Detail.csv)
- [Protezione civile Italia](https://github.com/pcm-dpc/COVID-19/blob/master/README.md)
  - diffusione del Virus
    - [dati (CSV)](https://github.com/pcm-dpc/COVID-19/tree/master/dati-regioni)
    - [dati (JSON)](https://github.com/pcm-dpc/COVID-19/tree/master/dati-json/dpc-covid19-ita-regioni.json)
    - [scheda riepilogativa (PDF)](https://github.com/pcm-dpc/COVID-19/raw/master/schede-riepilogative/regioni/dpc-covid19-ita-scheda-regioni-latest.pdf)
  - [Vaccinazioni](https://github.com/italia/covid19-opendata-vaccini/tree/master/dati)
    - [dati provinciali per classi di età, serie storica (CSV)](https://github.com/italia/covid19-opendata-vaccini/blob/master/dati/somministrazioni-vaccini-latest.csv)
 
## Keep In Touch
 
- Mastodon: https://troet.cafe/@COVID_BZ_Statistics
- Matrix: https://matrix.to/#/@antoniogulino:matrix.org
