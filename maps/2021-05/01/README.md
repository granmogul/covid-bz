[covid-bz](../../../README.md) / [maps](../../README.md) / [2021-05](../../2021-05/README.md) / [01](../01)
# Samstag,  1. Mai 2021

## Ganz Südtirol

Die 7-Tage-Inzidenz (PCR+AG) ist 83 auf 100.000. Vor einer Woche (am 24.4.) war sie 98. Also -2,3% täglich. Bei dieser Entschleunigung braucht es noch 21 Tage um auf 50 zu kommen (22. Mai).

## Die Lage in den Gemeinden
 
<img src="CovidCartinaPositiviPcrAgUltimi7giorni-2021-05-01-1.jpg" alt="Landkarte mit Gemeinden und deren 7-Tage-Inzidenz je 100" width="50%" align="right">

Heute (01.05.) wurden
+68 Positive mit Wohnsitz in Südtirol mitgeteilt, in 26 verschiedene Gemeinden (von 116).
In 42 Gemeinden (2 mehr als [gestern](../../2021-04/30/README.md)) gibt es schon seit mindestens 7 Tagen keinen neuen Fall mehr,
in 15 davon (gleich viele wie [gestern](../../2021-04/30/README.md)) sogar schon seit 21 Tagen oder mehr.
 
Bemerkenswert (im negativen Sinne) sind heute:
- **Gsies** (2.320 Einw.): Heute = +3, vorhergehende 6 Tage = +12, vorhergehende 14 Tage = +18, 7-Tage-Inzidenz = 600/100.000
- **Truden** im Naturpark (1.040 Einw.): Heute = +3, vorhergehende 6 Tage = +2, vorhergehende 14 Tage = +8, 7-Tage-Inzidenz = 500/100.000

Nennenswert sind auch: **Welsberg-Taisten** (2.914 Einw.), **Welschnofen** (1.973 Einw.), **Gargazon** (1.726 Einw.), **U.L.Frau I.W.-St.Felix** (762 Einw.) und **Laurein** (342 Einw.).


<img src="CovidCartinaCovidFree-2021-05-01-1.jpg" alt="Cartina con comuni covid-free" width="50%" align="right">
 
Folgende 8 Gemeinden (gleich viele aber nicht alle die selben von [gestern](../../2021-04/30/README.md)) könnte man als **Covid-Free** betrachten, denn sie haben schon seit 21 Tagen keinen Fall mehr gehabt, niemand befindet sich in Quarantäne oder Isolation und die SABES zählt zur Zeit keinen "aktiv Positiven".

1. Tirol (2.450), letzter positiver Fall am 20. **März**
1. Schluderns (1.838), letzter positiver Fall am 20. **März**
1. **Niederdorf (1.597)**, letzter positiver Fall am 10. April
1. Andrian (1.030), letzter positiver Fall am 4. April
1. Glurns (900), letzter positiver Fall am 2. April
1. Altrei (398), letzter positiver Fall am 1. April
1. Kuens (395), letzter positiver Fall am 10. **März**
1. Waidbruck (195), letzter positiver Fall am 6. **Februar**
- <del>_Barbian (1.755)_</del>, +1 Positiver heute. Vorletzter positive Fall war am 25. März (vor 37 Tagen)

Weitere Informationen: 
- [PDF, Daten und Text](../01) - in tedesco e italiano, se non indicato diversamente
  - [tägliche Pressemitteilung](20210501.txt) - solo tedesco
  - Karten - Cartine con dettaglio comunale
    - [**Covid-freie** Gemeinden](CovidCartinaCovidFree-2021-05-01.pdf)
    - [Cartina con evidenziati i comuni con almeno un nuovo caso](CovidCartinaPositiviPcrAgOggi-2021-05-01.pdf)
    - [Summe der PCR- und AG-Positiven der letzten 7 Tagen](CovidCartinaPositiviPcrAgUltimi7giorni-2021-05-01.pdf)
    - [Summe der PCR- **und AG**-Positiven der letzten **21** Tagen](CovidCartinaPositiviPcrAgUltimi21giorni-2021-05-01.pdf)
    - [**Anstieg bzw. Rückgang** der PCR- und AG-Positiven der letzten 7 Tagen im Vergleich zu den letzten 21 Tagen](CovidCartinaAccelerazione7su21giorniPositiviPcrAgUltimi-2021-05-01.pdf)
    - [Persone trovate positive con test PCR rispetto tutti i positivi (PCR e AG). Ultimi 21 giorni](CovidCartinaPositivi21PctPcr-2021-05-01.pdf)
    - [Karte mit aktueller Quarantäne](CovidCartinaQuarantena-2021-05-01.pdf)
    - [Residenti messi in quarantena negli ultimi 7 giorni](CovidCartinaQuarantenaMandatiUltimi7giorni-2021-05-01.pdf)
    - [Persone con almento un test positivo (PCR o AG), da 1° ottobre in poi](CovidCartinaPositiviTotaleWelle2-2021-05-01.pdf)
  - [weiteres Material](../01)
 
 
## Seniorenheime
 
Für Details siehe 
- [Liste der suspekten Heime](Traueranzeigen-Seniorenheime-Hotspots-Welle2-20210501.txt)
- [Grafiken](Traueranzeigen-SeniorenheimeCumuliert_2020-05-01_to_2021-04-28.pdf)
 
 
# Links zu anderen Südtirolbezogene Webseiten
 
- Dashboard von [Markus Falk](https://www.markusfalk.com/)
  - [Version 1](https://www.markusfalk.com/dashboard/#lang=de&dset=ST&reg=all&tab=PRED&from=2020-08-11&to=true)
  - [Version 2](https://www.markusfalk.com/dashboard-beta/#lang=de&dset=ST_ALL&reg=all&tab=PRED&from=2020-08-11&to=true)
- Tabellen von [Moritz Mair](https://moritzmair.info/)
  - [Impfzahlen in Südtirol nach Alter](https://covvac.moritzmair.info/?tab=1)
- Ivan Sieder
  - [Interaktive Karten](https://map.corona-bz.simedia.cloud/)
- Zivilschutz Südtirol
  - Ohne Gemeindedetails
    - [Grafiken](http://www.provinz.bz.it/sicherheit-zivilschutz/zivilschutz/aktuelle-daten-zum-coronavirus.asp)
    - [dati provinciali con serie storica a partire da 15 marzo 2020](https://afbs.provinz.bz.it/upload/coronavirus/Corona.Data.Total.csv)
  - Mit Gemeindedetails
    - [Grafici e cartine con dettaglio comunale](http://www.provincia.bz.it/sicurezza-protezione-civile/protezione-civile/aktuelle-daten-zum-coronavirus-in-den-gemeinden.asp)
    - [Aktuelle Excel-Datei mit Positiven (PCR+AG)](https://afbs.provinz.bz.it/upload/coronavirus/CoronavirusPositivi.xlsx)
    - [Aktuelle Excel-Datei mit Quarantäne](https://afbs.provinz.bz.it/upload/coronavirus/CoronavirusQuarantaene.xlsx)
    - [dati comunali con serie storica a partire da 18 dicembre](https://afbs.provinz.bz.it/upload/coronavirus/Corona.Data.Detail.csv)
- [Protezione civile Italia](https://github.com/pcm-dpc/COVID-19/blob/master/README.md)
  - diffusione del Virus
    - [dati (CSV)](https://github.com/pcm-dpc/COVID-19/tree/master/dati-regioni)
    - [dati (JSON)](https://github.com/pcm-dpc/COVID-19/tree/master/dati-json/dpc-covid19-ita-regioni.json)
    - [scheda riepilogativa (PDF)](https://github.com/pcm-dpc/COVID-19/raw/master/schede-riepilogative/regioni/dpc-covid19-ita-scheda-regioni-latest.pdf)
  - [Vaccinazioni](https://github.com/italia/covid19-opendata-vaccini/tree/master/dati)
    - [dati provinciali per classi di età, serie storica (CSV)](https://github.com/italia/covid19-opendata-vaccini/blob/master/dati/somministrazioni-vaccini-latest.csv)
 
## Keep In Touch
 
- Mastodon: https://troet.cafe/@COVID_BZ_Statistics
- Matrix: https://matrix.to/#/@antoniogulino:matrix.org
