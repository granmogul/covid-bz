[covid-bz](../../../README.md) / [maps](../../README.md) / [2021-05](../../2021-05/README.md) / [10](../10)
 
# Montag, 10. Mai 2021
 
<img src="CovidCartinaPositiviPcrAgUltimi7giorni-2021-05-10-1.jpg" alt="7-Tage-Inzidenz" width="50%" align="right">

Trotz der geringen Anzahl an gemeldeten positive Fällen, hat **Gsies** es geschafft einen zu haben. Für den Rest gibt es Nichts Neues.

Die **7-Tage-Inzidenz** (um die 90 auf 100.000) ist zwar weiterhin beim sinken,
aber in den letzten 9 Wochen im Durchschnitt nur um -1.8% täglich,
was die **50-er Marke erst in 4 Wochen** "möglich" machen würde.


- [Die 7-Tage-Inzidenz](#die-7-tage-inzidenz)
- [Nasenflügeltests in den Schulen](#nasenflügeltests-in-den-schulen)
- [Die Lage in den Gemeinden](#die-lage-in-den-gemeinden)
- [Seniorenheime](#seniorenheime)
- [Links zu anderen Südtirolbezogene Webseiten](#links-zu-anderen-südtirolbezogene-webseiten)
- [Keep In Touch](#keep-in-touch)  _(feedback is welcome)_


## Die 7-Tage-Inzidenz


Die 7-Tage-Inzidenz (302 PCR + 168 AG) ist 89 auf 100.000. Vor einer Woche (am 03.05.) war sie 97. Also -1.3% täglich. Bei genau _dieser_ Entschleunigung braucht es noch 6 Wochen um auf 50 zu kommen (22. Juni um "genau" zu sein).


## Nasenflügeltests in den Schulen 
_Hinweis: die von der SABES gelieferten Daten sind nicht ganz kohärent. Hier werden sie so wie die Daten gemeldet wurden wiedergegeben und verglichen._
 
> Nasenflügeltests in den Schulen (bis 09.05.2021)401.368 Tests gesamt an 534 Schulen, 345 positive Ergebnisse, davon 188 PCR-positiv, 126 PCR-negativ, 19 ausständig/zu überprüfen

Auswertung von 401.368 (+8.835 mehr als [vorgestern](../08/README.md)) Nasenflügeltests an 534 Schulen (+1)
 
davon waren zwar  345 (-5) positiv (0,86 pro mille) aber
- nur 188 davon PCR-positiv (+11) 
- 126 PCR-negativ (+8) 
- 19 noch zu überprüfen (-24)
 
Dies bedeutet, dass die Nasenflügeltests, im Durchschnitt 3,1 bis 3,6 **Falsch positive** auf 10.000 "real negativen" Personen produziert haben.
 
_Wie viele "real positive" bei diesen Test "falsch negativ" sind, kann man aus diesen Werten nicht ausrechnen._

 
## Die Lage in den Gemeinden
 
<img src="CovidCartinaPositiviPcrAgOggi-2021-05-10-1.jpg" alt="Cartina con comuni con nuovi positivi oggi" width="50%" align="right">
 
Heute (10.05.) wurden
+13 Positive mit Wohnsitz in Südtirol mitgeteilt, in 11 verschiedene Gemeinden (von 116).
In 38 Gemeinden (1 weniger als [gestern](../09/README.md)) gibt es schon seit mindestens 7 Tagen keinen neuen Fall mehr,
in 15 davon (1 mehr als [gestern](../09/README.md)) sogar schon seit 21 Tagen oder mehr.
 
Besonders negativ fällt heute, trotz der für Montage übliche sehr geringen Anzahl an positive Falle, auf:
- **Gsies** (2.320 Einw.): Heute = +1, vorhergehende 6 Tage = +15, vorhergehende 14 Tage = +24, 7-Tage-Inzidenz = 700/100.000

Nicht viel besser sind **Sexten** (1.880 Einw.), **Stilfs** (1.141 Einw.) und **Taufers im Münstertal** (969 Einw.)


<img src="CovidCartinaCovidFree-2021-05-10-1.jpg" alt="Cartina con comuni covid-free" width="50%" align="right">
 
Folgende 11 Gemeinden  (die selben wie [gestern](../09/README.md)) könnte man als **Covid-Free** betrachten, denn sie haben schon seit 21 Tagen keinen Fall mehr gehabt, niemand befindet sich in Quarantäne oder Isolation und die SABES zählt zur Zeit keinen "aktiv Positiven".
 
1. Marling (2.791), letzter positiver Fall am 24. März
1. Freienfeld (2.656), letzter positiver Fall am 12. April
1. Mölten (1.692), letzter positiver Fall am 6. April
1. Magreid a.d.W. (1.274), letzter positiver Fall am 15. April
1. Pfatten (1.057), letzter positiver Fall am 13. April
1. Andrian (1.030), letzter positiver Fall am 4. April
1. Hafling (778), letzter positiver Fall am 18. April
1. Altrei (398), letzter positiver Fall am 1. April
1. Kuens (395), letzter positiver Fall am 10. März
1. Proveis (265), letzter positiver Fall am 3. März
1. Waidbruck (195), letzter positiver Fall am 6. Februar
 
Weitere Informationen: 
- [PDF, Daten und Text](../10) - in tedesco e italiano, se non indicato diversamente
  - [tägliche Pressemitteilung](20210510.txt) - solo tedesco
  - Karten - Cartine con dettaglio comunale
    - [**Covid-freie** Gemeinden](CovidCartinaCovidFree-2021-05-10.pdf)
    - [Cartina con evidenziati i comuni con almeno un nuovo caso](CovidCartinaPositiviPcrAgOggi-2021-05-10.pdf)
    - [Summe der PCR- und AG-Positiven der letzten 7 Tagen](CovidCartinaPositiviPcrAgUltimi7giorni-2021-05-10.pdf)
    - [Summe der PCR- **und AG**-Positiven der letzten **21** Tagen](CovidCartinaPositiviPcrAgUltimi21giorni-2021-05-10.pdf)
    - [**Anstieg bzw. Rückgang** der PCR- und AG-Positiven der letzten 7 Tagen im Vergleich zu den letzten 21 Tagen](CovidCartinaAccelerazione7su21giorniPositiviPcrAgUltimi-2021-05-10.pdf)
    - [Persone trovate positive con test PCR rispetto tutti i positivi (PCR e AG). Ultimi 21 giorni](CovidCartinaPositivi21PctPcr-2021-05-10.pdf)
    - [Karte mit aktueller Quarantäne](CovidCartinaQuarantena-2021-05-10.pdf)
    - [Residenti messi in quarantena negli ultimi 7 giorni](CovidCartinaQuarantenaMandatiUltimi7giorni-2021-05-10.pdf)
    - [Persone con almento un test positivo (PCR o AG), da 1° ottobre in poi](CovidCartinaPositiviTotaleWelle2-2021-05-10.pdf)
  - [weiteres Material](../10)
 
 
## Seniorenheime
 
Für Details siehe 
- [Liste der suspekten Heime](Traueranzeigen-Seniorenheime-Hotspots-Welle2-20210510.txt)
- [Grafiken](Traueranzeigen-SeniorenheimeCumuliert_2020-05-01_to_2021-05-07.pdf)
 
 
# Links zu anderen Südtirolbezogene Webseiten
 
- Dashboard von [Markus Falk](https://www.markusfalk.com/)
  - [Version 1](https://www.markusfalk.com/dashboard/#lang=de&dset=ST&reg=all&tab=PRED&from=2020-08-11&to=true)
  - [Version 2](https://www.markusfalk.com/dashboard-beta/#lang=de&dset=ST_ALL&reg=all&tab=PRED&from=2020-08-11&to=true)
- Tabellen von [Moritz Mair](https://moritzmair.info/)
  - [Impfzahlen in Südtirol nach Alter](https://covvac.moritzmair.info/?tab=1)
- Ivan Sieder
  - [Interaktive Karten](https://map.corona-bz.simedia.cloud/)
- Zivilschutz Südtirol
  - Ohne Gemeindedetails
    - [Grafiken](http://www.provinz.bz.it/sicherheit-zivilschutz/zivilschutz/aktuelle-daten-zum-coronavirus.asp)
    - [dati provinciali con serie storica a partire da 15 marzo 2020](https://afbs.provinz.bz.it/upload/coronavirus/Corona.Data.Total.csv)
  - Mit Gemeindedetails
    - [Grafici e cartine con dettaglio comunale](http://www.provincia.bz.it/sicurezza-protezione-civile/protezione-civile/aktuelle-daten-zum-coronavirus-in-den-gemeinden.asp)
    - [Aktuelle Excel-Datei mit Positiven (PCR+AG)](https://afbs.provinz.bz.it/upload/coronavirus/CoronavirusPositivi.xlsx)
    - [Aktuelle Excel-Datei mit Quarantäne](https://afbs.provinz.bz.it/upload/coronavirus/CoronavirusQuarantaene.xlsx)
    - [dati comunali con serie storica a partire da 18 dicembre](https://afbs.provinz.bz.it/upload/coronavirus/Corona.Data.Detail.csv)
- [Protezione civile Italia](https://github.com/pcm-dpc/COVID-19/blob/master/README.md)
  - diffusione del Virus
    - [dati (CSV)](https://github.com/pcm-dpc/COVID-19/tree/master/dati-regioni)
    - [dati (JSON)](https://github.com/pcm-dpc/COVID-19/tree/master/dati-json/dpc-covid19-ita-regioni.json)
    - [scheda riepilogativa (PDF)](https://github.com/pcm-dpc/COVID-19/raw/master/schede-riepilogative/regioni/dpc-covid19-ita-scheda-regioni-latest.pdf)
  - [Vaccinazioni](https://github.com/italia/covid19-opendata-vaccini/tree/master/dati)
    - [dati provinciali per classi di età, serie storica (CSV)](https://github.com/italia/covid19-opendata-vaccini/blob/master/dati/somministrazioni-vaccini-latest.csv)
 
## Keep In Touch
 
- Mastodon: https://troet.cafe/@COVID_BZ_Statistics
- Matrix: https://matrix.to/#/@antoniogulino:matrix.org
