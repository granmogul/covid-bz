https://www.sabes.it/de/news.asp?aktuelles_action=4&aktuelles_article_id=655628

Coronavirus – Mitteilung des Sanitätsbetriebes | 10.05.2021 | 10:47

In den letzten 24 Stunden wurden 251 PCR-Tests untersucht und dabei 3 Neuinfektionen festgestellt. Zusätzlich gab es 10 positive Antigentests.

Foto: 123rf

Bisher (10. Mai) wurden insgesamt 563.037 Abstriche untersucht, die von 213.694 Personen stammen.

Die Zahlen im Überblick:

PCR-Abstriche:

Untersuchte Abstriche gestern (9. Mai): 251

Mittels PCR-Test neu positiv getestete Personen: 3

Gesamtzahl der mittels PCR-Test positiv getesteten Personen: 47.556

Gesamtzahl der untersuchten Abstriche: 563.037

Gesamtzahl der mit Abstrichen getesteten Personen: 213.694 (+78)

Antigentests:

Gesamtzahl der durchgeführten Antigentests: 1.399.353

Gesamtzahl der positiven Antigentests: 25.816

Durchgeführte Antigentests gestern: 824

Mittels Antigentest neu positiv getestete Personen: 10

Nasenflügeltests bis 09.05.2021 (ohne Schulen) 171.068 Tests, davon 302 positiv

Nasenflügeltests in den Schulen (bis 09.05.2021)401.368 Tests gesamt an 534 Schulen, 345 positive Ergebnisse, davon 188 PCR-positiv, 126 PCR-negativ, 19 ausständig/zu überprüfen

Weitere Daten:

Auf Normalstationen im Krankenhaus untergebrachte Covid-19-Patienten/-Patientinnen: 31

In Privatkliniken untergebrachte Covid-19-Patienten/-Patientinnen (post-akut bzw. aus Seniorenwohnheimen übernommen): 21

In Gossensaß und Sarns untergebrachte Covid-19-Patienten/-Patientinnen: 6

Anzahl der auf Intensivstationen aufgenommenen Covid-Patienten/Patientinnen: 6 (davon 6 als ICU-Covid klassifiziert)Gesamtzahl der mit Covid-19 Verstorbenen: 1.167 (+0)

Personen in Quarantäne/häuslicher Isolation: 2.128

Personen, die Quarantäne/häusliche Isolation beendet haben: 132.121

Personen betroffen von verordneter Quarantäne/häuslicher Isolation: 134.249

Geheilte Personen insgesamt: 71.073 (+33)

Positiv getestete Mitarbeiter und Mitarbeiterinnen des Sanitätsbetriebes: 2.016, davon 1.632 geheilt. Positiv getestete Basis-, Kinderbasisärzte und Bereitschaftsärzte: 60, davon 45 geheilt. (Stand: 11.04.2021)

(VS)

