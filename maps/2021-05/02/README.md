[covid-bz](../../../README.md) / [maps](../../README.md) / [2021-05](../../2021-05/README.md) / [02](../02)
# Sonntag,  2. Mai 2021


## Ganz Südtirol

Die 7-Tage-Inzidenz (PCR+AG) ist 86 auf 100.000. Vor einer Woche (am 25.4.) war sie 111. Also -3,6% täglich. Bei genau dieser Entschleunigung braucht es noch genau 15 Tage um auf 50 zu kommen (17. Mai).

## Die Lage in den Gemeinden
 
<img src="CovidCartinaPositiviPcrAgUltimi7giorni-2021-05-02-1.jpg" alt="7-Tages-Inzidenz auf Gemeindeebene" width="50%" align="right">
 
Heute (02.05.) wurden
+55 Positive mit Wohnsitz in Südtirol mitgeteilt, in 22 verschiedene Gemeinden (von 116).
In 40 Gemeinden (2 weniger als [gestern](../01/README.md)) gibt es schon seit mindestens 7 Tagen keinen neuen Fall mehr,
in 13 davon (2 weniger als [gestern](../01/README.md)) sogar schon seit 21 Tagen oder mehr.

Bemerkenswert (im negative Sinne) sind heute die selben wie [gestern](../01/README.md):
- **Gsies** (2.320 Einw.): Heute = +5, vorhergehende 6 Tage = +11, vorhergehende 14 Tage = +18, 7-Tage-Inzidenz = 700/100.000"            
- **Truden** im Naturpark (1.040 Einw.): Heute = +1, vorhergehende 6 Tage = +5, vorhergehende 14 Tage = +8, 7-Tage-Inzidenz = 600/100.000

Nennenswert sind auch: **Mühlbach** (3.151 Einw.), **Welschnofen** (1.973 Einw.), **U.L.Frau I.W.-St.Felix** (762 Einw.) und **Laurein** (342 Einw.)

<img src="CovidCartinaCovidFree-2021-05-02-1.jpg" alt="Cartina con comuni covid-free" width="50%" align="right">
 
Folgende 7 Gemeinden (1 weniger als [gestern](../01/README.md)) könnte man als **Covid-Free** betrachten, denn sie haben schon seit 21 Tagen keinen Fall mehr gehabt, niemand befindet sich in Quarantäne oder Isolation und die SABES zählt zur Zeit keinen "aktiv Positiven".
 
1. Schluderns (1.838)
1. Niederdorf (1.597)
1. Andrian (1.030)
1. Glurns (900)
1. Altrei (398)
1. Kuens (395)
1. Waidbruck (195)
- <del>_Tirol (2.450)_</del>, +1 Positiver heute.  Vorletzter positiver Fall war am 20. März (vor 43 Tagen)
 
Weitere Informationen: 
- [PDF, Daten und Text](../02) - in tedesco e italiano, se non indicato diversamente
  - [tägliche Pressemitteilung](20210502.txt) - solo tedesco
  - Karten - Cartine con dettaglio comunale
    - [**Covid-freie** Gemeinden](CovidCartinaCovidFree-2021-05-02.pdf)
    - [Cartina con evidenziati i comuni con almeno un nuovo caso](CovidCartinaPositiviPcrAgOggi-2021-05-02.pdf)
    - [Summe der PCR- und AG-Positiven der letzten 7 Tagen](CovidCartinaPositiviPcrAgUltimi7giorni-2021-05-02.pdf)
    - [Summe der PCR- **und AG**-Positiven der letzten **21** Tagen](CovidCartinaPositiviPcrAgUltimi21giorni-2021-05-02.pdf)
    - [**Anstieg bzw. Rückgang** der PCR- und AG-Positiven der letzten 7 Tagen im Vergleich zu den letzten 21 Tagen](CovidCartinaAccelerazione7su21giorniPositiviPcrAgUltimi-2021-05-02.pdf)
    - [Persone trovate positive con test PCR rispetto tutti i positivi (PCR e AG). Ultimi 21 giorni](CovidCartinaPositivi21PctPcr-2021-05-02.pdf)
    - [Karte mit aktueller Quarantäne](CovidCartinaQuarantena-2021-05-02.pdf)
    - [Residenti messi in quarantena negli ultimi 7 giorni](CovidCartinaQuarantenaMandatiUltimi7giorni-2021-05-02.pdf)
    - [Persone con almento un test positivo (PCR o AG), da 1° ottobre in poi](CovidCartinaPositiviTotaleWelle2-2021-05-02.pdf)
  - [weiteres Material](../02)
 
 
## Seniorenheime
 
### Zum Ausbruch im Seniorenheim Partschins

Heute habe ich einen weiter Trauerfall mit Bezug zum Seniorenheim Partschins gefunden, den 5. in kurzer Zeit.

Vom 19. bis 30. April habe ich 26 Trauerfälle mit Bezug auf einen Seniorenheim in Südtirol gefunden (es gibt 77 davon, mit zur Zeit um die 3.800 Gäste, aber insgesamt um die 4500 Betten)
5 dieser 26 Trauerfälle (20%) beziehen sich auf das Seniorenheim von Partschins.
Dieses hat aber nur 45 Gäste (jetzt 5 weniger). Also sind innerhalb von 12 Tagen mehr als 10% der Senioren des Seniorenheimes von Partschins gestorben.
Laut offiziellen Daten gab es in Partschins, nur 20 positive Fälle zwischen Heimgäste, Mitarbeiter und Dorfbewohner.




#### Burggrafenamt: Coronainfektionen trotz Impfung in Seniorenheim
[Stol, Mittwoch, 21. April 2021](  https://www.stol.it/artikel/chronik/wirbel-um-promi-frau-und-re-infektion-in-heim)

> Derzeit läuft die Kontaktnachverfolgung in einem Burggräfler Seniorenheim, wo sich 13 komplett geimpfte Bewohner und 4 Mitarbeiter erneut mit Covid infiziert haben. Ein Senior und eine Seniorin wurden ins Meraner Spital gebracht.
> 
> Die Frau ist gestorben, doch soll dies nicht Covid-19, sondern einer gastrointestinalen Infektion zuzuschreiben sein. Ersten Informationen zufolge könnte das Virus im Heim von einer nicht geimpften Pflegekraft verbreitet worden sein.
> 
> „Die Erhebungen laufen“, sagt Landesrat Widmann. Man nehme die Re-Infektionen sehr ernst, zeige sich doch, dass das Virus „nach wie vor stark unter uns ist“.


#### Partschins und Innichen waren die erste Heime, in denen geimpft wurde

[RAI News](https://www.rainews.it/tgr/tagesschau/articoli/2021/01/tag-Corona-Impfungen-Seniorenheime-Auftakt-477573c4-7260-47fd-9854-d584f3cdfa65.html) und [PM](http://www.vds-suedtirol.it/de/seniorenwohnheime-beginnen-mit-coronaimpfung)

>Die freiwillige Coronaimpfung für Mitarbeitende und Heimbewohner der Seniorenwohnheime hat heute begonnen. Den Auftakt machten die Heime in Partschins und Innichen. 
>[...]
>Gesundheitslandesrat Widmann erklärte, dass nach der ersten Teilimpfung weiterhin große Vorsicht geboten sei, um die Heime bis zum zweiten Impftermin coronafrei zu halten.



#### In Partschins gab es seit Anfang März verschärfte Maßnahmen wegen südafrikanischen Coronavirus-Mutante

[Pressemitteilung des Landes vom 27.02.2021, 15:57](https://www.provinz.bz.it/sicherheit-zivilschutz/zivilschutz/news.asp?aktuelles_action=4&aktuelles_article_id=653355)
>Nach Nachweis der südafrikanischen Coronavirus-Mutante in Tirol, Partschins und Schlanders sind die verschärften Maßnahmen auch auf diese drei Südtiroler Gemeinden ausgedehnt worden.
Verordnung Nr.11/2021

[Auch Algund, Schlanders und andere 10 Vinschger und Burgräfler Gemeinden waren am 5. März betroffen](https://www.provinz.bz.it/sicherheit-zivilschutz/zivilschutz/news.asp?aktuelles_action=4&aktuelles_article_id=653614)

#### Seniorenheim Partschins Johann Nepomuk Schöpf

Seniorenheim Partschins Johann Nepomuk Schöpf befindet sich im Zentrum von Partschins
Seit Oktober 2019 befinden wir uns im neuen Haus!
Das neue Seniorenheim verfügt über 50 Betten in Einzelzimmern verteilt auf zwei Stockwerke und 2 Kurzzeitpflegebetten.
[[1](http://www.vds-suedtirol.it/de/swh/495/swh-page/neubau-seniorenheim-partschins)]

Seit September 2019 steht den Bewohnern des Seniorenheim Partschins ein festes Ärzteteam bestehend aus Frau Dr. Raimondo Federica, Frau Dr. Angelova Ivelina und Herrn Dr. Morbini Martino zur Verfügung.
[[2](http://www.vds-suedtirol.it/de/swh/495/news/neues-aerzteteam-ab-september-2019)]



### Weiter Infos zu den Todesfällen in Südtiroler Heime
Für Details siehe 
- [Liste der suspekten Heime](Traueranzeigen-Seniorenheime-Hotspots-Welle2-20210502.txt)
- [Grafiken](Traueranzeigen-SeniorenheimeCumuliert_2020-05-01_to_2021-04-29.pdf)
 
 
# Links zu anderen Südtirolbezogene Webseiten
 
- Dashboard von [Markus Falk](https://www.markusfalk.com/)
  - [Version 1](https://www.markusfalk.com/dashboard/#lang=de&dset=ST&reg=all&tab=PRED&from=2020-08-11&to=true)
  - [Version 2](https://www.markusfalk.com/dashboard-beta/#lang=de&dset=ST_ALL&reg=all&tab=PRED&from=2020-08-11&to=true)
- Tabellen von [Moritz Mair](https://moritzmair.info/)
  - [Impfzahlen in Südtirol nach Alter](https://covvac.moritzmair.info/?tab=1)
- Ivan Sieder
  - [Interaktive Karten](https://map.corona-bz.simedia.cloud/)
- Zivilschutz Südtirol
  - Ohne Gemeindedetails
    - [Grafiken](http://www.provinz.bz.it/sicherheit-zivilschutz/zivilschutz/aktuelle-daten-zum-coronavirus.asp)
    - [dati provinciali con serie storica a partire da 15 marzo 2020](https://afbs.provinz.bz.it/upload/coronavirus/Corona.Data.Total.csv)
  - Mit Gemeindedetails
    - [Grafici e cartine con dettaglio comunale](http://www.provincia.bz.it/sicurezza-protezione-civile/protezione-civile/aktuelle-daten-zum-coronavirus-in-den-gemeinden.asp)
    - [Aktuelle Excel-Datei mit Positiven (PCR+AG)](https://afbs.provinz.bz.it/upload/coronavirus/CoronavirusPositivi.xlsx)
    - [Aktuelle Excel-Datei mit Quarantäne](https://afbs.provinz.bz.it/upload/coronavirus/CoronavirusQuarantaene.xlsx)
    - [dati comunali con serie storica a partire da 18 dicembre](https://afbs.provinz.bz.it/upload/coronavirus/Corona.Data.Detail.csv)
- [Protezione civile Italia](https://github.com/pcm-dpc/COVID-19/blob/master/README.md)
  - diffusione del Virus
    - [dati (CSV)](https://github.com/pcm-dpc/COVID-19/tree/master/dati-regioni)
    - [dati (JSON)](https://github.com/pcm-dpc/COVID-19/tree/master/dati-json/dpc-covid19-ita-regioni.json)
    - [scheda riepilogativa (PDF)](https://github.com/pcm-dpc/COVID-19/raw/master/schede-riepilogative/regioni/dpc-covid19-ita-scheda-regioni-latest.pdf)
  - [Vaccinazioni](https://github.com/italia/covid19-opendata-vaccini/tree/master/dati)
    - [dati provinciali per classi di età, serie storica (CSV)](https://github.com/italia/covid19-opendata-vaccini/blob/master/dati/somministrazioni-vaccini-latest.csv)
 
## Keep In Touch
 
- Mastodon: https://troet.cafe/@COVID_BZ_Statistics
- Matrix: https://matrix.to/#/@antoniogulino:matrix.org
