[covid-bz](../../../README.md) / [maps](../../README.md) / [2021-05](../../2021-05/README.md) / [28](../28)
 
# Freitag, 28. Mai 2021
 
Ist im Seniorenheim von Schluderns etwas los?

<img src="CovidCartinaPositiviPcrAgUltimi7giorni-2021-05-28-1.jpg" alt="7-Tage-Inzidenz" width="50%" align="right">
 
Die **7-Tage-Inzidenz** (um die 60 auf 100.000) ist zwar weiterhin beim sinken,
aber in den letzten 5 Wochen im Durchschnitt nur um -1.8% täglich,
was die **50-er Marke erst in 1 Wochen** "möglich" machen würde.


- [Die 7-Tage-Inzidenz](#die-7-tage-inzidenz)
- [Nasenflügeltests in den Schulen](#nasenflügeltests-in-den-schulen)
- [Die Lage in den Gemeinden](#die-lage-in-den-gemeinden)
- [Seniorenheime](#seniorenheime)
- [Links zu anderen Südtirolbezogene Webseiten](#links-zu-anderen-südtirolbezogene-webseiten)
- [Keep In Touch](#keep-in-touch)  _(feedback is welcome)_


## Die 7-Tage-Inzidenz


Die 7-Tage-Inzidenz (233 PCR + 82 AG) ist 59 auf 100.000.
Vor einer Woche (am 21.05.) war sie 68.
Also -2.0% täglich.
Bei genau _dieser_ Entschleunigung braucht es noch 1 Wochen um auf 50 zu kommen (5. Juni um "genau" zu sein).


## Nasenflügeltests in den Schulen 
_Hinweis: die von der SABES gelieferten Daten sind nicht ganz kohärent. Hier werden sie so wie die Daten gemeldet wurden wiedergegeben und verglichen._
 
> Nasenflügeltests in den Schulen (bis 27.05.2021) 607.773 Tests gesamt an 544 Schulen, 493 positive Ergebnisse, davon 250 bestätigt, 178 PCR-negativ, 65  ausständig/zu überprüfen
 
Auswertung von xxx.xxx (+xx.xxx mehr als [gestern](../04/README.md)) Nasenflügeltests an xxx Schulen (-xxx)
 
davon waren zwar  xxx (+xxx) positiv (x,xx pro mille) aber
- nur xxx davon PCR-positiv (+xxx) 
- xxx PCR-negativ (xxx) 
- xx noch zu überprüfen (xxx)
 
Dies bedeutet, dass die Nasenflügeltests, im Durchschnitt x,x bis x,x **Falsch positive** auf 10.000 "real negativen" Personen produziert haben.
 
_Wie viele "real positive" bei diesen Test "falsch negativ" sind, kann man aus diesen Werten nicht ausrechnen._
 
Im Vergleich zu [gestern](../04/README.md) gab es +xx positive auf +xx.xxx, dies bedeutet 0,xx pro mille, also "_im Durchschnitt_" alle "**Falsch Positiv**".
 
## Die Lage in den Gemeinden
 
<img src="CovidCartinaPositiviPcrAgOggi-2021-05-28-1.jpg" alt="Cartina con comuni con nuovi positivi oggi" width="50%" align="right">
 
Heute (28.05.) wurden
+40 Positive mit Wohnsitz in Südtirol mitgeteilt, in 19 verschiedene Gemeinden (von 116).
In 58 Gemeinden (x mehr als [gestern](../01/README.md)) gibt es schon seit mindestens 7 Tagen keinen neuen Fall mehr,
in 21 davon sogar schon seit 21 Tagen oder mehr.

Bemerkenswert negativ sind heute:
- Toblach (3.351 Einw.): Heute = +1, vorhergehende 6 Tage = +7, vorhergehende 14 Tage = +12, 7-Tage-Inzidenz = 200/100.000
- Schluderns (1.838 Einw.): Heute = +5, vorhergehende 6 Tage = +6, vorhergehende 14 Tage = +1, 7-Tage-Inzidenz = 600/100.000

Nicht viel besser sind:
- Ahrntal (6.022 Einw.): Heute = +3, vorhergehende 6 Tage = +14, vorhergehende 14 Tage = +14, 7-Tage-Inzidenz = 280
- Klausen (5.215 Einw.): Heute = +0, vorhergehende 6 Tage = +16, vorhergehende 14 Tage = +16, 7-Tage-Inzidenz = 310
- Abtei (3.505 Einw.): Heute = +1, vorhergehende 6 Tage = +7, vorhergehende 14 Tage = +11, 7-Tage-Inzidenz = 230
- Corvara (1.378 Einw.): Heute = +3, vorhergehende 6 Tage = +3, vorhergehende 14 Tage = +0, 7-Tage-Inzidenz = 440
- Taufers im Münstertal (969 Einw.): Heute = +0, vorhergehende 6 Tage = +9, vorhergehende 14 Tage = +14, 7-Tage-Inzidenz = 930 

<img src="CovidCartinaCovidFree-2021-05-28-1.jpg" alt="Cartina con comuni covid-free" width="50%" align="right">
 
Folgende 18 Gemeinden könnte man als **Covid-Free** betrachten, denn sie haben schon seit 21 Tagen keinen Fall mehr gehabt, niemand befindet sich in Quarantäne oder Isolation und die SABES zählt zur Zeit keinen "aktiv Positiven".
 
1. St.Leonhard i.P. (3.558)
1. Tisens (1.965)
1. Montan (1.701)
1. Mölten (1.692)
1. Aldein (1.656)
1. Riffian (1.362)
1. Magreid a.d.W. (1.274)
1. Rodeneck (1.240)
1. Schnals (1.228)
1. Pfatten (1.057)
1. Truden im Naturpark (1.040)
1. Andrian (1.030)
1. Hafling (778)
1. U.L.Frau I.W.-St.Felix (762)
1. Plaus (724)
1. Kurtinig a.d.W. (665)
1. Prettau (546)
1. Waidbruck (195)
 
Weitere Informationen: 
- [PDF, Daten und Text](../28) - in tedesco e italiano, se non indicato diversamente
  - [tägliche Pressemitteilung](20210528.txt) - solo tedesco
  - Karten - Cartine con dettaglio comunale
    - [**Covid-freie** Gemeinden](CovidCartinaCovidFree-2021-05-28.pdf)
    - [Cartina con evidenziati i comuni con almeno un nuovo caso](CovidCartinaPositiviPcrAgOggi-2021-05-28.pdf)
    - [Summe der PCR- und AG-Positiven der letzten 7 Tagen](CovidCartinaPositiviPcrAgUltimi7giorni-2021-05-28.pdf)
    - [Summe der PCR- **und AG**-Positiven der letzten **21** Tagen](CovidCartinaPositiviPcrAgUltimi21giorni-2021-05-28.pdf)
    - [**Anstieg bzw. Rückgang** der PCR- und AG-Positiven der letzten 7 Tagen im Vergleich zu den letzten 21 Tagen](CovidCartinaAccelerazione7su21giorniPositiviPcrAgUltimi-2021-05-28.pdf)
    - [Persone trovate positive con test PCR rispetto tutti i positivi (PCR e AG). Ultimi 21 giorni](CovidCartinaPositivi21PctPcr-2021-05-28.pdf)
    - [Karte mit aktueller Quarantäne](CovidCartinaQuarantena-2021-05-28.pdf)
    - [Residenti messi in quarantena negli ultimi 7 giorni](CovidCartinaQuarantenaMandatiUltimi7giorni-2021-05-28.pdf)
    - [Persone con almento un test positivo (PCR o AG), da 1° ottobre in poi](CovidCartinaPositiviTotaleWelle2-2021-05-28.pdf)
  - [weiteres Material](../28)
 
 
## Seniorenheime
 
Für Details siehe 
- [Liste der suspekten Heime](Traueranzeigen-Seniorenheime-Hotspots-Welle2-20210528.txt)
- [Grafiken](Traueranzeigen-SeniorenheimeCumuliert_2020-05-01_to_2021-05-25.pdf)
 
 
# Links zu anderen Südtirolbezogene Webseiten
 
- Dashboard von [Markus Falk](https://www.markusfalk.com/)
  - [Version 1](https://www.markusfalk.com/dashboard/#lang=de&dset=ST&reg=all&tab=PRED&from=2020-08-11&to=true)
  - [Version 2](https://www.markusfalk.com/dashboard-beta/#lang=de&dset=ST_ALL&reg=all&tab=PRED&from=2020-08-11&to=true)
- Tabellen von [Moritz Mair](https://moritzmair.info/)
  - [Impfzahlen in Südtirol nach Alter](https://covvac.moritzmair.info/?tab=1)
- Ivan Sieder
  - [Interaktive Karten](https://map.corona-bz.simedia.cloud/)
- Zivilschutz Südtirol
  - Ohne Gemeindedetails
    - [Grafiken](http://www.provinz.bz.it/sicherheit-zivilschutz/zivilschutz/aktuelle-daten-zum-coronavirus.asp)
    - [dati provinciali con serie storica a partire da 15 marzo 2020](https://afbs.provinz.bz.it/upload/coronavirus/Corona.Data.Total.csv)
  - Mit Gemeindedetails
    - [Grafici e cartine con dettaglio comunale](http://www.provincia.bz.it/sicurezza-protezione-civile/protezione-civile/aktuelle-daten-zum-coronavirus-in-den-gemeinden.asp)
    - [Aktuelle Excel-Datei mit Positiven (PCR+AG)](https://afbs.provinz.bz.it/upload/coronavirus/CoronavirusPositivi.xlsx)
    - [Aktuelle Excel-Datei mit Quarantäne](https://afbs.provinz.bz.it/upload/coronavirus/CoronavirusQuarantaene.xlsx)
    - [dati comunali con serie storica a partire da 18 dicembre](https://afbs.provinz.bz.it/upload/coronavirus/Corona.Data.Detail.csv)
- [Protezione civile Italia](https://github.com/pcm-dpc/COVID-19/blob/master/README.md)
  - diffusione del Virus
    - [dati (CSV)](https://github.com/pcm-dpc/COVID-19/tree/master/dati-regioni)
    - [dati (JSON)](https://github.com/pcm-dpc/COVID-19/tree/master/dati-json/dpc-covid19-ita-regioni.json)
    - [scheda riepilogativa (PDF)](https://github.com/pcm-dpc/COVID-19/raw/master/schede-riepilogative/regioni/dpc-covid19-ita-scheda-regioni-latest.pdf)
  - [Vaccinazioni](https://github.com/italia/covid19-opendata-vaccini/tree/master/dati)
    - [dati provinciali per classi di età, serie storica (CSV)](https://github.com/italia/covid19-opendata-vaccini/blob/master/dati/somministrazioni-vaccini-latest.csv)
 
## Keep In Touch
 
- Mastodon: https://troet.cafe/@COVID_BZ_Statistics
- Matrix: https://matrix.to/#/@antoniogulino:matrix.org
