https://www.sabes.it/de/news.asp?aktuelles_action=4&aktuelles_article_id=656225

Coronavirus – Mitteilung des Sanitätsbetriebes | 28.05.2021 | 10:46

In den letzten 24 Stunden wurden 825 PCR-Tests untersucht und dabei 41 Neuinfektionen festgestellt. Zusätzlich gab es 9 positive Antigentests.

Coronavirus: 41 Neuinfektionen durch PCR-Test und 9 positive Antigentests (Foto: 123rf)

Bisher (28. Mai) wurden insgesamt 576.156 Abstriche untersucht, die von 217.404 Personen stammen.

Die Zahlen im Überblick:

PCR-Abstriche:

Untersuchte Abstriche gestern (27. Mai): 825

Mittels PCR-Test neu positiv getestete Personen: 41

Gesamtzahl der mittels PCR-Test positiv getesteten Personen: 48.252

Gesamtzahl der untersuchten Abstriche: 576.156

Gesamtzahl der mit Abstrichen getesteten Personen: 217.404 (+260)

Antigentests:

Gesamtzahl der durchgeführten Antigentests: 1.485.471

Gesamtzahl der positiven Antigentests: 26.091

Durchgeführte Antigentests gestern: 4.505

Mittels Antigentest neu positiv getestete Personen: 9

Nasenflügeltests bis 27.05.2021 (ohne Schulen) 487.901 Tests, davon 685 positiv

Nasenflügeltests in den Schulen (bis 27.05.2021)607.773 Tests gesamt an 544 Schulen, 493 positive Ergebnisse, davon 250 bestätigt, 178 PCR-negativ, 65  ausständig/zu überprüfen

Weitere Daten:

Auf Normalstationen im Krankenhaus untergebrachte Covid-19-Patienten/-Patientinnen: 17

In Privatkliniken untergebrachte Covid-19-Patienten/-Patientinnen (post-akut bzw. aus Seniorenwohnheimen übernommen): 4

In Gossensaß und Sarns untergebrachte Covid-19-Patienten/-Patientinnen: 0

Anzahl der auf Intensivstationen aufgenommenen Covid-Patienten/Patientinnen: 5 (davon 4 als ICU-Covid klassifiziert) Gesamtzahl der mit Covid-19 Verstorbenen: 1.175 (+0)

Personen in Quarantäne/häuslicher Isolation: 1.535

Personen, die Quarantäne/häusliche Isolation beendet haben: 134.565

Personen betroffen von verordneter Quarantäne/häuslicher Isolation: 136.100

Geheilte Personen insgesamt: 72.419 (+76)

Positiv getestete Mitarbeiter und Mitarbeiterinnen des Sanitätsbetriebes: 2.043, davon 1.681 geheilt. Positiv getestete Basis-, Kinderbasisärzte und Bereitschaftsärzte: 58, davon 46 geheilt. (Stand: 13.05.2021)

(VW)

