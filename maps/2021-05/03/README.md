[covid-bz](../../../README.md) / [maps](../../README.md) / [2021-05](../../2021-05/README.md) / [03](../03)
# Montag,  3. Mai 2021

## Nasenflügeltests in den Schulen (UPDATE) 
Über das Wochenende wurden natürlich keine Tests durchgeführt, aber es sind weitere Ergebnisse der PCR-Tests verfügbar.

Auswertung von 268.482 Tests an 514 Schulen, 310 positive Ergebnisse, davon 164 PCR-positiv, 115 PCR-negativ, 31 ausständig/zu überprüfen.

davon waren zwar  310 (+11) positiv (1,1 pro mille) aber
- nur 164 PCR-positiv (+20)
- 115 PCR-negativ (+18)
- 31 noch zu überprüfen (-27)

Dies bedeutet, dass die Nasenflügeltests, im Durchschnitt 4 bis 5,5 **Falsch positive** auf 10.000 "real negativen" Personen produzieren.

Wie viele "real positive" bei diesen Test "falsch negativ" sind, kann man aus diesen Werten nicht ausrechnen.

NB: die von der SABES gelieferten Daten sind nicht ganz kohärent. Hier werden sie so wie die Daten gemeldet wurden wiedergegeben und verglichen.


## Die Lage in den Gemeinden

<img src="CovidCartinaPositiviPcrAgUltimi7giorni-2021-05-03-1.jpg" alt="Landkarte mit Gemeinden und deren 7-Tage-Inzidenz je 100" width="50%" align="right">


Heute (03.05.) wurden
+11 Positive mit Wohnsitz in Südtirol mitgeteilt, in [10 verschiedene Gemeinden](CovidCartinaPositiviPcrAgOggi-2021-05-03-1.jpg?inline=false) (von 116).
In 39 Gemeinden (1 weniger als [gestern](../02/README.md)) gibt es schon seit mindestens 7 Tagen keinen neuen Fall mehr,
in 14 davon (1 mehr als [gestern](../02/README.md))  sogar schon seit 21 Tagen oder mehr.
 
Trotz den, für den Montag üblichen sehr wenigen Fällen, gibt es eine bemerkenswerte Gemeinde:
- **Taufers im Münstertal** (969 Einw.): Heute = +1, vorhergehende 6 Tage = +4, vorhergehende 14 Tage = +2, 7-Tage-Inzidenz = 500/100.000

Nenneswert sind: **Mühlbach** (3.151 Einw.), **Gsies** (2.320), **Welschnofen** (1.973), **Stilfs** (1.141), **Truden** im Naturpark (1.040), **U.L.Frau I.W.-St.Felix** (762) und **Laurein** (342)

<img src="CovidCartinaCovidFree-2021-05-03-1.jpg" alt="Cartina con comuni covid-free" width="50%" align="right">
 
Folgende 8 Gemeinden  (1 mehr als [gestern](../02/README.md)) könnte man als **Covid-Free** betrachten, denn sie haben schon seit 21 Tagen keinen Fall mehr gehabt, niemand befindet sich in Quarantäne oder Isolation und die SABES zählt zur Zeit keinen "aktiv Positiven".
 
1. **Marling** (2.791), letzter positiver Fall am 24. März
1. Schluderns (1.838), letzter positiver Fall am 20. März
1. Niederdorf (1.597), letzter positiver Fall am 10. April
1. Andrian (1.030), letzter positiver Fall am 4. April
1. Glurns (900), letzter positiver Fall am 2. April
1. Altrei (398), letzter positiver Fall am 1. April
1. Kuens (395), letzter positiver Fall am 10. März
1. Waidbruck (195), letzter positiver Fall am 6. Februar
 
Weitere Informationen: 
- [PDF, Daten und Text](../03) - in tedesco e italiano, se non indicato diversamente
  - [tägliche Pressemitteilung](20210503.txt) - solo tedesco
  - Karten - Cartine con dettaglio comunale
    - [**Covid-freie** Gemeinden](CovidCartinaCovidFree-2021-05-03.pdf)
    - [Cartina con evidenziati i comuni con almeno un nuovo caso](CovidCartinaPositiviPcrAgOggi-2021-05-03.pdf)
    - [Summe der PCR- und AG-Positiven der letzten 7 Tagen](CovidCartinaPositiviPcrAgUltimi7giorni-2021-05-03.pdf)
    - [Summe der PCR- **und AG**-Positiven der letzten **21** Tagen](CovidCartinaPositiviPcrAgUltimi21giorni-2021-05-03.pdf)
    - [**Anstieg bzw. Rückgang** der PCR- und AG-Positiven der letzten 7 Tagen im Vergleich zu den letzten 21 Tagen](CovidCartinaAccelerazione7su21giorniPositiviPcrAgUltimi-2021-05-03.pdf)
    - [Persone trovate positive con test PCR rispetto tutti i positivi (PCR e AG). Ultimi 21 giorni](CovidCartinaPositivi21PctPcr-2021-05-03.pdf)
    - [Karte mit aktueller Quarantäne](CovidCartinaQuarantena-2021-05-03.pdf)
    - [Residenti messi in quarantena negli ultimi 7 giorni](CovidCartinaQuarantenaMandatiUltimi7giorni-2021-05-03.pdf)
    - [Persone con almento un test positivo (PCR o AG), da 1° ottobre in poi](CovidCartinaPositiviTotaleWelle2-2021-05-03.pdf)
  - [weiteres Material](../03)
 
 
## Seniorenheime
 
Für Details siehe 
- [Liste der suspekten Heime](Traueranzeigen-Seniorenheime-Hotspots-Welle2-20210503.txt)
- [Grafiken](Traueranzeigen-SeniorenheimeCumuliert_2020-05-01_to_2021-04-30.pdf)
 
 
# Links zu anderen Südtirolbezogene Webseiten
 
- Dashboard von [Markus Falk](https://www.markusfalk.com/)
  - [Version 1](https://www.markusfalk.com/dashboard/#lang=de&dset=ST&reg=all&tab=PRED&from=2020-08-11&to=true)
  - [Version 2](https://www.markusfalk.com/dashboard-beta/#lang=de&dset=ST_ALL&reg=all&tab=PRED&from=2020-08-11&to=true)
- Tabellen von [Moritz Mair](https://moritzmair.info/)
  - [Impfzahlen in Südtirol nach Alter](https://covvac.moritzmair.info/?tab=1)
- Ivan Sieder
  - [Interaktive Karten](https://map.corona-bz.simedia.cloud/)
- Zivilschutz Südtirol
  - Ohne Gemeindedetails
    - [Grafiken](http://www.provinz.bz.it/sicherheit-zivilschutz/zivilschutz/aktuelle-daten-zum-coronavirus.asp)
    - [dati provinciali con serie storica a partire da 15 marzo 2020](https://afbs.provinz.bz.it/upload/coronavirus/Corona.Data.Total.csv)
  - Mit Gemeindedetails
    - [Grafici e cartine con dettaglio comunale](http://www.provincia.bz.it/sicurezza-protezione-civile/protezione-civile/aktuelle-daten-zum-coronavirus-in-den-gemeinden.asp)
    - [Aktuelle Excel-Datei mit Positiven (PCR+AG)](https://afbs.provinz.bz.it/upload/coronavirus/CoronavirusPositivi.xlsx)
    - [Aktuelle Excel-Datei mit Quarantäne](https://afbs.provinz.bz.it/upload/coronavirus/CoronavirusQuarantaene.xlsx)
    - [dati comunali con serie storica a partire da 18 dicembre](https://afbs.provinz.bz.it/upload/coronavirus/Corona.Data.Detail.csv)
- [Protezione civile Italia](https://github.com/pcm-dpc/COVID-19/blob/master/README.md)
  - diffusione del Virus
    - [dati (CSV)](https://github.com/pcm-dpc/COVID-19/tree/master/dati-regioni)
    - [dati (JSON)](https://github.com/pcm-dpc/COVID-19/tree/master/dati-json/dpc-covid19-ita-regioni.json)
    - [scheda riepilogativa (PDF)](https://github.com/pcm-dpc/COVID-19/raw/master/schede-riepilogative/regioni/dpc-covid19-ita-scheda-regioni-latest.pdf)
  - [Vaccinazioni](https://github.com/italia/covid19-opendata-vaccini/tree/master/dati)
    - [dati provinciali per classi di età, serie storica (CSV)](https://github.com/italia/covid19-opendata-vaccini/blob/master/dati/somministrazioni-vaccini-latest.csv)
 
## Keep In Touch
 
- Mastodon: https://troet.cafe/@COVID_BZ_Statistics
- Matrix: https://matrix.to/#/@antoniogulino:matrix.org
