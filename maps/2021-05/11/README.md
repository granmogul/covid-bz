[covid-bz](../../../README.md) / [maps](../../README.md) / [2021-05](../../2021-05/README.md) / [11](../11)
 
# Dienstag, 11. Mai 2021

<img src="CovidCartinaPositiviPcrAgUltimi7giorni-2021-05-11-1.jpg" alt="7-Tage-Inzidenz" width="50%" align="right">
 
Auch heute bestätigen sich der kleine Brennpunkt im Westen (**Stilfs** und **Taufers im Münstertal**) und der immer breitere Flächenbrand im Osten: besonders **Gsies** plus **Sexten**, **Welsberg-Taisten** und **Sand in Taufers**; jetzt auch noch **Mühlwald**? Und **Vintl** (neben **Mühlbach**)?

Die **7-Tage-Inzidenz** (um die 80 auf 100.000) ist zwar weiterhin beim sinken,
aber in den letzten 5 Wochen im Durchschnitt nur um -1.7% täglich,
was die **50-er Marke erst in 4 Wochen** "möglich" machen würde.


- [Die 7-Tage-Inzidenz](#die-7-tage-inzidenz)
- [Nasenflügeltests in den Schulen](#nasenflügeltests-in-den-schulen)
- [Die Lage in den Gemeinden](#die-lage-in-den-gemeinden)
- [Seniorenheime](#seniorenheime)
- [Links zu anderen Südtirolbezogene Webseiten](#links-zu-anderen-südtirolbezogene-webseiten)
- [Keep In Touch](#keep-in-touch)  _(feedback is welcome)_


## Die 7-Tage-Inzidenz

Die 7-Tage-Inzidenz (302 PCR + 139 AG) ist 83 auf 100.000.
Vor einer Woche (am 04.05.) war sie 102.
Also -2.8% täglich.
Bei genau _dieser_ Entschleunigung braucht es noch 3 Wochen um auf 50 zu kommen (28. Mai um "genau" zu sein).

## Nasenflügeltests in den Schulen 

_Heute gab es keine Ajournierung, deshalb gilt die Auswertung von [gestern](../10/README.md#nasenflügeltests-in-den-schulen)_
 
 
## Die Lage in den Gemeinden
 
<img src="CovidCartinaPositiviPcrAgOggi-2021-05-11-1.jpg" alt="Cartina con comuni con nuovi positivi oggi" width="50%" align="right">
 
Heute (11.05.) wurden
+67 Positive mit Wohnsitz in Südtirol mitgeteilt, in 31 verschiedene Gemeinden (von 116).
In 43 Gemeinden (5 mehr als [gestern](../10/README.md)) gibt es schon seit mindestens 7 Tagen keinen neuen Fall mehr,
in 16 davon (1 mehr als [gestern](../10/README.md)) sogar schon seit 21 Tagen oder mehr.
 
Besonders negativ aufgefallen sind die Daten von heute von
- **Gsies** (2.320 Einw.): Heute = +4, vorhergehende 6 Tage = +13, vorhergehende 14 Tage = +27, 7-Tage-Inzidenz = 700/100.000"
- **Sexten** (1.880 Einw.): Heute = +5, vorhergehende 6 Tage = +10, vorhergehende 14 Tage = +2, 7-Tage-Inzidenz = 800/100.000"
- **Stilfs** (1.141 Einw.): Heute = +1, vorhergehende 6 Tage = +5, vorhergehende 14 Tage = +8, 7-Tage-Inzidenz = 500/100.000" 

Nicht viel besser sieht es in diesen vier Pusterer Gemeinden aus: **Sand in Taufers** (5.496 Einw.; **heute +9 Fälle**), **Vintl** (3.339 Einw.; +5), **Welsberg-Taisten** (2.914 Einw.; +2) und **Mühlwald** (1.432 Einw.; +1)


<img src="CovidCartinaCovidFree-2021-05-11-1.jpg" alt="Cartina con comuni covid-free" width="50%" align="right">
 
Folgende 10 Gemeinden (1 weniger als [gestern](../10/README.md)) könnte man als **Covid-Free** betrachten, denn sie haben schon seit 21 Tagen keinen Fall mehr gehabt, niemand befindet sich in Quarantäne oder Isolation und die SABES zählt zur Zeit keinen "aktiv Positiven".
 
1. Freienfeld (2.656), letzter positiver Fall am 12. April
1. Mölten (1.692), letzter positiver Fall am 6. April
1. Magreid a.d.W. (1.274), letzter positiver Fall am 15. April
1. Pfatten (1.057), letzter positiver Fall am 13. April
1. Andrian (1.030), letzter positiver Fall am 4. April
1. Hafling (778), letzter positiver Fall am 18. April
1. Altrei (398), letzter positiver Fall am 1. April
1. Kuens (395), letzter positiver Fall am 10. März
1. Proveis (265), letzter positiver Fall am 3. März
1. Waidbruck (195), letzter positiver Fall am 6. Februar
- <del>_Marling (2.791)_</del>, heute ist wieder jemand in Quarantäne, vorletzter positiver Fall war am 24. März

Weitere Informationen: 
- [PDF, Daten und Text](../11) - in tedesco e italiano, se non indicato diversamente
  - [tägliche Pressemitteilung](20210511.txt) - solo tedesco
  - Karten - Cartine con dettaglio comunale
    - [**Covid-freie** Gemeinden](CovidCartinaCovidFree-2021-05-11.pdf)
    - [Cartina con evidenziati i comuni con almeno un nuovo caso](CovidCartinaPositiviPcrAgOggi-2021-05-11.pdf)
    - [Summe der PCR- und AG-Positiven der letzten 7 Tagen](CovidCartinaPositiviPcrAgUltimi7giorni-2021-05-11.pdf)
    - [Summe der PCR- **und AG**-Positiven der letzten **21** Tagen](CovidCartinaPositiviPcrAgUltimi21giorni-2021-05-11.pdf)
    - [**Anstieg bzw. Rückgang** der PCR- und AG-Positiven der letzten 7 Tagen im Vergleich zu den letzten 21 Tagen](CovidCartinaAccelerazione7su21giorniPositiviPcrAgUltimi-2021-05-11.pdf)
    - [Persone trovate positive con test PCR rispetto tutti i positivi (PCR e AG). Ultimi 21 giorni](CovidCartinaPositivi21PctPcr-2021-05-11.pdf)
    - [Karte mit aktueller Quarantäne](CovidCartinaQuarantena-2021-05-11.pdf)
    - [Residenti messi in quarantena negli ultimi 7 giorni](CovidCartinaQuarantenaMandatiUltimi7giorni-2021-05-11.pdf)
    - [Persone con almento un test positivo (PCR o AG), da 1° ottobre in poi](CovidCartinaPositiviTotaleWelle2-2021-05-11.pdf)
  - [weiteres Material](../11)
 
 
## Seniorenheime
 
Für Details siehe 
- [Liste der suspekten Heime](Traueranzeigen-Seniorenheime-Hotspots-Welle2-20210511.txt)
- [Grafiken](Traueranzeigen-SeniorenheimeCumuliert_2020-05-01_to_2021-05-08.pdf)
 
 
# Links zu anderen Südtirolbezogene Webseiten
 
- Dashboard von [Markus Falk](https://www.markusfalk.com/)
  - [Version 1](https://www.markusfalk.com/dashboard/#lang=de&dset=ST&reg=all&tab=PRED&from=2020-08-11&to=true)
  - [Version 2](https://www.markusfalk.com/dashboard-beta/#lang=de&dset=ST_ALL&reg=all&tab=PRED&from=2020-08-11&to=true)
- Tabellen von [Moritz Mair](https://moritzmair.info/)
  - [Impfzahlen in Südtirol nach Alter](https://covvac.moritzmair.info/?tab=1)
- Ivan Sieder
  - [Interaktive Karten](https://map.corona-bz.simedia.cloud/)
- Zivilschutz Südtirol
  - Ohne Gemeindedetails
    - [Grafiken](http://www.provinz.bz.it/sicherheit-zivilschutz/zivilschutz/aktuelle-daten-zum-coronavirus.asp)
    - [dati provinciali con serie storica a partire da 15 marzo 2020](https://afbs.provinz.bz.it/upload/coronavirus/Corona.Data.Total.csv)
  - Mit Gemeindedetails
    - [Grafici e cartine con dettaglio comunale](http://www.provincia.bz.it/sicurezza-protezione-civile/protezione-civile/aktuelle-daten-zum-coronavirus-in-den-gemeinden.asp)
    - [Aktuelle Excel-Datei mit Positiven (PCR+AG)](https://afbs.provinz.bz.it/upload/coronavirus/CoronavirusPositivi.xlsx)
    - [Aktuelle Excel-Datei mit Quarantäne](https://afbs.provinz.bz.it/upload/coronavirus/CoronavirusQuarantaene.xlsx)
    - [dati comunali con serie storica a partire da 18 dicembre](https://afbs.provinz.bz.it/upload/coronavirus/Corona.Data.Detail.csv)
- [Protezione civile Italia](https://github.com/pcm-dpc/COVID-19/blob/master/README.md)
  - diffusione del Virus
    - [dati (CSV)](https://github.com/pcm-dpc/COVID-19/tree/master/dati-regioni)
    - [dati (JSON)](https://github.com/pcm-dpc/COVID-19/tree/master/dati-json/dpc-covid19-ita-regioni.json)
    - [scheda riepilogativa (PDF)](https://github.com/pcm-dpc/COVID-19/raw/master/schede-riepilogative/regioni/dpc-covid19-ita-scheda-regioni-latest.pdf)
  - [Vaccinazioni](https://github.com/italia/covid19-opendata-vaccini/tree/master/dati)
    - [dati provinciali per classi di età, serie storica (CSV)](https://github.com/italia/covid19-opendata-vaccini/blob/master/dati/somministrazioni-vaccini-latest.csv)
 
## Keep In Touch
 
- Mastodon: https://troet.cafe/@COVID_BZ_Statistics
- Matrix: https://matrix.to/#/@antoniogulino:matrix.org
