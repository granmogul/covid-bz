[covid-bz](../../../README.md) / [maps](../../README.md) / [2021-05](../../2021-05/README.md) / [18](../18)
 
# Dienstag, 18. Mai 2021

Auch heute gibt es Fälle im **Obervinschgau** (heute ist wieder Stilfs dran) und im **Unteren Eisacktal** (Klausen, Lajen und St. Ulrich haben zusammen +9 Fälle). Besonders hervorzugeben ist **Natz−Schabs** mit +20 Fällen in einer Woche. Auch bei den drei Gemeinden **Vintl, Terenten, Pfalzen** ist immer wieder etwas. Und im **obersten Pustertal** ist es weiterhin nicht ruhig.

<img src="CovidCartinaPositiviPcrAgUltimi7giorni-2021-05-18-1.jpg" alt="7-Tage-Inzidenz" width="50%" align="right">
 
Die **7-Tage-Inzidenz** (um die 80 auf 100.000) ist zwar weiterhin beim sinken,
aber in den letzten 9 Wochen im Durchschnitt nur um -1.4% täglich,
was die **50-er Marke erst in 4 Wochen** "möglich" machen würde.


- [Die 7-Tage-Inzidenz](#die-7-tage-inzidenz)
- <del>[Nasenflügeltests in den Schulen](#nasenflügeltests-in-den-schulen)</del> heute keine neue Daten
- [Die Lage in den Gemeinden](#die-lage-in-den-gemeinden)
- [Seniorenheime](#seniorenheime)
- [Links zu anderen Südtirolbezogene Webseiten](#links-zu-anderen-südtirolbezogene-webseiten)
- [Keep In Touch](#keep-in-touch)  _(feedback is welcome)_


## Die 7-Tage-Inzidenz


Die 7-Tage-Inzidenz (293 PCR + 122 AG) ist 78 auf 100.000.
Vor einer Woche (am 11.05.) war sie 83.
Also -0.9% täglich.
Bei genau _dieser_ Entschleunigung braucht es noch 7 Wochen um auf 50 zu kommen (8. Juli um "genau" zu sein).


## Nasenflügeltests in den Schulen 
_Hinweis: die von der SABES gelieferten Daten sind nicht ganz kohärent. Hier werden sie so wie die Daten gemeldet wurden wiedergegeben und verglichen._
 
Heute die selben daten wie  [vorgestern](../16/README.md)

> Nasenflügeltests in den Schulen (bis 15.05.2021)486.593 Tests gesamt an 548 Schulen, 386 positive Ergebnisse, davon 219 bestätigt, 130 PCR-negativ, 37 ausständig/zu überprüfen

 
## Die Lage in den Gemeinden
 
<img src="CovidCartinaPositiviPcrAgOggi-2021-05-18-1.jpg" alt="Cartina con comuni con nuovi positivi oggi" width="50%" align="right">
 
Heute (18.05.) wurden
+74 Positive mit Wohnsitz in Südtirol mitgeteilt, in 34 verschiedene Gemeinden (von 116).
In 36 Gemeinden (2 weniger als [gestern](../17/README.md)) gibt es schon seit mindestens 7 Tagen keinen neuen Fall mehr,
in 15 davon  (2 weniger als [gestern](../17/README.md)) sogar schon seit 21 Tagen oder mehr.
 
Bemerkenswert sind heute:
- **Natz-Schabs** (3.230 Einw.): Heute = +6, vorhergehende 6 Tage = +17, vorhergehende 14 Tage = +8, 7-Tage-Inzidenz = 700/100.000"
- **Stilfs** (1.141 Einw.): Heute = +1, vorhergehende 6 Tage = +7, vorhergehende 14 Tage = +14, 7-Tage-Inzidenz = 700/100.000"     

Nicht aus den Augen zu verlieren sind  **Klausen** (5.215 Einw., heute = +6),  **Toblach** (3.351 Einw., +4),  **Vintl** (3.339 Einw., +4),  **Terenten** (1.766 Einw., +1) und  **Taufers im Münstertal** (969 Einw., +0)

<img src="CovidCartinaCovidFree-2021-05-18-1.jpg" alt="Cartina con comuni covid-free" width="50%" align="right">
 
Folgende 13 Gemeinden  (2 weniger als [gestern](../17/README.md)) könnte man als **Covid-Free** betrachten, denn sie haben schon seit 21 Tagen keinen Fall mehr gehabt, niemand befindet sich in Quarantäne oder Isolation und die SABES zählt zur Zeit keinen "aktiv Positiven".
 
1. Kurtasch a.d.W. (2.239)
1. Tisens (1.965)
1. Mölten (1.692)
1. Magreid a.d.W. (1.274)
1. Schnals (1.228)
1. Pfatten (1.057)
1. Andrian (1.030)
1. Hafling (778)
1. Plaus (724)
1. Altrei (398)
1. Kuens (395)
1. Proveis (265)
1. Waidbruck (195)
- <del>Brenner (2.232) </del>
- <del>Vöran (959) </del>

Weitere Informationen: 
- [PDF, Daten und Text](../18) - in tedesco e italiano, se non indicato diversamente
  - [tägliche Pressemitteilung](20210518.txt) - solo tedesco
  - Karten - Cartine con dettaglio comunale
    - [**Covid-freie** Gemeinden](CovidCartinaCovidFree-2021-05-18.pdf)
    - [Cartina con evidenziati i comuni con almeno un nuovo caso](CovidCartinaPositiviPcrAgOggi-2021-05-18.pdf)
    - [Summe der PCR- und AG-Positiven der letzten 7 Tagen](CovidCartinaPositiviPcrAgUltimi7giorni-2021-05-18.pdf)
    - [Summe der PCR- **und AG**-Positiven der letzten **21** Tagen](CovidCartinaPositiviPcrAgUltimi21giorni-2021-05-18.pdf)
    - [**Anstieg bzw. Rückgang** der PCR- und AG-Positiven der letzten 7 Tagen im Vergleich zu den letzten 21 Tagen](CovidCartinaAccelerazione7su21giorniPositiviPcrAgUltimi-2021-05-18.pdf)
    - [Persone trovate positive con test PCR rispetto tutti i positivi (PCR e AG). Ultimi 21 giorni](CovidCartinaPositivi21PctPcr-2021-05-18.pdf)
    - [Karte mit aktueller Quarantäne](CovidCartinaQuarantena-2021-05-18.pdf)
    - [Residenti messi in quarantena negli ultimi 7 giorni](CovidCartinaQuarantenaMandatiUltimi7giorni-2021-05-18.pdf)
    - [Persone con almento un test positivo (PCR o AG), da 1° ottobre in poi](CovidCartinaPositiviTotaleWelle2-2021-05-18.pdf)
  - [weiteres Material](../18)
 
 
## Seniorenheime
 
Für Details siehe 
- [Liste der suspekten Heime](Traueranzeigen-Seniorenheime-Hotspots-Welle2-20210518.txt)
- [Grafiken](Traueranzeigen-SeniorenheimeCumuliert_2020-05-01_to_2021-05-15.pdf)
 
 
# Links zu anderen Südtirolbezogene Webseiten
 
- Dashboard von [Markus Falk](https://www.markusfalk.com/)
  - [Version 1](https://www.markusfalk.com/dashboard/#lang=de&dset=ST&reg=all&tab=PRED&from=2020-08-11&to=true)
  - [Version 2](https://www.markusfalk.com/dashboard-beta/#lang=de&dset=ST_ALL&reg=all&tab=PRED&from=2020-08-11&to=true)
- Tabellen von [Moritz Mair](https://moritzmair.info/)
  - [Impfzahlen in Südtirol nach Alter](https://covvac.moritzmair.info/?tab=1)
- Ivan Sieder
  - [Interaktive Karten](https://map.corona-bz.simedia.cloud/)
- Zivilschutz Südtirol
  - Ohne Gemeindedetails
    - [Grafiken](http://www.provinz.bz.it/sicherheit-zivilschutz/zivilschutz/aktuelle-daten-zum-coronavirus.asp)
    - [dati provinciali con serie storica a partire da 15 marzo 2020](https://afbs.provinz.bz.it/upload/coronavirus/Corona.Data.Total.csv)
  - Mit Gemeindedetails
    - [Grafici e cartine con dettaglio comunale](http://www.provincia.bz.it/sicurezza-protezione-civile/protezione-civile/aktuelle-daten-zum-coronavirus-in-den-gemeinden.asp)
    - [Aktuelle Excel-Datei mit Positiven (PCR+AG)](https://afbs.provinz.bz.it/upload/coronavirus/CoronavirusPositivi.xlsx)
    - [Aktuelle Excel-Datei mit Quarantäne](https://afbs.provinz.bz.it/upload/coronavirus/CoronavirusQuarantaene.xlsx)
    - [dati comunali con serie storica a partire da 18 dicembre](https://afbs.provinz.bz.it/upload/coronavirus/Corona.Data.Detail.csv)
- [Protezione civile Italia](https://github.com/pcm-dpc/COVID-19/blob/master/README.md)
  - diffusione del Virus
    - [dati (CSV)](https://github.com/pcm-dpc/COVID-19/tree/master/dati-regioni)
    - [dati (JSON)](https://github.com/pcm-dpc/COVID-19/tree/master/dati-json/dpc-covid19-ita-regioni.json)
    - [scheda riepilogativa (PDF)](https://github.com/pcm-dpc/COVID-19/raw/master/schede-riepilogative/regioni/dpc-covid19-ita-scheda-regioni-latest.pdf)
  - [Vaccinazioni](https://github.com/italia/covid19-opendata-vaccini/tree/master/dati)
    - [dati provinciali per classi di età, serie storica (CSV)](https://github.com/italia/covid19-opendata-vaccini/blob/master/dati/somministrazioni-vaccini-latest.csv)
 
## Keep In Touch
 
- Mastodon: https://troet.cafe/@COVID_BZ_Statistics
- Matrix: https://matrix.to/#/@antoniogulino:matrix.org
