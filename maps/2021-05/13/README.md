[covid-bz](../../../README.md) / [maps](../../README.md) / [2021-05](../../2021-05/README.md) / [13](../13)
 
# Donnerstag, 13. Mai 2021
 
<img src="CovidCartinaPositiviPcrAgUltimi7giorni-2021-05-13-1.jpg" alt="7-Tage-Inzidenz" width="50%" align="right">
 
**Gsies**, mit seinen 2.320 Einwohner, ist schon seit 8 Wochen aktiv. In diesen weniger als 2 Monaten wurden 74 Fälle gemeldet, also 3,2% der Wohnbevölkerung. **Terenten** und **Vintl** haben in den letzten Tagen auch besonders viele Fälle gehabt.

Am anderen Ende vom Land, wechseln sich **Stilfs** und **Taufers im Münstertal** im Tagestakt ab und haben eine 7-Tagesinzidenz bei 400-500. Regelmäßig gesellen sich anderen Gemeinden vom **Oberen Vinschgau** dazu. Nur **Prad** scheint sich aus diesem Spiel (noch) heraus zu halten.

Auch das Gebiet um **Klausen** ist weiterhin aktiv. Jetzt kann man auch **St. Ulrich** dazu zählen. Die anderen Gemeinden sind **Villanders**, **Lajen**, **Barbian** und **Kastelruth**. Heute hatten sie zusammen +5 Fälle. Bezogen auf ihre 23.284 Einwohner, sind es mehr als 10 mal so viel wie im Landesdurchschnitt. In den letzten 2-3 Wochen sind die Fälle um +2,5% täglich angestiegen. Insgesamt sind in den letzten 3 Wochen aber nur 46 Fälle gemeldet worden, also 0,2% der Bevölkerung.

Landesweit ist die **7-Tage-Inzidenz** (um die 80 auf 100.000) zwar weiterhin beim sinken,
aber in den letzten 9 Wochen im Durchschnitt nur um -1.7% täglich,
was die **50-er Marke erst in 4 Wochen** "möglich" machen würde.


- [Die 7-Tage-Inzidenz](#die-7-tage-inzidenz)
- [Nasenflügeltests in den Schulen](#nasenflügeltests-in-den-schulen)
- [Die Lage in den Gemeinden](#die-lage-in-den-gemeinden)
- [Seniorenheime](#seniorenheime)
- [Links zu anderen Südtirolbezogene Webseiten](#links-zu-anderen-südtirolbezogene-webseiten)
- [Keep In Touch](#keep-in-touch)  _(feedback is welcome)_


## Die 7-Tage-Inzidenz

Die 7-Tage-Inzidenz (318 PCR + 127 AG) ist 84 auf 100.000.
Vor einer Woche (am 06.05.) war sie 93.
Also -1.5% täglich.
Bei genau _dieser_ Entschleunigung braucht es noch 5 Wochen um auf 50 zu kommen (16. Juni um "genau" zu sein).


## Nasenflügeltests in den Schulen 
_Hinweis: die von der SABES gelieferten Daten sind nicht ganz kohärent. Hier werden sie so wie die Daten gemeldet wurden wiedergegeben und verglichen._
 
> Nasenflügeltests in den Schulen (bis 12.05.2021)445.447 Tests gesamt an 537 Schulen, 368 positive Ergebnisse, davon 205 bestätigt, 125 PCR-negativ, 38 ausständig/zu überprüfen

Auswertung von 445.447 (+8.233 mehr als [gestern](../12/README.md)) Nasenflügeltests an 537 Schulen (+1)
 
davon waren zwar  368 (+3) positiv (0,83 pro mille) aber
- nur 205 davon PCR-positiv (+7) 
- 125 PCR-negativ (+2) 
-  38 noch zu überprüfen (-6)
 
Dies bedeutet, dass die Nasenflügeltests, im Durchschnitt 2,8 bis 3,7 **Falsch positive** auf 10.000 "real negativen" Personen produziert haben.
 
_Wie viele "real positive" bei diesen Test "falsch negativ" sind, kann man aus diesen Werten nicht ausrechnen._
 
 
## Die Lage in den Gemeinden
 
<img src="CovidCartinaPositiviPcrAgOggi-2021-05-13-1.jpg" alt="Cartina con comuni con nuovi positivi oggi" width="50%" align="right">
 
Heute (13.05.) wurden
+45 Positive mit Wohnsitz in Südtirol mitgeteilt, in 29 verschiedene Gemeinden (von 116).
In 39 Gemeinden (1 weniger als [gestern](../12/README.md)) gibt es schon seit mindestens 7 Tagen keinen neuen Fall mehr,
in 19 davon (1 mehr als [gestern](../12/README.md)) sogar schon seit 21 Tagen oder mehr.
 
Ein besonderer Hinweis gilt für 
- **Gsies** (2.320 Einw.): Heute = +1, vorhergehende 6 Tage = +15, vorhergehende 14 Tage = +25, 7-Tage-Inzidenz = 700/100.000

Nicht sofort aus den Augen verlieren sollte man **Vintl** (3.339 Einw., heute +1), **Sexten** (1.880 Einw.), **Terenten** (1.766 Einw., heute +3), **Barbian** (1.755 Einw., heute +1), **Stilfs** (1.141 Einw.) und **Taufers im Münstertal** (969 Einw., heute +1)

<img src="CovidCartinaCovidFree-2021-05-13-1.jpg" alt="Cartina con comuni covid-free" width="50%" align="right">
 
Folgende 11 Gemeinden (gleich viele, aber nicht alle die selben von gestern [gestern](../12/README.md)) könnte man als **Covid-Free** betrachten, denn sie haben schon seit 21 Tagen keinen Fall mehr gehabt, niemand befindet sich in Quarantäne oder Isolation und die SABES zählt zur Zeit keinen "aktiv Positiven".
 
1. Kurtasch a.d.W. (2.239), letzter positiver Fall am 21. April
1. Mölten (1.692), letzter positiver Fall am 6. April
1. Magreid a.d.W. (1.274), letzter positiver Fall am 15. April
1. Pfatten (1.057), letzter positiver Fall am 13. April
1. Andrian (1.030), letzter positiver Fall am 4. April
1. Hafling (778), letzter positiver Fall am 18. April
1. **Plaus** (724), letzter positiver Fall am 22. April
1. Altrei (398), letzter positiver Fall am 1. April
1. Kuens (395), letzter positiver Fall am 10. März
1. Proveis (265), letzter positiver Fall am 3. März
1. Waidbruck (195), letzter positiver Fall am 6. Februar
- <del>_Freienfeld (2.656)_</del>, heute +1 Fall, vorletzter positiver Fall am 12. April


Weitere Informationen: 
- [PDF, Daten und Text](../13) - in tedesco e italiano, se non indicato diversamente
  - [tägliche Pressemitteilung](20210513.txt) - solo tedesco
  - Karten - Cartine con dettaglio comunale
    - [**Covid-freie** Gemeinden](CovidCartinaCovidFree-2021-05-13.pdf)
    - [Cartina con evidenziati i comuni con almeno un nuovo caso](CovidCartinaPositiviPcrAgOggi-2021-05-13.pdf)
    - [Summe der PCR- und AG-Positiven der letzten 7 Tagen](CovidCartinaPositiviPcrAgUltimi7giorni-2021-05-13.pdf)
    - [Summe der PCR- **und AG**-Positiven der letzten **21** Tagen](CovidCartinaPositiviPcrAgUltimi21giorni-2021-05-13.pdf)
    - [**Anstieg bzw. Rückgang** der PCR- und AG-Positiven der letzten 7 Tagen im Vergleich zu den letzten 21 Tagen](CovidCartinaAccelerazione7su21giorniPositiviPcrAgUltimi-2021-05-13.pdf)
    - [Persone trovate positive con test PCR rispetto tutti i positivi (PCR e AG). Ultimi 21 giorni](CovidCartinaPositivi21PctPcr-2021-05-13.pdf)
    - [Karte mit aktueller Quarantäne](CovidCartinaQuarantena-2021-05-13.pdf)
    - [Residenti messi in quarantena negli ultimi 7 giorni](CovidCartinaQuarantenaMandatiUltimi7giorni-2021-05-13.pdf)
    - [Persone con almento un test positivo (PCR o AG), da 1° ottobre in poi](CovidCartinaPositiviTotaleWelle2-2021-05-13.pdf)
  - [weiteres Material](../13)
 
 
## Seniorenheime
 
Für Details siehe 
- [Liste der suspekten Heime](Traueranzeigen-Seniorenheime-Hotspots-Welle2-20210513.txt)
- [Grafiken](Traueranzeigen-SeniorenheimeCumuliert_2020-05-01_to_2021-05-10.pdf)
 
 
# Links zu anderen Südtirolbezogene Webseiten
 
- Dashboard von [Markus Falk](https://www.markusfalk.com/)
  - [Version 1](https://www.markusfalk.com/dashboard/#lang=de&dset=ST&reg=all&tab=PRED&from=2020-08-11&to=true)
  - [Version 2](https://www.markusfalk.com/dashboard-beta/#lang=de&dset=ST_ALL&reg=all&tab=PRED&from=2020-08-11&to=true)
- Tabellen von [Moritz Mair](https://moritzmair.info/)
  - [Impfzahlen in Südtirol nach Alter](https://covvac.moritzmair.info/?tab=1)
- Ivan Sieder
  - [Interaktive Karten](https://map.corona-bz.simedia.cloud/)
- Zivilschutz Südtirol
  - Ohne Gemeindedetails
    - [Grafiken](http://www.provinz.bz.it/sicherheit-zivilschutz/zivilschutz/aktuelle-daten-zum-coronavirus.asp)
    - [dati provinciali con serie storica a partire da 15 marzo 2020](https://afbs.provinz.bz.it/upload/coronavirus/Corona.Data.Total.csv)
  - Mit Gemeindedetails
    - [Grafici e cartine con dettaglio comunale](http://www.provincia.bz.it/sicurezza-protezione-civile/protezione-civile/aktuelle-daten-zum-coronavirus-in-den-gemeinden.asp)
    - [Aktuelle Excel-Datei mit Positiven (PCR+AG)](https://afbs.provinz.bz.it/upload/coronavirus/CoronavirusPositivi.xlsx)
    - [Aktuelle Excel-Datei mit Quarantäne](https://afbs.provinz.bz.it/upload/coronavirus/CoronavirusQuarantaene.xlsx)
    - [dati comunali con serie storica a partire da 18 dicembre](https://afbs.provinz.bz.it/upload/coronavirus/Corona.Data.Detail.csv)
- [Protezione civile Italia](https://github.com/pcm-dpc/COVID-19/blob/master/README.md)
  - diffusione del Virus
    - [dati (CSV)](https://github.com/pcm-dpc/COVID-19/tree/master/dati-regioni)
    - [dati (JSON)](https://github.com/pcm-dpc/COVID-19/tree/master/dati-json/dpc-covid19-ita-regioni.json)
    - [scheda riepilogativa (PDF)](https://github.com/pcm-dpc/COVID-19/raw/master/schede-riepilogative/regioni/dpc-covid19-ita-scheda-regioni-latest.pdf)
  - [Vaccinazioni](https://github.com/italia/covid19-opendata-vaccini/tree/master/dati)
    - [dati provinciali per classi di età, serie storica (CSV)](https://github.com/italia/covid19-opendata-vaccini/blob/master/dati/somministrazioni-vaccini-latest.csv)
 
## Keep In Touch
 
- Mastodon: https://troet.cafe/@COVID_BZ_Statistics
- Matrix: https://matrix.to/#/@antoniogulino:matrix.org
