# COVID-BZ

Daten und Informationen betreffend COVID-19 in Südtirol (Italien).

Dati e informazioni riguardanti COVID-19 in Alto Adige (Italia)

## Maps
In [maps](maps/README.md) si possono trovare i pdf con una cartina, dove ogni comune riceve il suo colore, in base p.es. alla numerosità di positivi ogni 100 residenti e cose simili.



## Keep In Touch

- Mastodon: https://troet.cafe/@COVID_BZ_Statistics
- Telegram: https://t.me/COVID_BZ_Statistics
